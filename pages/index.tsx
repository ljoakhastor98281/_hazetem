import { useSession } from 'next-auth/client'
import { useRouter } from 'next/router'
import Layout from '../components/layouts/navLayout'
import clsx from 'clsx'
import axios from 'axios'
import React, { useEffect } from 'react'
import { DataGrid, GridColDef } from '@material-ui/data-grid'
import {
  Box,
  Grid,
  // Card,
  // CardActions,
  // CardContent,
  List,
  ListItem,
  ListItemAvatar,
  Avatar,
  ListItemText,
  // Typography,
  // Button,
  // Divider,
  Tabs,
  Tab,
  Button,
} from '@material-ui/core'
import SwipeableViews from 'react-swipeable-views'
import { useTranslation } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { useTheme } from '@material-ui/core/styles'
// import zalgo from '../server/utils/zalgo'
import { makeStyles } from '@material-ui/core/styles'
import { IUser } from '../server/api/User/user.model'
import { ICompany } from '../server/api/Company/company.model'
import { IService } from '../server/api/Service/service.model'
import {
  IRequestHistory,
  IRequestStat,
} from '../server/api/RequestHistory/requesthistory.model'
import 'react-nice-dates/build/style.css'
import { Doughnut } from 'react-chartjs-2'
import FavoriteIcon from '@material-ui/icons/Favorite'
import ShopIcon from '@material-ui/icons/Shop'
import DirectionsRunIcon from '@material-ui/icons/DirectionsRun'
import EmojiEventsIcon from '@material-ui/icons/EmojiEvents'
import PeopleIcon from '@material-ui/icons/People'
import BusinessIcon from '@material-ui/icons/Business'
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart'
import { MonthShortEN } from '@AppEnums'

const useStyles = makeStyles((theme) => ({
  textPadding: {
    paddingLeft: 20,
    color: 'gray',
  },
  cardRoot: {
    margin: theme.spacing(1),
    backgroundColor: 'white',
    boxShadow: '0 2px 5px 2px rgba(189, 189, 189, 0.43)',
    '&:hover': {
      boxShadow: '0 2px 8px 2px rgba(116, 115, 115, 0.253)',
    },
  },
  smallCardRoot: {
    display: 'flex',
    // flexDirection: 'column',
    flexWrap: 'wrap',
    margin: theme.spacing(1),
    padding: theme.spacing(2),
    backgroundColor: 'white',
    boxShadow: '0 2px 5px 2px rgba(189, 189, 189, 0.43)',
    '&:hover': {
      boxShadow: '0 2px 8px 2px rgba(116, 115, 115, 0.253)',
    },
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  smallCardSubtitle: {
    color: 'gray',
    alignItems: 'right',
  },
  subCardRoot: {
    height: '47%',
    backgroundColor: 'white',
    padding: theme.spacing(1),
    boxShadow: '0 2px 5px 2px rgba(189, 189, 189, 0.43)',
    '&:hover': {
      boxShadow: '0 2px 8px 2px rgba(116, 115, 115, 0.253)',
    },
    display: 'flex',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  cardListRoot: {
    margin: theme.spacing(1),
    padding: theme.spacing(2),
    backgroundColor: 'white',
    boxShadow: '0 2px 5px 2px rgba(189, 189, 189, 0.43)',
    '&:hover': {
      boxShadow: '0 2px 8px 2px rgba(116, 115, 115, 0.253)',
    },
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  cardTitle: {
    fontSize: 20,
    margin: theme.spacing(1),
  },
  subCardTitle: {
    margin: theme.spacing(1),
  },
  pos: {
    marginBottom: 12,
  },
  listRoot: {
    // maxWidth: '36ch',
    backgroundColor: 'white',
  },
  listInline: {
    display: 'inline',
  },
  miniBox: {
    display: 'flex',
    flexWrap: 'wrap',
    // position: 'relative',
    // justifyItems: 'stretch',
    margin: theme.spacing(3),
  },
  doughnutCard: {
    margin: theme.spacing(1),
    padding: theme.spacing(2),
    backgroundColor: 'white',
    boxShadow: '0 2px 5px 2px rgba(189, 189, 189, 0.43)',
    '&:hover': {
      boxShadow: '0 2px 8px 2px rgba(116, 115, 115, 0.253)',
    },
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
}))

interface TabPanelProps {
  children?: React.ReactNode
  dir?: string
  index: number
  value: number
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </div>
  )
}

function a11yProps(index: number) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  }
}

export default function Home() {
  const [session, loading] = useSession()
  const { t } = useTranslation()
  const classes = useStyles()
  // const [covidData, setCovidData] = useState(null)
  const router = useRouter()
  const theme = useTheme()
  const [recentUserNumber, setRecentUserNumber] = React.useState<IUser[]>([])
  const [recentUser, setRecentUser] = React.useState<IUser[]>([])
  const [recentCompanyNumber, setRecentCompanyNumber] = React.useState<IUser[]>(
    [],
  )
  const [recentUserReqNumber, setRecentUserReqNumber] = React.useState<IUser[]>(
    [],
  )
  const [recentCompany, setRecentCompany] = React.useState<ICompany[]>([])
  const [mostPopularService, setMostPopularService] =
    React.useState<IRequestStat[]>()
  const [mostPopularCompany, setMostPopularCompany] =
    React.useState<IRequestStat[]>()
  const [recentReqNumber, setRecentReqNumber] = React.useState<
    IRequestHistory[]
  >([])
  const [ongoingService, setOngoingService] = React.useState<IRequestHistory[]>(
    [],
  )
  const [approvedService, setApprovedService] = React.useState<
    IRequestHistory[]
  >([])
  const [cancelledService, setCancelledService] = React.useState<
    IRequestHistory[]
  >([])
  const [declinedService, setDeclinedService] = React.useState<
    IRequestHistory[]
  >([])
  const [expiredService, setExpiredService] = React.useState<IRequestHistory[]>(
    [],
  )
  const [recentServiceNumber, setRecentServiceNumber] = React.useState<
    IService[]
  >([])
  const [showStatus, setShowStatus] = React.useState(0)

  const handleShowStatusChange = (
    event: React.ChangeEvent<unknown>,
    newValue: number,
  ) => {
    setShowStatus(newValue)
  }

  const handleShowStatusChangeIndex = (index: number) => {
    setShowStatus(index)
  }

  const userColors = ['#bee9e8', '#62b6cb', '#1b4965', '#cae9ff', '#5fa8d3']
  const getUserColor = (key: number) => userColors[key]

  const companyColors = ['#96e8bc', '#b6f9c9', '#7dd181', '#4b7f52', '#c9ffe2']
  const getCompanyColor = (key: number) => companyColors[key]

  // const [list, setList] = useState<ICompany[]>()
  useEffect(() => {
    async function fetchData() {
      if (!session) {
        return
      }

      const res = await axios.get('/requesthistory', {
        params: {
          withFullInfo: true,
        },
      })
      res.data.request_histories.map((requesthistory: IRequestHistory) => {
        requesthistory.id = requesthistory._id
      })

      const popular_service_res = await axios.get(
        '/requesthistory/most_requested/service',
        {
          params: {
            topMostWanted: 5,
          },
        },
      )
      setMostPopularService(popular_service_res.data.rank)
      if (session.level <= 1 || session.level >= 4) {
        const popular_company_res = await axios.get(
          '/requesthistory/most_requested/company',
          {
            params: {
              topMostWanted: 5,
            },
          },
        )
        setMostPopularCompany(popular_company_res.data.rank)
      }

      if (session.level < 2) {
        const resUser = await axios.get('/user', {
          params: {
            latest: 4,
            level: 4,
          },
        })
        setRecentUser(resUser.data.users)
        const resUserNum = await axios.get('/user', {
          params: {
            newlyCreated: true,
            days_ago: 7,
            level: 4,
          },
        })
        setRecentUserNumber(resUserNum.data.users)

        const resCompany = await axios.get('/company', {
          params: {
            latest: 4,
          },
        })
        setRecentCompany(resCompany.data.companys)
        const resCompNum = await axios.get('/user', {
          params: {
            newlyCreated: true,
            days_ago: 7,
            level: 2,
          },
        })
        setRecentCompanyNumber(resCompNum.data.users)
        const resSerNum = await axios.get('/user', {
          params: {
            newlyCreated: true,
            days_ago: 7,
          },
        })
        setRecentServiceNumber(resSerNum.data.users)
        const resNorReq = await axios.get('/requesthistory', {
          params: {
            getRHByDays: true,
            days_ago: 7,
          },
        })
        setRecentUserReqNumber(resNorReq.data.request_histories)
      } else if (session.level == 2 || session.level == 3) {
        const resComReq = await axios.get('/requesthistory', {
          params: {
            getRHByDays: true,
            days_ago: 7,
          },
        })
        setRecentReqNumber(resComReq.data.request_histories)
      } else if (session.level == 4) {
        const resNorReq = await axios.get('/requesthistory', {
          params: {
            getRHByDays: true,
            days_ago: 30,
          },
        })
        setRecentUserReqNumber(resNorReq.data.request_histories)
      }

      const ongoingSer = res.data.request_histories.filter(
        (requesthistory: IRequestHistory) =>
          requesthistory.archive_status == 0 && requesthistory.status == 0,
      )
      const canSer = res.data.request_histories.filter(
        (requesthistory: IRequestHistory) =>
          requesthistory.archive_status == 0 && requesthistory.status == 1,
      )
      const deSer = res.data.request_histories.filter(
        (requesthistory: IRequestHistory) =>
          requesthistory.archive_status == 0 && requesthistory.status == 2,
      )
      const appSer = res.data.request_histories.filter(
        (requesthistory: IRequestHistory) =>
          requesthistory.archive_status == 0 && requesthistory.status == 3,
      )
      const expSer = res.data.request_histories.filter(
        (requesthistory: IRequestHistory) =>
          requesthistory.archive_status == 0 && requesthistory.status == 4,
      )

      setOngoingService(() => ongoingSer)
      setApprovedService(() => appSer)
      setCancelledService(() => canSer)
      setDeclinedService(() => deSer)
      setExpiredService(() => expSer)

      // const today = new Date()
      // const yesterday = new Date(today.getTime() - 86400000)
      // const yesterday_list = yesterday.toLocaleDateString().split('/')
      // const daytosearch =
      //   (yesterday_list[1].length == 2
      //     ? yesterday_list[1]
      //     : '0' + yesterday_list[1]) +
      //   '/' +
      //   (yesterday_list[0].length == 2
      //     ? yesterday_list[0]
      //     : '0' + yesterday_list[0]) +
      //   '/' +
      //   yesterday_list[2]
      // const x = {
      //   resource:
      //     'http://www.chp.gov.hk/files/misc/latest_situation_of_reported_cases_covid_19_eng.csv',
      //   section: 1,
      //   format: 'json',
      //   filters: [[1, 'eq', [daytosearch]]],
      // }
      // await axios
      //   .get('https://api.data.gov.hk/v2/filter', {
      //     params: {
      //       q: x,
      //     },
      //   })
      //   .then((res) => {
      //     setCovidData(res.data[0])
      //   })
    }
    fetchData()
  }, [session])

  const today = new Date()

  const displayDate = t('date-short', {
    date: today.getDate().toString(),
    month: t(MonthShortEN[today.getMonth() + 1]),
    year: today.getFullYear().toString(),
  })

  // const displayDate = today.getDate().toString() +
  //   ' / ' +
  //   (today.getMonth() + 1).toString() +
  //   ' / ' +
  //   today.getFullYear().toString()

  const columns: GridColDef[] = [
    {
      field: 'service_name',
      headerName: t('service_name'),
      width: 300,
      valueGetter: (params) => {
        if (!params.row.service) return '*deleted service'
        return params.row.service.service_name
      },
    },
    {
      field: 'company_name',
      headerName: t('company_name'),
      width: 300,
      valueGetter: (params) => {
        return params.row.company?.company_name
      },
    },
    {
      field: 'requester',
      headerName: t('requester'),
      width: 200,
      valueGetter: (params) => {
        return params.row.requester?.username
      },
    },
    {
      field: 'status',
      headerName: t('request_status'),
      width: 180,
      valueGetter: (params) => {
        switch (params.row.status) {
          case 0:
            return 'ONGOING'
          case 1:
            return 'CANCELLED'
          case 2:
            return 'DECLINED'
          case 3:
            return 'APPROVED'
          case 4:
            return 'EXPIRED'
          default:
            return 'UNKNOWN'
        }
      },
    },
  ]

  if (!loading && !session) {
    router.replace(`${router.locale}/signin`)
  }
  if (
    session &&
    mostPopularService &&
    mostPopularCompany &&
    recentCompanyNumber &&
    recentUserNumber &&
    recentUser &&
    recentCompany &&
    recentReqNumber &&
    recentServiceNumber &&
    recentUserReqNumber &&
    session.level < 2
  ) {
    return (
      <Layout>
        <Box fontWeight="fontWeightBold" fontSize={40} m={1}>
          {t('welcome')}
        </Box>
        <Box className={classes.textPadding}>{t('check-update')}</Box>
        <Box
          fontWeight="fontWeightBold"
          fontFamily="Monospace"
          fontSize={25}
          textAlign="center"
        >
          {t('weekly')}
        </Box>
        <div
          style={{
            flexGrow: 1,
          }}
        >
          <Grid
            className={classes.miniBox}
            item
            xs={12}
            style={{
              borderTop: '1px solid grey',
              borderBottom: '1px solid grey',
              paddingTop: theme.spacing(2),
              paddingBottom: theme.spacing(2),
            }}
          >
            <Grid
              item
              xs
              className={classes.smallCardRoot}
              style={{ flexDirection: 'column' }}
            >
              <PeopleIcon
                style={{
                  color: '#247ba0',
                  fontSize: 60,
                  alignItems: 'right',
                }}
              />
              <Box fontWeight="fontWeightBold" fontSize={20}>
                {t('new-client-num')}
              </Box>
              <Box
                fontWeight="fontWeightBold"
                style={{ color: 'gray' }}
                fontSize={25}
              >
                {recentUserNumber.length}
              </Box>
            </Grid>
            <Grid
              item
              xs
              className={classes.smallCardRoot}
              style={{ flexDirection: 'column' }}
            >
              <BusinessIcon
                style={{
                  color: '#b2dbbf',
                  fontSize: 60,
                  alignItems: 'right',
                }}
              />
              <Box fontWeight="fontWeightBold" fontSize={20}>
                {t('new-comp-num')}
              </Box>
              <Box
                fontWeight="fontWeightBold"
                style={{ color: 'gray' }}
                fontSize={25}
              >
                {recentCompanyNumber.length}
              </Box>
            </Grid>
            <Grid
              item
              xs
              className={classes.smallCardRoot}
              style={{ flexDirection: 'column' }}
            >
              <AddShoppingCartIcon
                style={{
                  color: '#70c1b3',
                  fontSize: 60,
                  alignItems: 'right',
                }}
              />
              <Box fontWeight="fontWeightBold" fontSize={20}>
                {t('new-req-num')}
              </Box>
              <Box
                fontWeight="fontWeightBold"
                style={{ color: 'gray' }}
                fontSize={25}
              >
                {recentUserReqNumber.length}
              </Box>
            </Grid>
            <Grid
              item
              xs
              className={classes.smallCardRoot}
              style={{ flexDirection: 'column' }}
            >
              <ShopIcon
                style={{
                  color: '#9ceaef',
                  fontSize: 60,
                  alignItems: 'right',
                }}
              />
              <Box fontWeight="fontWeightBold" fontSize={20}>
                {t('new-ser-created')}
              </Box>
              <Box
                fontWeight="fontWeightBold"
                style={{ color: 'gray' }}
                fontSize={25}
              >
                {recentServiceNumber.length}
              </Box>
            </Grid>
          </Grid>
        </div>
        <div
          style={{
            flexGrow: 1,
          }}
        >
          <Grid
            className={classes.miniBox}
            item
            xs={12}
            style={{
              paddingTop: theme.spacing(2),
              // paddingBottom: theme.spacing(2),
              margin: theme.spacing(3),
            }}
          >
            <Grid
              style={{
                margin: theme.spacing(1),
              }}
            >
              <Grid
                item
                xs
                className={classes.subCardRoot}
                style={{ flexDirection: 'column' }}
              >
                <Box fontWeight="fontWeightBold" className={classes.cardTitle}>
                  {t('today')}
                </Box>
                <Box
                  className={classes.subCardTitle}
                  fontWeight="fontWeightBold"
                  fontFamily="Monospace"
                  fontSize={40}
                >
                  {displayDate}
                </Box>
              </Grid>
              <Grid
                item
                xs
                className={classes.subCardRoot}
                style={{ marginTop: theme.spacing(2) }}
              >
                <FavoriteIcon style={{ color: '#996690', fontSize: 40 }} />
                <Box
                  className={classes.subCardTitle}
                  fontWeight="fontWeightBold"
                  fontFamily="Monospace"
                  fontSize={30}
                >
                  {t('greeting')}
                </Box>
              </Grid>
            </Grid>
            <Grid
              item
              xs
              className={classes.cardRoot}
              style={{ padding: theme.spacing(2) }}
            >
              <Box fontWeight="fontWeightBold" className={classes.cardTitle}>
                {t('new-clients')}
              </Box>
              <List className={classes.listRoot}>
                {recentUser.map((data, key) => (
                  <ListItem key={`User-${data._id}`}>
                    <ListItemAvatar>
                      <Avatar
                        alt={data.username}
                        style={{
                          backgroundColor: getUserColor(key),
                        }}
                      />
                    </ListItemAvatar>
                    <ListItemText primary={data.username} />
                  </ListItem>
                ))}
              </List>
            </Grid>
            <Grid
              item
              xs
              className={classes.cardRoot}
              style={{ padding: theme.spacing(2) }}
            >
              <Box fontWeight="fontWeightBold" className={classes.cardTitle}>
                {t('new-comp')}
              </Box>
              <List className={classes.listRoot}>
                {recentCompany.map((data, key) => (
                  <ListItem key={`Company-${data._id}`}>
                    <ListItemAvatar>
                      <Avatar
                        alt={data.company_name}
                        // className={classes.avatarColor}
                        style={{
                          backgroundColor: getCompanyColor(key),
                        }}
                      />
                    </ListItemAvatar>
                    <ListItemText primary={data.company_name} />
                  </ListItem>
                ))}
              </List>
            </Grid>
          </Grid>
          <Grid
            className={classes.miniBox}
            item
            xs={12}
            // style={{
            //   paddingTop: theme.spacing(2),
            //   paddingBottom: theme.spacing(2),
            // }}
          >
            <Grid
              className={clsx(classes.smallCardRoot, classes.sectionMobile)}
              style={{
                padding: theme.spacing(2),
                flexDirection: 'column',
              }}
            >
              <Box>
                <EmojiEventsIcon
                  style={{
                    color: '#f0e54c',
                    fontSize: 60,
                    alignItems: 'center',
                  }}
                />
                <Box fontWeight="fontWeightBold" fontSize={20}>
                  {t('most-popular-service')}
                </Box>
              </Box>
              {/* <List className={classes.listRoot}> */}
              {mostPopularService.map((data, index) => {
                return (
                  <Box key={`Popular-Service-${index}`} fontSize={20} p={3}>
                    {data.name}
                  </Box>
                )
              })}
              {/* </List> */}
            </Grid>
            <Grid
              className={clsx(classes.smallCardRoot, classes.sectionDesktop)}
              style={{
                width: '100%',
                padding: theme.spacing(2),
              }}
            >
              <Box>
                <EmojiEventsIcon
                  style={{
                    color: '#f0e54c',
                    fontSize: 60,
                    alignItems: 'center',
                  }}
                />
                <Box fontWeight="fontWeightBold" fontSize={20}>
                  {t('most-popular-service')}
                </Box>
              </Box>
              {/* <List className={classes.listRoot}> */}
              {mostPopularService.map((data, index) => {
                return (
                  <Box key={`Popular-Service-${index}`} fontSize={20} p={3}>
                    {data.name}
                  </Box>
                )
              })}
              {/* </List> */}
            </Grid>
            <Grid
              className={clsx(classes.smallCardRoot, classes.sectionMobile)}
              style={{
                padding: theme.spacing(2),
                flexDirection: 'column',
              }}
            >
              <Box>
                <EmojiEventsIcon
                  style={{
                    color: '#f0e54c',
                    fontSize: 60,
                    alignItems: 'center',
                  }}
                />
                <Box fontWeight="fontWeightBold" fontSize={20}>
                  {t('most-popular-vendor')}
                </Box>
              </Box>
              {/* <List className={classes.listRoot}> */}
              {mostPopularCompany.map((data, index) => {
                return (
                  <Box key={`Popular-Company-${index}`} fontSize={20} p={3}>
                    <Button
                      href={`${router.locale}/company/service/${data._id}`}
                    >
                      {data.name}
                    </Button>
                  </Box>
                )
              })}
              {/* </List> */}
            </Grid>
            <Grid
              className={clsx(classes.smallCardRoot, classes.sectionDesktop)}
              style={{
                width: '100%',
                padding: theme.spacing(2),
              }}
            >
              <Box>
                <EmojiEventsIcon
                  style={{
                    color: '#f0e54c',
                    fontSize: 60,
                    alignItems: 'center',
                  }}
                />
                <Box fontWeight="fontWeightBold" fontSize={20}>
                  {t('most-popular-vendor')}
                </Box>
              </Box>
              {/* <List className={classes.listRoot}> */}
              {mostPopularCompany.map((data, index) => {
                return (
                  <Box key={`Popular-Company-${index}`} fontSize={20} p={3}>
                    <Button
                      href={`${router.locale}/company/service/${data._id}`}
                    >
                      {data.name}
                    </Button>
                  </Box>
                )
              })}
              {/* </List> */}
            </Grid>
          </Grid>
          <Grid
            className={classes.miniBox}
            style={{ justifyContent: 'space-between' }}
            item
            xs={12}
            // style={{
            //   paddingTop: theme.spacing(2),
            //   paddingBottom: theme.spacing(2),
            // }}
          >
            {/* <Card className={classes.cardRoot}>
                <Calendar locale={enGB} />
              </Card> */}
            <Grid item xs={3} className={classes.doughnutCard}>
              <Box fontWeight="fontWeightBold" className={classes.cardTitle}>
                {t('ser-status-data')}
              </Box>
              <Doughnut
                type="doughnut"
                style={{
                  marginTop: theme.spacing(4),
                }}
                data={{
                  labels: [
                    'ONGOING',
                    'APPROVED',
                    'DECLINED',
                    'CANCEL',
                    'EXPIRED',
                  ],
                  datasets: [
                    {
                      label: 'status',
                      backgroundColor: [
                        '#80a1d4',
                        '#9db4c0',
                        '#c0b9dd',
                        '#ded9e2',
                        '#f7f4ea',
                      ],
                      hoverBackgroundColor: [
                        '#80a2d4a4',
                        '#9db4c0b5',
                        '#c0b9dda9',
                        '#ded9e2a2',
                        '#f7f4eaa2',
                      ],
                      data: [
                        ongoingService.length,
                        approvedService.length,
                        declinedService.length,
                        cancelledService.length,
                        expiredService.length,
                      ],
                    },
                  ],
                }}
                options={{
                  responsive: true,
                  plugins: {
                    legend: {
                      display: true,
                      position: 'bottom',
                    },
                  },
                }}
              />
            </Grid>
            <Grid item xs className={classes.cardListRoot}>
              <Box fontWeight="fontWeightBold" className={classes.cardTitle}>
                {t('recent-trading')}
              </Box>
              <Tabs
                value={showStatus}
                onChange={handleShowStatusChange}
                indicatorColor="primary"
                textColor="primary"
                variant="fullWidth"
                aria-label="full width tabs example"
              >
                <Tab label={t('ongoing')} {...a11yProps(0)} />
                <Tab label={t('approved')} {...a11yProps(1)} />
                <Tab label={t('declined')} {...a11yProps(3)} />
                <Tab label={t('cancel')} {...a11yProps(4)} />
                <Tab label={t('expired')} {...a11yProps(5)} />
              </Tabs>
              <SwipeableViews
                axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                index={showStatus}
                onChangeIndex={handleShowStatusChangeIndex}
              >
                <TabPanel value={showStatus} index={0} dir={theme.direction}>
                  <DataGrid
                    autoHeight
                    rows={ongoingService}
                    columns={columns}
                    pageSize={5}
                    disableColumnMenu
                  />
                </TabPanel>
                <TabPanel value={showStatus} index={1} dir={theme.direction}>
                  <DataGrid
                    autoHeight
                    rows={approvedService}
                    columns={columns}
                    pageSize={5}
                    disableColumnMenu
                  />
                </TabPanel>
                <TabPanel value={showStatus} index={2} dir={theme.direction}>
                  <DataGrid
                    autoHeight
                    rows={declinedService}
                    columns={columns}
                    pageSize={5}
                    disableColumnMenu
                  />
                </TabPanel>
                <TabPanel value={showStatus} index={3} dir={theme.direction}>
                  <DataGrid
                    autoHeight
                    rows={cancelledService}
                    columns={columns}
                    pageSize={5}
                    disableColumnMenu
                  />
                </TabPanel>
                <TabPanel value={showStatus} index={4} dir={theme.direction}>
                  <DataGrid
                    autoHeight
                    rows={expiredService}
                    columns={columns}
                    pageSize={5}
                    disableColumnMenu
                  />
                </TabPanel>
              </SwipeableViews>
            </Grid>
          </Grid>
        </div>
      </Layout>
    )
  } else if (
    session &&
    mostPopularService &&
    recentUserReqNumber &&
    session.level < 4 &&
    session.level > 1
  ) {
    return (
      <Layout>
        <Box fontWeight="fontWeightBold" fontSize={40} m={1}>
          {t('welcome')}
        </Box>
        <Box className={classes.textPadding}>{t('check-update')}</Box>
        <Box
          fontWeight="fontWeightBold"
          fontFamily="Monospace"
          fontSize={25}
          textAlign="center"
        >
          {t('weekly')}
        </Box>
        <div
          style={{
            flexGrow: 1,
          }}
        >
          <Grid
            className={classes.miniBox}
            item
            xs={12}
            style={{
              borderTop: '1px solid grey',
              borderBottom: '1px solid grey',
              paddingTop: theme.spacing(2),
              paddingBottom: theme.spacing(2),
            }}
          >
            <Grid
              item
              xs
              className={classes.smallCardRoot}
              style={{ flexDirection: 'column' }}
            >
              <Box fontWeight="fontWeightBold" className={classes.cardTitle}>
                {t('today')}
              </Box>
              <Box
                className={classes.subCardTitle}
                fontWeight="fontWeightBold"
                fontFamily="Monospace"
                fontSize={40}
              >
                {displayDate}
              </Box>
            </Grid>
            <Grid item xs className={classes.smallCardRoot}>
              <FavoriteIcon style={{ color: '#996690', fontSize: 40 }} />
              <Box
                className={classes.subCardTitle}
                fontWeight="fontWeightBold"
                fontFamily="Monospace"
                fontSize={30}
              >
                {t('greeting')}
              </Box>
            </Grid>
            <Grid
              item
              xs
              className={classes.smallCardRoot}
              style={{ flexDirection: 'column' }}
            >
              <AddShoppingCartIcon
                style={{
                  color: '#70c1b3',
                  fontSize: 60,
                  alignItems: 'right',
                }}
              />
              <Box fontWeight="fontWeightBold" fontSize={20}>
                {t('new-req-num')}
              </Box>
              <Box
                fontWeight="fontWeightBold"
                style={{ color: 'gray' }}
                fontSize={25}
              >
                {recentReqNumber.length}
              </Box>
            </Grid>
            <Grid
              item
              xs
              className={classes.smallCardRoot}
              style={{ flexDirection: 'column' }}
            >
              <ShopIcon
                style={{
                  color: '#9ceaef',
                  fontSize: 60,
                  alignItems: 'right',
                }}
              />
              <Box fontWeight="fontWeightBold" fontSize={20}>
                {t('new-ser-created')}
              </Box>
              <Box
                fontWeight="fontWeightBold"
                style={{ color: 'gray' }}
                fontSize={25}
              >
                {recentServiceNumber.length}
              </Box>
            </Grid>
          </Grid>
        </div>
        <div
          style={{
            flexGrow: 1,
          }}
        >
          <Grid
            className={classes.miniBox}
            item
            xs={12}
            // style={{
            //   paddingTop: theme.spacing(2),
            //   paddingBottom: theme.spacing(2),
            // }}
          >
            <Grid
              className={clsx(classes.smallCardRoot, classes.sectionMobile)}
              style={{
                padding: theme.spacing(2),
                flexDirection: 'column',
              }}
            >
              <Box>
                <EmojiEventsIcon
                  style={{
                    color: '#f0e54c',
                    fontSize: 60,
                    alignItems: 'center',
                  }}
                />
                <Box fontWeight="fontWeightBold" fontSize={20}>
                  {t('most-popular-service')}
                </Box>
              </Box>
              {/* <List className={classes.listRoot}> */}
              {mostPopularService.map((data, index) => {
                return (
                  <Box key={`Popular-Service-${index}`} fontSize={20} p={3}>
                    {data.name}
                  </Box>
                )
              })}
              {/* </List> */}
            </Grid>
            <Grid
              className={clsx(classes.smallCardRoot, classes.sectionDesktop)}
              style={{
                width: '100%',
                padding: theme.spacing(2),
              }}
            >
              <Box>
                <EmojiEventsIcon
                  style={{
                    color: '#f0e54c',
                    fontSize: 60,
                    alignItems: 'center',
                  }}
                />
                <Box fontWeight="fontWeightBold" fontSize={20}>
                  {t('most-popular-service')}
                </Box>
              </Box>
              {/* <List className={classes.listRoot}> */}
              {mostPopularService.map((data, index) => {
                return (
                  <Box key={`Popular-Service-${index}`} fontSize={20} p={3}>
                    {data.name}
                  </Box>
                )
              })}
              {/* </List> */}
            </Grid>
          </Grid>
          <Grid
            className={classes.miniBox}
            item
            xs={12}
            style={{
              paddingTop: theme.spacing(2),
              paddingBottom: theme.spacing(2),
              justifyContent: 'space-between',
            }}
          >
            {/* <Card className={classes.cardRoot}>
                <Calendar locale={enGB} />
              </Card> */}
            <Grid item xs={3} className={classes.doughnutCard}>
              <Box fontWeight="fontWeightBold" className={classes.cardTitle}>
                {t('ser-status-data')}
              </Box>
              <Doughnut
                type="doughnut"
                style={{
                  marginTop: theme.spacing(4),
                }}
                data={{
                  labels: [
                    'ONGOING',
                    'APPROVED',
                    'DECLINED',
                    'CANCEL',
                    'EXPIRED',
                  ],
                  datasets: [
                    {
                      label: 'status',
                      backgroundColor: [
                        '#80a1d4',
                        '#9db4c0',
                        '#c0b9dd',
                        '#ded9e2',
                        '#f7f4ea',
                      ],
                      hoverBackgroundColor: [
                        '#80a2d4a4',
                        '#9db4c0b5',
                        '#c0b9dda9',
                        '#ded9e2a2',
                        '#f7f4eaa2',
                      ],
                      data: [
                        ongoingService.length,
                        approvedService.length,
                        declinedService.length,
                        cancelledService.length,
                        expiredService.length,
                      ],
                    },
                  ],
                }}
                options={{
                  responsive: true,
                  plugins: {
                    legend: {
                      display: true,
                      position: 'bottom',
                    },
                  },
                }}
              />
            </Grid>
            <Grid item xs className={classes.cardListRoot}>
              <Box fontWeight="fontWeightBold" className={classes.cardTitle}>
                {t('recent-trading')}
              </Box>
              <Tabs
                value={showStatus}
                onChange={handleShowStatusChange}
                indicatorColor="primary"
                textColor="primary"
                variant="fullWidth"
                aria-label="full width tabs example"
              >
                <Tab label={t('ongoing')} {...a11yProps(0)} />
                <Tab label={t('approved')} {...a11yProps(1)} />
                <Tab label={t('declined')} {...a11yProps(3)} />
                <Tab label={t('cancel')} {...a11yProps(4)} />
                <Tab label={t('expired')} {...a11yProps(5)} />
              </Tabs>
              <SwipeableViews
                axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                index={showStatus}
                onChangeIndex={handleShowStatusChangeIndex}
              >
                <TabPanel value={showStatus} index={0} dir={theme.direction}>
                  <DataGrid
                    autoHeight
                    rows={ongoingService}
                    columns={columns}
                    pageSize={5}
                    disableColumnMenu
                  />
                </TabPanel>
                <TabPanel value={showStatus} index={1} dir={theme.direction}>
                  <DataGrid
                    autoHeight
                    rows={approvedService}
                    columns={columns}
                    pageSize={5}
                    disableColumnMenu
                  />
                </TabPanel>
                <TabPanel value={showStatus} index={2} dir={theme.direction}>
                  <DataGrid
                    autoHeight
                    rows={declinedService}
                    columns={columns}
                    pageSize={5}
                    disableColumnMenu
                  />
                </TabPanel>
                <TabPanel value={showStatus} index={3} dir={theme.direction}>
                  <DataGrid
                    autoHeight
                    rows={cancelledService}
                    columns={columns}
                    pageSize={5}
                    disableColumnMenu
                  />
                </TabPanel>
                <TabPanel value={showStatus} index={4} dir={theme.direction}>
                  <DataGrid
                    autoHeight
                    rows={expiredService}
                    columns={columns}
                    pageSize={5}
                    disableColumnMenu
                  />
                </TabPanel>
              </SwipeableViews>
            </Grid>
          </Grid>
        </div>
      </Layout>
    )
  } else if (
    session &&
    mostPopularService &&
    mostPopularCompany &&
    session.level == 4
  ) {
    return (
      <Layout>
        <Box fontWeight="fontWeightBold" fontSize={40} m={1}>
          {t('welcome')}
        </Box>
        <Box className={classes.textPadding}>{t('check-update')}</Box>
        <div
          style={{
            flexGrow: 1,
          }}
        >
          <Grid
            className={classes.miniBox}
            item
            xs={12}
            style={{
              paddingTop: theme.spacing(2),
              paddingBottom: theme.spacing(2),
            }}
          >
            <Grid
              item
              xs
              className={classes.smallCardRoot}
              style={{ flexDirection: 'column' }}
            >
              <Box fontWeight="fontWeightBold" className={classes.cardTitle}>
                {t('today')}
              </Box>
              <Box
                className={classes.subCardTitle}
                fontWeight="fontWeightBold"
                fontFamily="Monospace"
                fontSize={40}
              >
                {displayDate}
              </Box>
            </Grid>
            <Grid item xs className={classes.smallCardRoot}>
              <FavoriteIcon style={{ color: '#996690', fontSize: 40 }} />
              <Box
                className={classes.subCardTitle}
                fontWeight="fontWeightBold"
                fontFamily="Monospace"
                fontSize={30}
              >
                {t('greeting')}
              </Box>
            </Grid>
            <Grid
              item
              xs
              className={classes.smallCardRoot}
              style={{ flexDirection: 'column' }}
            >
              <AddShoppingCartIcon
                style={{
                  color: '#70c1b3',
                  fontSize: 60,
                  alignItems: 'right',
                }}
              />
              <Box fontWeight="fontWeightBold" fontSize={20}>
                {t('monthly-req-num')}
              </Box>
              <Box
                fontWeight="fontWeightBold"
                style={{ color: 'gray' }}
                fontSize={25}
              >
                {recentUserReqNumber.length}
              </Box>
            </Grid>
            <Grid
              item
              xs
              className={classes.smallCardRoot}
              style={{ flexDirection: 'column' }}
            >
              <DirectionsRunIcon
                style={{
                  color: '#9ceaef',
                  fontSize: 60,
                  alignItems: 'right',
                }}
              />
              <Box fontWeight="fontWeightBold" fontSize={20}>
                {t('ongoing-ser-num')}
              </Box>
              <Box
                fontWeight="fontWeightBold"
                style={{ color: 'gray' }}
                fontSize={25}
              >
                {ongoingService.length}
              </Box>
            </Grid>
          </Grid>
        </div>
        <div
          style={{
            flexGrow: 1,
          }}
        >
          <Grid
            className={classes.miniBox}
            item
            xs={12}
            // style={{
            //   paddingTop: theme.spacing(2),
            //   paddingBottom: theme.spacing(2),
            // }}
          >
            <Grid
              className={clsx(classes.smallCardRoot, classes.sectionMobile)}
              style={{
                padding: theme.spacing(2),
                flexDirection: 'column',
              }}
            >
              <Box>
                <EmojiEventsIcon
                  style={{
                    color: '#f0e54c',
                    fontSize: 60,
                    alignItems: 'center',
                  }}
                />
                <Box fontWeight="fontWeightBold" fontSize={20}>
                  {t('most-popular-service')}
                </Box>
              </Box>
              {/* <List className={classes.listRoot}> */}
              {mostPopularService.map((data, index) => {
                return (
                  <Box key={`Popular-Service-${index}`} fontSize={20} p={3}>
                    {data.name}
                  </Box>
                )
              })}
              {/* </List> */}
            </Grid>
            <Grid
              className={clsx(classes.smallCardRoot, classes.sectionDesktop)}
              style={{
                width: '100%',
                padding: theme.spacing(2),
              }}
            >
              <Box>
                <EmojiEventsIcon
                  style={{
                    color: '#f0e54c',
                    fontSize: 60,
                    alignItems: 'center',
                  }}
                />
                <Box fontWeight="fontWeightBold" fontSize={20}>
                  {t('most-popular-service')}
                </Box>
              </Box>
              {/* <List className={classes.listRoot}> */}
              {mostPopularService.map((data, index) => {
                return (
                  <Box key={`Popular-Service-${index}`} fontSize={20} p={3}>
                    {data.name}
                  </Box>
                )
              })}
              {/* </List> */}
            </Grid>
            <Grid
              className={clsx(classes.smallCardRoot, classes.sectionMobile)}
              style={{
                padding: theme.spacing(2),
                flexDirection: 'column',
              }}
            >
              <Box>
                <EmojiEventsIcon
                  style={{
                    color: '#f0e54c',
                    fontSize: 60,
                    alignItems: 'center',
                  }}
                />
                <Box fontWeight="fontWeightBold" fontSize={20}>
                  {t('most-popular-vendor')}
                </Box>
              </Box>
              {/* <List className={classes.listRoot}> */}
              {mostPopularCompany.map((data, index) => {
                return (
                  <Box key={`Popular-Company-${index}`} fontSize={20} p={3}>
                    <Button
                      href={`${router.locale}/company/service/${data._id}`}
                    >
                      {data.name}
                    </Button>
                  </Box>
                )
              })}
              {/* </List> */}
            </Grid>
            <Grid
              className={clsx(classes.smallCardRoot, classes.sectionDesktop)}
              style={{
                width: '100%',
                padding: theme.spacing(2),
              }}
            >
              <Box>
                <EmojiEventsIcon
                  style={{
                    color: '#f0e54c',
                    fontSize: 60,
                    alignItems: 'center',
                  }}
                />
                <Box fontWeight="fontWeightBold" fontSize={20}>
                  {t('most-popular-vendor')}
                </Box>
              </Box>
              {/* <List className={classes.listRoot}> */}
              {mostPopularCompany.map((data, index) => {
                return (
                  <Box key={`Popular-Company-${index}`} fontSize={20} p={3}>
                    <Button
                      href={`${router.locale}/company/service/${data._id}`}
                    >
                      {data.name}
                    </Button>
                  </Box>
                )
              })}
              {/* </List> */}
            </Grid>
          </Grid>
          <Grid
            className={classes.miniBox}
            item
            xs={12}
            style={{
              paddingTop: theme.spacing(2),
              paddingBottom: theme.spacing(2),
              textAlign: 'center',
              justifyContent: 'space-between',
            }}
          >
            {/* <Card className={classes.cardRoot}>
                <Calendar locale={enGB} />
              </Card> */}
            <Grid item xs={3} className={classes.doughnutCard}>
              <Box fontWeight="fontWeightBold" className={classes.cardTitle}>
                {t('ser-status-data')}
              </Box>
              <Doughnut
                type="doughnut"
                style={{
                  marginTop: theme.spacing(4),
                }}
                data={{
                  labels: [
                    'ONGOING',
                    'APPROVED',
                    'DECLINED',
                    'CANCEL',
                    'EXPIRED',
                  ],
                  datasets: [
                    {
                      label: 'status',
                      backgroundColor: [
                        '#80a1d4',
                        '#9db4c0',
                        '#c0b9dd',
                        '#ded9e2',
                        '#f7f4ea',
                      ],
                      hoverBackgroundColor: [
                        '#80a2d4a4',
                        '#9db4c0b5',
                        '#c0b9dda9',
                        '#ded9e2a2',
                        '#f7f4eaa2',
                      ],
                      data: [
                        ongoingService.length,
                        approvedService.length,
                        declinedService.length,
                        cancelledService.length,
                        expiredService.length,
                      ],
                    },
                  ],
                }}
                options={{
                  responsive: true,
                  plugins: {
                    legend: {
                      display: true,
                      position: 'bottom',
                    },
                  },
                }}
              />
            </Grid>
            <Grid item xs className={classes.cardListRoot}>
              <Box fontWeight="fontWeightBold" className={classes.cardTitle}>
                {t('recent-trading')}
              </Box>
              <Tabs
                value={showStatus}
                onChange={handleShowStatusChange}
                indicatorColor="primary"
                textColor="primary"
                variant="fullWidth"
                aria-label="full width tabs example"
              >
                <Tab label={t('ongoing')} {...a11yProps(0)} />
                <Tab label={t('approved')} {...a11yProps(1)} />
                <Tab label={t('declined')} {...a11yProps(3)} />
                <Tab label={t('cancel')} {...a11yProps(4)} />
                <Tab label={t('expired')} {...a11yProps(5)} />
              </Tabs>
              <SwipeableViews
                axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                index={showStatus}
                onChangeIndex={handleShowStatusChangeIndex}
              >
                <TabPanel value={showStatus} index={0} dir={theme.direction}>
                  <DataGrid
                    autoHeight
                    rows={ongoingService}
                    columns={columns}
                    pageSize={5}
                    disableColumnMenu
                  />
                </TabPanel>
                <TabPanel value={showStatus} index={1} dir={theme.direction}>
                  <DataGrid
                    autoHeight
                    rows={approvedService}
                    columns={columns}
                    pageSize={5}
                    disableColumnMenu
                  />
                </TabPanel>
                <TabPanel value={showStatus} index={2} dir={theme.direction}>
                  <DataGrid
                    autoHeight
                    rows={declinedService}
                    columns={columns}
                    pageSize={5}
                    disableColumnMenu
                  />
                </TabPanel>
                <TabPanel value={showStatus} index={3} dir={theme.direction}>
                  <DataGrid
                    autoHeight
                    rows={cancelledService}
                    columns={columns}
                    pageSize={5}
                    disableColumnMenu
                  />
                </TabPanel>
                <TabPanel value={showStatus} index={4} dir={theme.direction}>
                  <DataGrid
                    autoHeight
                    rows={expiredService}
                    columns={columns}
                    pageSize={5}
                    disableColumnMenu
                  />
                </TabPanel>
              </SwipeableViews>
            </Grid>
          </Grid>
        </div>
      </Layout>
    )
  }
  return null
}

export const getStaticProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
})
