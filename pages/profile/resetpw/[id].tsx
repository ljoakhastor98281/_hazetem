import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import {
  Typography,
  TextField,
  IconButton,
  Button,
  Paper,
  Box,
} from '@material-ui/core'
import axios from 'axios'
import 'react-toastify/dist/ReactToastify.css'
import { useTranslation } from 'next-i18next'
import Layout from '../../../components/layouts/navLayout'
import { useRouter } from 'next/router'
import { useSession } from 'next-auth/client'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import Divider from '@material-ui/core/Divider'
import ListItemText from '@material-ui/core/ListItemText'
import Avatar from '@material-ui/core/Avatar'
import { ToastContainer, toast } from 'react-toastify'
import InputAdornment from '@material-ui/core/InputAdornment'
import Badge from '@material-ui/core/Badge'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { Visibility, VisibilityOff } from '@material-ui/icons'
import CheckCircleOutlineOutlinedIcon from '@material-ui/icons/CheckCircleOutlineOutlined'

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  layout: {
    // display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    maxWidth: '768px',
    margin: '0 auto',
  },
  paper: {
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(8),
      padding: `${theme.spacing(6)}px ${theme.spacing(4)}px`,
    },
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  margin: {
    marginRight: theme.spacing(3),
  },
  avatar: {
    width: theme.spacing(15),
    height: theme.spacing(15),
    margin: theme.spacing(3),
  },
  insideListText: {
    left: 10,
    width: '100%',
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  section: {
    flexDirection: 'row',
    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column',
    },
  },
}))

export default function Resetpw() {
  const router = useRouter()
  const classes = useStyles({})
  const { t } = useTranslation()
  const [session, loading] = useSession()
  const [formData, setFormData] = React.useState({
    _id: '',
    company_name: '',
    image_data: '',
  })
  const [accData, setAccData] = React.useState({
    old_password: '',
    new_password: '',
    new_password2: '',
  })
  const [gotData, setGotData] = useState(false)

  useEffect(() => {
    async function fetchData() {
      if (!session) {
        return
      } else {
        if (session?.level == 2 || session?.level == 3) {
          await axios
            .get(`/company/own/${session.user.company_id}`)
            .then((res) => {
              setFormData({
                _id: res.data.company._id,
                company_name: res.data.company.company_name,
                image_data: res.data.company.image_data,
              })
              setGotData(true)
            })
            .catch(() => {
              alert('error')
            })
        }
      }
    }
    fetchData()
  }, [session])

  const [showPassword, setShowPassword] = useState(false)
  const [showPassword2, setShowPassword2] = useState(false)

  const handleClick = () => {
    setShowPassword((prev) => !prev)
  }
  const handleClick2 = () => {
    setShowPassword2((prev) => !prev)
  }

  const handleSubmit = async (e: { preventDefault: () => void }) => {
    e.preventDefault()
    if (accData.new_password == accData.new_password2) {
      await axios
        .put(`/user/changepw/${router.query.id}`, {
          old_password: accData.old_password,
          new_password: accData.new_password,
        })
        .then(() => {
          toast.success(t('change-password-sucess'), {
            position: 'bottom-right',
          })
          setTimeout(() => {
            router.push(`/${router.locale}/profile`)
          }, 5000)
        })
        .catch((err) => {
          switch (err.response.status) {
            case 404: {
              toast.error(t('error-without-status'), {
                position: 'bottom-right',
              })
              break
            }
            case 403: {
              toast.error(t('access-other-ac'), {
                position: 'bottom-right',
              })
              break
            }
            case 400: {
              // console.log(err.response.data)
              if (err.response.data.msg == 'invalid-old') {
                toast.error(t('incorrect-old-password'), {
                  position: 'bottom-right',
                })
                break
              } else if (err.response.data.msg == 'invalid-new') {
                toast.error(t('incorrect-new-password'), {
                  position: 'bottom-right',
                })
                break
              } else {
                toast.error(t('error-without-status'), {
                  position: 'bottom-right',
                })
                break
              }
            }
            case 500: {
              toast.error(t('error-without-status'), {
                position: 'bottom-right',
              })
              break
            }
            default: {
              break
            }
          }
        })
    } else {
      toast.error(t('re-enter-password-message'), {
        position: 'bottom-right',
      })
    }
  }

  return (
    <>
      {!session && !loading && (
        <meta httpEquiv="refresh" content="0; /signin" />
      )}
      {session?.level != 2 && session?.level != 3 && (
        <>
          <Layout>
            <div className={classes.layout}>
              <Paper className={classes.paper} elevation={2}>
                <Box display="flex" justifyContent="space-between" width="100%">
                  <Button
                    startIcon={<ArrowBackIosIcon />}
                    href={`/${router.locale}/profile/${router.query.id}`}
                  >
                    {t('back')}
                  </Button>
                </Box>
                <form className={classes.form} onSubmit={handleSubmit}>
                  <Box
                    display="flex"
                    alignItems="center"
                    justifyContent="center"
                    flexDirection="column"
                  >
                    <Avatar className={classes.avatar} />
                    <Typography component="h1" variant="h4" gutterBottom>
                      {formData?.company_name}
                    </Typography>
                  </Box>
                  <Typography variant="h5" gutterBottom>
                    {t('change-password')}
                  </Typography>
                  <List className={classes.root}>
                    <ListItem className={classes.section}>
                      <ListItemText
                        className={classes.insideListText}
                        primary={t('old-password')}
                        primaryTypographyProps={{
                          style: { fontWeight: 'bold' },
                        }}
                      />
                      <TextField
                        margin="normal"
                        required
                        fullWidth
                        type="password"
                        id="old_password"
                        name="old_password"
                        autoComplete="old_password"
                        variant="outlined"
                        defaultValue={accData.old_password}
                        onChange={(e) =>
                          setAccData({
                            ...accData,
                            old_password: e.target.value,
                          })
                        }
                      />
                    </ListItem>
                    <Divider variant="middle" component="li" />
                    <ListItem className={classes.section}>
                      <ListItemText
                        className={classes.insideListText}
                        primary={t('new-password')}
                        primaryTypographyProps={{
                          style: { fontWeight: 'bold' },
                        }}
                      />
                      <TextField
                        InputProps={{
                          endAdornment: (
                            <InputAdornment position="end">
                              <IconButton onClick={handleClick} edge="end">
                                {showPassword ? (
                                  <Visibility />
                                ) : (
                                  <VisibilityOff />
                                )}
                              </IconButton>
                            </InputAdornment>
                          ),
                        }}
                        margin="normal"
                        required
                        fullWidth
                        type={showPassword ? 'text' : 'password'}
                        id="new_password"
                        name="new_password"
                        autoComplete="new_password"
                        variant="outlined"
                        error={
                          accData.new_password.length < 8 &&
                          accData.new_password.length > 0
                        }
                        helperText={
                          accData.new_password.length < 8 ? (
                            t('password-error')
                          ) : (
                            <CheckCircleOutlineOutlinedIcon
                              fontSize="small"
                              color="primary"
                            />
                          )
                        }
                        defaultValue={accData.new_password}
                        onChange={(e) =>
                          setAccData({
                            ...accData,
                            new_password: e.target.value,
                          })
                        }
                      />
                    </ListItem>
                    <Divider variant="middle" component="li" />
                    <ListItem className={classes.section}>
                      <ListItemText
                        className={classes.insideListText}
                        primary={t('re-enter-password')}
                        primaryTypographyProps={{
                          style: { fontWeight: 'bold' },
                        }}
                      />
                      <TextField
                        InputProps={{
                          endAdornment: (
                            <InputAdornment position="end">
                              <IconButton onClick={handleClick2} edge="end">
                                {showPassword2 ? (
                                  <Visibility />
                                ) : (
                                  <VisibilityOff />
                                )}
                              </IconButton>
                            </InputAdornment>
                          ),
                        }}
                        margin="normal"
                        type={showPassword2 ? 'text' : 'password'}
                        error={
                          accData.new_password !== accData.new_password2 &&
                          accData.new_password2.length > 0
                        }
                        helperText={
                          accData.new_password == accData.new_password2 &&
                          accData.new_password2.length > 0 ? (
                            <CheckCircleOutlineOutlinedIcon
                              fontSize="small"
                              color="primary"
                            />
                          ) : (
                            t('re-enter-password-message')
                          )
                        }
                        required
                        fullWidth
                        id="new_password2"
                        name="new_password2"
                        autoComplete="new_password2"
                        variant="outlined"
                        defaultValue={accData.new_password2}
                        onChange={(e) =>
                          setAccData({
                            ...accData,
                            new_password2: e.target.value,
                          })
                        }
                      />
                    </ListItem>
                  </List>
                  <Box mb={6} textAlign="center">
                    <Button
                      //   disabled={submitting}
                      type="submit"
                      variant="contained"
                      color="primary"
                      className={classes.submit}
                    >
                      {t('update')}
                    </Button>
                  </Box>
                </form>
              </Paper>
              <ToastContainer />
            </div>
          </Layout>
        </>
      )}
      {session && session.level > 1 && session.level < 4 && gotData && (
        <>
          <Layout>
            <div className={classes.layout}>
              <Paper className={classes.paper} elevation={2}>
                <Box display="flex" justifyContent="space-between" width="100%">
                  <Button
                    startIcon={<ArrowBackIosIcon />}
                    href={`/${router.locale}/profile/${router.query.id}`}
                  >
                    {t('back')}
                  </Button>
                </Box>
                <form className={classes.form} onSubmit={handleSubmit}>
                  <Box
                    display="flex"
                    alignItems="center"
                    justifyContent="center"
                    flexDirection="column"
                  >
                    <Badge
                      overlap="circle"
                      anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'right',
                      }}
                    >
                      <Avatar
                        className={classes.avatar}
                        src={
                          formData.image_data
                            ? formData.image_data
                            : 'https://img.icons8.com/pastel-glyph/128/000000/person-male--v3.png'
                        }
                      />
                    </Badge>
                    <Typography component="h1" variant="h4" gutterBottom>
                      {formData?.company_name}
                    </Typography>
                  </Box>
                  <Typography variant="h5" gutterBottom>
                    {t('Change Password')}
                  </Typography>
                  <List className={classes.root}>
                    <ListItem className={classes.section}>
                      <ListItemText
                        className={classes.insideListText}
                        primary={t('Old Password')}
                        primaryTypographyProps={{
                          style: { fontWeight: 'bold' },
                        }}
                      />
                      <TextField
                        margin="normal"
                        required
                        fullWidth
                        type="password"
                        id="old_password"
                        name="old_password"
                        autoComplete="old_password"
                        variant="outlined"
                        defaultValue={accData.old_password}
                        onChange={(e) =>
                          setAccData({
                            ...accData,
                            old_password: e.target.value,
                          })
                        }
                      />
                    </ListItem>
                    <Divider variant="middle" component="li" />
                    <ListItem className={classes.section}>
                      <ListItemText
                        className={classes.insideListText}
                        primary={t('New Password')}
                        primaryTypographyProps={{
                          style: { fontWeight: 'bold' },
                        }}
                      />
                      <TextField
                        InputProps={{
                          endAdornment: (
                            <InputAdornment position="end">
                              <IconButton onClick={handleClick} edge="end">
                                {showPassword ? (
                                  <Visibility />
                                ) : (
                                  <VisibilityOff />
                                )}
                              </IconButton>
                            </InputAdornment>
                          ),
                        }}
                        margin="normal"
                        required
                        fullWidth
                        type={showPassword ? 'text' : 'password'}
                        id="new_password"
                        name="new_password"
                        autoComplete="new_password"
                        variant="outlined"
                        error={
                          accData.new_password.length < 8 &&
                          accData.new_password.length > 0
                        }
                        helperText={
                          accData.new_password.length < 8 ? (
                            t('password-error')
                          ) : (
                            <CheckCircleOutlineOutlinedIcon
                              fontSize="small"
                              color="primary"
                            />
                          )
                        }
                        defaultValue={accData.new_password}
                        onChange={(e) =>
                          setAccData({
                            ...accData,
                            new_password: e.target.value,
                          })
                        }
                      />
                    </ListItem>
                    <ListItem className={classes.section}>
                      <ListItemText
                        className={classes.insideListText}
                        primary={t('re-enter-password')}
                        primaryTypographyProps={{
                          style: { fontWeight: 'bold' },
                        }}
                      />
                      <TextField
                        InputProps={{
                          endAdornment: (
                            <InputAdornment position="end">
                              <IconButton onClick={handleClick2} edge="end">
                                {showPassword2 ? (
                                  <Visibility />
                                ) : (
                                  <VisibilityOff />
                                )}
                              </IconButton>
                            </InputAdornment>
                          ),
                        }}
                        margin="normal"
                        type={showPassword2 ? 'text' : 'password'}
                        error={
                          accData.new_password !== accData.new_password2 &&
                          accData.new_password2.length > 0
                        }
                        helperText={
                          accData.new_password == accData.new_password2 &&
                          accData.new_password2.length > 0 ? (
                            <CheckCircleOutlineOutlinedIcon
                              fontSize="small"
                              color="primary"
                            />
                          ) : (
                            t('re-enter-password-message')
                          )
                        }
                        required
                        fullWidth
                        id="new_password2"
                        name="new_password2"
                        autoComplete="new_password2"
                        variant="outlined"
                        defaultValue={accData.new_password2}
                        onChange={(e) =>
                          setAccData({
                            ...accData,
                            new_password2: e.target.value,
                          })
                        }
                      />
                    </ListItem>
                  </List>
                  <Box mb={6} textAlign="center">
                    <Button
                      //   disabled={submitting}
                      type="submit"
                      variant="contained"
                      color="primary"
                      className={classes.submit}
                    >
                      {t('update')}
                    </Button>
                  </Box>
                </form>
              </Paper>
              <ToastContainer />
            </div>
          </Layout>
        </>
      )}
    </>
  )
}

export const getStaticProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
})

export const getStaticPaths = async () => {
  return {
    paths: [],
    fallback: 'blocking',
  }
}
