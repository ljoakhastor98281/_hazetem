import React, { useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Typography, IconButton, Paper, Box } from '@material-ui/core'
import axios from 'axios'
import 'react-toastify/dist/ReactToastify.css'
import { useTranslation } from 'next-i18next'
import { ICompany } from '@/server/api/Company/company.model'
// import { IUser } from '@/server/api/User/user.model'
import Layout from '../../components/layouts/navLayout'
import router from 'next/router'
import { useSession } from 'next-auth/client'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import Divider from '@material-ui/core/Divider'
import ListItemText from '@material-ui/core/ListItemText'
import Avatar from '@material-ui/core/Avatar'
import EditIcon from '@material-ui/icons/Edit'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  layout: {
    // display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    maxWidth: '768px',
    margin: '0 auto',
  },
  paper: {
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(8),
      padding: `${theme.spacing(6)}px ${theme.spacing(4)}px`,
    },
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  margin: {
    marginRight: theme.spacing(3),
  },
  avatar: {
    width: theme.spacing(15),
    height: theme.spacing(15),
    margin: theme.spacing(3),
  },
  insideListText: {
    width: '100%',
  },
  section: {
    flexDirection: 'row',
    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column',
    },
  },
}))

export default function CompanyProfile() {
  const classes = useStyles({})
  const { t } = useTranslation()
  const [renderedData, setRenderedData] = React.useState<ICompany>()
  const [accData, setAccData] = React.useState({
    username: '',
    email: '',
    normal_user_company_name: '',
    normal_user_company_address: '',
  })

  const [session, loading] = useSession()
  useEffect(() => {
    async function fetchData() {
      if (!session) {
        return
      } else if (session?.level == 2 || session?.level == 3) {
        const res = await axios.get(`/company/own/${session.user.company_id}`)
        setRenderedData(() => res.data.company)
      }
      await axios
        .get(`/user/${session.user._id}`)
        .then((res) => {
          setAccData({
            username: res.data.user.username,
            email: res.data.user.email,
            normal_user_company_name: res.data.user.normal_user_company_name,
            normal_user_company_address:
              res.data.user.normal_user_company_address,
          })
        })
        .catch(() => {
          return
        })
    }
    fetchData()
  }, [session])

  return (
    <>
      {!session && !loading && (
        <meta httpEquiv="refresh" content="0; /signin" />
      )}
      {session && (
        <>
          <Layout>
            <div className={classes.layout}>
              <Paper className={classes.paper} elevation={2}>
                <Box display="flex" alignItems="right" justifyContent="right">
                  <IconButton
                    edge="end"
                    href={`/${router.locale}/profile/${session.user._id}`}
                  >
                    <EditIcon />
                  </IconButton>
                </Box>
                <Box
                  display="flex"
                  alignItems="center"
                  justifyContent="center"
                  flexDirection="column"
                >
                  <Avatar
                    className={classes.avatar}
                    src={
                      renderedData?.image_data
                        ? renderedData?.image_data
                        : 'https://img.icons8.com/pastel-glyph/128/000000/person-male--v3.png'
                    }
                  />
                  {/* <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" /> */}
                  <Typography component="h1" variant="h4" gutterBottom>
                    {renderedData?.company_name}
                  </Typography>
                </Box>
                <form className={classes.form}>
                  <Typography variant="h5" gutterBottom>
                    {t('acct-info')}
                  </Typography>
                  <List className={classes.root}>
                    <ListItem className={classes.section}>
                      <ListItemText
                        className={classes.insideListText}
                        primary={t('username')}
                        primaryTypographyProps={{
                          style: { fontWeight: 'bold' },
                        }}
                      />
                      <ListItemText
                        className={classes.insideListText}
                        primary={accData.username}
                      />
                    </ListItem>
                    <Divider variant="middle" component="li" />
                    <ListItem className={classes.section}>
                      <ListItemText
                        className={classes.insideListText}
                        primary={t('email')}
                        primaryTypographyProps={{
                          style: { fontWeight: 'bold' },
                        }}
                      />
                      <ListItemText
                        className={classes.insideListText}
                        primary={accData.email}
                      />
                    </ListItem>
                  </List>
                  <br />
                  <br />
                  {accData.normal_user_company_name ||
                  accData.normal_user_company_address ? (
                    <>
                      <Typography variant="h5" gutterBottom>
                        {t('comp-info') + t('optional')}
                      </Typography>
                      <List className={classes.root}>
                        <ListItem className={classes.section}>
                          <ListItemText
                            className={classes.insideListText}
                            primary={t('company_name')}
                            primaryTypographyProps={{
                              style: { fontWeight: 'bold' },
                            }}
                          />
                          <ListItemText
                            className={classes.insideListText}
                            primary={accData.normal_user_company_name}
                          />
                        </ListItem>
                        <Divider variant="middle" component="li" />
                        <ListItem className={classes.section}>
                          <ListItemText
                            className={classes.insideListText}
                            primary={t('company_address')}
                            primaryTypographyProps={{
                              style: { fontWeight: 'bold' },
                            }}
                          />
                          <ListItemText
                            className={classes.insideListText}
                            primary={accData.normal_user_company_address}
                          />
                        </ListItem>
                      </List>
                    </>
                  ) : null}
                  {session?.level == 2 || session?.level == 3 ? (
                    <>
                      <br />
                      <br />
                      <Typography
                        variant="h5"
                        gutterBottom
                        // style={{ margin: 20 }}
                      >
                        {t('comp-info')}
                      </Typography>
                      <List className={classes.root}>
                        <ListItem className={classes.section}>
                          <ListItemText
                            className={classes.insideListText}
                            primary={t('business_outline')}
                            primaryTypographyProps={{
                              style: { fontWeight: 'bold' },
                            }}
                          />
                          <ListItemText
                            className={classes.insideListText}
                            primary={renderedData?.business_outline}
                          />
                        </ListItem>
                        <Divider variant="middle" component="li" />
                        <ListItem className={classes.section}>
                          <ListItemText
                            className={classes.insideListText}
                            primary={t('company_outline')}
                            primaryTypographyProps={{
                              style: { fontWeight: 'bold' },
                            }}
                          />
                          <ListItemText
                            className={classes.insideListText}
                            primary={renderedData?.company_outline}
                          />
                        </ListItem>
                        <Divider variant="middle" component="li" />
                        <ListItem className={classes.section}>
                          <ListItemText
                            className={classes.insideListText}
                            primary={t('company_start_year')}
                            primaryTypographyProps={{
                              style: { fontWeight: 'bold' },
                            }}
                          />
                          <ListItemText
                            className={classes.insideListText}
                            primary={renderedData?.company_start_year}
                          />
                        </ListItem>
                        <Divider variant="middle" component="li" />
                        <ListItem className={classes.section}>
                          <ListItemText
                            className={classes.insideListText}
                            primary={t('number_of_staff')}
                            primaryTypographyProps={{
                              style: { fontWeight: 'bold' },
                            }}
                          />
                          <ListItemText
                            className={classes.insideListText}
                            primary={renderedData?.number_of_staff}
                          />
                        </ListItem>
                        <Divider variant="middle" component="li" />
                        <ListItem className={classes.section}>
                          <ListItemText
                            className={classes.insideListText}
                            primary={t('company_address')}
                            primaryTypographyProps={{
                              style: { fontWeight: 'bold' },
                            }}
                          />
                          <ListItemText
                            className={classes.insideListText}
                            primary={renderedData?.company_address}
                          />
                        </ListItem>
                        <Divider variant="middle" component="li" />
                        <ListItem className={classes.section}>
                          <ListItemText
                            className={classes.insideListText}
                            primary={t('contact_person')}
                            primaryTypographyProps={{
                              style: { fontWeight: 'bold' },
                            }}
                          />
                          <ListItemText
                            className={classes.insideListText}
                            primary={renderedData?.contact_person}
                          />
                        </ListItem>
                        <Divider variant="middle" component="li" />
                        <ListItem className={classes.section}>
                          <ListItemText
                            className={classes.insideListText}
                            primary={t('client_contact_number')}
                            primaryTypographyProps={{
                              style: { fontWeight: 'bold' },
                            }}
                          />
                          <ListItemText
                            className={classes.insideListText}
                            primary={renderedData?.client_contact_number}
                          />
                        </ListItem>
                        <Divider variant="middle" component="li" />
                        <ListItem className={classes.section}>
                          <ListItemText
                            className={classes.insideListText}
                            primary={t('contact_email')}
                            primaryTypographyProps={{
                              style: { fontWeight: 'bold' },
                            }}
                          />
                          <ListItemText
                            className={classes.insideListText}
                            primary={renderedData?.contact_email}
                          />
                        </ListItem>
                        <Divider variant="middle" component="li" />
                        <ListItem className={classes.section}>
                          <ListItemText
                            className={classes.insideListText}
                            primary={t('BR_expiry_date')}
                            primaryTypographyProps={{
                              style: { fontWeight: 'bold' },
                            }}
                          />
                          <ListItemText
                            className={classes.insideListText}
                            primary={
                              renderedData?.BR_expiry_date.toString().split(
                                `T`,
                              )[0]
                            }
                          />
                        </ListItem>
                        <Divider variant="middle" component="li" />
                        <ListItem className={classes.section}>
                          <ListItemText
                            className={classes.insideListText}
                            primary={t('tvp_account_name')}
                            primaryTypographyProps={{
                              style: { fontWeight: 'bold' },
                            }}
                          />
                          <ListItemText
                            className={classes.insideListText}
                            primary={renderedData?.tvp_account_name}
                          />
                        </ListItem>
                        <Divider variant="middle" component="li" />
                        <ListItem className={classes.section}>
                          <ListItemText
                            className={classes.insideListText}
                            primary={t('tvp_password')}
                            primaryTypographyProps={{
                              style: { fontWeight: 'bold' },
                            }}
                          />
                          <ListItemText
                            className={classes.insideListText}
                            primary={renderedData?.tvp_password}
                          />
                        </ListItem>
                      </List>
                      <br />
                      <br />
                      <Typography variant="h5" gutterBottom>
                        {t('proj-info')}
                      </Typography>
                      <List className={classes.root}>
                        <ListItem className={classes.section}>
                          <ListItemText
                            className={classes.insideListText}
                            primary={t('project_name')}
                            primaryTypographyProps={{
                              style: { fontWeight: 'bold' },
                            }}
                          />
                          <ListItemText
                            className={classes.insideListText}
                            primary={renderedData?.project_name}
                          />
                        </ListItem>
                        <Divider variant="middle" component="li" />
                        <ListItem className={classes.section}>
                          <ListItemText
                            className={classes.insideListText}
                            primary={t('date_of_agreement_signed')}
                            primaryTypographyProps={{
                              style: { fontWeight: 'bold' },
                            }}
                          />
                          <ListItemText
                            className={classes.insideListText}
                            primary={
                              renderedData?.date_of_agreement_signed
                                .toString()
                                .split(`T`)[0]
                            }
                          />
                        </ListItem>
                        <Divider variant="middle" component="li" />
                        <ListItem className={classes.section}>
                          <ListItemText
                            className={classes.insideListText}
                            primary={t('status')}
                            primaryTypographyProps={{
                              style: { fontWeight: 'bold' },
                            }}
                          />
                          <ListItemText
                            className={classes.insideListText}
                            primary={renderedData?.status}
                          />
                        </ListItem>
                        <Divider variant="middle" component="li" />
                        <ListItem className={classes.section}>
                          <ListItemText
                            className={classes.insideListText}
                            primary={t('sales_lead')}
                            primaryTypographyProps={{
                              style: { fontWeight: 'bold' },
                            }}
                          />
                          <ListItemText
                            className={classes.insideListText}
                            primary={renderedData?.sales_lead}
                          />
                        </ListItem>
                        <Divider variant="middle" component="li" />
                        <ListItem className={classes.section}>
                          <ListItemText
                            className={classes.insideListText}
                            primary={t('total_amount')}
                            primaryTypographyProps={{
                              style: { fontWeight: 'bold' },
                            }}
                          />
                          <ListItemText
                            className={classes.insideListText}
                            primary={'$' + renderedData?.total_amount}
                          />
                        </ListItem>
                        <Divider variant="middle" component="li" />
                        <ListItem className={classes.section}>
                          <ListItemText
                            className={classes.insideListText}
                            primary={t('remark')}
                            primaryTypographyProps={{
                              style: { fontWeight: 'bold' },
                            }}
                          />
                          <ListItemText
                            className={classes.insideListText}
                            primary={renderedData?.remark}
                          />
                        </ListItem>
                      </List>
                    </>
                  ) : null}
                </form>
              </Paper>
            </div>
          </Layout>
        </>
      )}
    </>
  )
}

export const getStaticProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
})

// export const getServerSideProps: GetServerSideProps = async () => {
//   const res = await axios.get(`/company`)
//   return { props: { note: res.data.company } }
// }

// export default CompanyProfile
