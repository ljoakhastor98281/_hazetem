import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import {
  Typography,
  TextField,
  IconButton,
  Button,
  Paper,
  Box,
} from '@material-ui/core'
import axios from 'axios'
import 'react-toastify/dist/ReactToastify.css'
import { useTranslation } from 'next-i18next'
// import { ICompany } from '@/server/api/Company/company.model'
// import { IUser } from '@/server/api/User/user.model'
import Layout from '../../components/layouts/navLayout'
import { useRouter } from 'next/router'
import { useSession } from 'next-auth/client'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import Divider from '@material-ui/core/Divider'
import ListItemText from '@material-ui/core/ListItemText'
import Avatar from '@material-ui/core/Avatar'
import { ToastContainer, toast } from 'react-toastify'
import InputAdornment from '@material-ui/core/InputAdornment'
import Badge from '@material-ui/core/Badge'
import AddCircleIcon from '@material-ui/icons/AddCircle'
import { DropzoneDialog } from 'material-ui-dropzone'
import { toBase64 } from '@/server/utils/helper'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'
import VpnKeyIcon from '@material-ui/icons/VpnKey'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import CheckCircleOutlineOutlinedIcon from '@material-ui/icons/CheckCircleOutlineOutlined'
import SignatureDialog from '@/components/elements/SignatureDialog'

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  layout: {
    // display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    maxWidth: '768px',
    margin: '0 auto',
  },
  paper: {
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(8),
      padding: `${theme.spacing(6)}px ${theme.spacing(4)}px`,
    },
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  margin: {
    marginRight: theme.spacing(3),
  },
  avatar: {
    width: theme.spacing(15),
    height: theme.spacing(15),
    // margin: theme.spacing(3),
    margin: 'auto',
  },
  insideListText: {
    left: 10,
    width: '100%',
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  button: {
    color: 'white',
    borderRadius: 8,
    backgroundColor: '#4ea1d3',
    margin: 1,
    '&:hover': {
      backgroundColor: '#3f51B5',
    },
  },
  section: {
    flexDirection: 'row',
    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column',
    },
  },
  sectionDesktop: {
    display: 'flex',
    border: '1px solid ',
    borderColor: 'grey',
    [theme.breakpoints.down('xs')]: {
      display: 'none',
    },
  },
  sectionMobile: {
    display: 'none',
    [theme.breakpoints.down('xs')]: {
      display: 'flex',
    },
  },
}))

export default function CompanyProfile() {
  const router = useRouter()
  const classes = useStyles({})
  const { t } = useTranslation()
  const [session, loading] = useSession()
  const [formData, setFormData] = React.useState({
    _id: '',
    company_name: '',
    business_outline: '',
    status: '',
    date_of_agreement_signed: '',
    sales_lead: '',
    total_amount: '',
    contact_person: '',
    contact_email: '',
    client_contact_number: '',
    number_of_staff: '',
    company_start_year: '',
    company_outline: '',
    company_address: '',
    tvp_account_name: '',
    tvp_password: '',
    remark: '',
    admin_modified: '',
    project_name: '',
    BR_expiry_date: '',
    image_data: '',
    chop_image_data: '',
    signature_image_data: '',
  })
  const [accData, setAccData] = React.useState({
    username: '',
    email: '',
    password: '',
    normal_user_company_name: '',
    normal_user_company_address: '',
  })
  const [gotData, setGotData] = useState(false)
  const [gotAccData, setGotAccData] = useState(false)

  useEffect(() => {
    async function fetchData() {
      if (!session || gotData) {
        return
      } else {
        await axios
          .get(`/user/${session.user._id}`)
          .then((res) => {
            setAccData({
              username: res.data.user.username,
              email: res.data.user.email,
              password: res.data.user.password,
              normal_user_company_name: res.data.user.normal_user_company_name,
              normal_user_company_address:
                res.data.user.normal_user_company_address,
            })
            setGotAccData(true)
          })
          .catch(() => {
            return
          })
        if (session?.level == 2 || session?.level == 3) {
          await axios
            .get(`/company/own/${session.user.company_id}`)
            .then((res) => {
              setFormData({
                _id: res.data.company._id,
                company_name: res.data.company.company_name,
                business_outline: res.data.company.business_outline,
                status: res.data.company.status,
                date_of_agreement_signed:
                  res.data.company.date_of_agreement_signed,
                sales_lead: res.data.company.sales_lead,
                total_amount: res.data.company.total_amount,
                contact_person: res.data.company.contact_person,
                contact_email: res.data.company.contact_email,
                client_contact_number: res.data.company.client_contact_number,
                number_of_staff: res.data.company.number_of_staff,
                company_start_year: res.data.company.company_start_year,
                company_outline: res.data.company.company_outline,
                company_address: res.data.company.company_address,
                tvp_account_name: res.data.company.tvp_account_name,
                tvp_password: res.data.company.tvp_password,
                remark: res.data.company.remark,
                admin_modified: res.data.company.admin_modified,
                project_name: res.data.company.project_name,
                BR_expiry_date: res.data.company.BR_expiry_date,
                image_data: res.data.company.image_data
                  ? res.data.company.image_data
                  : '',
                chop_image_data: res.data.company.chop_image_data
                  ? res.data.company.chop_image_data
                  : '',
                signature_image_data: res.data.company.signature_image_data
                  ? res.data.company.signature_image_data
                  : '',
              })
              setGotData(true)
            })
            .catch(() => {
              alert('error')
            })
        }
      }
    }
    fetchData()
  }, [session, gotData])

  const [image_open, set_image_open] = useState(false)
  const [chop_image_open, set_chop_image_open] = useState(false)
  const [signature_image_open, set_signature_image_open] = useState(false)
  const [signature_pad_open, set_signature_pad_open] = useState(false)

  const showError = () =>
    toast.error(t('error'), {
      position: 'bottom-right',
    })
  const showSuccess = () =>
    toast.success(t('update-success'), {
      position: 'bottom-right',
    })

  const handleChange = (e: { target: { name: string; value: string } }) => {
    setFormData({ ...formData, [e.target.name]: e.target.value })
  }

  const handleAccChange = (e: { target: { name: string; value: string } }) => {
    setAccData({ ...accData, [e.target.name]: e.target.value })
  }

  const handleSubmit = async (e: { preventDefault: () => void }) => {
    e.preventDefault()
    formData.contact_email = accData.email
    let res
    let accRes
    try {
      accRes = await axios.put(`/user/change/${session?.user._id}`, accData)
      if (session?.level == 2 || session?.level == 3) {
        res = await axios.put(`/company/${session.user.company_id}`, formData)
      }
    } catch (err) {
      if (err.response) {
        // console.log(err.response.data)
        switch (err.response.status) {
          case 409:
            if (err.response.data.msg == 'conflict-username') {
              toast.error(t('register-409-fail'), {
                position: 'bottom-right',
              })
              break
            } else if (err.response.data.msg == 'conflict-email') {
              toast.error(t('register-409-fail'), {
                position: 'bottom-right',
              })
              break
            } else if (err.response.data.msg == 'conflict-company_name') {
              toast.error(t('register-409-company-fail'), {
                position: 'bottom-right',
              })
              break
            } else {
              toast.error(t('error-without-status'), {
                position: 'bottom-right',
              })
              break
            }
          case 400:
            if (err.response.data.msg == 'invalid-username-length') {
              toast.error(t('username-error'), {
                position: 'bottom-right',
              })
              break
            } else if (err.response.data.msg == 'invalid-email') {
              toast.error(t('email-error'), {
                position: 'bottom-right',
              })
              break
            } else {
              toast.error(t('error-without-status'), {
                position: 'bottom-right',
              })
              break
            }
          case 403:
            toast.error(t('access-other-info'), {
              position: 'bottom-right',
            })
            break
          default:
            showError()
            break
        }
      }
      // if (err.message) console.log(err.message)
    }
    if (session?.level == 2 || session?.level == 3) {
      if (res?.status == 200 && accRes?.status == 200) {
        showSuccess()
        await new Promise((r) => setTimeout(r, 3000))
        router.push(`/${router.locale}/profile`)
      }
    } else {
      if (accRes?.status == 200) {
        showSuccess()
        await new Promise((r) => setTimeout(r, 3000))
        router.push(`/${router.locale}/profile`)
      }
    }
  }

  const handleImageOpen = async () => {
    set_image_open(true)
  }

  const handleChopImageOpen = async () => {
    set_chop_image_open(true)
  }

  const handleSignatureImageOpen = async () => {
    set_signature_image_open(true)
    set_signature_pad_open(false)
  }

  const handleSignaturePadOpen = async () => {
    set_signature_image_open(false)
    set_signature_pad_open(true)
  }

  const handleImageClose = async () => {
    set_image_open(false)
  }
  const handleChopImageClose = async () => {
    set_chop_image_open(false)
  }

  const handleSignatureImageClose = async () => {
    set_signature_image_open(false)
  }

  const handleSignaturePadClose = async () => {
    set_signature_pad_open(false)
  }

  const handleImageSave = async (files: File[]) => {
    setFormData({
      ...formData,
      image_data: (await toBase64(files[0])) as string,
    })
    set_image_open(false)
  }

  const handleChopImageSave = async (files: File[]) => {
    setFormData({
      ...formData,
      chop_image_data: (await toBase64(files[0])) as string,
    })
    set_chop_image_open(false)
  }

  const handleSignatureImageSave = async (files: File[]) => {
    setFormData({
      ...formData,
      signature_image_data: (await toBase64(files[0])) as string,
    })
    set_signature_image_open(false)
  }

  const handleSignaturePadSave = async (data: string) => {
    setFormData({
      ...formData,
      signature_image_data: data,
    })
    set_signature_pad_open(false)
  }

  return (
    <>
      {!session && !loading && (
        <meta httpEquiv="refresh" content="0; /signin" />
      )}
      {session?.level != 2 && session?.level != 3 && gotAccData && (
        <>
          <Layout>
            <div className={classes.layout}>
              <Paper className={classes.paper} elevation={2}>
                <Box display="flex" justifyContent="space-between" width="100%">
                  <Button
                    startIcon={<ArrowBackIosIcon />}
                    href={`/${router.locale}/profile`}
                  >
                    {t('back')}
                  </Button>
                  <Button
                    startIcon={<VpnKeyIcon />}
                    href={`/${router.locale}/profile/resetpw/${router.query.id}`}
                    className={classes.sectionDesktop}
                  >
                    {t('change-pw')}
                  </Button>
                  <IconButton
                    href={`/${router.locale}/profile/resetpw/${router.query.id}`}
                    className={classes.sectionMobile}
                  >
                    <VpnKeyIcon />
                  </IconButton>
                </Box>
                <form className={classes.form} onSubmit={handleSubmit}>
                  <Box
                    display="flex"
                    alignItems="center"
                    justifyContent="center"
                    flexDirection="column"
                  >
                    <Avatar className={classes.avatar} />
                    <Typography component="h1" variant="h4" gutterBottom>
                      {formData?.company_name}
                    </Typography>
                  </Box>
                  <Typography variant="h5" gutterBottom>
                    {t('acct-info')}
                  </Typography>
                  <List className={classes.root}>
                    <ListItem className={classes.section}>
                      <ListItemText
                        className={classes.insideListText}
                        primary={t('username')}
                        primaryTypographyProps={{
                          style: { fontWeight: 'bold' },
                        }}
                      />
                      <TextField
                        margin="normal"
                        required
                        fullWidth
                        id="username"
                        name="username"
                        autoComplete="username"
                        variant="outlined"
                        defaultValue={accData.username}
                        error={
                          accData.username.length < 8 &&
                          accData.username.length > 0
                        }
                        helperText={
                          accData.username.length < 8 ? (
                            t('username-error')
                          ) : (
                            <CheckCircleOutlineOutlinedIcon
                              fontSize="small"
                              color="primary"
                            />
                          )
                        }
                        onChange={handleAccChange}
                      />
                    </ListItem>
                    <Divider variant="middle" component="li" />
                    <ListItem className={classes.section}>
                      <ListItemText
                        className={classes.insideListText}
                        primary={t('email')}
                        primaryTypographyProps={{
                          style: { fontWeight: 'bold' },
                        }}
                      />
                      <TextField
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        name="email"
                        type="email"
                        autoComplete="email"
                        variant="outlined"
                        error={
                          accData.email.length > 0 &&
                          !/\S+@\S+\.\S+/.test(accData.email)
                        }
                        helperText={
                          !/\S+@\S+\.\S+/.test(accData.email) ? (
                            t('email-error')
                          ) : (
                            <CheckCircleOutlineOutlinedIcon
                              color="primary"
                              fontSize="small"
                            />
                          )
                        }
                        defaultValue={accData.email}
                        onChange={handleAccChange}
                      />
                    </ListItem>
                  </List>
                  <br />
                  <br />
                  <Typography variant="h5" gutterBottom>
                    {t('comp-info') + t('optional')}
                  </Typography>
                  <List className={classes.root}>
                    <ListItem className={classes.section}>
                      <ListItemText
                        className={classes.insideListText}
                        primary={t('company_name')}
                        primaryTypographyProps={{
                          style: { fontWeight: 'bold' },
                        }}
                      />
                      <TextField
                        margin="normal"
                        fullWidth
                        id="normal_user_company_name"
                        name="normal_user_company_name"
                        autoComplete="normal_user_company_name"
                        variant="outlined"
                        defaultValue={accData?.normal_user_company_name}
                        onChange={handleAccChange}
                      />
                    </ListItem>
                    <Divider variant="middle" component="li" />
                    <ListItem className={classes.section}>
                      <ListItemText
                        className={classes.insideListText}
                        primary={t('company_address')}
                        primaryTypographyProps={{
                          style: { fontWeight: 'bold' },
                        }}
                      />
                      <TextField
                        margin="normal"
                        fullWidth
                        id="normal_user_company_address"
                        name="normal_user_company_address"
                        autoComplete="normal_user_company_address"
                        variant="outlined"
                        defaultValue={accData?.normal_user_company_address}
                        onChange={handleAccChange}
                      />
                    </ListItem>
                  </List>
                  <Box mb={6} textAlign="center">
                    <Button
                      //   disabled={submitting}
                      type="submit"
                      variant="contained"
                      color="primary"
                      className={classes.submit}
                    >
                      {t('update')}
                    </Button>
                  </Box>
                </form>
              </Paper>
              <ToastContainer />
            </div>
          </Layout>
        </>
      )}
      {(session?.level as number) > 1 &&
        (session?.level as number) < 4 &&
        gotAccData &&
        gotData && (
          <>
            <Layout>
              <div className={classes.layout}>
                <Paper className={classes.paper} elevation={2}>
                  <Box
                    display="flex"
                    justifyContent="space-between"
                    width="100%"
                  >
                    <Button
                      startIcon={<ArrowBackIosIcon />}
                      href={`/${router.locale}/profile`}
                    >
                      {t('back')}
                    </Button>
                    <Button
                      startIcon={<VpnKeyIcon />}
                      className={classes.sectionDesktop}
                      href={`/${router.locale}/profile/resetpw/${router.query.id}`}
                    >
                      {t('change-pw')}
                    </Button>
                    <Button
                      startIcon={<VpnKeyIcon />}
                      className={classes.sectionMobile}
                      href={`/${router.locale}/profile/resetpw/${router.query.id}`}
                    ></Button>
                  </Box>
                  <form className={classes.form} onSubmit={handleSubmit}>
                    <Box
                      display="flex"
                      alignItems="center"
                      justifyContent="center"
                      flexDirection="column"
                    >
                      {session?.level == 2 ? (
                        <Button
                          onClick={handleImageOpen}
                          style={{ backgroundColor: 'transparent' }}
                        >
                          <Badge
                            overlap="circle"
                            anchorOrigin={{
                              vertical: 'bottom',
                              horizontal: 'right',
                            }}
                            badgeContent={<AddCircleIcon />}
                          >
                            <Avatar
                              className={classes.avatar}
                              src={
                                formData.image_data
                                  ? formData.image_data
                                  : 'https://img.icons8.com/pastel-glyph/128/000000/person-male--v3.png'
                              }
                            />
                          </Badge>
                        </Button>
                      ) : (
                        <Avatar
                          className={classes.avatar}
                          src={
                            formData.image_data
                              ? formData.image_data
                              : 'https://img.icons8.com/pastel-glyph/128/000000/person-male--v3.png'
                          }
                        />
                      )}
                      <DropzoneDialog
                        open={image_open}
                        onSave={handleImageSave}
                        acceptedFiles={['image/png', 'image/jpeg']}
                        showPreviews={true}
                        maxFileSize={5000000}
                        onClose={handleImageClose}
                        filesLimit={1}
                        onDrop={handleImageSave}
                        showAlerts={['error']}
                        getFileAddedMessage={(fileName) => {
                          toast.success(
                            t('file-added-success', { fileName: fileName }),
                            { position: 'bottom-left' },
                          )
                          return t('file-added-success', { fileName: fileName })
                        }}
                        clearOnUnmount={false}
                      />
                      {/* <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" /> */}
                      <Typography component="h1" variant="h4" gutterBottom>
                        {formData?.company_name}
                      </Typography>
                    </Box>
                    <Typography variant="h5" gutterBottom>
                      {t('acct-info')}
                    </Typography>
                    <List className={classes.root}>
                      <ListItem className={classes.section}>
                        <ListItemText
                          className={classes.insideListText}
                          primary={t('username')}
                          primaryTypographyProps={{
                            style: { fontWeight: 'bold' },
                          }}
                        />
                        <TextField
                          margin="normal"
                          required
                          fullWidth
                          id="username"
                          name="username"
                          autoComplete="username"
                          variant="outlined"
                          defaultValue={accData.username}
                          error={
                            accData.username.length < 8 &&
                            accData.username.length > 0
                          }
                          helperText={
                            accData.username.length < 8 ? (
                              t('username-error')
                            ) : (
                              <CheckCircleOutlineOutlinedIcon
                                fontSize="small"
                                color="primary"
                              />
                            )
                          }
                          onChange={handleAccChange}
                        />
                      </ListItem>
                      <Divider variant="middle" component="li" />
                      <ListItem className={classes.section}>
                        <ListItemText
                          className={classes.insideListText}
                          primary={t('email')}
                          primaryTypographyProps={{
                            style: { fontWeight: 'bold' },
                          }}
                        />
                        <TextField
                          margin="normal"
                          required
                          fullWidth
                          id="email"
                          name="email"
                          autoComplete="email"
                          variant="outlined"
                          defaultValue={accData.email}
                          error={
                            accData.email.length > 0 &&
                            !/\S+@\S+\.\S+/.test(accData.email)
                          }
                          helperText={
                            !/\S+@\S+\.\S+/.test(accData.email) ? (
                              t('email-error')
                            ) : (
                              <CheckCircleOutlineOutlinedIcon
                                color="primary"
                                fontSize="small"
                              />
                            )
                          }
                          onChange={handleAccChange}
                        />
                      </ListItem>
                    </List>
                    <br />
                    <br />
                    <Typography variant="h5" gutterBottom>
                      {t('comp-info')}
                    </Typography>
                    <List className={classes.root}>
                      <ListItem className={classes.section}>
                        <ListItemText
                          className={classes.insideListText}
                          primary={t('business_outline')}
                          primaryTypographyProps={{
                            style: { fontWeight: 'bold' },
                          }}
                        />
                        <TextField
                          margin="normal"
                          required
                          fullWidth
                          id="business_outline"
                          name="business_outline"
                          autoComplete="business_outline"
                          variant="outlined"
                          defaultValue={formData?.business_outline}
                          onChange={handleChange}
                        />
                      </ListItem>
                      <Divider variant="middle" component="li" />
                      <ListItem className={classes.section}>
                        <ListItemText
                          className={classes.insideListText}
                          primary={t('company_outine')}
                          primaryTypographyProps={{
                            style: { fontWeight: 'bold' },
                          }}
                        />
                        <TextField
                          margin="normal"
                          required
                          fullWidth
                          id="company_outline"
                          name="company_outline"
                          autoComplete="company_outline"
                          variant="outlined"
                          defaultValue={formData?.company_outline}
                          onChange={handleChange}
                        />
                      </ListItem>
                      <Divider variant="middle" component="li" />
                      <ListItem className={classes.section}>
                        <ListItemText
                          className={classes.insideListText}
                          primary={t('company_start_year')}
                          primaryTypographyProps={{
                            style: { fontWeight: 'bold' },
                          }}
                        />
                        <TextField
                          margin="normal"
                          required
                          fullWidth
                          type="number"
                          onKeyDown={(evt) =>
                            ['e', 'E', '+', '-'].includes(evt.key) &&
                            evt.preventDefault()
                          }
                          id="company_start_year"
                          name="company_start_year"
                          autoComplete="company_start_year"
                          variant="outlined"
                          defaultValue={formData?.company_start_year}
                          onChange={handleChange}
                        />
                      </ListItem>
                      <Divider variant="middle" component="li" />
                      <ListItem className={classes.section}>
                        <ListItemText
                          className={classes.insideListText}
                          primary={t('number_of_staff')}
                          primaryTypographyProps={{
                            style: { fontWeight: 'bold' },
                          }}
                        />
                        <TextField
                          margin="normal"
                          required
                          fullWidth
                          id="number_of_staff"
                          name="number_of_staff"
                          autoComplete="number_of_staff"
                          type="number"
                          onKeyDown={(evt) =>
                            ['e', 'E', '+', '-'].includes(evt.key) &&
                            evt.preventDefault()
                          }
                          variant="outlined"
                          defaultValue={formData?.number_of_staff}
                          onChange={handleChange}
                        />
                      </ListItem>
                      <Divider variant="middle" component="li" />
                      <ListItem className={classes.section}>
                        <ListItemText
                          className={classes.insideListText}
                          primary={t('company_address')}
                          primaryTypographyProps={{
                            style: { fontWeight: 'bold' },
                          }}
                        />
                        <TextField
                          margin="normal"
                          required
                          fullWidth
                          id="company_address"
                          name="company_address"
                          autoComplete="company_address"
                          variant="outlined"
                          defaultValue={formData?.company_address}
                          onChange={handleChange}
                        />
                      </ListItem>
                      <Divider variant="middle" component="li" />
                      <ListItem className={classes.section}>
                        <ListItemText
                          className={classes.insideListText}
                          primary={t('contact_person')}
                          primaryTypographyProps={{
                            style: { fontWeight: 'bold' },
                          }}
                        />
                        <TextField
                          margin="normal"
                          required
                          fullWidth
                          id="contact_person"
                          name="contact_person"
                          autoComplete="contact_person"
                          variant="outlined"
                          defaultValue={formData?.contact_person}
                          onChange={handleChange}
                        />
                      </ListItem>
                      <Divider variant="middle" component="li" />
                      <ListItem className={classes.section}>
                        <ListItemText
                          className={classes.insideListText}
                          primary={t('client_contact_number')}
                          primaryTypographyProps={{
                            style: { fontWeight: 'bold' },
                          }}
                        />
                        <TextField
                          margin="normal"
                          required
                          fullWidth
                          type="number"
                          onKeyDown={(evt) =>
                            ['e', 'E', '+', '-'].includes(evt.key) &&
                            evt.preventDefault()
                          }
                          id="client_contact_number"
                          name="client_contact_number"
                          autoComplete="client_contact_number"
                          variant="outlined"
                          defaultValue={formData?.client_contact_number}
                          onChange={handleChange}
                        />
                      </ListItem>
                      <Divider variant="middle" component="li" />
                      <ListItem className={classes.section}>
                        <ListItemText
                          className={classes.insideListText}
                          primary={t('BR_expiry_date')}
                          primaryTypographyProps={{
                            style: { fontWeight: 'bold' },
                          }}
                        />
                        <TextField
                          margin="normal"
                          required
                          fullWidth
                          id="BR_expiry_date"
                          name="BR_expiry_date"
                          autoComplete="BR_expiry_date"
                          variant="outlined"
                          type="date"
                          defaultValue={
                            formData?.BR_expiry_date.toString().split(`T`)[0]
                          }
                          onChange={handleChange}
                        />
                      </ListItem>
                      <Divider variant="middle" component="li" />
                      <ListItem className={classes.section}>
                        <ListItemText
                          className={classes.insideListText}
                          primary={t('tvp_account_name')}
                          primaryTypographyProps={{
                            style: { fontWeight: 'bold' },
                          }}
                        />
                        <TextField
                          margin="normal"
                          fullWidth
                          id="tvp_account_name"
                          name="tvp_account_name"
                          autoComplete="tvp_account_name"
                          variant="outlined"
                          defaultValue={formData?.tvp_account_name}
                          onChange={handleChange}
                        />
                      </ListItem>
                      <Divider variant="middle" component="li" />
                      <ListItem className={classes.section}>
                        <ListItemText
                          className={classes.insideListText}
                          primary={t('tvp_password')}
                          primaryTypographyProps={{
                            style: { fontWeight: 'bold' },
                          }}
                        />
                        <TextField
                          margin="normal"
                          fullWidth
                          id="tvp_password"
                          name="tvp_password"
                          autoComplete="tvp_password"
                          variant="outlined"
                          defaultValue={formData?.tvp_password}
                          onChange={handleChange}
                        />
                      </ListItem>
                    </List>
                    <br />
                    <br />
                    <Typography variant="h5" gutterBottom>
                      {t('proj-info')}
                    </Typography>
                    <List className={classes.root}>
                      <ListItem className={classes.section}>
                        <ListItemText
                          className={classes.insideListText}
                          primary={t('project_name')}
                          primaryTypographyProps={{
                            style: { fontWeight: 'bold' },
                          }}
                        />
                        <TextField
                          margin="normal"
                          required
                          fullWidth
                          id="project_name"
                          name="project_name"
                          autoComplete="project_name"
                          variant="outlined"
                          defaultValue={formData?.project_name}
                          onChange={handleChange}
                        />
                      </ListItem>
                      <Divider variant="middle" component="li" />
                      <ListItem className={classes.section}>
                        <ListItemText
                          className={classes.insideListText}
                          primary={t('date_of_agreement_signed')}
                          primaryTypographyProps={{
                            style: { fontWeight: 'bold' },
                          }}
                        />
                        <TextField
                          margin="normal"
                          required
                          fullWidth
                          type="date"
                          id="date_of_agreement_signed"
                          name="date_of_agreement_signed"
                          autoComplete="date_of_agreement_signed"
                          variant="outlined"
                          defaultValue={
                            formData?.date_of_agreement_signed
                              .toString()
                              .split(`T`)[0]
                          }
                          onChange={handleChange}
                        />
                      </ListItem>
                      <Divider variant="middle" component="li" />
                      <ListItem className={classes.section}>
                        <ListItemText
                          className={classes.insideListText}
                          primary={t('status')}
                          primaryTypographyProps={{
                            style: { fontWeight: 'bold' },
                          }}
                        />
                        <TextField
                          margin="normal"
                          required
                          fullWidth
                          id="status"
                          name="status"
                          autoComplete="status"
                          variant="outlined"
                          defaultValue={formData?.status}
                          onChange={handleChange}
                        />
                      </ListItem>
                      <Divider variant="middle" component="li" />
                      <ListItem className={classes.section}>
                        <ListItemText
                          className={classes.insideListText}
                          primary={t('sales_lead')}
                          primaryTypographyProps={{
                            style: { fontWeight: 'bold' },
                          }}
                        />
                        <TextField
                          margin="normal"
                          required
                          fullWidth
                          id="sales_lead"
                          name="sales_lead"
                          autoComplete="sales_lead"
                          variant="outlined"
                          defaultValue={formData?.sales_lead}
                          onChange={handleChange}
                        />
                      </ListItem>
                      <Divider variant="middle" component="li" />
                      <ListItem className={classes.section}>
                        <ListItemText
                          className={classes.insideListText}
                          primary={t('total_amount')}
                          primaryTypographyProps={{
                            style: { fontWeight: 'bold' },
                          }}
                        />
                        <TextField
                          margin="normal"
                          required
                          fullWidth
                          id="total_amount"
                          name="total_amount"
                          autoComplete="total_amount"
                          variant="outlined"
                          type="number"
                          onKeyDown={(evt) =>
                            ['e', 'E', '+', '-'].includes(evt.key) &&
                            evt.preventDefault()
                          }
                          InputProps={{
                            startAdornment: (
                              <InputAdornment position="start">
                                $
                              </InputAdornment>
                            ),
                          }}
                          defaultValue={formData?.total_amount}
                          onChange={handleChange}
                        />
                      </ListItem>
                      <Divider variant="middle" component="li" />
                      <ListItem className={classes.section}>
                        <ListItemText
                          className={classes.insideListText}
                          primary={t('remark')}
                          primaryTypographyProps={{
                            style: { fontWeight: 'bold' },
                          }}
                        />
                        <TextField
                          margin="normal"
                          fullWidth
                          id="remark"
                          name="remark"
                          autoComplete="remark"
                          variant="outlined"
                          defaultValue={formData?.remark}
                          onChange={handleChange}
                        />
                      </ListItem>
                    </List>

                    {(session?.level as number) == 2 ? (
                      <>
                        <br />
                        <br />
                        <Typography variant="h5" gutterBottom>
                          {t('signature-and-chop')}
                        </Typography>
                        <List className={classes.root}>
                          <ListItem className={classes.section}>
                            <ListItemText
                              className={classes.insideListText}
                              primary={t('chop')}
                              primaryTypographyProps={{
                                style: { fontWeight: 'bold' },
                              }}
                            />
                            <Box
                              display="flex"
                              // alignItems="right"
                              justifyContent="right"
                            >
                              <Button
                                onClick={handleChopImageOpen}
                                style={{ backgroundColor: 'transparent' }}
                              >
                                <Badge
                                  overlap="circle"
                                  anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'right',
                                  }}
                                  badgeContent={<AddCircleIcon />}
                                >
                                  <Avatar
                                    className={classes.avatar}
                                    src={
                                      formData.chop_image_data
                                        ? formData.chop_image_data
                                        : 'https://img.icons8.com/windows/32/000000/rubber-stamp-bottom-view.png'
                                    }
                                  />
                                </Badge>
                              </Button>

                              <DropzoneDialog
                                open={chop_image_open}
                                onSave={handleChopImageSave}
                                acceptedFiles={['image/png', 'image/jpeg']}
                                showPreviews={true}
                                maxFileSize={5000000}
                                onClose={handleChopImageClose}
                                filesLimit={1}
                                onDrop={handleChopImageSave}
                                showAlerts={['error']}
                                getFileAddedMessage={(fileName) => {
                                  toast.success(
                                    t('file-added-success', {
                                      fileName: fileName,
                                    }),
                                    { position: 'bottom-left' },
                                  )
                                  return t('file-added-success', {
                                    fileName: fileName,
                                  })
                                }}
                                clearOnUnmount={false}
                              />
                            </Box>
                          </ListItem>
                          <Divider variant="middle" component="li" />
                          <ListItem className={classes.section}>
                            <ListItemText
                              className={classes.insideListText}
                              primary={t('signature')}
                              primaryTypographyProps={{
                                style: { fontWeight: 'bold' },
                              }}
                            />

                            <div>
                              <Avatar
                                className={classes.avatar}
                                src={
                                  formData.signature_image_data
                                    ? formData.signature_image_data
                                    : 'https://img.icons8.com/fluent/48/000000/signature.png'
                                }
                              />
                              <Button
                                onClick={handleSignatureImageOpen}
                                className={classes.button}
                                color="primary"
                              >
                                {t('upload-signature')}
                              </Button>
                              <Button
                                onClick={handleSignaturePadOpen}
                                className={classes.button}
                                color="primary"
                              >
                                {t('sign-here')}
                              </Button>
                            </div>
                            <DropzoneDialog
                              open={signature_image_open}
                              onSave={handleSignatureImageSave}
                              acceptedFiles={['image/png', 'image/jpeg']}
                              showPreviews={true}
                              maxFileSize={5000000}
                              onClose={handleSignatureImageClose}
                              filesLimit={1}
                              onDrop={handleSignatureImageSave}
                              showAlerts={['error']}
                              getFileAddedMessage={(fileName) => {
                                toast.success(
                                  t('file-added-success', {
                                    fileName: fileName,
                                  }),
                                  { position: 'bottom-left' },
                                )
                                return t('file-added-success', {
                                  fileName: fileName,
                                })
                              }}
                              clearOnUnmount={false}
                            />
                            <SignatureDialog
                              open={signature_pad_open}
                              onClose={handleSignaturePadClose}
                              title={t('company-signature')}
                              handleSave={handleSignaturePadSave}
                            />
                          </ListItem>
                          <Divider variant="middle" component="li" />
                        </List>
                      </>
                    ) : (
                      <div></div>
                    )}

                    <Box mb={6} textAlign="center">
                      <Button
                        //   disabled={submitting}
                        type="submit"
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                      >
                        {t('update')}
                      </Button>
                    </Box>
                  </form>
                </Paper>
                <ToastContainer />
              </div>
            </Layout>
          </>
        )}
    </>
  )
}

export const getStaticProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
})

export const getStaticPaths = async () => {
  return {
    paths: [],
    fallback: 'blocking',
  }
}

// export const getServerSideProps: GetServerSideProps = async () => {
//   const res = await axios.get(`/company`)
//   return { props: { note: res.data.company } }
// }

// export default CompanyProfile
