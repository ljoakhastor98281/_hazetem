// This is an example of to protect an API route
import { getSession } from 'next-auth/client'
import type { NextApiRequest, NextApiResponse } from 'next'
import { useTranslation } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

const ProtectedHandler = async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req })
  const { t } = useTranslation()

  if (session) {
    return res.send({
      content: t('protected-content'),
    })
  }

  res.send({
    error: t('protected-error'),
  })
}

export const getStaticProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
})

export default ProtectedHandler
