import axios from 'axios'
import NextAuth from 'next-auth'
import Providers from 'next-auth/providers'

// For more information on each option (and a full list of options) go to
// https://next-auth.js.org/configuration/options
export default NextAuth({
  // https://next-auth.js.org/configuration/providers
  providers: [
    Providers.Credentials({
      // The name to display on the sign in form (e.g. 'Sign in with...')
      name: 'Credentials',
      // The credentials is used to generate a suitable form on the sign in page.
      // You can specify whatever fields you are expecting to be submitted.
      // e.g. domain, username, password, 2FA token, etc.
      credentials: {
        username: { label: 'Username', type: 'text' },
        password: { label: 'Password', type: 'text' },
      },
      async authorize(credentials, _req) {
        // console.log('credentials', credentials)
        try {
          const res = await axios.post('/user/signin', credentials)
          // console.log('auth', res?.data)
          if (res.data) {
            return res.data
          }
        } catch (e) {
          return null
        }
      },
    }),
  ],

  // Database optional. MySQL, Maria DB, Postgres and MongoDB are supported.
  // https://next-auth.js.org/configuration/databases
  //
  // Notes:
  // * You must install an appropriate node_module for your database
  // * The Email provider requires a database (OAuth providers do not)
  database: process.env.DATABASE_URL ?? '',

  // The secret should be set to a reasonably long random string.
  // It is used to sign cookies and to sign and encrypt JSON Web Tokens, unless
  // a separate secret is defined explicitly for encrypting the JWT.
  secret: '0a5e90f2b35894134611120648be9dce' ?? '',

  session: {
    // Use JSON Web Tokens for session instead of database sessions.
    // This option can be used with or without a database for users/accounts.
    // Note: `jwt` is automatically set to `true` if no database is specified.
    jwt: true,

    // Seconds - How long until an idle session expires and is no longer valid.
    // maxAge: 30 * 24 * 60 * 60, // 30 days

    // Seconds - Throttle how frequently to write to database to extend a session.
    // Use it to limit write operations. Set to 0 to always update the database.
    // Note: This option is ignored if using JSON Web Tokens
    // updateAge: 24 * 60 * 60, // 24 hours
  },

  // JSON Web tokens are only used for sessions if the `jwt: true` session
  // option is set - or by default if no database is specified.
  // https://next-auth.js.org/configuration/options#jwt
  jwt: {
    // A secret to use for key generation (you should set this explicitly)
    secret: '0a5e90f2b35894134611120648be9dce',
    // Set to true to use encryption (default: false)
    // encryption: true,
    // You can define your own encode/decode functions for signing and encryption
    // if you want to override the default behaviour.
    // encode: async ({ secret, token, maxAge }) => {},
    // decode: async ({ secret, token, maxAge }) => {},
  },

  // You can define custom pages to override the built-in ones. These will be regular Next.js pages
  // so ensure that they are placed outside of the '/api' folder, e.g. signIn: '/auth/mycustom-signin'
  // The routes shown here are the default URLs that will be used when a custom
  // pages is not specified for that route.
  // https://next-auth.js.org/configuration/pages
  // pages: {
  //   signIn: '/signin', // Displays signin buttons
  // signOut: '/signout', // Displays form with sign out button
  // error: '/login', // Error code passed in query string as ?error=
  // verifyRequest: '/verify-request', // Used for check email page
  // newUser: null, // If set, new users will be directed here on first sign in
  // },

  // Callbacks are asynchronous functions you can use to control what happens
  // when an action is performed.
  // https://next-auth.js.org/configuration/callbacks
  callbacks: {
    // async signIn(user, account, profile) {
    //   console.log('signIn', user, account, profile)

    //   return true
    // },
    // async redirect(url, baseUrl) {
    //   console.log('redirect', url, baseUrl)
    //   return baseUrl
    // },
    async session(session, token) {
      // @ts-ignore
      session.user = token.data
      session.accessToken = token.accessToken
      session.level = token.level

      return session
    },
    async jwt(token, user, account, profile, isNewUser) {
      // console.log('jwt', token, user, account, profile, isNewUser)
      if (user) {
        token.email = user.email
        token.username = user.username
        token.level = user.level
        token.data = user
        token.accessToken = user.accessToken
      }
      // console.log(token)

      return token
    },
  },

  // Events are useful for logging
  // https://next-auth.js.org/configuration/events
  events: {},

  // Enable debug messages in the console if you are having problems
  debug: true,
})
