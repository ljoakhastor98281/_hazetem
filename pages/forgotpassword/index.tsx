import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import {
  Box,
  Button,
  TextField,
  Card,
  Link,
  Typography,
} from '@material-ui/core'
import { Fonts } from '@/components/constants/AppEnums'
import { CremaTheme } from '@/types/AppContextPropsType'
import { useTranslation } from 'next-i18next'
import HazedawnWhiteIcon from '@/components/icons/HazedawnWhite'
import axios from 'axios'
import { toast, ToastContainer } from 'react-toastify'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { useRouter } from 'next/router'
import 'react-toastify/dist/ReactToastify.css'
import { BallSpinner } from 'react-spinners-kit'
import DoneIcon from '@material-ui/icons/Done'

const useStyles = makeStyles((theme: CremaTheme) => ({
  cardRoot: {
    maxWidth: '45rem',
    width: '100%',
    overflow: 'hidden',
    boxShadow: '0 10px 15px -3px rgba(0, 0, 0, 0.1)',
    textAlign: 'center',
    position: 'relative',
    paddingTop: 20,
    '&:before': {
      content: "''",
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      width: 130,
      height: 9,
      borderBottomRightRadius: 80,
      borderBottomLeftRadius: 80,
      marginRight: 'auto',
      marginLeft: 'auto',
      backgroundColor: theme.palette.primary.main,
    },
  },
  textUppercase: {
    textTransform: 'uppercase',
  },
  formRoot: {
    textAlign: 'left',
  },
  myTextFieldRoot: {
    width: '100%',
  },
  checkboxRoot: {
    marginLeft: -12,
  },
  pointer: {
    cursor: 'pointer',
  },
  btnRoot: {
    // borderRadius: theme.overrides.MuiCard.root.borderRadius,
    width: '10rem',
    fontWeight: Fonts.REGULAR,
    fontSize: 16,
    textTransform: 'capitalize',
  },
  btnRootFull: {
    width: '100%',
  },
  dividerRoot: {
    marginBottom: 16,
    marginLeft: -48,
    marginRight: -48,
  },
  textPrimary: {
    color: theme.palette.text.primary,
  },
  colorTextPrimary: {
    color: theme.palette.primary.main,
  },
  underlineNone: {
    textDecoration: 'none',
  },
  textGrey: {
    color: theme.palette.grey[500],
  },
  spinnerbox: {
    justifyContent: 'center',
    textAlign: 'center',
    display: 'flex',
  },
}))

const ForgotPassword = () => {
  const { t } = useTranslation()
  const classes = useStyles({})
  const router = useRouter()

  const [formData, setFormData] = useState({
    email: '',
  })
  const [isSendingEmail, setIsSendingEmail] = useState(false)
  const [done, setDone] = useState(false)

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    setIsSendingEmail(true)
    await axios
      .post('/mail/forgotpw', {
        email: formData.email,
      })
      .then(() => {
        toast.success(t('forget-pw-confirm-email'), {
          position: 'bottom-right',
        })
        setDone(true)
        setTimeout(() => {
          router.push({
            pathname: '/forgotpassword/sendcode',
            query: { email: formData.email },
          })
        }, 5000)
      })
      .catch((err) => {
        setIsSendingEmail(false)
        switch (err.response.status) {
          case 418: {
            toast.error(t('418-error'), {
              position: 'bottom-right',
            })
            break
          }
          default: {
            toast.error(t('not-registered-email'), {
              position: 'bottom-right',
            })
            break
          }
        }
      })
  }

  return (
    <Box
      flex={1}
      display="flex"
      height="100vh"
      position="relative"
      bgcolor="#f3f4f6"
      flexDirection="column"
      justifyContent="center"
    >
      <Box textAlign="center">
        <HazedawnWhiteIcon size="140" />
      </Box>

      <Box
        display="flex"
        flexDirection="column"
        justifyContent="center"
        alignItems="center"
      >
        <Card className={classes.cardRoot}>
          <Box px={{ xs: 6, sm: 10 }}>
            <Box
              component="h2"
              mb={{ xs: 3 }}
              color="text.primary"
              fontWeight={Fonts.REGULAR}
              fontSize={{ xs: 24 }}
            >
              {t('forget-your-password')}
            </Box>
          </Box>

          <Box px={{ xs: 6, sm: 10 }}>
            <Box
              component="h5"
              mb={{ xs: 3 }}
              color="text.primary"
              fontWeight={Fonts.REGULAR}
              fontSize={{ xs: 18 }}
            >
              {t('forget-pw-email')}
            </Box>
          </Box>

          <Box flex={1} display="flex" flexDirection="column">
            <Box
              px={{ xs: 6, sm: 10 }}
              pt={8}
              flex={1}
              display="flex"
              flexDirection="column"
            >
              {isSendingEmail ? (
                <>
                  <Box
                    mb={{ xs: 5 }}
                    margin-left="auto"
                    margin-right="auto"
                    className={classes.spinnerbox}
                  >
                    <BallSpinner
                      size={80}
                      color="#1B2AE3"
                      loading={isSendingEmail}
                    />
                  </Box>
                  <Box
                    mb={{ xs: 5 }}
                    margin-left="auto"
                    margin-right="auto"
                    className={classes.spinnerbox}
                  >
                    {done ? (
                      <DoneIcon />
                    ) : (
                      <Typography>{t('sendemail')}</Typography>
                    )}
                  </Box>
                </>
              ) : (
                <form className={classes.formRoot} onSubmit={handleSubmit}>
                  <Box mb={{ xs: 5 }}>
                    <TextField
                      placeholder={t('email') ?? 'Email'}
                      name="email"
                      label={t('email')}
                      variant="outlined"
                      className={classes.myTextFieldRoot}
                      type="text"
                      defaultValue={formData.email}
                      onChange={(e) =>
                        setFormData({ ...formData, email: e.target.value })
                      }
                    />
                  </Box>

                  <Box
                    mb={6}
                    display="flex"
                    flexDirection={{ xs: 'column', sm: 'row' }}
                    alignItems={{ sm: 'center' }}
                    justifyContent={{ sm: 'space-between' }}
                  >
                    <Button
                      variant="contained"
                      color="secondary"
                      type="submit"
                      disabled={isSendingEmail}
                      className={classes.btnRoot}
                    >
                      {t('send-pw')}
                    </Button>

                    <Box
                      ml={{ xs: 0, sm: 4 }}
                      mt={{ xs: 3, sm: 0 }}
                      color="text.secondary"
                      fontSize={15}
                    >
                      <Box className={classes.textGrey} component="span" mr={2}>
                        {t('have-pw')}
                      </Box>
                      <br />
                      <Box component="span">
                        <Link
                          href="/signin"
                          className={`${classes.underlineNone} ${classes.colorTextPrimary}`}
                        >
                          {t('signin')}
                        </Link>
                      </Box>
                    </Box>
                  </Box>
                </form>
              )}

              <ToastContainer />
            </Box>
          </Box>
        </Card>
      </Box>
    </Box>
  )
}

export const getStaticProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
})

export default ForgotPassword
