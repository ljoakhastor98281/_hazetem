import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Box, Button, TextField, Card, Link } from '@material-ui/core'
import { Fonts } from '@/components/constants/AppEnums'
import { CremaTheme } from '@/types/AppContextPropsType'
import { useTranslation } from 'next-i18next'
import HazedawnWhiteIcon from '@/components/icons/HazedawnWhite'
import axios from 'axios'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { useRouter } from 'next/router'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { CountdownCircleTimer } from 'react-countdown-circle-timer'

const useStyles = makeStyles((theme: CremaTheme) => ({
  cardRoot: {
    // maxWidth: '36rem',
    maxWidth: '45rem',
    width: '100%',
    overflow: 'hidden',
    boxShadow: '0 10px 15px -3px rgba(0, 0, 0, 0.1)',
    textAlign: 'center',
    position: 'relative',
    paddingTop: 20,
    [theme.breakpoints.up('xl')]: {
      paddingTop: 32,
    },
    '&:before': {
      content: "''",
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      width: 130,
      height: 9,
      borderBottomRightRadius: 80,
      borderBottomLeftRadius: 80,
      marginRight: 'auto',
      marginLeft: 'auto',
      backgroundColor: theme.palette.primary.main,
    },
  },
  textUppercase: {
    textTransform: 'uppercase',
  },
  formRoot: {
    textAlign: 'left',
    [theme.breakpoints.up('xl')]: {
      marginBottom: 24,
    },
  },
  myTextFieldRoot: {
    width: '100%',
  },
  checkboxRoot: {
    marginLeft: -12,
  },
  pointer: {
    cursor: 'pointer',
  },
  btnRoot: {
    // borderRadius: theme.overrides.MuiCard.root.borderRadius,
    width: '10rem',
    fontWeight: Fonts.REGULAR,
    fontSize: 16,
    textTransform: 'capitalize',
  },
  btnRootFull: {
    width: '100%',
  },
  dividerRoot: {
    marginBottom: 16,
    marginLeft: -48,
    marginRight: -48,
    [theme.breakpoints.up('xl')]: {
      marginBottom: 32,
    },
  },
  textPrimary: {
    color: theme.palette.text.primary,
  },
  colorTextPrimary: {
    color: theme.palette.primary.main,
  },
  underlineNone: {
    textDecoration: 'none',
  },
  textGrey: {
    color: theme.palette.grey[500],
  },
  back: {
    position: 'absolute',
    left: 10,
  },
  text: {
    color: '#aaa',
  },
  value: {
    fontSize: '40px',
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    display: 'flex',
  },
}))

const SendVerificationCode = () => {
  const { t } = useTranslation()
  const classes = useStyles({})
  const router = useRouter()
  const [formData, setFormData] = useState({
    ver_code: '',
  })
  const [disable, setDisable] = useState(false)
  const [isSendingEmail, setIsSendingEmail] = useState(false)
  const duration = 60

  // const showError = () =>
  //   toast.error(t('request-timeout'), {
  //     position: 'bottom-right',
  //   })
  // const showSuccess = () =>
  //   toast.success(t('create-success'), {
  //     position: 'bottom-right',
  //   })

  useEffect(() => {
    const timer = setTimeout(async () => {
      toast.error(t('request-timeout'), {
        position: 'bottom-right',
      })
      setDisable(true)
    }, duration * 1000)
    return () => {
      clearTimeout(timer)
    }
  }, [t])

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    await axios
      .put('/user/sendcode', formData)
      .then(() => {
        toast.success(t('code-success'), {
          position: 'bottom-right',
        })
        setTimeout(async () => {
          router.push(`/${router.locale}/forgotpassword/reset`)
        }, 5000)
      })
      .catch((err) => {
        switch (err.response.status) {
          case 400: {
            toast.error(t('incorrect-verification-code'), {
              position: 'bottom-right',
            })
            break
          }
          case 404: {
            toast.error(t('verification-error'), {
              position: 'bottom-right',
            })
            break
          }
          case 410: {
            toast.error(t('expired-verification'), {
              position: 'bottom-right',
            })
            break
          }
          case 500: {
            toast.error(t('error-without-status'), {
              position: 'bottom-right',
            })
            break
          }
          default: {
            break
          }
        }
      })
  }

  interface TimeProps {
    remainingTime: number
  }

  const renderTime = ({ remainingTime }: TimeProps) => {
    if (remainingTime === 0) {
      return <div className="timer">{t('timeout')}</div>
    }

    return (
      <div className="timer">
        <div className="text">{t('remaining')}</div>
        <div className="value">{remainingTime}</div>
        <div className="text">{t('seconds')}</div>
      </div>
    )
  }
  return (
    <Box
      flex={1}
      display="flex"
      height="100vh"
      position="relative"
      bgcolor="#f3f4f6"
      flexDirection="column"
      justifyContent="center"
    >
      <Box mb={{ xs: 6, md: 8, xl: 18 }} textAlign="center">
        <HazedawnWhiteIcon size="140" />
      </Box>

      <Box
        display="flex"
        flexDirection="column"
        justifyContent="center"
        alignItems="center"
      >
        <Card className={classes.cardRoot}>
          <Box px={{ xs: 6, sm: 10, xl: 15 }}>
            <Button
              startIcon={<ArrowBackIosIcon />}
              href={`/${router.locale}/forgotpassword`}
              className={classes.back}
            >
              {t('back')}
            </Button>
            <br /> <br />
            <Box
              component="h2"
              mb={{ xs: 3, xl: 6 }}
              color="text.primary"
              fontWeight={Fonts.REGULAR}
              fontSize={{ xs: 24, xl: 26 }}
            >
              {t('verification-code-sent')}
            </Box>
          </Box>

          <Box px={{ xs: 6, sm: 10, xl: 15 }}>
            <Box
              component="h5"
              mb={{ xs: 3, xl: 6 }}
              color="text.primary"
              fontWeight={Fonts.REGULAR}
              fontSize={{ xs: 18, xl: 18 }}
            >
              {t('enter-code')}
            </Box>
          </Box>

          <Box className={classes.center}>
            <CountdownCircleTimer
              //   onComplete={() => {
              //     disable ?
              //     return [true, 1000]
              //   }}
              isPlaying
              duration={duration}
              colors={[
                ['#004777', 0.33],
                ['#F7B801', 0.33],
                ['#A30000', 0.33],
              ]}
            >
              {renderTime}
            </CountdownCircleTimer>
          </Box>

          <Box flex={1} display="flex" flexDirection="column">
            <Box
              px={{ xs: 6, sm: 10, xl: 15 }}
              pt={8}
              flex={1}
              display="flex"
              flexDirection="column"
            >
              <form className={classes.formRoot} onSubmit={handleSubmit}>
                <Box mb={{ xs: 5, xl: 8 }}>
                  <TextField
                    placeholder={t('verification-code') ?? 'Verification Code'}
                    name="ver_code"
                    label={t('verification-code')}
                    variant="outlined"
                    className={classes.myTextFieldRoot}
                    type="text"
                    defaultValue={formData.ver_code}
                    disabled={disable}
                    onChange={(
                      e: React.ChangeEvent<
                        HTMLInputElement | HTMLTextAreaElement
                      >,
                    ) => {
                      setFormData({ ...formData, ver_code: e.target.value })
                    }}
                  />
                </Box>

                <Box
                  mb={6}
                  display="flex"
                  flexDirection={{ xs: 'column', sm: 'row' }}
                  alignItems={{ sm: 'center' }}
                  justifyContent={{ sm: 'space-between' }}
                >
                  <Button
                    variant="contained"
                    color="secondary"
                    type="submit"
                    disabled={disable}
                    className={classes.btnRoot}
                  >
                    {t('continue')}
                  </Button>

                  {disable ? (
                    isSendingEmail ? (
                      <a>{t('sending-code')}</a>
                    ) : (
                      <Box
                        ml={{ xs: 0, sm: 4 }}
                        mt={{ xs: 3, sm: 0 }}
                        color="text.secondary"
                        fontSize={15}
                      >
                        <Box
                          className={classes.textGrey}
                          component="span"
                          mr={2}
                        >
                          {t('trouble-receiving-code')}
                        </Box>
                        <br />
                        <Box component="span">
                          <Link
                            href="#"
                            className={`${classes.underlineNone} ${classes.colorTextPrimary}`}
                            onClick={async () => {
                              setIsSendingEmail(true)
                              await axios
                                .post('/mail/forgotpw', {
                                  email: router.query.email,
                                })
                                .then(() => {
                                  toast.success(t('forget-pw-confirm-email'), {
                                    position: 'bottom-right',
                                  })
                                  setIsSendingEmail(false)
                                  setDisable(true)
                                  setTimeout(() => {
                                    router.reload()
                                  }, 5000)
                                })
                                .catch(() => {
                                  toast.error(t('418-error'), {
                                    position: 'bottom-right',
                                  })
                                  setTimeout(async () => {
                                    router.push(
                                      `/${router.locale}/forgotpassword`,
                                    )
                                  }, 5000)
                                })
                            }}
                          >
                            {t('resend-code')}
                          </Link>
                        </Box>
                      </Box>
                    )
                  ) : (
                    <></>
                  )}
                </Box>
              </form>
              <ToastContainer />
            </Box>
          </Box>
        </Card>
      </Box>
    </Box>
  )
}

export const getStaticProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
})

export default SendVerificationCode
