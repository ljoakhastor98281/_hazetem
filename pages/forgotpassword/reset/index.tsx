import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Box, Button, TextField, Card } from '@material-ui/core'
import { Fonts } from '@/components/constants/AppEnums'
import { CremaTheme } from '@/types/AppContextPropsType'
import { useTranslation } from 'next-i18next'
import HazedawnWhiteIcon from '@/components/icons/HazedawnWhite'
import axios from 'axios'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { useRouter } from 'next/router'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import CheckCircleOutlineOutlinedIcon from '@material-ui/icons/CheckCircleOutlineOutlined'
import { IconButton, InputAdornment } from '@material-ui/core'
import { Visibility, VisibilityOff } from '@material-ui/icons'

const useStyles = makeStyles((theme: CremaTheme) => ({
  cardRoot: {
    // maxWidth: '36rem',
    maxWidth: '45rem',
    width: '100%',
    overflow: 'hidden',
    boxShadow: '0 10px 15px -3px rgba(0, 0, 0, 0.1)',
    textAlign: 'center',
    position: 'relative',
    paddingTop: 20,
    [theme.breakpoints.up('xl')]: {
      paddingTop: 32,
    },
    '&:before': {
      content: "''",
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      width: 130,
      height: 9,
      borderBottomRightRadius: 80,
      borderBottomLeftRadius: 80,
      marginRight: 'auto',
      marginLeft: 'auto',
      backgroundColor: theme.palette.primary.main,
    },
  },
  textUppercase: {
    textTransform: 'uppercase',
  },
  formRoot: {
    textAlign: 'left',
    [theme.breakpoints.up('xl')]: {
      marginBottom: 24,
    },
  },
  myTextFieldRoot: {
    width: '100%',
  },
  checkboxRoot: {
    marginLeft: -12,
  },
  pointer: {
    cursor: 'pointer',
  },
  btnRoot: {
    // borderRadius: theme.overrides.MuiCard.root.borderRadius,
    // width: '10rem',
    fontWeight: Fonts.REGULAR,
    fontSize: 16,
    textTransform: 'capitalize',
    justifyContent: 'center',
  },
  btnRootFull: {
    width: '100%',
  },
  dividerRoot: {
    marginBottom: 16,
    marginLeft: -48,
    marginRight: -48,
    [theme.breakpoints.up('xl')]: {
      marginBottom: 32,
    },
  },
  textPrimary: {
    color: theme.palette.text.primary,
  },
  colorTextPrimary: {
    color: theme.palette.primary.main,
  },
  underlineNone: {
    textDecoration: 'none',
  },
  textGrey: {
    color: theme.palette.grey[500],
  },
  back: {
    position: 'absolute',
    left: 10,
  },
  text: {
    color: '#aaa',
  },
  value: {
    fontSize: '40px',
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    display: 'flex',
  },
}))

const ResetPassword = () => {
  const { t } = useTranslation()
  const classes = useStyles({})
  const router = useRouter()
  const [formData, setFormData] = useState({
    password: '',
    password2: '',
  })
  const [readyToSend, setReadyToSend] = useState(false)
  // const [isSubmitting, setIsSubmitting] = useState(false)

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    // TO-DO
    if (formData.password != formData.password2) {
      toast.error(t('re-enter-password-message'), {
        position: 'bottom-right',
      })
    } else {
      await axios
        .put('/user/forgotpw', { new_password: formData.password })
        .then(() => {
          toast.success(t('password-success'), {
            position: 'bottom-right',
          })
          setTimeout(() => {
            router.push(`/${router.locale}/signin`)
          }, 5000)
        })
        .catch((err) => {
          switch (err.response.status) {
            case 410: {
              toast.error(t('expired-verification-token'), {
                position: 'bottom-right',
              })
              router.push(`/${router.locale}/forgotpassword`)
              break
            }
            case 404: {
              toast.error(t('verification-error'), {
                position: 'bottom-right',
              })
              break
            }
            case 500: {
              toast.error(t('error-without-status'), {
                position: 'bottom-right',
              })
              break
            }
            default: {
              break
            }
          }
        })
    }
  }

  const [showPassword, setShowPassword] = useState(false)
  const [showPassword2, setShowPassword2] = useState(false)

  const handleClick = () => {
    setShowPassword((prev) => !prev)
  }
  const handleClick2 = () => {
    setShowPassword2((prev) => !prev)
  }

  return (
    <Box
      flex={1}
      display="flex"
      height="100vh"
      position="relative"
      bgcolor="#f3f4f6"
      flexDirection="column"
      justifyContent="center"
    >
      <Box mb={{ xs: 6, md: 8, xl: 18 }} textAlign="center">
        <HazedawnWhiteIcon size="140" />
      </Box>

      <Box
        display="flex"
        flexDirection="column"
        justifyContent="center"
        alignItems="center"
      >
        <Card className={classes.cardRoot}>
          <Box px={{ xs: 6, sm: 10, xl: 15 }}>
            <Button
              startIcon={<ArrowBackIosIcon />}
              href={`/${router.locale}/forgotpassword`}
              className={classes.back}
            >
              {t('back')}
            </Button>
            <br /> <br />
            <Box
              component="h2"
              mb={{ xs: 3, xl: 6 }}
              color="text.primary"
              fontWeight={Fonts.REGULAR}
              fontSize={{ xs: 24, xl: 26 }}
            >
              {t('password-reset')}
            </Box>
          </Box>
          {/* 
          <Box px={{ xs: 6, sm: 10, xl: 15 }}>
            <Box
              component="h5"
              mb={{ xs: 3, xl: 6 }}
              color="text.primary"
              fontWeight={Fonts.REGULAR}
              fontSize={{ xs: 18, xl: 18 }}
            >
              {t('Account name:')}
            </Box>
          </Box> */}

          <Box flex={1} display="flex" flexDirection="column">
            <Box
              px={{ xs: 6, sm: 10, xl: 15 }}
              pt={8}
              flex={1}
              display="flex"
              flexDirection="column"
            >
              <form className={classes.formRoot} onSubmit={handleSubmit}>
                <Box mb={{ xs: 5, xl: 8 }}>
                  <TextField
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <IconButton onClick={handleClick} edge="end">
                            {showPassword ? <Visibility /> : <VisibilityOff />}
                          </IconButton>
                        </InputAdornment>
                      ),
                    }}
                    placeholder={
                      t('change-my-password') ?? 'Change my password'
                    }
                    name="password"
                    label={t('change-my-password')}
                    variant="outlined"
                    className={classes.myTextFieldRoot}
                    type={showPassword ? 'text' : 'password'}
                    defaultValue={formData.password}
                    error={
                      formData.password.length < 8 &&
                      formData.password.length > 0
                    }
                    helperText={
                      formData.password.length < 8 ? (
                        t('password-error')
                      ) : (
                        <CheckCircleOutlineOutlinedIcon
                          fontSize="small"
                          color="primary"
                        />
                      )
                    }
                    onChange={(e) =>
                      setFormData({ ...formData, password: e.target.value })
                    }
                  />
                </Box>
                <Box mb={{ xs: 5, xl: 8 }}>
                  <TextField
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <IconButton onClick={handleClick2} edge="end">
                            {showPassword2 ? <Visibility /> : <VisibilityOff />}
                          </IconButton>
                        </InputAdornment>
                      ),
                    }}
                    placeholder={
                      t('re-enter-password') ?? 'Re-enter your new password'
                    }
                    name="password2"
                    label={t('re-enter-password')}
                    variant="outlined"
                    className={classes.myTextFieldRoot}
                    type={showPassword2 ? 'text' : 'password'}
                    error={
                      formData.password !== formData.password2 &&
                      formData.password2.length > 0
                    }
                    helperText={
                      formData.password == formData.password2 &&
                      formData.password2.length > 0 ? (
                        <CheckCircleOutlineOutlinedIcon
                          fontSize="small"
                          color="primary"
                        />
                      ) : (
                        t('re-enter-password-message')
                      )
                    }
                    defaultValue={formData.password2}
                    onChange={(e) =>
                      setFormData({ ...formData, password2: e.target.value })
                    }
                  />
                </Box>
                <Box
                  mb={6}
                  display="flex"
                  flexDirection={{ xs: 'column', sm: 'row' }}
                  alignItems={{ sm: 'center' }}
                  justifyContent={{ sm: 'space-between' }}
                >
                  <Button
                    variant="contained"
                    color="secondary"
                    type="submit"
                    fullWidth
                    disabled={readyToSend}
                    className={classes.btnRoot}
                  >
                    {t('change-password')}
                  </Button>
                </Box>
              </form>
              <ToastContainer />
            </Box>
          </Box>
        </Card>
      </Box>
    </Box>
  )
}

export const getStaticProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
})

export default ResetPassword
