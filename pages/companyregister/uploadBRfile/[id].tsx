import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Box, Button, Card } from '@material-ui/core'
import { Fonts } from '@/components/constants/AppEnums'
import { CremaTheme } from '@/types/AppContextPropsType'
import { useTranslation } from 'next-i18next'
import HazedawnWhiteIcon from '@/components/icons/HazedawnWhite'
import axios from 'axios'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'
import { DropzoneArea } from 'material-ui-dropzone'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { useRouter } from 'next/router'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

const useStyles = makeStyles((theme: CremaTheme) => ({
  cardRoot: {
    maxWidth: '36rem',
    width: '90%',
    overflow: 'hidden',
    boxShadow: '0 10px 15px -3px rgba(0, 0, 0, 0.1)',
    textAlign: 'center',
    position: 'relative',
    paddingTop: 20,
    [theme.breakpoints.up('xl')]: {
      paddingTop: 32,
    },
    '&:before': {
      content: "''",
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      width: 130,
      height: 9,
      borderBottomRightRadius: 80,
      borderBottomLeftRadius: 80,
      marginRight: 'auto',
      marginLeft: 'auto',
      backgroundColor: theme.palette.primary.main,
    },
  },
  textUppercase: {
    textTransform: 'uppercase',
  },
  formRoot: {
    textAlign: 'left',
    [theme.breakpoints.up('xl')]: {
      marginBottom: 24,
    },
  },
  myTextFieldRoot: {
    width: '49%',
  },
  checkboxRoot: {
    marginLeft: -12,
  },
  pointer: {
    cursor: 'pointer',
  },
  btnRoot: {
    // borderRadius: theme.overrides.MuiCard.root.borderRadius,
    width: '10rem',
    fontWeight: Fonts.REGULAR,
    fontSize: 16,
    textTransform: 'capitalize',
  },
  btnRootFull: {
    width: '100%',
  },
  dividerRoot: {
    marginBottom: 16,
    marginLeft: -48,
    marginRight: -48,
    [theme.breakpoints.up('xl')]: {
      marginBottom: 32,
    },
  },
  textPrimary: {
    color: theme.palette.text.primary,
  },
  fieldmargin: {
    marginRight: theme.spacing(1),
  },
  colorTextPrimary: {
    color: theme.palette.primary.main,
  },
  underlineNone: {
    textDecoration: 'none',
  },
  textGrey: {
    color: theme.palette.grey[500],
  },
  back: {
    position: 'absolute',
    left: 10,
  },
  uploadbutton: {
    height: '30',
    marginBottom: 30,
    textAlign: 'left',
  },
}))

const UploadBRfile = () => {
  const { t } = useTranslation()
  const classes = useStyles({})
  const router = useRouter()

  const showError = () =>
    toast.error(t('upload-fail'), {
      position: 'bottom-right',
    })
  const showSuccess = () =>
    toast.success(t('upload-success'), {
      position: 'bottom-right',
    })
  const [state, setState] = useState({
    files: [],
  })

  const handleChange = async (files: any) => {
    setState({
      files: files,
    })
  }
  const handleSave = async () => {
    let sus = state.files.length !== 0 ? true : false
    state.files.forEach(async (file: File) => {
      const formData = new FormData()
      formData.append('user_id', router.query.id as string)
      formData.append('uploaded', 'false')
      // console.log(
      //   new File(
      //     [file],
      //     `BR.${
      //       file.type == 'image/png'
      //         ? 'png'
      //         : file.type == 'image/jpeg'
      //         ? 'jpg'
      //         : 'pdf'
      //     }`,
      //     { type: file.type },
      //   ),
      // )
      formData.append(
        'file',
        new File(
          [file],
          `BR.${
            file.type == 'image/png'
              ? 'png'
              : file.type == 'image/jpeg'
              ? 'jpg'
              : 'pdf'
          }`,
          { type: file.type },
        ),
      )
      const config = {
        headers: {
          'content-type': 'multpart/form-data',
        },
      }
      const res = await axios.post('/user/upload', formData, config)
      if (res.data.msg !== 'sus' || res.status !== 200) {
        sus = false
      }
    })

    if (sus) {
      showSuccess()
      await new Promise((r) => setTimeout(r, 3000))
      router.push(`/signin`)
      return
    } else {
      showError()
    }
  }

  return (
    <Box
      flex={1}
      display="flex"
      position="relative"
      bgcolor="#f3f4f6"
      flexDirection="column"
      justifyContent="center"
    >
      <Box mb={{ xs: 6, md: 8, xl: 18 }} textAlign="center">
        <HazedawnWhiteIcon size="140" />
      </Box>
      <Box
        display="flex"
        flexDirection="column"
        justifyContent="center"
        alignItems="center"
      >
        <Card className={classes.cardRoot}>
          <Box flex={1} display="flex" flexDirection="column">
            <Box
              component="h2"
              mb={{ xs: 3, xl: 6 }}
              color="text.primary"
              fontWeight={Fonts.REGULAR}
              fontSize={{ xs: 24, xl: 26 }}
            >
              <Button
                startIcon={<ArrowBackIosIcon />}
                href={`/${router.locale}/companyregister`}
                className={classes.back}
              >
                {t('back')}
              </Button>
            </Box>

            <Box
              px={{ xs: 6, sm: 10, xl: 15 }}
              pt={8}
              flex={1}
              display="flex"
              flexDirection="column"
            >
              <form className={classes.formRoot}>
                <Box className={classes.uploadbutton}>
                  <DropzoneArea
                    acceptedFiles={[
                      'application/pdf',
                      'image/png',
                      'image/jpeg',
                    ]}
                    onChange={handleChange}
                    showFileNames
                    dropzoneText={t('upload-br')}
                    // showAlerts={false}
                    filesLimit={1}
                    maxFileSize={5242880}
                    // showPreviewsInDropzone={false}
                    // showPreviews
                  />
                </Box>
                <Box
                  mb={6}
                  display="flex"
                  flexDirection={{ xs: 'column', sm: 'row' }}
                  alignItems={{ sm: 'center' }}
                  justifyContent={{ sm: 'space-between' }}
                >
                  <Button
                    variant="contained"
                    color="secondary"
                    // type="submit"
                    fullWidth
                    // className={classes.btnRoot}
                    onClick={handleSave}
                  >
                    {t('upload')}
                  </Button>
                </Box>
              </form>
              <ToastContainer />
            </Box>
          </Box>
        </Card>
      </Box>
    </Box>
  )
}

export const getStaticProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
})
export const getStaticPaths = async () => {
  return {
    paths: [],
    fallback: 'blocking',
  }
}

export default UploadBRfile
