import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import {
  Typography,
  Box,
  Button,
  TextField,
  IconButton,
  Card,
} from '@material-ui/core'
import { Fonts } from '@/components/constants/AppEnums'
import { CremaTheme } from '@/types/AppContextPropsType'
import { useTranslation } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import HazedawnWhiteIcon from '@/components/icons/HazedawnWhite'
import axios from 'axios'
import clsx from 'clsx'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'
import InputAdornment from '@material-ui/core/InputAdornment'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { useRouter } from 'next/router'
import CheckCircleOutlineOutlinedIcon from '@material-ui/icons/CheckCircleOutlineOutlined'
import { Visibility, VisibilityOff } from '@material-ui/icons'

const useStyles = makeStyles((theme: CremaTheme) => ({
  cardRoot: {
    maxWidth: '900px',
    width: '90%',
    overflow: 'hidden',
    boxShadow: '0 10px 15px -3px rgba(0, 0, 0, 0.1)',
    textAlign: 'center',
    position: 'relative',
    paddingTop: 20,
    [theme.breakpoints.up('xl')]: {
      paddingTop: 32,
    },
    '&:before': {
      content: "''",
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      width: 130,
      height: 9,
      borderBottomRightRadius: 80,
      borderBottomLeftRadius: 80,
      marginRight: 'auto',
      marginLeft: 'auto',
      backgroundColor: theme.palette.primary.main,
    },
  },
  textUppercase: {
    textTransform: 'uppercase',
  },
  formRoot: {
    textAlign: 'left',
    [theme.breakpoints.up('xl')]: {
      marginBottom: 24,
    },
  },
  myTextFieldRoot: {
    width: '48%',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  checkboxRoot: {
    marginLeft: -12,
  },
  pointer: {
    cursor: 'pointer',
  },
  btnRoot: {
    // borderRadius: theme.overrides.MuiCard.root.borderRadius,
    width: '10rem',
    fontWeight: Fonts.REGULAR,
    fontSize: 16,
    textTransform: 'capitalize',
  },
  btnRootFull: {
    width: '100%',
  },
  dividerRoot: {
    marginBottom: 16,
    marginLeft: -48,
    marginRight: -48,
    [theme.breakpoints.up('xl')]: {
      marginBottom: 32,
    },
  },
  textPrimary: {
    color: theme.palette.text.primary,
  },
  fieldmargin: {
    marginRight: theme.spacing(3),
  },
  colorTextPrimary: {
    color: theme.palette.primary.main,
  },
  underlineNone: {
    textDecoration: 'none',
  },
  textGrey: {
    color: theme.palette.grey[500],
  },
  back: {
    position: 'absolute',
    left: 10,
  },
  textfieldLabel: {
    width: '100%',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
  },
}))

const AdminRegister = () => {
  const { t } = useTranslation()
  const classes = useStyles({})
  const showError = () =>
    toast.error(t('register-fail'), {
      position: 'bottom-right',
    })
  const show409userError = () =>
    toast.error(t('register-409-fail'), {
      position: 'bottom-right',
    })
  const show409companyError = () =>
    toast.error(t('register-409-company-fail'), {
      position: 'bottom-right',
    })
  const showEmailError = () =>
    toast.error(t('register-400-email-fail'), {
      position: 'bottom-right',
    })
  const showSuccess = () =>
    toast.success(t('register-success'), {
      position: 'bottom-right',
    })
  const router = useRouter()
  const [showPassword, setShowPassword] = useState(false)
  const [showPassword2, setShowPassword2] = useState(false)
  // const numericOnly(event:any): boolean => { // restrict e,+,-,E characters in  input type number
  //   debugger
  //   const charCode = (event.which) ? event.which : event.keyCode;
  //   if (charCode == 101 || charCode == 69 || charCode == 45 || charCode == 43) {
  //     return false;
  //   }
  //   return true;

  // }
  // const numericOnly = (event: any) => {
  //   // noinspection JSDeprecatedSymbols
  //   const charCode = event.which ? event.which : event.key || event.keyCode // keyCode is deprecated but needed for some browsers
  //   return !(
  //     charCode === 101 ||
  //     charCode === 69 ||
  //     charCode === 45 ||
  //     charCode === 43
  //   )
  // }
  const [formData, setFormData] = useState({
    username: '',
    password: '',
    company_name: '',
    business_outline: '',
    status: '',
    date_of_agreement_signed: '',
    sales_lead: '',
    total_amount: '',
    contact_person: '',
    contact_email: '',
    client_contact_number: '',
    number_of_staff: '',
    company_start_year: '',
    company_outline: '',
    company_address: '',
    tvp_account_name: '',
    tvp_password: '',
    remark: '',
    project_name: '',
    BR_expiry_date: '',
  })

  const handleClick = () => {
    setShowPassword((prev) => !prev)
  }
  const handleClick2 = () => {
    setShowPassword2((prev) => !prev)
  }
  const handleSubmit = async (e: { preventDefault: () => void }) => {
    e.preventDefault()
    const company = {
      company_name: formData.company_name,
      business_outline: formData.business_outline,
      status: formData.status,
      date_of_agreement_signed: formData.date_of_agreement_signed,
      sales_lead: formData.sales_lead,
      total_amount: formData.total_amount,
      contact_person: formData.contact_person,
      contact_email: formData.contact_email,
      client_contact_number: formData.client_contact_number,
      number_of_staff: formData.number_of_staff,
      company_start_year: formData.company_start_year,
      company_outline: formData.company_outline,
      company_address: formData.company_address,
      tvp_account_name: formData.tvp_account_name,
      tvp_password: formData.tvp_password,
      remark: formData.remark,
      project_name: formData.project_name,
      BR_expiry_date: formData.BR_expiry_date,
    }
    await axios
      .post('/company', company)
      .then(async (res) => {
        const user = {
          username: formData.username,
          email: formData.contact_email,
          password: formData.password,
          company_id: res.data.company._id,
        }
        await axios
          .post('/user/signup/admin', user)
          .then(async (res) => {
            showSuccess()
            await new Promise((r) => setTimeout(r, 3000))
            router.push(`/companyregister/uploadBRfile/${res?.data?.user._id}`)
          })
          .catch((err) => {
            switch (err.response.status) {
              case 409:
                show409userError()
                break
              default:
                showError()
                break
            }
          })
      })
      .catch((err) => {
        switch (err.response.status) {
          case 400:
            showEmailError()
            break
          case 409:
            show409companyError()
            break
          default:
            showError()
            break
        }
      })
  }

  return (
    <Box
      flex={1}
      display="flex"
      position="relative"
      bgcolor="#f3f4f6"
      flexDirection="column"
      justifyContent="center"
    >
      <Box mb={{ xs: 6, md: 8, xl: 18 }} textAlign="center">
        <HazedawnWhiteIcon size="140" />
      </Box>
      <Box
        display="flex"
        flexDirection="column"
        justifyContent="center"
        alignItems="center"
      >
        <Card className={classes.cardRoot}>
          <Box flex={1} display="flex" flexDirection="column">
            <Box
              component="h2"
              mb={{ xs: 3, xl: 6 }}
              color="text.primary"
              fontWeight={Fonts.REGULAR}
              fontSize={{ xs: 24, xl: 26 }}
            >
              <Button
                startIcon={<ArrowBackIosIcon />}
                href={`/${router.locale}/signin`}
                className={classes.back}
              >
                {t('back')}
              </Button>
            </Box>
            <Typography
              style={{ marginTop: 50 }}
              component="h1"
              variant="h4"
              gutterBottom
            >
              {t('comp-admin-reg')}
            </Typography>

            <Box
              px={{ xs: 6, sm: 10, xl: 15 }}
              pt={8}
              flex={1}
              display="flex"
              flexDirection="column"
            >
              <form className={classes.formRoot} onSubmit={handleSubmit}>
                {/* <input name="csrfToken" type="hidden" /> */}
                <TextField
                  margin="normal"
                  name="username"
                  required
                  fullWidth
                  id="username"
                  label={t('username')}
                  type="text"
                  error={
                    formData.username.length < 8 && formData.username.length > 0
                  }
                  helperText={
                    formData.username.length < 8 ? (
                      t('username-error')
                    ) : (
                      <CheckCircleOutlineOutlinedIcon
                        fontSize="small"
                        color="primary"
                      />
                    )
                  }
                  defaultValue={formData.username}
                  onChange={(e) =>
                    setFormData({ ...formData, username: e.target.value })
                  }
                />
                <TextField
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton onClick={handleClick} edge="end">
                          {showPassword ? <Visibility /> : <VisibilityOff />}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                  margin="normal"
                  type={showPassword ? 'text' : 'password'}
                  required
                  fullWidth
                  id="password"
                  label={t('password')}
                  name="password"
                  defaultValue={formData.password}
                  error={
                    formData.password.length < 8 && formData.password.length > 0
                  }
                  helperText={
                    formData.password.length < 8 ? (
                      t('password-error')
                    ) : (
                      <CheckCircleOutlineOutlinedIcon
                        fontSize="small"
                        color="primary"
                      />
                    )
                  }
                  onChange={(e) =>
                    setFormData({ ...formData, password: e.target.value })
                  }
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="company_name"
                  label={t('company_name')}
                  name="company_name"
                  autoComplete="company_name"
                  autoFocus
                  defaultValue={formData.company_name}
                  onChange={(e) =>
                    setFormData({ ...formData, company_name: e.target.value })
                  }
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="project_name"
                  label={t('project_name')}
                  name="project_name"
                  autoComplete="project_name"
                  defaultValue={formData.project_name}
                  onChange={(e) =>
                    setFormData({ ...formData, project_name: e.target.value })
                  }
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="business_outline"
                  label={t('business_outline')}
                  name="Business Outline"
                  autoComplete="business_outline"
                  defaultValue={formData.business_outline}
                  onChange={(e) =>
                    setFormData({
                      ...formData,
                      business_outline: e.target.value,
                    })
                  }
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="status"
                  label={t('status')}
                  name="status"
                  autoComplete="status"
                  className={clsx(classes.myTextFieldRoot, classes.fieldmargin)}
                  defaultValue={formData.status}
                  onChange={(e) =>
                    setFormData({ ...formData, status: e.target.value })
                  }
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  name="date_of_agreement_signed"
                  label={t('date_of_agreement_signed')}
                  type="date"
                  id="date_of_agreement_signed"
                  className={classes.myTextFieldRoot}
                  autoComplete="date_of_agreement_signed"
                  InputLabelProps={{
                    shrink: true,
                    className: classes.textfieldLabel,
                  }}
                  defaultValue={formData.date_of_agreement_signed}
                  onChange={(e) =>
                    setFormData({
                      ...formData,
                      date_of_agreement_signed: e.target.value,
                    })
                  }
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="sales_lead"
                  label={t('sales_lead')}
                  name="sales_lead"
                  autoComplete="sales_lead"
                  className={clsx(classes.myTextFieldRoot, classes.fieldmargin)}
                  defaultValue={formData.sales_lead}
                  onChange={(e) =>
                    setFormData({ ...formData, sales_lead: e.target.value })
                  }
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="total_amount"
                  label={t('total_amount')}
                  name="total_amount"
                  type="number"
                  onKeyDown={(evt) =>
                    ['e', 'E', '+', '-'].includes(evt.key) &&
                    evt.preventDefault()
                  }
                  autoComplete="total_amount"
                  className={classes.myTextFieldRoot}
                  defaultValue={formData.total_amount}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">$</InputAdornment>
                    ),
                  }}
                  onChange={(e) =>
                    setFormData({ ...formData, total_amount: e.target.value })
                  }
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="contact_person"
                  label={t('contact_person')}
                  name="contact_person"
                  autoComplete="contact_person"
                  className={clsx(classes.myTextFieldRoot, classes.fieldmargin)}
                  defaultValue={formData.contact_person}
                  onChange={(e) =>
                    setFormData({ ...formData, contact_person: e.target.value })
                  }
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="client_contact_number"
                  label={t('client_contact_number')}
                  name="client_contact_number"
                  type="number"
                  onKeyDown={(evt) =>
                    ['e', 'E', '+', '-'].includes(evt.key) &&
                    evt.preventDefault()
                  }
                  autoComplete="client_contact_number"
                  className={classes.myTextFieldRoot}
                  defaultValue={formData.client_contact_number}
                  InputLabelProps={{
                    className: classes.textfieldLabel,
                  }}
                  onChange={(e) =>
                    setFormData({
                      ...formData,
                      client_contact_number: e.target.value,
                    })
                  }
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="contact_email"
                  type="email"
                  label={t('contact_email')}
                  name="contact_email"
                  autoComplete="contact_email"
                  error={
                    formData.contact_email.length > 0 &&
                    !/\S+@\S+\.\S+/.test(formData.contact_email)
                  }
                  helperText={
                    !/\S+@\S+\.\S+/.test(formData.contact_email) ? (
                      t('email-error')
                    ) : (
                      <CheckCircleOutlineOutlinedIcon
                        color="primary"
                        fontSize="small"
                      />
                    )
                  }
                  defaultValue={formData.contact_email}
                  onChange={(e) =>
                    setFormData({ ...formData, contact_email: e.target.value })
                  }
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="number_of_staff"
                  label={t('number_of_staff')}
                  name="number_of_staff"
                  autoComplete="number_of_staff"
                  type="number"
                  onKeyDown={(evt) =>
                    ['e', 'E', '+', '-'].includes(evt.key) &&
                    evt.preventDefault()
                  }
                  className={clsx(classes.myTextFieldRoot, classes.fieldmargin)}
                  defaultValue={formData.number_of_staff}
                  onChange={(e) =>
                    setFormData({
                      ...formData,
                      number_of_staff: e.target.value,
                    })
                  }
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="company_start_year"
                  label={t('company_start_year')}
                  name="company_start_year"
                  type="number"
                  onKeyDown={(evt) =>
                    ['e', 'E', '+', '-'].includes(evt.key) &&
                    evt.preventDefault()
                  }
                  className={classes.myTextFieldRoot}
                  autoComplete="company_start_year"
                  defaultValue={formData.company_start_year}
                  onChange={(e) =>
                    setFormData({
                      ...formData,
                      company_start_year: e.target.value,
                    })
                  }
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="company_outline"
                  label={t('company_outline')}
                  name="company_outline"
                  autoComplete="company_outline"
                  defaultValue={formData.company_outline}
                  onChange={(e) =>
                    setFormData({
                      ...formData,
                      company_outline: e.target.value,
                    })
                  }
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  type="date"
                  id="BR_expiry_date"
                  label={t('BR_expiry_date')}
                  name="BR_expiry_date"
                  autoComplete="BR_expiry_date"
                  defaultValue={formData.BR_expiry_date}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  onChange={(e) =>
                    setFormData({ ...formData, BR_expiry_date: e.target.value })
                  }
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="company_address"
                  label={t('company_address')}
                  name="company_address"
                  autoComplete="company_address"
                  defaultValue={formData.company_address}
                  onChange={(e) =>
                    setFormData({
                      ...formData,
                      company_address: e.target.value,
                    })
                  }
                />
                <TextField
                  margin="normal"
                  fullWidth
                  id="tvp_account_name"
                  label={t('tvp_account_name')}
                  name="tvp_account_name"
                  autoComplete="tvp_account_name"
                  defaultValue={formData.tvp_account_name}
                  helperText={t('tvp-acname-remark')}
                  onChange={(e) =>
                    setFormData({
                      ...formData,
                      tvp_account_name: e.target.value,
                    })
                  }
                />
                <TextField
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton onClick={handleClick2} edge="end">
                          {showPassword2 ? <Visibility /> : <VisibilityOff />}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                  margin="normal"
                  fullWidth
                  id="tvp_password"
                  label={t('tvp_password')}
                  name="tvp_password"
                  type={showPassword2 ? 'text' : 'password'}
                  autoComplete="tvp_password"
                  helperText={t('tvp-password-remark')}
                  defaultValue={formData.tvp_password}
                  onChange={(e) =>
                    setFormData({ ...formData, tvp_password: e.target.value })
                  }
                />
                <TextField
                  margin="normal"
                  fullWidth
                  id="remark"
                  label={t('remark')}
                  name="remark"
                  autoComplete="remark"
                  helperText={t('remark-text')}
                  defaultValue={formData.remark}
                  onChange={(e) =>
                    setFormData({ ...formData, remark: e.target.value })
                  }
                />
                <Box
                  mb={{ xs: 3, xl: 4 }}
                  display="flex"
                  flexDirection={{ xs: 'row', sm: 'row-reverse' }}
                  alignItems={{ sm: 'center' }}
                  justifyContent={{ sm: 'space-between' }}
                  fontSize={15}
                />
                <Box
                  mb={6}
                  display="flex"
                  flexDirection={{ xs: 'column', sm: 'row' }}
                  alignItems={{ sm: 'center' }}
                  justifyContent={{ sm: 'space-between' }}
                >
                  <Button
                    variant="contained"
                    color="secondary"
                    type="submit"
                    fullWidth
                    // className={classes.btnRoot}
                    // onClick={handleSubmit}
                  >
                    {t('register')}
                  </Button>
                  {/* <Button
                    variant="contained"
                    color="secondary"
                    type="submit"
                    className={classes.btnRoot}
                    href="/signin"
                  >
                    {t('back')}
                  </Button> */}
                </Box>
              </form>
              <ToastContainer />
            </Box>
          </Box>
        </Card>
      </Box>
    </Box>
  )
}

export const getStaticProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
})

export default AdminRegister
