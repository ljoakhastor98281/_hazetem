import BRExpiryDateTable from '@/components/elements/BRExpiryDateTable'
import React from 'react'
import { useSession } from 'next-auth/client'
import { ICompany } from '@/server/api/Company/company.model'
import axios from 'axios'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { useTranslation } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import Layout from '@/components/layouts/navLayout'
import { IBRCompanyTableRow } from '@components/interfaces/interfaces'

export default function BRExpiryList() {
  const [renderedData, setRenderedData] = React.useState<IBRCompanyTableRow[]>(
    [],
  )
  const [session, loading] = useSession()
  const { t } = useTranslation()

  React.useEffect(() => {
    async function fetchData() {
      if (!session) {
        return
      } else {
        await axios
          .get('/company', {
            params: {
              br_expiry: true,
            },
          })
          .then((res) => {
            const renderlist: IBRCompanyTableRow[] = []
            res.data.companys.map((company: ICompany) => {
              renderlist.push({
                id: company._id.toString(),
                company_name: company.company_name,
                contact_person: company.contact_person,
                contact_email: company.contact_email,
                client_contact_number: company.client_contact_number,
                BR_expiry_date: company.BR_expiry_date,
              })
            })
            setRenderedData(renderlist)
          })
          .catch((err) => {
            switch (err.response.status) {
              case 403: {
                toast.error(t('not-auth-msg'), {
                  position: 'bottom-right',
                })
                break
              }
              case 500: {
                toast.error(t('error-without-status'), {
                  position: 'bottom-right',
                })
                break
              }
              default:
                break
            }
          })
      }
    }
    fetchData()
  }, [session, t])

  return (
    <Layout>
      {!session && !loading && (
        <meta httpEquiv="refresh" content="0; /signin" />
      )}
      {session && session?.level > 1 && (
        <meta httpEquiv="refresh" content="0; /levelerror" />
      )}
      {session && session.level < 2 && (
        <BRExpiryDateTable
          renderedData={renderedData}
          pageSizeOptions={[5, 10, 15, 20, 25, 30]}
        />
      )}
      <ToastContainer />
    </Layout>
  )
}

export const getStaticProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
})
