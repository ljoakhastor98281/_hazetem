import React from 'react'
import Layout from '../components/layouts/navLayout'
import ErrorIcon from '@material-ui/icons/Error'
import { useTranslation } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { Box, Button } from '@material-ui/core'
import { useRouter } from 'next/router'
// import { style } from '@material-ui/system'

export default function Custom404() {
  const { t } = useTranslation()
  const router = useRouter()
  // return <Layout><h1>404 - Page Not Found</h1></Layout>
  return (
    <Layout>
      <Box
        display="flex"
        flexDirection="column"
        textAlign="center"
        justifyContent="center"
        alignItems="center"
        style={{ margin: 100 }}
      >
        <ErrorIcon style={{ fontSize: 120, color: '#b46f6a', margin: 10 }} />
        <h1
          style={{
            fontSize: 80,
            color: '#b46f6a',
            margin: 10,
            // fontFamily: 'Cabin Sketch, cursive',
          }}
        >
          {t('404-error')}
        </h1>
        <h5
          style={{
            fontSize: 30,
            color: '#b46f6a',
            margin: 10,
            // fontFamily: 'Cabin Sketch, cursive',
          }}
        >
          {t('404-subtitle')}
        </h5>

        <Button
          href={`/${router.locale}`}
          variant="outlined"
          style={{
            color: '#b46f6a',
            margin: 30,
            border: '1',
            borderRadius: '8',
            borderColor: '#b46f6a',
          }}
        >
          {t('go-back-home')}
        </Button>
      </Box>
    </Layout>
  )
}

export const getStaticProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
})
