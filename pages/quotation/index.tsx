import React, { useState } from 'react'
import clsx from 'clsx'
import {
  DataGrid,
  GridColDef,
  GridPageChangeParams,
  GridRowId,
  GridSelectionModelChangeParams,
  GridToolbarContainer,
  GridToolbarColumnsButton,
  GridToolbarFilterButton,
  GridToolbarExport,
  GridToolbarDensitySelector,
  GridValueGetterParams,
  GridValueFormatterParams,
  GridCellParams,
} from '@material-ui/data-grid'
import {
  Toolbar,
  Typography,
  AppBar,
  Tab,
  Tabs,
  Box,
  IconButton,
} from '@material-ui/core'
import SwipeableViews from 'react-swipeable-views'

import Button from '@material-ui/core/Button'
import ArchiveIcon from '@material-ui/icons/Archive'
import UnarchiveIcon from '@material-ui/icons/Unarchive'
import EditIcon from '@material-ui/icons/Edit'
import AddCircleIcon from '@material-ui/icons/AddCircle'
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf'
import LinkIcon from '@material-ui/icons/Link'
import {
  createStyles,
  makeStyles,
  useTheme,
  withStyles,
} from '@material-ui/core/styles'
import { Theme } from '@material-ui/core/styles'
import Layout from '../../components/layouts/navLayout'
import axios from 'axios'
import { IInvoice } from '../../server/api/Invoice/invoice.model'
import router from 'next/router'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import DehazeIcon from '@material-ui/icons/Dehaze'
import { useSession } from 'next-auth/client'
import { useEffect } from 'react'
import { useTranslation } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { Session } from 'next-auth'
import { webInfo } from 'server/config'
import Tooltip from '@material-ui/core/Tooltip'
import { fade } from '@material-ui/core/styles/colorManipulator'
import { IServiceItem } from '@/server/api/Service/service.model'

const useToolbarStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(1),
    },
    highlight:
      theme.palette.type === 'light'
        ? {
            color: '#004369',
            backgroundColor: '#d8e9ef',
            margin: theme.spacing(1),
          }
        : {
            color: theme.palette.text.primary,
            backgroundColor: theme.palette.secondary.dark,
          },
    title: {
      flex: '1 1 100%',
      alignItems: 'center',
      justifyContent: 'center',
      textAlign: 'center',
      fontSize: 50,
      [theme.breakpoints.down('xs')]: {
        fontSize: 40,
      },
      fontFamily: 'Sintony, sans-serif',
    },
    subtitle: {
      flex: '1 1 100%',
      fontSize: 20,
      [theme.breakpoints.down('sm')]: {
        fontSize: 15,
      },
    },
    button: {
      margin: theme.spacing(1),
      maxWidth: 200,
      width: '100%',
      backgroundColor: '#4ea1d3',
      color: 'white',
    },
    button2: {
      margin: theme.spacing(1),
      maxWidth: 200,
      width: '100%',
      backgroundColor: '#e85a71',
      color: 'white',
    },
    addButton: {
      margin: theme.spacing(1),
      maxWidth: 250,
      backgroundColor: '#4ea1d3',
      color: 'white',
      fontSize: 40,
    },
    input: {
      marginLeft: theme.spacing(1),
      flex: 1,
    },
    iconButton: {
      padding: 10,
    },
    divider: {
      height: 28,
      margin: 4,
    },
    sectionDesktop: {
      display: 'none',
      [theme.breakpoints.up('lg')]: {
        display: 'flex',
      },
    },
    sectionMobile: {
      display: 'flex',
      [theme.breakpoints.up('lg')]: {
        display: 'none',
      },
    },
    tooltip: {
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
    },
  }),
)

// const StyledDataGrid = withStyles({
//   root: {
//     '& .MuiDataGrid-window': {
//       overflowY: 'visible !important',
//     },
//     '& .MuiDataGrid-columnsHeader': {
//       alignItems: 'center',
//     },
//     '& .MuiDataGrid-row': {
//       maxHeight: 'none !important',
//     },
//     '& .MuiDataGrid-viewport': {
//       maxHeight: 'max-content !important',
//     },
//     '& .MuiDataGrid-cell': {
//       lineHeight: 'unset !important',
//       verticalAlign: 'middle',
//       position: 'relative',
//       display: 'flex !important',
//       alignItems: 'center',
//       maxHeight: 'none !important',
//       whiteSpace: 'normal !important',
//       overflowWrap: 'break-word',
//     },
//   },
// })(DataGrid)

interface TabPanelProps {
  children?: React.ReactNode
  dir?: string
  index: number
  value: number
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3} height={700}>
          {children}
        </Box>
      )}
    </div>
  )
}

interface EnhancedTableToolbarProps {
  numSelected: number
  session: Session
  showUnArchive: number
  onEditclick: () => void
  onRestoreClick: (e: React.MouseEvent<unknown>) => void
  onFilterClick: (e: React.MouseEvent<unknown>) => void
  onViewPdfClick: (event: React.MouseEvent<HTMLElement>) => void
  onCopyPdfLinkClick: (event: React.MouseEvent<HTMLElement>) => void
  onArchiveClick: () => void
  onUnArchiveClick: () => void
  textFieldData: { filteredData: string }
  setTextFieldData: React.Dispatch<
    React.SetStateAction<{
      filteredData: string
    }>
  >
}

const EnhancedTableToolbar = (props: EnhancedTableToolbarProps) => {
  const classes = useToolbarStyles()
  const { t } = useTranslation()
  const { numSelected } = props
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  return (
    <>
      <Toolbar className={clsx(classes.root)}>
        <Typography className={classes.title} id="tableTitle" component="div">
          {t('company-quotation')}
        </Typography>
      </Toolbar>
      {numSelected > 0 ? (
        <>
          <Toolbar
            className={clsx(classes.root, classes.sectionDesktop, {
              [classes.highlight]: numSelected > 0,
            })}
          >
            <Typography
              className={classes.subtitle}
              color="inherit"
              variant="subtitle1"
              component="div"
            >
              {t('selected')} {numSelected}
            </Typography>
            {props.session.level <= 1 ? (
              <Button
                variant="contained"
                aria-label="edit"
                color="primary"
                disabled={numSelected == 1 ? false : true}
                className={classes.button}
                onClick={props.onEditclick}
                startIcon={<EditIcon />}
              >
                {t('edit')}
              </Button>
            ) : (
              <></>
            )}
            <Button
              variant="contained"
              aria-label="view-pdf"
              color="primary"
              className={classes.button}
              onClick={props.onViewPdfClick}
              startIcon={<PictureAsPdfIcon />}
            >
              {t('view-pdf')}
            </Button>
            <Button
              variant="contained"
              aria-label="copy-pdf"
              color="primary"
              disabled={numSelected == 1 ? false : true}
              className={classes.button}
              onClick={props.onCopyPdfLinkClick}
              startIcon={<LinkIcon />}
            >
              {t('copy-pdf-link')}
            </Button>
            {props.showUnArchive == 0 ? (
              <Button
                variant="contained"
                aria-label="archive"
                color="secondary"
                className={classes.button2}
                onClick={props.onArchiveClick}
                startIcon={<ArchiveIcon />}
              >
                {t('archive')}
              </Button>
            ) : (
              <Button
                variant="contained"
                aria-label="unarchive"
                color="secondary"
                className={classes.button2}
                onClick={props.onUnArchiveClick}
                startIcon={<UnarchiveIcon />}
              >
                {t('unarchive')}
              </Button>
            )}
          </Toolbar>
          <Toolbar
            className={clsx(classes.root, classes.sectionMobile, {
              [classes.highlight]: numSelected > 0,
            })}
          >
            <Typography
              className={classes.subtitle}
              color="inherit"
              variant="subtitle1"
              component="div"
            >
              {t('selected')} {numSelected}
            </Typography>
            <IconButton
              aria-controls="action-menu"
              aria-haspopup="true"
              onClick={handleClick}
              color="primary"
            >
              <MoreVertIcon />
            </IconButton>
            <Menu
              id="action-menu"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleClose}
              getContentAnchorEl={null}
              anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
              transformOrigin={{ vertical: 'top', horizontal: 'center' }}
            >
              <MenuItem
                onClick={props.onEditclick}
                disabled={numSelected == 1 ? false : true}
              >
                <ListItemIcon>
                  <EditIcon fontSize="small" />
                </ListItemIcon>
                <ListItemText primary={t('edit')} />
              </MenuItem>
              <MenuItem onClick={props.onViewPdfClick}>
                <ListItemIcon>
                  <PictureAsPdfIcon fontSize="small" />
                </ListItemIcon>
                <ListItemText primary={t('view-pdf')} />
              </MenuItem>
              <MenuItem onClick={props.onCopyPdfLinkClick}>
                <ListItemIcon>
                  <LinkIcon fontSize="small" />
                </ListItemIcon>
                <ListItemText primary={t('copy-pdf-link')} />
              </MenuItem>
              {props.showUnArchive == 0 ? (
                <MenuItem onClick={props.onArchiveClick}>
                  <ListItemIcon>
                    <ArchiveIcon fontSize="small" />
                  </ListItemIcon>
                  <ListItemText primary={t('archive')} />
                </MenuItem>
              ) : (
                <MenuItem onClick={props.onUnArchiveClick}>
                  <ListItemIcon>
                    <UnarchiveIcon fontSize="small" />
                  </ListItemIcon>
                  <ListItemText primary={t('unarchive')} />
                </MenuItem>
              )}
            </Menu>
          </Toolbar>
        </>
      ) : (
        <></>
      )}
    </>
  )
}

const CellTooltip = withStyles({
  arrow: {
    color: fade('#282c37', 0.9),
  },
  tooltip: {
    fontSize: '1em',
    color: 'white',
    backgroundColor: fade('#282c37', 0.9),
    whiteSpace: 'pre-line',
  },
})(Tooltip)

// export default function QuotationList({ list }: ITableProps) {
const QuotationList = () => {
  const [session, loading] = useSession()
  const [renderedData, setRenderedData] = useState<IInvoice[]>([])
  const [archivedData, setArchivedData] = React.useState<IInvoice[]>([])
  const [unarchivedData, setUnarchivedData] = React.useState<IInvoice[]>([])
  // let vendorMenuCurParent: VendorMenuParent = VendorMenuParent.SHOW_PDF
  const [showUnArchive, setShowUnArchive] = React.useState(0)

  useEffect(() => {
    async function fetchData() {
      if (!session) {
        return
      }
      const archived: IInvoice[] = []
      const unarchived: IInvoice[] = []
      const res = await axios.get('/invoice', {
        params: {
          withClientInfo: true,
        },
      })
      res.data.invoices.map((invoice: IInvoice) => {
        invoice.id = invoice._id
        invoice.archive_status == 0
          ? unarchived.push(invoice)
          : archived.push(invoice)
      })
      setRenderedData(() => res.data.invoices)
      setArchivedData(archived)
      setUnarchivedData(unarchived)
    }
    fetchData()
  }, [session])

  // const [page, setPage] = React.useState(0)
  const classes = useToolbarStyles()
  const theme = useTheme()
  const { t } = useTranslation()
  const [selected, setSelected] = useState<GridRowId[]>([])
  const [formData, setFormData] = useState({ filteredData: '' })
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
  const [page, setPage] = React.useState(0)
  const [pageSize, setPageSize] = useState(5)

  const columns: GridColDef[] = [
    { field: 'invoice_number', headerName: t('invoice_number'), width: 170 },
    {
      field: 'project_name',
      headerName: t('project_name'),
      width: 250,
      renderCell: (params: GridCellParams) => (
        <CellTooltip title={params.row.project_name} arrow placement="top">
          <div className={classes.tooltip}>{params.row.project_name}</div>
        </CellTooltip>
      ),
    },
    {
      field: 'company_name',
      headerName: t('company_name'),
      width: 250,
      renderCell: (params: GridCellParams) => (
        <CellTooltip
          title={params.row.client?.company_name}
          arrow
          placement="top"
        >
          <div className={classes.tooltip}>
            {params.row.client?.company_name}
          </div>
        </CellTooltip>
      ),
    },
    {
      field: 'from',
      headerName: t('from'),
      width: 150,
      renderCell: (params: GridCellParams) => (
        <CellTooltip title={params.row.from} arrow placement="top">
          <div className={classes.tooltip}>{params.row.from}</div>
        </CellTooltip>
      ),
    },
    {
      field: 'date',
      headerName: t('date'),
      width: 130,
      valueGetter: (params: GridValueGetterParams) => {
        let result = params.row.date
        if (result) result = result.toString().split('T')[0]
        return result
      },
    },
    {
      field: 'quotation_items',
      headerName: t('quotation-item'),
      width: 190,
      renderCell: (params) => (
        <strong>
          <Button
            variant="contained"
            color="primary"
            size="small"
            style={{ marginLeft: 16, backgroundColor: '#4ea1d3' }}
            onClick={() => {
              router.push(`/quotation/item/${params.row._id}`)
            }}
          >
            {
              (params.row.quotation_items as IServiceItem[]).filter(
                (item: IServiceItem) => item.needed,
              ).length
            }
          </Button>
        </strong>
      ),
    },
    {
      field: 'price',
      headerName: t('price'),
      width: 120,
      valueFormatter: (params: GridValueFormatterParams) => {
        if (params.row.price) {
          return `$ ${params.row.price}`
        } else {
          return '-'
        }
      },
    },
    {
      field: 'remark',
      headerName: t('remark'),
      width: 250,
      renderCell: (params: GridCellParams) => (
        <CellTooltip title={params.row.remark} arrow placement="top">
          <div className={classes.tooltip}>{params.row.remark}</div>
        </CellTooltip>
      ),
    },
    {
      field: 'admin_added',
      headerName: t('admin_added'),
      width: 150,
      hide: true,
      valueGetter: (params: GridValueGetterParams) => {
        if (params.row.admin_added) {
          return params.row.admin_added.toString().split('T')[0]
        } else return params.row.admin_added
      },
    },
    {
      field: 'admin_modified',
      headerName: t('admin_modified'),
      width: 160,
      hide: true,
      valueGetter: (params: GridValueGetterParams) => {
        if (params.row.admin_modified) {
          return params.row.admin_modified.toString().split('T')[0]
        } else return params.row.admin_modified
      },
    },
  ]

  const handleChangePage = (param: GridPageChangeParams) => {
    setPage(param.page)
  }

  const handlePageSizeChange = async (param: GridPageChangeParams) => {
    setPageSize(param.pageSize)
  }

  const handleSelectionChange = (params: GridSelectionModelChangeParams) => {
    setSelected(params.selectionModel)
  }

  const updateRenderedData = async () => {
    const archived: IInvoice[] = []
    const unarchived: IInvoice[] = []
    const res = await axios.get('/invoice/', {
      params: {
        withClientInfo: true,
      },
    })
    res.data.invoices.map((invoice: IInvoice) => {
      invoice.id = invoice._id
      invoice.archive_status == 0
        ? unarchived.push(invoice)
        : archived.push(invoice)
    })
    setArchivedData(archived)
    setUnarchivedData(unarchived)
    setRenderedData(res.data.invoices)
    setSelected([])
  }

  const archiveButtonHandler = async () => {
    const decision = window.confirm(t('archive-alert'))
    if (!decision) {
      return
    }
    selected.map(async (item: GridRowId) => {
      await axios.get(`/invoice/archive/${item}`)
      updateRenderedData()
    })
  }

  const unArchiveButtonHandler = async () => {
    const decision = window.confirm(t('unarchive-alert'))
    if (!decision) {
      return
    }
    selected.map(async (item: GridRowId) => {
      await axios.get(`/invoice/unarchive/${item}`)
      updateRenderedData()
    })
  }

  const addButtonHandler = async () => {
    router.push(`/quotation/add`)
  }

  const editButtonHandler = async () => {
    selected.map((item: GridRowId) => {
      router.push(`/quotation/${item}`)
    })
  }
  const filterButtonHandler = async (e: React.MouseEvent<unknown>) => {
    e.preventDefault()
    const { filteredData } = formData
    const returnObj: IInvoice[] = []
    renderedData.map((invoice: IInvoice) => {
      if (invoice.project_name.includes(filteredData)) {
        returnObj.push(invoice)
      }
    })
    setRenderedData(returnObj)
    setSelected([])
  }

  const restoreButtonHandler = async (e: React.MouseEvent<unknown>) => {
    e.preventDefault()
    const returnObj: IInvoice[] = []
    renderedData.map((invoice: IInvoice) => {
      returnObj.push(invoice)
    })
    setRenderedData(returnObj)
    setSelected([])
  }

  // const handleMenuItemClick = (
  //   event: React.MouseEvent<HTMLElement>,
  //   index: number,
  // ) => {
  //   setVendorSelectedIndex(index)
  //   setVendorAnchorEl(null)
  //   selected.map(async (id: GridRowId) => {
  //     if (vendorMenuCurParent === VendorMenuParent.SHOW_PDF)
  //       window.open(`/quotation/pdf/${id}`)
  //     else if (vendorMenuCurParent === VendorMenuParent.COPY_PDF_LINK) {
  //       const uri = await getQuotationPdfUri(id, vendor_options[index])
  //       navigator.clipboard.writeText(uri)
  //       window.alert(t('copy-pdf-link-alert'))
  //     }
  //   })
  // }

  const viewPdfButtonHandler = async () => {
    if (selected.length > 5) {
      window.alert(
        'You can at most open 5 PDFs in your browser in one request.',
      )
      return
    }
    selected.map((id: GridRowId) => {
      window.open(`/quotation/pdf/${id}`)
    })
  }

  const copyPdfLinkButtonHandler = async () => {
    selected.map(async (id: GridRowId) => {
      const share_res = await axios.get(`/invoice/share_id/${id}`)
      const share_id = share_res.data.share_id
      const url = `${webInfo.baseUrl}quotation/pdf/shared/${share_id}`
      await navigator.clipboard.writeText(url)
      window.alert(t('copy-pdf-link-alert'))
    })
  }

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  const handleShowUnArchiveChange = (
    event: React.ChangeEvent<unknown>,
    newValue: number,
  ) => {
    setShowUnArchive(newValue)
    setSelected([])
  }

  const handleShowUnArchiveChangeIndex = (index: number) => {
    setShowUnArchive(index)
    setSelected([])
  }

  const buttonStyles = makeStyles((theme: Theme) =>
    createStyles({
      button: {
        margin: theme.spacing(1),
        padding: theme.spacing(1),
      },
      sectionDesktop: {
        display: 'none',
        [theme.breakpoints.up('md')]: {
          display: 'flex',
        },
      },
      sectionMobile: {
        display: 'flex',
        [theme.breakpoints.up('md')]: {
          display: 'none',
        },
      },
    }),
  )

  function a11yProps(index: number) {
    return {
      id: `full-width-tab-${index}`,
      'aria-controls': `full-width-tabpanel-${index}`,
    }
  }

  function CustomToolbar() {
    const classes = buttonStyles()
    return (
      <>
        <div className={classes.sectionDesktop}>
          <GridToolbarContainer>
            <GridToolbarColumnsButton />
            <GridToolbarFilterButton />
            <GridToolbarDensitySelector />
            <GridToolbarExport />
          </GridToolbarContainer>
        </div>
        <div className={classes.sectionMobile}>
          <Button
            aria-controls="grid-menu"
            aria-haspopup="true"
            onClick={handleClick}
            variant="contained"
            color="primary"
            startIcon={<DehazeIcon />}
            className={classes.button}
          >
            {t('filter')}
          </Button>
          <Menu
            id="grid-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
            getContentAnchorEl={null}
            anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
            transformOrigin={{ vertical: 'top', horizontal: 'center' }}
          >
            <MenuItem onClick={handleClose}>
              <GridToolbarColumnsButton />
            </MenuItem>
            <MenuItem onClick={handleClose}>
              <GridToolbarFilterButton />
            </MenuItem>
            <MenuItem>
              <GridToolbarDensitySelector />
            </MenuItem>
            <MenuItem>
              <GridToolbarExport />
            </MenuItem>
          </Menu>
        </div>
      </>
    )
  }
  return (
    <>
      {!session && !loading && (
        <meta httpEquiv="refresh" content="0; /signin" />
      )}
      {session && session.level > 3 && (
        <meta httpEquiv="refresh" content="0; /levelerror" />
      )}
      {session && session.level < 4 && (
        <>
          <Layout>
            <div
              background-color={theme.palette.background.paper}
              max-width={500}
            >
              <AppBar position="static" color="default">
                <Tabs
                  value={showUnArchive}
                  onChange={handleShowUnArchiveChange}
                  indicatorColor="primary"
                  textColor="primary"
                  variant="fullWidth"
                  aria-label="full width tabs example"
                >
                  <Tab label={t('company-quotation')} {...a11yProps(0)} />
                  <Tab
                    label={t('archived-company-quotation')}
                    {...a11yProps(1)}
                  />
                </Tabs>
              </AppBar>
              <SwipeableViews
                axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                index={showUnArchive}
                onChangeIndex={handleShowUnArchiveChangeIndex}
              >
                <TabPanel value={showUnArchive} index={0} dir={theme.direction}>
                  <div style={{ height: 400, width: '100%' }}>
                    <EnhancedTableToolbar
                      numSelected={selected.length}
                      session={session}
                      showUnArchive={showUnArchive}
                      onEditclick={editButtonHandler}
                      onRestoreClick={restoreButtonHandler}
                      onFilterClick={filterButtonHandler}
                      onViewPdfClick={viewPdfButtonHandler}
                      onCopyPdfLinkClick={copyPdfLinkButtonHandler}
                      onArchiveClick={archiveButtonHandler}
                      onUnArchiveClick={unArchiveButtonHandler}
                      textFieldData={formData}
                      setTextFieldData={setFormData}
                    />
                    {session.level < 2 ? (
                      <Button
                        variant="contained"
                        color="primary"
                        className={classes.addButton}
                        startIcon={<AddCircleIcon />}
                        onClick={addButtonHandler}
                      >
                        {t('add-quotation')}
                      </Button>
                    ) : (
                      <div></div>
                    )}

                    <DataGrid
                      autoHeight
                      rows={unarchivedData}
                      columns={columns}
                      checkboxSelection={true}
                      page={page}
                      pageSize={pageSize}
                      onPageSizeChange={handlePageSizeChange}
                      onPageChange={handleChangePage}
                      rowsPerPageOptions={[5, 10, 15, 20, 25]}
                      onSelectionModelChange={handleSelectionChange}
                      disableSelectionOnClick
                      pagination
                      components={{
                        Toolbar: CustomToolbar,
                      }}
                      disableColumnMenu
                    />
                  </div>
                </TabPanel>
                <TabPanel value={showUnArchive} index={1} dir={theme.direction}>
                  <div style={{ height: 400, width: '100%' }}>
                    <EnhancedTableToolbar
                      numSelected={selected.length}
                      session={session}
                      showUnArchive={showUnArchive}
                      onEditclick={editButtonHandler}
                      onRestoreClick={restoreButtonHandler}
                      onFilterClick={filterButtonHandler}
                      onViewPdfClick={viewPdfButtonHandler}
                      onCopyPdfLinkClick={copyPdfLinkButtonHandler}
                      onArchiveClick={archiveButtonHandler}
                      onUnArchiveClick={unArchiveButtonHandler}
                      textFieldData={formData}
                      setTextFieldData={setFormData}
                    />
                    {session.level < 2 ? (
                      <Button
                        variant="contained"
                        color="primary"
                        className={classes.addButton}
                        startIcon={<AddCircleIcon />}
                        onClick={addButtonHandler}
                      >
                        {t('add-quotation')}
                      </Button>
                    ) : (
                      <div></div>
                    )}

                    <DataGrid
                      autoHeight
                      rows={archivedData}
                      columns={columns}
                      checkboxSelection={true}
                      page={page}
                      pageSize={pageSize}
                      onPageSizeChange={handlePageSizeChange}
                      onPageChange={handleChangePage}
                      rowsPerPageOptions={[5, 10, 15, 20, 25]}
                      onSelectionModelChange={handleSelectionChange}
                      disableSelectionOnClick
                      pagination
                      components={{
                        Toolbar: CustomToolbar,
                      }}
                      disableColumnMenu
                    />
                  </div>
                </TabPanel>
              </SwipeableViews>
            </div>
          </Layout>
        </>
      )}
    </>
  )
}

export const getStaticProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
})

export default QuotationList
