import React from 'react'
import { IQuotationPdfProps } from '@/components/elements/QuotationPdf/CommonProps'
import axios from 'axios'
import { IInvoice } from '@/server/api/Invoice/invoice.model'
import QuotationPdfGetter from '@/components/elements/QuotationPdf/QuotationPdfGetter'
import { useRouter } from 'next/router'
import { useState } from 'react'
import { IRequestHistory } from '@/server/api/RequestHistory/requesthistory.model'
import { IUser } from '@/server/api/User/user.model'
import { ICompany } from '@/server/api/Company/company.model'

const ShowQuotationPdf = () => {
  const router = useRouter()
  const [quotation_props, set_quotation_props] = useState<IQuotationPdfProps>()
  const [width, setWidth] = React.useState(0)
  const [height, setHeight] = React.useState(0)
  React.useEffect(() => {
    async function fetchData() {
      if (!router || !router.query.id) return
      const quotation_res = await axios.get(`/invoice/${router.query.id}`, {
        params: {
          withClientInfo: true,
          withVendorImages: true,
        },
      })
      const quotation_info: IInvoice = quotation_res.data.invoice
      const request_history_res = await axios.get(
        `/requesthistory/${quotation_info.request_history}`,
        { params: { withFullInfo: true } },
      )
      const request_history_info: IRequestHistory =
        request_history_res.data.request_history
      const vendor_info: ICompany = quotation_info.client as ICompany
      const props: IQuotationPdfProps = {
        company_name: (request_history_info.requester as IUser)
          .normal_user_company_name,
        company_address: (request_history_info.requester as IUser)
          .normal_user_company_address,
        email: (request_history_info.requester as IUser).email,
        contact_person: (request_history_info.requester as IUser).username,
        project_name: quotation_info.project_name,
        quotation_items: quotation_info.quotation_items,
        total_price: quotation_info.price,
        vendor: quotation_info.from,
        quotation_number: quotation_info.invoice_number,
        limited_info: false,
        vendor_image: vendor_info.image_data,
        chop_image: vendor_info.chop_image_data,
        signature_image: vendor_info.signature_image_data,
      }
      set_quotation_props(props)
    }
    setWidth(window.innerWidth)
    setHeight(window.innerHeight)
    fetchData()
  }, [router])
  // const QuotationPdf = getQuotationPdf(props, width, height)
  return (
    <>
      {quotation_props && (
        <>
          <QuotationPdfGetter
            quotation_pdf_props={quotation_props}
            height={height}
            width={width}
          />
        </>
      )}
    </>
  )
}

// export const getServerSideProps: GetServerSideProps = async (context) => {
//   const id = context.query.id
//   const vendor = context.query.vendor
//   const quotation_res = await axios.get(`/invoice/${id}`)
//   const quotation_info: IInvoice = quotation_res.data.invoice
//   const company_res = await axios.get(`/company/${quotation_info.client}`)
//   const company_info: ICompany = company_res.data.company
//   const props: IQuotationPdfProps = {
//     company_name: company_info.company_name,
//     company_address: company_info.company_address,
//     company_email: company_info.contact_email,
//     company_contact_person: company_info.contact_person,
//     project_name: company_info.project_name,
//     quotation_items: quotation_info.quotation_items,
//     total_price: quotation_info.price,
//     vendor: vendor as string,
//     quotation_number: quotation_info.invoice_number,
//   }
//   return { props }
// }

export default ShowQuotationPdf
