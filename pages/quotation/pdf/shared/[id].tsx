import React from 'react'
import { IQuotationPdfProps } from '@/components/elements/QuotationPdf/CommonProps'
import axios from 'axios'
import { IInvoice } from '@/server/api/Invoice/invoice.model'
import QuotationPdfGetter from '@/components/elements/QuotationPdf/QuotationPdfGetter'
import { useRouter } from 'next/router'
import { useState } from 'react'
import { IUser } from '@/server/api/User/user.model'
import { ICompany } from '@/server/api/Company/company.model'

const ShowQuotationPdf = () => {
  const router = useRouter()
  const [quotation_props, set_quotation_props] = useState<IQuotationPdfProps>()
  const [width, setWidth] = React.useState(0)
  const [height, setHeight] = React.useState(0)
  const [error_msg, set_error_msg] = React.useState<string>()
  React.useEffect(() => {
    async function fetchData() {
      if (!router || !router.query.id) return
      try {
        const quotation_res = await axios.get(`/invoice/${router.query.id}`, {
          params: { byShareId: true },
        })
        const quotation_info: IInvoice = quotation_res.data.invoice
        const user_info: IUser = quotation_res.data.user_info
        const images: ICompany = quotation_res.data.images
        const props: IQuotationPdfProps = {
          company_name: user_info.normal_user_company_name,
          company_address: user_info.normal_user_company_address,
          email: user_info.email,
          contact_person: user_info.username,
          project_name: quotation_info.project_name,
          quotation_items: quotation_info.quotation_items,
          total_price: quotation_info.price,
          vendor: quotation_info.from,
          quotation_number: quotation_info.invoice_number,
          limited_info: false,
          vendor_image: images.image_data,
          chop_image: images.chop_image_data,
          signature_image: images.signature_image_data,
        }
        set_quotation_props(props)
      } catch (err) {
        if (err.response) {
          // console.log(err.response)
          set_error_msg(err.response.data.msg)
        } else if (err.request) {
          // console.log(err.request)
        } else {
          if (err.message) {
            // console.log(err.message)
            set_error_msg(err.message)
          } else {
            set_error_msg('error occurs!')
          }
        }
      }
    }
    setWidth(window.innerWidth)
    setHeight(window.innerHeight)
    fetchData()
  }, [router])
  return (
    <>
      {error_msg ? (
        <div>{error_msg}</div>
      ) : (
        quotation_props && (
          <>
            <QuotationPdfGetter
              quotation_pdf_props={quotation_props}
              height={height}
              width={width}
            />
          </>
        )
      )}
    </>
  )
}

export default ShowQuotationPdf
