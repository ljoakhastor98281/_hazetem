import React, { useEffect, useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Typography, Button, TextField, Paper, Box } from '@material-ui/core'
import axios from 'axios'
import InputLabel from '@material-ui/core/InputLabel'
import MenuItem from '@material-ui/core/MenuItem'
import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select'
import { ICompany } from '@/server/api/Company/company.model'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { useTranslation } from 'next-i18next'
import Layout from '../../../components/layouts/navLayout'
import clsx from 'clsx'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'
import { useSession } from 'next-auth/client'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { useRouter } from 'next/router'
import ServiceItemForm, {
  ServiceItemFormUsage,
} from '@/components/elements/ServiceItemForm'
import { IServiceItem } from '@/server/api/Service/service.model'

const useStyles = makeStyles((theme) => ({
  layout: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '100%',
    maxWidth: '900px',
    margin: '0 auto',
  },
  paper: {
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(8),
      padding: `${theme.spacing(6)}px ${theme.spacing(4)}px`,
    },
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    width: '31%',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  buttonProgress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
  button: {
    display: 'block',
    marginTop: theme.spacing(2),
  },
  formControl: {
    // margin: theme.spacing(1),
    width: '100%',
  },
  textField: {
    width: '31%',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  margin: {
    margin: theme.spacing(1),
  },
}))

interface IFormData {
  quotation_items: IServiceItem[]
  from: string
  client: string
  project_name: string
  quotation_number: string
  remark: string
  date: string
  invoice_number: string
  admin_modified: string
}

const Quotationadd = () => {
  const classes = useStyles()
  const { t } = useTranslation()
  const [session, loading] = useSession()
  const [companyList, setCompanyList] = useState<ICompany[]>()
  const router = useRouter()

  useEffect(() => {
    async function fetchData() {
      if (!session) return
      const res = await axios.get('/company')
      res.data.companys.map((company: ICompany) => {
        company.id = company._id
      })
      setCompanyList(res.data.companys)
      setFormData({
        quotation_items: [],
        from: '',
        client: '',
        project_name: '',
        quotation_number: '',
        remark: '',
        date: '',
        invoice_number:
          new Date().getFullYear().toString().substr(-2) +
          ('0' + (new Date().getMonth() + 1)).slice(-2) +
          ('0' + new Date().getDate()).slice(-2) +
          ('0' + new Date().getHours()).slice(-2) +
          ('0' + new Date().getMinutes()).slice(-2) +
          ('0000' + (Math.random() * (10000 - 1) + 1)).slice(-5),
        admin_modified: '',
      })
    }
    fetchData()
  }, [session])

  const showError = () =>
    toast.error(t('create-fail'), {
      position: 'bottom-right',
    })
  const showSuccess = () =>
    toast.success(t('create-success'), {
      position: 'bottom-right',
    })
  const [formData, setFormData] = useState<IFormData>()
  const [company, setCompany] = useState('')

  // event: React.ChangeEvent<{ value: string }>
  const handlecompanyChange = (event: any) => {
    setCompany(event.target.value as string)
  }
  const handleChange = (e: { target: { name: string; value: string } }) => {
    if (formData) setFormData({ ...formData, [e.target.name]: e.target.value })
  }

  const handleSubmit = async (e: { preventDefault: () => void }) => {
    formData.client = company
    e.preventDefault()
    let res
    try {
      res = await axios.post('/invoice/', formData)
    } catch (err) {
      if (err) {
        // console.log(err)
        showError()
      }
    }
    if (res?.status == 200) {
      showSuccess()
      await new Promise((r) => setTimeout(r, 3000))
      router.push(`/${router.locale}/quotation`)
    }
  }
  return (
    <>
      {!session && !loading && (
        <meta httpEquiv="refresh" content="0; /signin" />
      )}
      {session && session?.level > 3 && (
        <meta httpEquiv="refresh" content="0; /levelerror" />
      )}
      {session && companyList && formData && (
        <>
          <Layout>
            <div className={classes.layout}>
              <Paper className={classes.paper} elevation={2}>
                <Button
                  startIcon={<ArrowBackIosIcon />}
                  href={`/${router.locale}/quotation`}
                >
                  {t('back')}
                </Button>
                <br />
                <br />
                <Box
                  display="flex"
                  alignItems="center"
                  justifyContent="center"
                  flexDirection="column"
                >
                  <Typography component="h1" variant="h4" gutterBottom>
                    {t('add-quotation')}
                  </Typography>
                </Box>
                <form className={classes.form} onSubmit={handleSubmit}>
                  <div>
                    <FormControl
                      className={clsx(classes.formControl, classes.margin)}
                    >
                      <InputLabel id="company-label">
                        {t('company_name')}
                      </InputLabel>
                      <Select
                        labelId="company-label"
                        required
                        id="company-label"
                        value={company}
                        onChange={handlecompanyChange}
                      >
                        {companyList.length > 0 ? (
                          companyList.map((company: ICompany) => (
                            <MenuItem
                              value={company._id.toString()}
                              key={company._id.toString()}
                            >
                              {`${company.company_name.toString()}`}
                            </MenuItem>
                          ))
                        ) : (
                          <MenuItem value="">
                            <em>None</em>
                          </MenuItem>
                        )}
                      </Select>
                    </FormControl>
                  </div>
                  <TextField
                    className={clsx(classes.textField, classes.margin)}
                    margin="normal"
                    required
                    disabled
                    fullWidth
                    id="invoice_number"
                    label={t('invoice_number')}
                    name="invoice_number"
                    autoComplete="invoice_number"
                    defaultValue={formData.invoice_number}
                    onChange={handleChange}
                  />
                  <TextField
                    className={clsx(classes.textField, classes.margin)}
                    margin="normal"
                    required
                    fullWidth
                    id="from"
                    label={t('from')}
                    name="from"
                    autoComplete="from"
                    onChange={handleChange}
                  />
                  <TextField
                    className={clsx(classes.textField, classes.margin)}
                    margin="normal"
                    required
                    fullWidth
                    type="date"
                    id="date"
                    label={t('date')}
                    name="date"
                    autoComplete="date"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    onChange={handleChange}
                  />
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="project_name"
                    label={t('project_name')}
                    name="project_name"
                    autoComplete="project_name"
                    className={classes.margin}
                    onChange={handleChange}
                  />
                  <TextField
                    margin="normal"
                    fullWidth
                    variant="outlined"
                    name="remark"
                    label={t('remark')}
                    id="remark"
                    multiline
                    rows={10}
                    rowsMax={10}
                    autoComplete="remark"
                    className={classes.margin}
                    onChange={handleChange}
                  />
                  <ServiceItemForm
                    initialItems={formData.quotation_items.filter(
                      (item: IServiceItem) => item.needed,
                    )}
                    usage={ServiceItemFormUsage.SERVICE_ITEMS}
                    onUpdate={(items: IServiceItem[]) => {
                      setFormData({
                        ...formData,
                        quotation_items: items,
                      })
                    }}
                  />
                  <Box mb={6} textAlign="center">
                    <Button
                      //   disabled={submitting}
                      type="submit"
                      fullWidth
                      variant="contained"
                      color="primary"
                      className={classes.submit}
                      // onClick={update}
                    >
                      {t('create')}
                    </Button>
                  </Box>
                </form>
                <ToastContainer />
              </Paper>
            </div>
          </Layout>
        </>
      )}
    </>
  )
}

export const getStaticProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
})

// export const getServerSideProps: GetServerSideProps = async () => {
//   const res = await axios.get('/company')
//   res.data.companys.map((company: ICompany) => {
//     company.id = company._id
//   })
//   return { props: { list: res.data.companys } }
// }

export default Quotationadd
