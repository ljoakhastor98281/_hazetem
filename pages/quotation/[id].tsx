import React, { useEffect, useState } from 'react'
import clsx from 'clsx'
import { makeStyles } from '@material-ui/core/styles'
import { Typography, Button, TextField, Paper, Box } from '@material-ui/core'
import axios from 'axios'
import { IInvoice } from '@/server/api/Invoice/invoice.model'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'
import Layout from '../../components/layouts/navLayout'
import ReceiptIcon from '@material-ui/icons/Receipt'
import IconButton from '@material-ui/core/IconButton'
import { useRouter } from 'next/router'
import { useSession } from 'next-auth/client'
import { useTranslation } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import ServiceItemForm, {
  ServiceItemFormUsage,
} from '@/components/elements/ServiceItemForm'
import { IServiceItem } from '@/server/api/Service/service.model'

const useStyles = makeStyles((theme) => ({
  layout: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '100%',
    maxWidth: '900px',
    margin: '0 auto',
  },
  paper: {
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(8),
      padding: `${theme.spacing(6)}px ${theme.spacing(4)}px`,
    },
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  buttonProgress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
  textField: {
    width: '48%',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  topButton: {
    float: 'right',
    position: 'relative',
  },
  margin: {
    margin: theme.spacing(1),
    marginTop: theme.spacing(1),
  },
  title: {
    marginTop: theme.spacing(3),
    flex: '1 1 100%',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    fontSize: 30,
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
}))

interface IFormData {
  _id: string
  client: string
  quotation_items: IServiceItem[]
  from: string
  project_name: string
  invoice_number: string
  remark: string
  date: Date
  admin_modified: Date
}

const QuotationEdit = () => {
  const classes = useStyles({})
  const { t } = useTranslation()
  const router = useRouter()
  const [session, loading] = useSession()
  const [loadedInvoice, setLoadedInvoice] = useState<IInvoice>()
  const [formData, setFormData] = useState<IFormData>()
  useEffect(() => {
    async function fetchData() {
      if (!session) return
      const { id } = router.query
      const res = await axios.get(`/invoice/${id}`)
      setLoadedInvoice(() => res.data.invoice)
      setFormData({
        _id: res.data.invoice._id,
        client: res.data.invoice.client,
        quotation_items: res.data.invoice.quotation_items,
        from: res.data.invoice.from,
        project_name: res.data.invoice.project_name,
        invoice_number: res.data.invoice.invoice_number,
        remark: res.data.invoice.remark,
        date: res.data.invoice.date.toString().split(`T`)[0],
        admin_modified: res.data.invoice.admin_modified,
      })
    }

    fetchData()
  }, [router.query, session])

  const showSuccess = () =>
    toast.success(t('update-success'), {
      position: 'bottom-right',
    })

  const handleChange = (e: { target: { name: string; value: string } }) => {
    if (formData) {
      setFormData({ ...formData, [e.target.name]: e.target.value })
    }
  }

  const handleSubmit = async (e: { preventDefault: () => void }) => {
    e.preventDefault()
    // console.log(formData)
    let res
    try {
      res = await axios.put(`/invoice/${loadedInvoice?._id}`, formData)
    } catch (err) {
      if (err) {
        switch (err.response.status) {
          case 403:
            toast.error(t('user-level-cannot-access'), {
              position: 'bottom-right',
            })
            break
          case 409:
            if (err.response.data.msg == 'conflict-service-name') {
              toast.error(t('409-service-name'), {
                position: 'bottom-right',
              })
              break
            } else if (err.response.data.msg == 'conflict-item-title') {
              toast.error(t('409-service-item'), {
                position: 'bottom-right',
              })
              break
            } else {
              toast.error(t('error-without-status'), {
                position: 'bottom-right',
              })
              break
            }
          default:
            toast.error(t('error-without-status'), {
              position: 'bottom-right',
            })
            break
        }
      }
    }
    if (res?.status == 200) {
      showSuccess()
      await new Promise((r) => setTimeout(r, 5000))
      router.push(`/${router.locale}/quotation`)
    }
  }

  return (
    <>
      {!session && !loading && (
        <meta httpEquiv="refresh" content="0; /signin" />
      )}
      {session && session?.level > 3 && (
        <meta httpEquiv="refresh" content="0; /levelerror" />
      )}
      {session && loadedInvoice && formData && (
        <>
          <Layout>
            <div className={classes.layout}>
              <Paper className={classes.paper} elevation={2}>
                <div className={classes.sectionDesktop}>
                  <Box
                    display="flex"
                    justifyContent="space-between"
                    width="100%"
                  >
                    <Button
                      startIcon={<ArrowBackIosIcon />}
                      href={`/${router.locale}/quotation`}
                    >
                      {t('back')}
                    </Button>
                    <Button
                      size="small"
                      className={clsx(classes.topButton, classes.margin)}
                      variant="outlined"
                      color="primary"
                      href={`/${router.locale}/quotation/item/${loadedInvoice._id}`}
                    >
                      {t('invoice-item-detail')}
                    </Button>
                  </Box>
                </div>
                <div className={classes.sectionMobile}>
                  <Box
                    display="flex"
                    justifyContent="space-between"
                    width="100%"
                  >
                    <IconButton href={`/${router.locale}/quotation`}>
                      <ArrowBackIosIcon />
                    </IconButton>
                    <IconButton
                      href={`/${router.locale}/quotation/item/${loadedInvoice._id}`}
                    >
                      <ReceiptIcon />
                    </IconButton>
                  </Box>
                </div>
                <br></br>
                <Box
                  display="flex"
                  alignItems="center"
                  justifyContent="center"
                  flexDirection="column"
                  className={classes.title}
                >
                  <Typography component="h1" variant="h4" gutterBottom>
                    {t('quotation-edit-list')}
                  </Typography>
                </Box>
                <br></br>
                <form className={classes.form} onSubmit={handleSubmit}>
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="invoice_number"
                    label={t('invoice_number')}
                    name="invoice_number"
                    type="number"
                    onKeyDown={(evt) =>
                      ['e', 'E', '+', '-'].includes(evt.key) &&
                      evt.preventDefault()
                    }
                    autoComplete="invoice_number"
                    className={clsx(classes.margin)}
                    defaultValue={formData.invoice_number}
                    onChange={handleChange}
                  />
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="from"
                    label={t('from')}
                    name="from"
                    autoComplete="from"
                    className={clsx(classes.margin, classes.textField)}
                    defaultValue={formData.from}
                    onChange={handleChange}
                  />
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    type="date"
                    id="date"
                    label={t('date')}
                    name="date"
                    autoComplete="date"
                    className={clsx(classes.margin, classes.textField)}
                    InputLabelProps={{
                      shrink: true,
                    }}
                    defaultValue={formData?.date?.toString().split(`T`)[0]}
                    onChange={handleChange}
                  />
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="project_name"
                    label={t('project_name')}
                    name="project_name"
                    autoComplete="project_name"
                    className={clsx(classes.margin)}
                    defaultValue={formData.project_name}
                    onChange={handleChange}
                  />
                  <TextField
                    margin="normal"
                    variant="outlined"
                    fullWidth
                    name="remark"
                    label={t('remark')}
                    id="remark"
                    multiline
                    rows={10}
                    rowsMax={10}
                    autoComplete="remark"
                    className={clsx(classes.margin)}
                    defaultValue={formData.remark}
                    onChange={handleChange}
                  />
                  <ServiceItemForm
                    initialItems={formData.quotation_items.filter(
                      (item: IServiceItem) => item.needed,
                    )}
                    usage={ServiceItemFormUsage.QUOTATION_ITEMS}
                    onUpdate={(items: IServiceItem[]) => {
                      setFormData({
                        ...formData,
                        quotation_items: items,
                      })
                    }}
                  />
                  <Box mb={6} textAlign="center">
                    <Button
                      // disabled={submitting}
                      size="large"
                      type="submit"
                      variant="contained"
                      color="primary"
                      className={classes.submit}
                      // onClick={update}
                    >
                      {/* {submitting && (
                  <CircularProgress
                    size={24}
                    className={classes.buttonProgress}
                  />
                )} */}
                      {t('update')}
                    </Button>
                  </Box>
                </form>
                <ToastContainer />
              </Paper>
            </div>
          </Layout>
        </>
      )}
    </>
  )
}

export const getStaticProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
})

export const getStaticPaths = async () => {
  return {
    paths: [],
    fallback: 'blocking',
  }
}

export default QuotationEdit
