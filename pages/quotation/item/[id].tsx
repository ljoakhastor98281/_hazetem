import React, { useState } from 'react'
import clsx from 'clsx'
import {
  DataGrid,
  GridColDef,
  GridRowId,
  GridSelectionModelChangeParams,
  GridToolbarContainer,
  GridToolbarColumnsButton,
  GridToolbarFilterButton,
  GridToolbarExport,
  GridToolbarDensitySelector,
} from '@material-ui/data-grid'
import { Toolbar, Typography, IconButton } from '@material-ui/core'
import Button from '@material-ui/core/Button'
import DeleteIcon from '@material-ui/icons/Delete'
import EditIcon from '@material-ui/icons/Edit'
import AddCircleIcon from '@material-ui/icons/AddCircle'
import { createStyles, makeStyles, withStyles } from '@material-ui/core/styles'
import { Theme } from '@material-ui/core/styles'
import { lighten } from '@material-ui/core/styles'
import Layout from '../../../components/layouts/navLayout'
import axios from 'axios'
import { IInvoice } from '../../../server/api/Invoice/invoice.model'
import { useRouter } from 'next/router'
import { format } from 'date-fns'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import DehazeIcon from '@material-ui/icons/Dehaze'
import { useSession } from 'next-auth/client'
import { useEffect } from 'react'
import { useTranslation } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { IServiceItem } from '@/server/api/Service/service.model'

const useToolbarStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(1),
    },
    highlight:
      theme.palette.type === 'light'
        ? {
            color: '#004369',
            backgroundColor: '#d8e9ef',
            margin: theme.spacing(1),
          }
        : {
            color: theme.palette.text.primary,
            backgroundColor: theme.palette.secondary.dark,
          },
    title: {
      flex: '1 1 100%',
      alignItems: 'center',
      justifyContent: 'center',
      textAlign: 'center',
      fontSize: 50,
      [theme.breakpoints.down('xs')]: {
        fontSize: 40,
      },
      fontFamily: 'Sintony, sans-serif',
    },
    subtitle: {
      flex: '1 1 100%',
      fontSize: 20,
      [theme.breakpoints.down('sm')]: {
        fontSize: 15,
      },
    },
    button: {
      margin: theme.spacing(1),
      maxWidth: 200,
      width: '100%',
      backgroundColor: '#4ea1d3',
      color: 'white',
    },
    button2: {
      margin: theme.spacing(1),
      maxWidth: 200,
      width: '100%',
      backgroundColor: '#e85a71',
      color: 'white',
    },
    addButton: {
      margin: theme.spacing(1),
      maxWidth: 250,
      backgroundColor: '#4ea1d3',
      color: 'white',
      fontSize: 40,
    },
    input: {
      marginLeft: theme.spacing(1),
      flex: 1,
    },
    iconButton: {
      padding: 10,
    },
    divider: {
      height: 28,
      margin: 4,
    },
    sectionDesktop: {
      display: 'none',
      [theme.breakpoints.up('md')]: {
        display: 'flex',
      },
    },
    sectionMobile: {
      display: 'flex',
      [theme.breakpoints.up('md')]: {
        display: 'none',
      },
    },
  }),
)

const StyledDataGrid = withStyles({
  root: {
    '& .MuiDataGrid-window': {
      overflowY: 'visible !important',
    },
    '& .MuiDataGrid-columnsHeader': {
      alignItems: 'center',
    },
    '& .MuiDataGrid-row': {
      maxHeight: 'none !important',
    },
    '& .MuiDataGrid-viewport': {
      maxHeight: 'max-content !important',
    },
    '& .MuiDataGrid-cell': {
      lineHeight: 'unset !important',
      verticalAlign: 'middle',
      position: 'relative',
      display: 'flex !important',
      alignItems: 'center',
      maxHeight: 'none !important',
      whiteSpace: 'normal !important',
      overflowWrap: 'break-word',
    },
  },
})(DataGrid)

interface EnhancedTableToolbarProps {
  numSelected: number
  onEditclick: () => void
  onDeleteClick: () => void
}

const EnhancedTableToolbar = (props: EnhancedTableToolbarProps) => {
  const classes = useToolbarStyles()
  const { t } = useTranslation()
  const router = useRouter()
  const { numSelected } = props
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  return (
    <>
      <Button
        startIcon={<ArrowBackIosIcon />}
        href={`/${router.locale}/quotation`}
      >
        {t('back')}
      </Button>
      <br />
      <br />
      <Toolbar className={clsx(classes.root)}>
        <Typography className={classes.title} id="tableTitle" component="div">
          {t('quotation-item')}
        </Typography>
      </Toolbar>
      {numSelected > 0 ? (
        <>
          <Toolbar
            className={clsx(classes.root, classes.sectionDesktop, {
              [classes.highlight]: numSelected > 0,
            })}
          >
            <Typography
              className={classes.subtitle}
              color="inherit"
              variant="subtitle1"
              component="div"
            >
              {t('selected')} {numSelected}
            </Typography>
            <Button
              variant="contained"
              aria-label="edit"
              color="primary"
              disabled={numSelected == 1 ? false : true}
              className={classes.button}
              onClick={props.onEditclick}
              startIcon={<EditIcon />}
            >
              {t('edit')}
            </Button>
            <Button
              variant="contained"
              color="secondary"
              aria-label="delete"
              className={classes.button2}
              onClick={props.onDeleteClick}
              startIcon={<DeleteIcon />}
            >
              {t('delete')}
            </Button>
          </Toolbar>
          <Toolbar
            className={clsx(classes.root, classes.sectionMobile, {
              [classes.highlight]: numSelected > 0,
            })}
          >
            <Typography
              className={classes.subtitle}
              color="inherit"
              variant="subtitle1"
              component="div"
            >
              {t('selected')} {numSelected}
            </Typography>
            <IconButton
              aria-controls="action-menu"
              aria-haspopup="true"
              onClick={handleClick}
              color="primary"
            >
              <MoreVertIcon />
            </IconButton>
            <Menu
              id="action-menu"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleClose}
              getContentAnchorEl={null}
              anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
              transformOrigin={{ vertical: 'top', horizontal: 'center' }}
            >
              <MenuItem
                onClick={props.onEditclick}
                disabled={numSelected == 1 ? false : true}
              >
                <ListItemIcon>
                  <EditIcon fontSize="small" />
                </ListItemIcon>
                <ListItemText primary={t('edit')} />
              </MenuItem>
              <MenuItem onClick={props.onDeleteClick}>
                <ListItemIcon>
                  <DeleteIcon fontSize="small" />
                </ListItemIcon>
                <ListItemText primary={t('delete')} />
              </MenuItem>
            </Menu>
          </Toolbar>
        </>
      ) : null}
    </>
  )
}

interface ITableRow {
  id: string
  title: string
  description: string
  price: number
  order: number
}

export default function QuotationItem() {
  // const [page, setPage] = useState(0)
  const classes = useToolbarStyles()
  const [selected, setSelected] = useState<GridRowId[]>([])
  const [loadedData, setLoadedData] = useState<ITableRow[]>()
  // const [pageSize, setPageSize] = useState(5)
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
  const [session, loading] = useSession()
  const router = useRouter()
  const { t } = useTranslation()
  const quotation_id = router.query.id

  useEffect(() => {
    async function fetchData() {
      if (!session) return
      const res = await axios.get(`/invoice/${quotation_id}`)
      const rows: ITableRow[] = []
      res.data.invoice.quotation_items.map(
        (item: IServiceItem, index: number) => {
          if (item.needed)
            rows.push({
              id: String(index),
              title: item.title,
              description: item.description,
              price: item.price,
              order: item.order,
            })
        },
      )
      setLoadedData(rows)
    }
    fetchData()
  }, [session, quotation_id])

  const columns: GridColDef[] = [
    { field: 'title', headerName: t('service-title'), width: 350 },
    { field: 'description', headerName: t('description'), width: 300 },
    { field: 'price', headerName: t('price'), width: 250 },
    { field: 'order', headerName: t('order'), width: 100 },
  ]

  // const handlePagSizeChange = async (param: GridPageChangeParams) => {
  //   setPageSize(param.pageSize)
  // }

  const handleSelectionChange = (params: GridSelectionModelChangeParams) => {
    setSelected(params.selectionModel)
  }

  const updateRenderedData = async () => {
    const res = await axios.get(`/invoice/${quotation_id}`)
    const newRows: ITableRow[] = []
    res.data.invoice.quotation_items.map(
      (item: IServiceItem, index: number) => {
        if (item.needed)
          newRows.push({
            id: String(index),
            title: item.title,
            description: item.description,
            price: item.price,
            order: item.order,
          })
      },
    )
    setLoadedData(newRows)
    setSelected([])
  }

  const addButtonHandler = async () => {
    router.push(`/quotation/item/additem/${quotation_id}`)
  }

  const editButtonHandler = async () => {
    selected.map((item: GridRowId) => {
      router.push(`/quotation/item/edititem/${quotation_id}?order=${item}`)
    })
  }

  const deleteButtonHandler = async () => {
    const decision = window.confirm(t('delete-alert'))
    if (!decision || !loadedData) {
      return
    }
    const itemsAfterDelete: IServiceItem[] = []
    loadedData.map((row: ITableRow) => {
      if (!selected.includes(row.id)) {
        itemsAfterDelete.push({
          description: row.description,
          title: row.title,
          price: row.price,
          order: row.order,
          needed: true,
        })
      }
    })
    const res = await axios.get(`/invoice/${quotation_id}`)
    const newQuotation: IInvoice = res.data.invoice
    newQuotation.quotation_items = itemsAfterDelete
    const formatted_date = format(new Date(newQuotation.date), 'yyyy-MM-dd')
    await axios.put(`/invoice/${quotation_id}`, {
      ...newQuotation,
      date: formatted_date,
    })
    updateRenderedData()
  }

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  const buttonStyles = makeStyles((theme: Theme) =>
    createStyles({
      button: {
        margin: theme.spacing(1),
        padding: theme.spacing(1),
      },
      sectionDesktop: {
        display: 'none',
        [theme.breakpoints.up('md')]: {
          display: 'flex',
        },
      },
      sectionMobile: {
        display: 'flex',
        [theme.breakpoints.up('md')]: {
          display: 'none',
        },
      },
    }),
  )

  function CustomToolbar() {
    const classes = buttonStyles()
    const { t } = useTranslation()
    return (
      <>
        <div className={classes.sectionDesktop}>
          <GridToolbarContainer>
            <GridToolbarColumnsButton />
            <GridToolbarFilterButton />
            <GridToolbarDensitySelector />
            <GridToolbarExport />
          </GridToolbarContainer>
        </div>
        <div className={classes.sectionMobile}>
          <Button
            aria-controls="grid-menu"
            aria-haspopup="true"
            onClick={handleClick}
            variant="contained"
            color="primary"
            startIcon={<DehazeIcon />}
            className={classes.button}
          >
            {t('filter')}
          </Button>
          <Menu
            id="grid-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
            getContentAnchorEl={null}
            anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
            transformOrigin={{ vertical: 'top', horizontal: 'center' }}
          >
            <MenuItem onClick={handleClose}>
              <GridToolbarColumnsButton />
            </MenuItem>
            <MenuItem onClick={handleClose}>
              <GridToolbarFilterButton />
            </MenuItem>
            <MenuItem>
              <GridToolbarDensitySelector />
            </MenuItem>
            <MenuItem>
              <GridToolbarExport />
            </MenuItem>
          </Menu>
        </div>
      </>
    )
  }

  return (
    <>
      {!session && !loading && (
        <meta httpEquiv="refresh" content="0; /signin" />
      )}
      {session && session?.level > 3 && (
        <meta httpEquiv="refresh" content="0; /levelerror" />
      )}
      {session && loadedData && (
        <>
          <Layout>
            <div style={{ height: 400, width: '100%' }}>
              <EnhancedTableToolbar
                numSelected={selected.length}
                onEditclick={editButtonHandler}
                onDeleteClick={deleteButtonHandler}
              />
              <Button
                variant="contained"
                color="primary"
                className={classes.addButton}
                startIcon={<AddCircleIcon />}
                onClick={addButtonHandler}
              >
                {t('add-quotation-item')}
              </Button>
              <StyledDataGrid
                autoHeight
                rows={loadedData}
                columns={columns}
                checkboxSelection={true}
                rowsPerPageOptions={[5, 10, 15, 20, 25]}
                onSelectionModelChange={handleSelectionChange}
                disableSelectionOnClick
                components={{
                  Toolbar: CustomToolbar,
                }}
                disableColumnMenu
              />
            </div>
          </Layout>
        </>
      )}
    </>
  )
}

export const getStaticProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
})

export const getStaticPaths = async () => {
  return {
    paths: [],
    fallback: 'blocking',
  }
}

// export const getServerSideProps: GetServerSideProps = async (context) => {
//   const { id } = context.query
//   const res = await axios.get(`/invoice/${id}`)
//   const rows: ITableRow[] = []
//   res.data.invoice.quotation_items.map(
//     (item: IQuotationItem, index: number) => {
//       rows.push({
//         id: String(index),
//         description: item.description,
//         price: item.price,
//         order: item.order,
//       })
//     },
//   )
//   return { props: { quotation_id: id, rows: rows } }
// }
