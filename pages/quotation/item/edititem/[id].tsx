import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Typography, Button, TextField, Paper, Box } from '@material-ui/core'
import axios from 'axios'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import Layout from '../../../../components/layouts/navLayout'
import clsx from 'clsx'
import { IInvoice } from '@/server/api/Invoice/invoice.model'
import { format } from 'date-fns'
// import { GetServerSideProps } from 'next'
import InputAdornment from '@material-ui/core/InputAdornment'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'
import { useRouter } from 'next/router'
import { useSession } from 'next-auth/client'
import { useEffect } from 'react'
import { useTranslation } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

const useStyles = makeStyles((theme) => ({
  layout: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    maxWidth: '900px',
    width: '100%',
    margin: '0 auto',
  },
  paper: {
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(8),
      padding: `${theme.spacing(6)}px ${theme.spacing(4)}px`,
    },
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    // width: '31%',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  buttonProgress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
  button: {
    display: 'block',
    marginTop: theme.spacing(2),
  },
  formControl: {
    // margin: theme.spacing(1),
    width: '100%',
  },
  textField: {
    width: '49%',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  margin: {
    marginRight: theme.spacing(1),
  },
}))

interface IFormData {
  title: string
  description: string
  price: number
  order: number
}

const Itemedit = () => {
  const classes = useStyles()
  const { t } = useTranslation()
  const showError = () =>
    toast.error(t('edit-fail'), {
      position: 'bottom-right',
    })
  const showSuccess = () =>
    toast.success(t('edit-success'), {
      position: 'bottom-right',
    })
  const [session, loading] = useSession()
  const router = useRouter()
  const quotation_id = router.query.id
  const orderNum = Number(router.query.order)
  const [quotation, setQuotation] = useState<IInvoice>()
  const [formData, setFormData] = useState<IFormData>()

  useEffect(() => {
    async function fetchData() {
      if (!session) return
      const res = await axios.get(`/invoice/${quotation_id}`)
      setQuotation(res.data.invoice)
      setFormData({
        title: res.data.invoice.quotation_items[orderNum].title,
        description: res.data.invoice.quotation_items[orderNum].description,
        price: res.data.invoice.quotation_items[orderNum].price,
        order: res.data.invoice.quotation_items[orderNum].order,
      })
    }
    fetchData()
  }, [session, quotation_id, orderNum])

  const handleChange = (e: { target: { name: string; value: string } }) => {
    if (formData) setFormData({ ...formData, [e.target.name]: e.target.value })
  }

  const handleSubmit = async (e: { preventDefault: () => void }) => {
    if (!formData) return
    e.preventDefault()
    let res
    try {
      const newQuotation: IInvoice = quotation as IInvoice
      newQuotation.quotation_items[orderNum].description = formData.description
      newQuotation.quotation_items[orderNum].price = Number(formData.price)
      newQuotation.quotation_items[orderNum].order = Number(formData.order)

      const formatted_date = format(new Date(newQuotation.date), 'yyyy-MM-dd')
      res = await axios.put(`/invoice/${quotation_id}`, {
        ...newQuotation,
        date: formatted_date,
      })
    } catch (err) {
      if (err) {
        // console.log(err)
        showError()
      }
    }
    if (res?.status == 200) {
      showSuccess()
      await new Promise((r) => setTimeout(r, 3000))
      router.push(`/quotation/item/${quotation_id}`)
    }
  }
  return (
    <>
      {!session && !loading && (
        <meta httpEquiv="refresh" content="0; /signin" />
      )}
      {session && session?.level > 3 && (
        <meta httpEquiv="refresh" content="0; /levelerror" />
      )}
      {session && quotation && formData && (
        <>
          <Layout>
            <main className={classes.layout}>
              <Paper className={classes.paper} elevation={2}>
                <Button
                  startIcon={<ArrowBackIosIcon />}
                  href={`/${router.locale}/quotation/item/${quotation_id}`}
                >
                  {t('back')}
                </Button>
                <br />
                <br />
                <Box
                  display="flex"
                  alignItems="center"
                  justifyContent="center"
                  flexDirection="column"
                >
                  <Typography component="h1" variant="h4" gutterBottom>
                    {t('edit-quotation-item')}
                  </Typography>
                </Box>
                <form className={classes.form} onSubmit={handleSubmit}>
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="title"
                    label={t('service-title')}
                    name="title"
                    autoComplete="title"
                    defaultValue={formData.title}
                    onChange={handleChange}
                  />
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    variant="outlined"
                    name="description"
                    label={t('description')}
                    id="description"
                    multiline
                    rows={10}
                    rowsMax={10}
                    autoComplete="description"
                    defaultValue={formData.description}
                    onChange={handleChange}
                  />
                  <TextField
                    className={clsx(classes.margin, classes.textField)}
                    margin="normal"
                    required
                    fullWidth
                    id="price"
                    label={t('price')}
                    name="price"
                    type="number"
                    autoComplete="price"
                    defaultValue={formData.price}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">$</InputAdornment>
                      ),
                    }}
                    onChange={handleChange}
                  />
                  <TextField
                    className={classes.textField}
                    margin="normal"
                    required
                    fullWidth
                    id="order"
                    label={t('order')}
                    name="order"
                    type="number"
                    disabled
                    autoComplete="order"
                    defaultValue={formData.order}
                    onChange={handleChange}
                  />
                  <Box mb={6} textAlign="center">
                    <Button
                      type="submit"
                      variant="contained"
                      color="primary"
                      className={classes.submit}
                    >
                      {t('update')}
                    </Button>
                  </Box>
                </form>
                <ToastContainer />
              </Paper>
            </main>
          </Layout>
        </>
      )}
    </>
  )
}

export const getStaticProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
})

export const getStaticPaths = async () => {
  return {
    paths: [],
    fallback: 'blocking',
  }
}

// export const getServerSideProps: GetServerSideProps = async (context) => {
//   const { id, order } = context.query
//   const res = await axios.get(`/invoice/${id}`)
//   return {
//     props: { quotation_id: id, item_order: order, quotation: res.data.invoice },
//   }
// }
export default Itemedit
