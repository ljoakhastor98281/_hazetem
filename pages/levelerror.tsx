import Layout from '../components/layouts/navLayout'
import { useTranslation } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { Box } from '@material-ui/core'
import PanToolIcon from '@material-ui/icons/PanTool'

export default function NotAuthorized() {
  const { t } = useTranslation()
  return (
    <Layout>
      <Box
        display="flex"
        flexDirection="column"
        textAlign="center"
        justifyContent="center"
        alignItems="center"
        style={{ margin: 100 }}
      >
        <PanToolIcon style={{ fontSize: 90, color: '#b46f6a', margin: 10 }} />
        <h1>{t('not-auth-msg')}</h1>
      </Box>
    </Layout>
  )
}

export const getStaticProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
})
