import React from 'react'
import { DropzoneDialog } from 'material-ui-dropzone'
import { Box, Button } from '@material-ui/core'
import axios from 'axios'

// remove later
import mongoose from 'mongoose'

export default function FileUploader() {
  const [open, setOpen] = React.useState(false)

  // hard code user id
  const user_id = new mongoose.Types.ObjectId()

  const handleOpen = async () => {
    setOpen(true)
  }

  const handleClose = async () => {
    setOpen(false)
  }

  const handleSave = async (files: File[]) => {
    // console.log(user_id.toHexString())
    let sus = true

    await files.forEach(async (file) => {
      // console.log(file)
      const formData = new FormData()
      formData.append('user_id', user_id.toHexString())
      formData.append('uploaded', 'false')
      formData.append('file', file)
      const config = {
        headers: {
          'content-type': 'multpart/form-data',
        },
      }
      const res = await axios.post('/user/upload', formData, config)
      if (res.data.msg !== 'sus' || res.status !== 200) {
        sus = false
      }
    })

    if (sus) {
      window.alert('sus')
    } else {
      window.alert('not sus')
    }

    setOpen(false)
  }

  return (
    <Box>
      <Button onClick={handleOpen}>upload files</Button>
      <DropzoneDialog
        open={open}
        onSave={handleSave}
        acceptedFiles={[
          'application/pdf',
          'image/png',
          'application/msword',
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
          'image/jpeg',
        ]}
        showPreviews={true}
        maxFileSize={5000000}
        onClose={handleClose}
      />
    </Box>
  )
}
