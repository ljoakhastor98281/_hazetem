import { AppProps } from 'next/app'
import { appWithTranslation } from 'next-i18next'
import axios from 'axios'
import { Provider } from 'next-auth/client'
import { isDev, webInfo } from '../server/config'
import '@/styles/_base.scss'
import '@/styles/index.css'

axios.defaults.baseURL = `${isDev ? webInfo.devUrl : webInfo.baseUrl}api/`
axios.defaults.timeout = 5000

const App = ({ Component, pageProps }: AppProps) => {
  return (
    <Provider session={pageProps.session}>
      <Component {...pageProps} />
    </Provider>
  )
}

export default appWithTranslation(App)
