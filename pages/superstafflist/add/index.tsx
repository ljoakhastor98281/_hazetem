import React, { useState } from 'react'
// import Typography from '@material-ui/core/Typography'
// import Box from '@material-ui/core/Box'
import { makeStyles } from '@material-ui/core/styles'
// import Paper from '@material-ui/core/Paper'
// import TextField from '@material-ui/core/TextField'
// import Button from '@material-ui/core/Button'
import { Typography, Button, TextField, Box, Card } from '@material-ui/core'
import axios from 'axios'
import { Fonts } from '@/components/constants/AppEnums'
import { CremaTheme } from '@/types/AppContextPropsType'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'
import { useTranslation } from 'next-i18next'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import Layout from '../../../components/layouts/navLayout'
import { useSession } from 'next-auth/client'
import { useRouter } from 'next/router'
import CheckCircleOutlineOutlinedIcon from '@material-ui/icons/CheckCircleOutlineOutlined'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { IconButton, InputAdornment } from '@material-ui/core'
import { Visibility, VisibilityOff } from '@material-ui/icons'

const useStyles = makeStyles((theme: CremaTheme) => ({
  cardRoot: {
    maxWidth: '700px',
    width: '100%',
    overflow: 'hidden',
    boxShadow: '0 10px 15px -3px rgba(0, 0, 0, 0.1)',
    textAlign: 'center',
    position: 'relative',
    paddingTop: 20,
    [theme.breakpoints.up('xl')]: {
      paddingTop: 32,
    },
    '&:before': {
      content: "''",
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      width: 130,
      height: 9,
      borderBottomRightRadius: 80,
      borderBottomLeftRadius: 80,
      marginRight: 'auto',
      marginLeft: 'auto',
      backgroundColor: theme.palette.primary.main,
    },
  },
  textUppercase: {
    textTransform: 'uppercase',
  },
  formRoot: {
    textAlign: 'left',
    [theme.breakpoints.up('xl')]: {
      marginBottom: 24,
    },
  },
  myTextFieldRoot: {
    width: '48%',
    [theme.breakpoints.down('xs')]: {
      width: '100%',
    },
  },
  checkboxRoot: {
    marginLeft: -12,
  },
  pointer: {
    cursor: 'pointer',
  },
  btnRoot: {
    // borderRadius: theme.overrides.MuiCard.root.borderRadius,
    width: '10rem',
    fontWeight: Fonts.REGULAR,
    fontSize: 16,
    textTransform: 'capitalize',
  },
  btnRootFull: {
    width: '100%',
  },
  dividerRoot: {
    marginBottom: 16,
    marginLeft: -48,
    marginRight: -48,
    [theme.breakpoints.up('xl')]: {
      marginBottom: 32,
    },
  },
  textPrimary: {
    color: theme.palette.text.primary,
  },
  fieldmargin: {
    marginRight: theme.spacing(3),
  },
  colorTextPrimary: {
    color: theme.palette.primary.main,
  },
  underlineNone: {
    textDecoration: 'none',
  },
  textGrey: {
    color: theme.palette.grey[500],
  },
  back: {
    position: 'absolute',
    left: 10,
  },
  textfieldLabel: {
    width: '100%',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
  },
}))

const AddSuperStaff = () => {
  const { t } = useTranslation()
  const classes = useStyles({})
  const [session, loading] = useSession()

  const [formData, setFormData] = useState({
    username: '',
    email: '',
    password: '',
    password2: '',
  })

  const showError = () =>
    toast.error(t('create-fail'), {
      position: 'bottom-right',
    })
  const showSuccess = () =>
    toast.success(t('create-success'), {
      position: 'bottom-right',
    })
  const show409Error = () =>
    toast.error(t('register-409-fail'), {
      position: 'bottom-right',
    })
  const router = useRouter()
  const [showPassword, setShowPassword] = useState(false)
  const [showPassword2, setShowPassword2] = useState(false)

  const handleClick = () => {
    setShowPassword((prev) => !prev)
  }
  const handleClick2 = () => {
    setShowPassword2((prev) => !prev)
  }
  //   const [submitting, setSubmitting] = useState(false)
  const handleSubmit = async (e: { preventDefault: () => void }) => {
    e.preventDefault()
    if (formData.password != formData.password2) {
      toast.error(t('re-enter-password-message'), {
        position: 'bottom-right',
      })
      return
    }
    // const { username, email, password } = formData
    try {
      const res = await axios.post('/user/signup/super_staff', formData)
      const { data } = res
      if (data) {
        showSuccess()
        await new Promise((r) => setTimeout(r, 3000))
        router.push('/superstafflist')
      }
    } catch (err) {
      // console.log(err.response.data)
      switch (err.response.status) {
        case 400:
          if (err.response.data.msg == 'invalid-username-length') {
            toast.error(t('username-error'), {
              position: 'bottom-right',
            })
            break
          } else if (err.response.data.msg == 'invalid email') {
            toast.error(t('email-error'), {
              position: 'bottom-right',
            })
            break
          } else if (err.response.data.msg == 'invalid-password-length') {
            toast.error(t('password-error'), {
              position: 'bottom-right',
            })
            break
          } else {
            toast.error(t('error-without-status'), {
              position: 'bottom-right',
            })
            break
          }
        case 409:
          show409Error()
          break
        default:
          showError()
          break
      }
    }
  }

  return (
    <>
      {!session && !loading && (
        <meta httpEquiv="refresh" content="0; /signin" />
      )}
      {session && session.level != 0 && (
        <meta httpEquiv="refresh" content="0; /levelerror" />
      )}
      {session && session.level == 0 && (
        <Layout>
          <Box
            flex={1}
            display="flex"
            position="relative"
            flexDirection="column"
            justifyContent="center"
          >
            <Box
              display="flex"
              flexDirection="column"
              justifyContent="center"
              alignItems="center"
            >
              <Card className={classes.cardRoot}>
                <Box flex={1} display="flex" flexDirection="column">
                  <Box
                    component="h2"
                    mb={{ xs: 3, xl: 6 }}
                    color="text.primary"
                    fontWeight={Fonts.REGULAR}
                    fontSize={{ xs: 24, xl: 26 }}
                  >
                    <Button
                      startIcon={<ArrowBackIosIcon />}
                      href={`/${router.locale}/superstafflist`}
                      className={classes.back}
                    >
                      {t('back')}
                    </Button>
                    <br />
                    <br />
                    <Typography component="h1" variant="h4" gutterBottom>
                      {t('add-super-staff')}
                    </Typography>
                  </Box>

                  <Box
                    px={{ xs: 6, sm: 10, xl: 15 }}
                    pt={8}
                    flex={1}
                    display="flex"
                    flexDirection="column"
                  >
                    <form className={classes.formRoot} onSubmit={handleSubmit}>
                      {/* <input name="csrfToken" type="hidden" /> */}
                      <TextField
                        margin="normal"
                        required
                        fullWidth
                        id="username"
                        label={t('username')}
                        name="Username"
                        autoComplete="username"
                        autoFocus
                        defaultValue={formData.username}
                        error={
                          formData.username.length < 8 &&
                          formData.username.length > 0
                        }
                        helperText={
                          formData.username.length < 8 ? (
                            t('username-error')
                          ) : (
                            <CheckCircleOutlineOutlinedIcon
                              fontSize="small"
                              color="primary"
                            />
                          )
                        }
                        onChange={(e) =>
                          setFormData({ ...formData, username: e.target.value })
                        }
                      />
                      <TextField
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        label={t('email')}
                        type="email"
                        name="email"
                        autoComplete="email"
                        error={
                          formData.email.length > 0 &&
                          !/\S+@\S+\.\S+/.test(formData.email)
                        }
                        helperText={
                          !/\S+@\S+\.\S+/.test(formData.email) ? (
                            t('email-error')
                          ) : (
                            <CheckCircleOutlineOutlinedIcon
                              color="primary"
                              fontSize="small"
                            />
                          )
                        }
                        defaultValue={formData.email}
                        onChange={(e) =>
                          setFormData({ ...formData, email: e.target.value })
                        }
                      />
                      <TextField
                        InputProps={{
                          endAdornment: (
                            <InputAdornment position="end">
                              <IconButton onClick={handleClick} edge="end">
                                {showPassword ? (
                                  <Visibility />
                                ) : (
                                  <VisibilityOff />
                                )}
                              </IconButton>
                            </InputAdornment>
                          ),
                        }}
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label={t('password')}
                        type={showPassword ? 'text' : 'password'}
                        id="password"
                        autoComplete="new-password"
                        error={
                          formData.password.length < 8 &&
                          formData.password.length > 0
                        }
                        helperText={
                          formData.password.length < 8 ? (
                            t('password-error')
                          ) : (
                            <CheckCircleOutlineOutlinedIcon
                              fontSize="small"
                              color="primary"
                            />
                          )
                        }
                        defaultValue={formData.password}
                        onChange={(e) =>
                          setFormData({ ...formData, password: e.target.value })
                        }
                      />
                      <TextField
                        InputProps={{
                          endAdornment: (
                            <InputAdornment position="end">
                              <IconButton onClick={handleClick2} edge="end">
                                {showPassword2 ? (
                                  <Visibility />
                                ) : (
                                  <VisibilityOff />
                                )}
                              </IconButton>
                            </InputAdornment>
                          ),
                        }}
                        name="password2"
                        required
                        fullWidth
                        label={t('re-enter-password')}
                        type={showPassword2 ? 'text' : 'password'}
                        error={
                          formData.password !== formData.password2 &&
                          formData.password2.length > 0
                        }
                        helperText={
                          formData.password == formData.password2 &&
                          formData.password2.length > 0 ? (
                            <CheckCircleOutlineOutlinedIcon
                              fontSize="small"
                              color="primary"
                            />
                          ) : (
                            t('re-enter-password-message')
                          )
                        }
                        defaultValue={formData.password2}
                        onChange={(e) =>
                          setFormData({
                            ...formData,
                            password2: e.target.value,
                          })
                        }
                      />
                      <Box
                        mb={{ xs: 3, xl: 4 }}
                        display="flex"
                        flexDirection={{ xs: 'row', sm: 'row-reverse' }}
                        alignItems={{ sm: 'center' }}
                        justifyContent={{ sm: 'space-between' }}
                        fontSize={15}
                      />
                      <Box
                        mb={6}
                        display="flex"
                        flexDirection={{ xs: 'column', sm: 'row' }}
                        alignItems={{ sm: 'center' }}
                        justifyContent={{ sm: 'space-between' }}
                      >
                        <Button
                          variant="contained"
                          color="secondary"
                          type="submit"
                          fullWidth
                          // className={classes.btnRoot}
                          // onClick={handleSubmit}
                        >
                          {t('create')}
                        </Button>
                        {/* <Button
                    variant="contained"
                    color="secondary"
                    type="submit"
                    className={classes.btnRoot}
                    href="/signin"
                  >
                    {t('back')}
                  </Button> */}
                      </Box>
                    </form>
                    <ToastContainer />
                  </Box>
                </Box>
              </Card>
            </Box>
          </Box>
        </Layout>
      )}
    </>
  )
}

export const getStaticProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
})

export default AddSuperStaff
