import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Typography, Button, TextField, Paper, Box } from '@material-ui/core'
import axios from 'axios'
import { useTranslation } from 'next-i18next'
import { IUser } from '@/server/api/User/user.model'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'
import Layout from '../../components/layouts/navLayout'
import { useSession } from 'next-auth/client'
import { useRouter } from 'next/router'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

const useStyles = makeStyles((theme) => ({
  layout: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '100%',
    maxWidth: '900px',
    margin: '0 auto',
  },
  paper: {
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(8),
      padding: `${theme.spacing(6)}px ${theme.spacing(4)}px`,
    },
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  buttonProgress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
  textField: {
    width: '48%',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  topButton: {
    float: 'right',
    position: 'relative',
  },
  margin: {
    margin: theme.spacing(1),
    marginTop: theme.spacing(1),
  },
  title: {
    marginTop: theme.spacing(3),
    flex: '1 1 100%',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    fontSize: 30,
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
}))

export default function SuperStaffEdit() {
  const router = useRouter()
  const classes = useStyles({})
  const { t } = useTranslation()
  const [formData, setFormData] = useState<IUser>()
  const [session, loading] = useSession()

  useEffect(() => {
    async function fetchData() {
      if (!session) {
        return
      } else {
        await axios
          .get(`/user/${router.query.id}`)
          .then((res) => {
            setFormData(() => res.data.user)
          })
          .catch(() => {
            return
          })
      }
    }
    fetchData()
  }, [session, router.query])

  const showError = () =>
    toast.error(t('error'), {
      position: 'bottom-right',
    })
  const showSuccess = () =>
    toast.success(t('update-success'), {
      position: 'bottom-right',
    })

  const handleChange = (e: { target: { name: string; value: string } }) => {
    if (formData) {
      setFormData({ ...formData, [e.target.name]: e.target.value })
    } else {
      toast.error(t('error-changing-data'), {
        position: 'bottom-right',
      })
    }
  }

  const handleSubmit = async (e: { preventDefault: () => void }) => {
    // console.log(formData)
    e.preventDefault()
    let res
    try {
      res = await axios.put(`/user/change/${router.query.id}`, formData)
    } catch (err) {
      if (err) {
        showError()
      }
    }
    if (res?.status == 200) {
      showSuccess()
      await new Promise((r) => setTimeout(r, 3000))
      router.push(`/superstafflist`)
    }
  }

  return (
    <>
      {!session && !loading && (
        <meta httpEquiv="refresh" content="0; /signin" />
      )}
      {session && session.level != 0 && (
        <meta httpEquiv="refresh" content="0; /levelerror" />
      )}
      {session && session.level == 0 && formData && (
        <>
          <Layout>
            <div className={classes.layout}>
              <Paper className={classes.paper} elevation={2}>
                <Button
                  startIcon={<ArrowBackIosIcon />}
                  href={`/${router.locale}/superstafflist`}
                >
                  {t('back')}
                </Button>
                <br></br>
                <Box
                  display="flex"
                  alignItems="center"
                  justifyContent="center"
                  flexDirection="column"
                  className={classes.title}
                >
                  <Typography component="h1" variant="h4" gutterBottom>
                    {t('staff-info')}
                  </Typography>
                </Box>
                <br></br>
                <form className={classes.form} onSubmit={handleSubmit}>
                  <TextField
                    margin="normal"
                    variant="outlined"
                    required
                    fullWidth
                    id="username"
                    label={t('username')}
                    name="username"
                    autoComplete="username"
                    className={classes.margin}
                    defaultValue={formData.username}
                    onChange={handleChange}
                  />
                  <TextField
                    margin="normal"
                    variant="outlined"
                    required
                    fullWidth
                    id="email"
                    label={t('email')}
                    name="email"
                    autoComplete="email"
                    className={classes.margin}
                    defaultValue={formData.email}
                    onChange={handleChange}
                  />
                  <Box mb={6} textAlign="center">
                    <Button
                      // disabled={submitting}
                      size="large"
                      type="submit"
                      variant="contained"
                      color="primary"
                      className={classes.submit}
                      // onClick={update}
                    >
                      {/* {submitting && (
                  <CircularProgress
                    size={24}
                    className={classes.buttonProgress}
                  />
                )} */}
                      {t('update')}
                    </Button>
                  </Box>
                </form>
                <ToastContainer />
              </Paper>
            </div>
          </Layout>
        </>
      )}
    </>
  )
}

export const getStaticProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
})

export const getStaticPaths = async () => {
  return {
    paths: [],
    fallback: 'blocking',
  }
}

// export const getServerSideProps: GetServerSideProps = async (context) => {
//   const { id } = context.query
//   const res = await axios.get(`/invoice/${id}`)
//   return { props: { note: res.data.invoice } }
// }
