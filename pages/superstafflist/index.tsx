import React, { useState } from 'react'
import clsx from 'clsx'
import {
  DataGrid,
  GridColDef,
  GridPageChangeParams,
  GridRowId,
  GridSelectionModelChangeParams,
  GridToolbarContainer,
  GridToolbarColumnsButton,
  GridToolbarFilterButton,
  GridToolbarExport,
  GridToolbarDensitySelector,
} from '@material-ui/data-grid'
import { Toolbar, Typography, IconButton } from '@material-ui/core'
import Button from '@material-ui/core/Button'
import DeleteIcon from '@material-ui/icons/Delete'
import AddCircleIcon from '@material-ui/icons/AddCircle'
import { createStyles, makeStyles } from '@material-ui/core/styles'
import { Theme } from '@material-ui/core/styles'
import Layout from '../../components/layouts/navLayout'
import { useTranslation } from 'next-i18next'
import axios from 'axios'
import { IUser } from '../../server/api/User/user.model'
import router from 'next/router'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import DehazeIcon from '@material-ui/icons/Dehaze'
import { useSession } from 'next-auth/client'
import { useEffect } from 'react'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
// import { lighten } from '@material-ui/core/styles'
// import { vendor_options } from '@/components/elements/QuotationPdf/CommonProps'
// import EditIcon from '@material-ui/icons/Edit'
// import { TextField } from '@material-ui/core'
// import { PagesSharp, Search } from '@material-ui/icons'

const useToolbarStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(1),
    },
    highlight:
      theme.palette.type === 'light'
        ? {
            color: '#004369',
            backgroundColor: '#d8e9ef',
            margin: theme.spacing(1),
          }
        : {
            color: theme.palette.text.primary,
            backgroundColor: theme.palette.secondary.dark,
          },
    title: {
      flex: '1 1 100%',
      alignItems: 'center',
      justifyContent: 'center',
      textAlign: 'center',
      fontSize: 50,
      [theme.breakpoints.down('xs')]: {
        fontSize: 40,
      },
      fontFamily: 'Sintony, sans-serif',
    },
    subtitle: {
      flex: '1 1 100%',
      fontSize: 20,
      [theme.breakpoints.down('sm')]: {
        fontSize: 15,
      },
    },
    button: {
      margin: theme.spacing(1),
      maxWidth: 200,
      width: '100%',
      backgroundColor: '#e85a71',
      color: 'white',
    },
    addButton: {
      margin: theme.spacing(1),
      maxWidth: 200,
      backgroundColor: '#4ea1d3',
      color: 'white',
      width: '100%',
    },
    input: {
      marginLeft: theme.spacing(1),
      flex: 1,
    },
    iconButton: {
      padding: 10,
    },
    divider: {
      height: 28,
      margin: 4,
    },
    sectionDesktop: {
      display: 'none',
      [theme.breakpoints.up('md')]: {
        display: 'flex',
      },
    },
    sectionMobile: {
      display: 'flex',
      [theme.breakpoints.up('md')]: {
        display: 'none',
      },
    },
  }),
)

// const StyledDataGrid = withStyles({
//   root: {
//     '& .MuiDataGrid-window': {
//       overflowY: 'visible !important',
//     },
//     '& .MuiDataGrid-columnsHeader': {
//       alignItems: 'center',
//     },
//     '& .MuiDataGrid-row': {
//       maxHeight: 'none !important',
//     },
//     '& .MuiDataGrid-viewport': {
//       maxHeight: 'max-content !important',
//     },
//     '& .MuiDataGrid-cell': {
//       lineHeight: 'unset !important',
//       verticalAlign: 'middle',
//       position: 'relative',
//       display: 'flex !important',
//       alignItems: 'center',
//       maxHeight: 'none !important',
//       whiteSpace: 'normal !important',
//       overflowWrap: 'break-word',
//     },
//   },
// })(DataGrid)

interface EnhancedTableToolbarProps {
  numSelected: number
  onDeleteClick: () => void
  textFieldData: { filteredData: string }
  setTextFieldData: React.Dispatch<
    React.SetStateAction<{
      filteredData: string
    }>
  >
}

const EnhancedTableToolbar = (props: EnhancedTableToolbarProps) => {
  const classes = useToolbarStyles()
  const { t } = useTranslation()
  const { numSelected } = props
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  return (
    <>
      <Toolbar className={clsx(classes.root)}>
        <Typography className={classes.title} id="tableTitle" component="div">
          {t('staff-list')}
        </Typography>
      </Toolbar>
      {numSelected > 0 ? (
        <>
          <Toolbar
            className={clsx(classes.root, classes.sectionDesktop, {
              [classes.highlight]: numSelected > 0,
            })}
          >
            <Typography
              className={classes.subtitle}
              color="inherit"
              variant="subtitle1"
              component="div"
            >
              {t('selected')} {numSelected}
            </Typography>
            <Button
              variant="contained"
              color="secondary"
              aria-label="delete"
              className={classes.button}
              onClick={props.onDeleteClick}
              startIcon={<DeleteIcon />}
            >
              {t('delete')}
            </Button>
          </Toolbar>
          <Toolbar
            className={clsx(classes.root, classes.sectionMobile, {
              [classes.highlight]: numSelected > 0,
            })}
          >
            <Typography
              className={classes.subtitle}
              color="inherit"
              variant="subtitle1"
              component="div"
            >
              {t('selected')} {numSelected}
            </Typography>
            <IconButton
              aria-controls="action-menu"
              aria-haspopup="true"
              onClick={handleClick}
              color="primary"
            >
              <MoreVertIcon />
            </IconButton>
            <Menu
              id="action-menu"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleClose}
              getContentAnchorEl={null}
              anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
              transformOrigin={{ vertical: 'top', horizontal: 'center' }}
            >
              <MenuItem onClick={props.onDeleteClick}>
                <ListItemIcon>
                  <DeleteIcon fontSize="small" />
                </ListItemIcon>
                <ListItemText primary={t('delete')} />
              </MenuItem>
            </Menu>
          </Toolbar>
        </>
      ) : (
        <></>
      )}
    </>
  )
}

// export default function QuotationList({ list }: ITableProps) {
export default function QuotationList() {
  const [session, loading] = useSession()
  const { t } = useTranslation()
  const [renderedData, setRenderedData] = useState<IUser[]>([])

  useEffect(() => {
    async function fetchData() {
      if (!session) {
        return
      }
      const res = await axios.get('/user')
      res.data.users.map((user: IUser) => {
        user.id = user._id
      })

      const filtered = res.data.users.filter((user: IUser) => user.level == 1)

      setRenderedData(() => filtered)
    }
    fetchData()
  }, [session])

  // const [page, setPage] = React.useState(0)
  const classes = useToolbarStyles()
  const [selected, setSelected] = useState<GridRowId[]>([])
  const [formData, setFormData] = useState({ filteredData: '' })
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
  const [page, setPage] = React.useState(0)
  const [pageSize, setPageSize] = useState(5)

  const columns: GridColDef[] = [
    { field: 'username', headerName: t('username'), width: 250 },
    { field: 'email', headerName: t('email'), width: 300 },
  ]

  const handlePageSizeChange = async (param: GridPageChangeParams) => {
    setPageSize(param.pageSize)
  }

  const handleChangePage = (param: GridPageChangeParams) => {
    setPage(param.page)
  }

  const handleSelectionChange = (params: GridSelectionModelChangeParams) => {
    setSelected(params.selectionModel)
  }

  const updateRenderedData = async () => {
    const res = await axios.get('/user/')
    res.data.users.map((user: IUser) => {
      user.id = user._id
    })
    const filtered = res.data.users.filter((user: IUser) => user.level == 1)
    setRenderedData(filtered)
    setSelected([])
  }

  const addButtonHandler = async () => {
    router.push(`/superstafflist/add`)
  }

  // const editButtonHandler = async () => {
  //   selected.map((item: GridRowId) => {
  //     router.push(`/superstafflist/${item}`)
  //   })
  // }

  const deleteButtonHandler = async () => {
    const decision = window.confirm(t('delete-alert'))
    if (!decision) {
      return
    }
    selected.map(async (item: GridRowId) => {
      await axios.delete(`/user/delete/${item}`)
      updateRenderedData()
    })
  }

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  const buttonStyles = makeStyles((theme: Theme) =>
    createStyles({
      button: {
        margin: theme.spacing(1),
        padding: theme.spacing(1),
      },
      sectionDesktop: {
        display: 'none',
        [theme.breakpoints.up('md')]: {
          display: 'flex',
        },
      },
      sectionMobile: {
        display: 'flex',
        [theme.breakpoints.up('md')]: {
          display: 'none',
        },
      },
    }),
  )

  function CustomToolbar() {
    const classes = buttonStyles()
    return (
      <>
        <div className={classes.sectionDesktop}>
          <GridToolbarContainer>
            <GridToolbarColumnsButton />
            <GridToolbarFilterButton />
            <GridToolbarDensitySelector />
            <GridToolbarExport />
          </GridToolbarContainer>
        </div>
        <div className={classes.sectionMobile}>
          <Button
            aria-controls="grid-menu"
            aria-haspopup="true"
            onClick={handleClick}
            variant="contained"
            color="primary"
            startIcon={<DehazeIcon />}
            className={classes.button}
          >
            {t('filter')}
          </Button>
          <Menu
            id="grid-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
            getContentAnchorEl={null}
            anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
            transformOrigin={{ vertical: 'top', horizontal: 'center' }}
          >
            <MenuItem onClick={handleClose}>
              <GridToolbarColumnsButton />
            </MenuItem>
            <MenuItem onClick={handleClose}>
              <GridToolbarFilterButton />
            </MenuItem>
            <MenuItem>
              <GridToolbarDensitySelector />
            </MenuItem>
            <MenuItem>
              <GridToolbarExport />
            </MenuItem>
          </Menu>
        </div>
      </>
    )
  }

  return (
    <>
      {!session && !loading && (
        <meta httpEquiv="refresh" content="0; /signin" />
      )}
      {session && session.level != 0 && (
        <meta httpEquiv="refresh" content="0; /levelerror" />
      )}
      {session && session.level == 0 && (
        <>
          <Layout>
            <div style={{ height: 400, width: '100%' }}>
              <EnhancedTableToolbar
                numSelected={selected.length}
                onDeleteClick={deleteButtonHandler}
                textFieldData={formData}
                setTextFieldData={setFormData}
              />
              <Button
                variant="contained"
                color="primary"
                className={classes.addButton}
                startIcon={<AddCircleIcon />}
                onClick={addButtonHandler}
              >
                {t('add-staff')}
              </Button>
              <DataGrid
                autoHeight
                rows={renderedData}
                columns={columns}
                pageSize={pageSize}
                page={page}
                onPageSizeChange={handlePageSizeChange}
                onPageChange={handleChangePage}
                checkboxSelection={true}
                rowsPerPageOptions={[5, 10, 15, 20, 25]}
                onSelectionModelChange={handleSelectionChange}
                disableSelectionOnClick
                components={{
                  Toolbar: CustomToolbar,
                }}
                disableColumnMenu
              />
            </div>
          </Layout>
        </>
      )}
    </>
  )
}

export const getStaticProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
})
