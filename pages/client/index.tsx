// just a placeholder for building, must export default something
const placeholder = () => {
  return (
    <>
      <p>client</p>
    </>
  )
}

export default placeholder

// import React from 'react'
// import clsx from 'clsx'
// import {
//   DataGrid,
//   GridColDef,
//   GridPageChangeParams,
//   GridRowId,
//   GridSelectionModelChangeParams,
//   GridToolbarContainer,
//   GridToolbarColumnsButton,
//   GridToolbarFilterButton,
//   GridToolbarExport,
//   GridToolbarDensitySelector,
// } from '@material-ui/data-grid'
// import Menu from '@material-ui/core/Menu';
// import MenuItem from '@material-ui/core/MenuItem'
// import Layout from '../../components/layouts/navLayout'
// import { Toolbar, Typography } from '@material-ui/core'
// import Button from '@material-ui/core/Button'
// import DeleteIcon from '@material-ui/icons/Delete'
// import EditIcon from '@material-ui/icons/Edit'
// import { createStyles, makeStyles } from '@material-ui/core/styles'
// import { Theme } from '@material-ui/core/styles'
// import { lighten } from '@material-ui/core/styles'
// import axios from 'axios'
// import { IRequestHistory } from '../../server/api/RequestHistory/requesthistory.model'
// import router from 'next/router'
// import { GetServerSideProps } from 'next'
// import DehazeIcon from '@material-ui/icons/Dehaze'
// import ListIcon from '@material-ui/icons/List';
// import ListItemIcon from '@material-ui/core/ListItemIcon'
// import ListItemText from '@material-ui/core/ListItemText'
// import { useSession } from 'next-auth/client'
// import ArchiveIcon from '@material-ui/icons/Archive'
// import FolderIcon from '@material-ui/icons/Folder'

// const useToolbarStyles = makeStyles((theme: Theme) =>
//   createStyles({
//     root: {
//       paddingLeft: theme.spacing(2),
//       paddingRight: theme.spacing(1),
//       [theme.breakpoints.down('sm')]: {
//         display: 'block',
//       }
//     },
//     highlight:
//       theme.palette.type === 'light'
//         ? {
//             color: theme.palette.secondary.main,
//             backgroundColor: lighten(theme.palette.secondary.light, 0.85),
//             margin: theme.spacing(1),
//           }
//         : {
//             color: theme.palette.text.primary,
//             backgroundColor: theme.palette.secondary.dark,
//           },
//     title: {
//       flex: '1 1 100%',
//       alignItems: 'center',
//       justifyContent: 'center',
//       textAlign: 'center',
//       fontSize: 50,
//       [theme.breakpoints.down('xs')]: {
//         fontSize: 40,
//       }
//     },
//     subtitle: {
//       flex: '1 1 100%',
//       fontSize: 20,
//       [theme.breakpoints.down('sm')]: {
//         fontSize: 15,
//       }
//     },
//     buttonMarigin: {
//       margin: theme.spacing(1),
//     },
//     button: {
//       margin: theme.spacing(1),
//       maxWidth: 120,
//       width: '100%',
//     },
//     mobileButton: {
//       margin: theme.spacing(1),
//     },
//     input: {
//       marginLeft: theme.spacing(1),
//       flex: 1,
//     },
//     iconButton: {
//       padding: 10,
//     },
//     sectionDesktop: {
//       display: 'none',
//       [theme.breakpoints.up('md')]: {
//         display: 'flex',
//       },
//     },
//     sectionMobile: {
//       display: 'flex',
//       [theme.breakpoints.up('md')]: {
//         display: 'none',
//       },
//     },
//   }),
// )

// interface EnhancedTableToolbarProps {
//   numSelected: number
//   onArchiveclick: () => void
//   onViewArchiveClick: () => void
// }

// const EnhancedTableToolbar = (props: EnhancedTableToolbarProps) => {
//   const classes = useToolbarStyles()
//   const { numSelected } = props
//   const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
//   const handleClick = (event: React.MouseEvent<HTMLElement>) => {
//     setAnchorEl(event.currentTarget);
//   };

//   const handleClose = () => {
//     setAnchorEl(null);
//   };

//   return (
//     <>
//       <Toolbar className={clsx(classes.root)}>
//         <Typography
//           className={classes.title}
//           id="tableTitle"
//           component="div"
//         >
//           Client List
//         </Typography>
//       </Toolbar>
//       {numSelected > 0 ? (
//         <Toolbar
//           className={clsx(classes.buttonMarigin,{
//             [classes.highlight]: numSelected > 0,
//           })}
//         >
//           <Typography
//             className={classes.subtitle}
//             color="inherit"
//             component="div"
//           >
//             {numSelected} selected
//           </Typography>
//           <Button
//             variant="contained"
//             aria-label="archive"
//             color="secondary"
//             className={classes.button}
//             onClick={props.onArchiveclick}
//             startIcon={<ArchiveIcon />}
//           >
//             Archive
//           </Button>
//         </Toolbar>
//       ) : (
//         <></>
//       )}
//     </>
//   )
// }

// interface ITableProps {
//   list: IRequestHistory[]
// }

// export default function Auth({ list }: ITableProps) {
//   const classes = useToolbarStyles()
//   const [page, setPage] = React.useState(0)
//   const [selected, setSelected] = React.useState<GridRowId[]>([])
//   const [formData, setFormData] = React.useState({ filteredData: '' })
//   const [renderedData, setRenderedData] = React.useState<IRequestHistory[]>(list)
//   // const [hideIRequestHistoryId, setHideIRequestHistoryId] = React.useState(false)
//   const [pageSize, setPageSize] = React.useState(5)
//   const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
//   const [session, loading] = useSession()

//   const columns: GridColDef[] = [
//     { field: '_id', headerName: 'id', width: 250 },
//     { field: 'requester', headerName: 'Requester', width: 250,
//       valueGetter: (params) => {
//         return params.row.requester.username
//       },
//     },
//     { field: 'service', headerName: 'Service', width: 250,
//       valueGetter: (params) => {
//         return params.row.service.service_name
//       },
//     },
//     { field: 'request_date', headerName: 'Request date', width: 250,
//       valueGetter: (params) => {
//         if (params.row.request_date) {
//           return params.row.request_date.toString().split('T')[0]
//         } else return params.row.request_date
//       },
//     },
//     { field: 'status', headerName: 'Status', width: 250 },
//   ]

//   const handlePagSizeChange = async (param: GridPageChangeParams) => {
//     setPageSize(param.pageSize)
//   }

//   const handleSelectionChange = (params: GridSelectionModelChangeParams) => {
//     setSelected(params.selectionModel)
//   }

//   const updateRenderedData = async () => {
//     const res = await axios.get('/requesthistory/')
//     res.data.requesthistories.map((requesthistory: IRequestHistory) => {
//       requesthistory.id = requesthistory._id
//     })
//     setRenderedData(res.data.requesthistories)
//     setSelected([])
//   }

//   const handleChangePage = (param: GridPageChangeParams) => {
//     setPage(param.rowCount)
//   }

//   const archiveButtonHandler = async () => {
//     const decision = window.confirm('Are you sure to archive all selected data?')
//     if (!decision) {
//       return
//     }
//     selected.map(async (item: GridRowId) => {
//       await axios.delete(`/requesthistory/${item}`)
//       updateRenderedData()
//     })
//   }

//   const viewArchiveButtonHandler = async () => {
//     const decision = window.confirm('Are you sure to delete all selected data?')
//     if (!decision) {
//       return
//     }
//     selected.map(async (item: GridRowId) => {
//       await axios.delete(`/requesthistory/${item}`)
//       updateRenderedData()
//     })
//   }

//   const handleClick = (event: React.MouseEvent<HTMLElement>) => {
//     setAnchorEl(event.currentTarget);
//   };

//   const handleClose = () => {
//     setAnchorEl(null);
//   };

//   const buttonStyles = makeStyles((theme: Theme) =>
//   createStyles({
//     button: {
//       margin: theme.spacing(1),
//       padding: theme.spacing(1),
//     },
//     sectionDesktop: {
//       display: 'none',
//       [theme.breakpoints.up('md')]: {
//         display: 'flex',
//       },
//     },
//     sectionMobile: {
//       display: 'flex',
//       [theme.breakpoints.up('md')]: {
//         display: 'none',
//       },
//     },
//   }))

//   function CustomToolbar() {
//     const classes = buttonStyles()
//     return (
//       <>
//         <div className={classes.sectionDesktop}>
//           <GridToolbarContainer>
//             <GridToolbarColumnsButton />
//             <GridToolbarFilterButton />
//             <GridToolbarDensitySelector />
//             <GridToolbarExport />
//           </GridToolbarContainer>
//         </div>
//         <div className={classes.sectionMobile}>
//           <Button
//             aria-controls="grid-menu"
//             aria-haspopup="true"
//             onClick={handleClick}
//             variant="contained"
//             color="primary"
//             startIcon={<DehazeIcon />}
//             className={classes.button}
//           >
//             Filter
//           </Button>
//           <Menu
//               id="grid-menu"
//               anchorEl={anchorEl}
//               keepMounted
//               open={Boolean(anchorEl)}
//               onClose={handleClose}
//               getContentAnchorEl={null}
//               anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
//               transformOrigin={{ vertical: "top", horizontal: "center" }}
//           >
//             <MenuItem onClick={handleClose}>
//               <GridToolbarColumnsButton />
//             </MenuItem>
//             <MenuItem onClick={handleClose}>
//               <GridToolbarFilterButton />
//             </MenuItem>
//             <MenuItem>
//               <GridToolbarDensitySelector/>
//             </MenuItem>
//             <MenuItem>
//               <GridToolbarExport />
//             </MenuItem>
//           </Menu>
//         </div>
//       </>
//     );
//   }

//   return (
//     <>
//       {!session && !loading && <meta http-equiv="refresh" content="0; /signin"/>}
//       {session && <>
//       <Layout>
//         <div style={{ height: 400, width: '100%' }}>
//           <EnhancedTableToolbar
//             numSelected={selected.length}
//             onArchiveclick={archiveButtonHandler}
//             onViewArchiveClick={viewArchiveButtonHandler}
//           />
//           <Button
//             variant="contained"
//             color="primary"
//             startIcon={<FolderIcon />}
//             onClick={viewArchiveButtonHandler}
//             className={classes.buttonMarigin}
//           >
//             View Archive
//           </Button>
//           <DataGrid
//             autoHeight
//             rows={renderedData}
//             columns={columns}
//             pageSize={pageSize}
//             onPageSizeChange={handlePagSizeChange}
//             onPageChange={handleChangePage}
//             checkboxSelection={true}
//             rowsPerPageOptions={[5, 10, 15, 20, 25]}
//             onSelectionModelChange={handleSelectionChange}
//             components={{Toolbar: CustomToolbar,}}
//             disableColumnMenu
//           />
//         </div>
//       </Layout>
//       </>}
//     </>
//     )
// }

// export const getServerSideProps: GetServerSideProps = async () => {
//   const res = await axios.get('/requesthistory', {
//     params: {
//       withFullInfo: true,
//     },
//   })
//   // console.log(res.data)
//   res.data.request_histories.map((requesthistory: IRequestHistory) => {
//     requesthistory.id = requesthistory._id
//   })
//   return { props: { list: res.data.request_histories } }
// }
