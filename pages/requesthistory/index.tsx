import React, { useEffect } from 'react'
import axios from 'axios'
import { IRequestHistory } from '../../server/api/RequestHistory/requesthistory.model'
import { useRouter } from 'next/router'
import { useSession } from 'next-auth/client'
import Layout from '../../components/layouts/navLayout'
import RequestHistoryList from '@/components/elements/RequestHistoryList'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

export default function RequestHistory() {
  const router = useRouter()
  const [renderedData, setRenderedData] = React.useState<IRequestHistory[]>()
  const [session, loading] = useSession()

  useEffect(() => {
    async function fetchData() {
      if (!session) {
        return
      }
      const res = await axios.get('/requesthistory', {
        params: {
          withFullInfo: true,
        },
      })
      res.data.request_histories.forEach((rh: IRequestHistory) => {
        rh.id = rh._id
      })
      setRenderedData(() => res.data.request_histories)
    }
    fetchData()
  }, [session])

  return (
    <Layout>
      {!session && !loading && (
        <meta httpEquiv="refresh" content="0; /signin" />
      )}
      {session && renderedData && (
        <RequestHistoryList
          requesthistorylist={renderedData}
          router={router}
          user_level={session.level}
        />
      )}
    </Layout>
  )
}

export const getStaticProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
})
