import React, { useEffect } from 'react'
import axios from 'axios'
import { useRouter } from 'next/router'
import { useSession } from 'next-auth/client'
import Layout from '@/components/layouts/navLayout'
import RequestedItemList from '@/components/elements/RequestedItemList'
import { IRequestedItemTableRow } from '@/components/interfaces/interfaces'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

// import clsx from 'clsx'
// import {
//   DataGrid,
//   GridColDef,
//   GridPageChangeParams,
//   GridRowId,
//   GridSelectionModelChangeParams,
//   GridToolbarContainer,
//   GridToolbarColumnsButton,
//   GridToolbarFilterButton,
//   GridToolbarExport,
//   GridToolbarDensitySelector,
// } from '@material-ui/data-grid'
// import { Toolbar, Typography } from '@material-ui/core'
// import Button from '@material-ui/core/Button'
// import DeleteIcon from '@material-ui/icons/Delete'
// import EditIcon from '@material-ui/icons/Edit'
// import { createStyles, makeStyles } from '@material-ui/core/styles'
// import { Theme } from '@material-ui/core/styles'
// import { lighten } from '@material-ui/core/styles'
// import { IRequestHistory } from '@/server/api/RequestHistory/requesthistory.model'
// import DehazeIcon from '@material-ui/icons/Dehaze'
// import Menu from '@material-ui/core/Menu'
// import MenuItem from '@material-ui/core/MenuItem'

export default function ItemList() {
  const router = useRouter()
  const [session, loading] = useSession()
  const [renderedData, setRenderedData] =
    React.useState<IRequestedItemTableRow[]>()

  useEffect(() => {
    async function fetchData() {
      if (!session) {
        return
      } else {
        const res = await axios.get(`/requesthistory/${router.query.id}`)
        const rows: IRequestedItemTableRow[] = []
        res.data.request_history.requested_items.map(
          (item: IRequestedItemTableRow, index: number) => {
            if (item.needed)
              rows.push({
                id: String(index),
                title: item.title,
                description: item.description,
                price: item.price,
                order: item.order,
                needed: item.needed,
              })
          },
        )
        // console.log(rows)
        setRenderedData(rows)
      }
    }
    fetchData()
  }, [router.query.id, session])

  return (
    <Layout>
      {!session && !loading && (
        <meta httpEquiv="refresh" content="0; /signin" />
      )}
      {session && renderedData && router && (
        <RequestedItemList list={renderedData} router={router} />
      )}
    </Layout>
  )
}

export const getStaticProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
})

export const getStaticPaths = async () => {
  return {
    paths: [],
    fallback: 'blocking',
  }
}
