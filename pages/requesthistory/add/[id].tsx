import React, { useEffect, useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Typography, Button, TextField, Paper, Box } from '@material-ui/core'
import axios from 'axios'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { useTranslation } from 'next-i18next'
import Layout from '../../../components/layouts/navLayout'
import clsx from 'clsx'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'
import { useSession } from 'next-auth/client'
import { useRouter } from 'next/router'
import { IRequestHistory } from '@/server/api/RequestHistory/requesthistory.model'
import ServiceItemForm, {
  ServiceItemFormUsage,
} from '@/components/elements/ServiceItemForm'
import { IServiceItem } from '@/server/api/Service/service.model'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

const useStyles = makeStyles((theme) => ({
  layout: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    maxWidth: '768px',
    margin: '0 auto',
  },
  paper: {
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(8),
      padding: `${theme.spacing(6)}px ${theme.spacing(4)}px`,
    },
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    width: '31%',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  buttonProgress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
  button: {
    display: 'block',
    marginTop: theme.spacing(2),
  },
  formControl: {
    // margin: theme.spacing(1),
    width: '100%',
  },
  list: {
    width: '100%',
  },
  listItem: {
    width: '100%',
  },
  accordian: {
    width: '100%',
  },
  textField: {
    width: '31%',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  margin: {
    margin: theme.spacing(1),
  },
}))

interface IFormData {
  quotation_items: IServiceItem[]
  request_history: string
  from: string
  client: string
  project_name: string
  quotation_number: string
  remark: string
  date: string
  invoice_number: string
  admin_modified: string
}

const Quotationadd = () => {
  const classes = useStyles()
  const { t } = useTranslation()
  const [session, loading] = useSession()
  const [curRequest, setCurRequest] = useState<IRequestHistory>()
  const router = useRouter()
  const request_history_id = router.query.id

  useEffect(() => {
    async function fetchData() {
      if (!session) return
      const res = await axios.get(`/requesthistory/${request_history_id}`, {
        params: { withFullInfo: true },
      })
      setCurRequest(res.data.request_history)
      setFormData({
        quotation_items: res.data.request_history.requested_items,
        request_history: res.data.request_history._id,
        from: res.data.request_history.company.company_name,
        client: res.data.request_history.company,
        project_name: '',
        quotation_number: '',
        remark: '',
        date: '',
        invoice_number:
          new Date().getFullYear().toString().substr(-2) +
          ('0' + (new Date().getMonth() + 1)).slice(-2) +
          ('0' + new Date().getDate()).slice(-2) +
          ('0' + new Date().getHours()).slice(-2) +
          ('0' + new Date().getMinutes()).slice(-2) +
          ('0000' + (Math.random() * (10000 - 1) + 1)).slice(-5),
        admin_modified: '',
      })
    }
    fetchData()
  }, [session, request_history_id])

  const showSuccess = () =>
    toast.success(t('create-success'), {
      position: 'bottom-right',
    })

  const [formData, setFormData] = useState<IFormData>()
  // event: React.ChangeEvent<{ value: string }>
  const handleChange = (e: { target: { name: string; value: string } }) => {
    if (formData) setFormData({ ...formData, [e.target.name]: e.target.value })
  }

  // const handleSubmit = async (e: { preventDefault: () => void }) => {
  //   console.log(formData)
  //   e.preventDefault()
  //   let res
  //   try {
  //     res = await axios.post('/invoice/', formData)
  //   } catch (err) {
  //     if (err) {
  //       showError()
  //     }
  //   }
  //   if (res?.status == 200) {
  //     showSuccess()
  //     await new Promise((r) => setTimeout(r, 3000))
  //     router.push(`/requesthistory`)
  //   }
  // }
  const handleSubmit = async (e: { preventDefault: () => void }) => {
    e.preventDefault()
    // console.log(formData)
    await axios
      .post('/invoice/', formData)
      .then(() => {
        showSuccess()
        setTimeout(() => {
          router.push(`/requesthistory`)
        }, 3000)
      })
      .catch((err) => {
        // console.log(err.response.data)
        switch (err.response.status) {
          case 400:
            // console.log(err.response.data)
            if (err.response.data.msg == 'please input a valid date') {
              toast.error(t('miss-date'), {
                position: 'bottom-right',
              })
              break
            } else if (
              err.response.data.msg == 'please input nonempty project name'
            ) {
              toast.error(t('miss-project-name'), {
                position: 'bottom-right',
              })
              break
            } else if (err.response.data.msg == 'invoice already exists') {
              toast.error(t('invoice-exist'), {
                position: 'bottom-right',
              })
              break
            } else {
              toast.error(t('error-without-status'), {
                position: 'bottom-right',
              })
              break
            }
          case 403: {
            toast.error(t('user-level-cannot-access'), {
              position: 'bottom-right',
            })
            break
          }
          case 409: {
            if (err.response.data.msg == 'conflict-item-title') {
              toast.error(t('409-service-item'), {
                position: 'bottom-right',
              })
              break
            } else {
              toast.error(t('error-without-status'), {
                position: 'bottom-right',
              })
              break
            }
          }
          case 500: {
            toast.error(t('error-without-status'), {
              position: 'bottom-right',
            })
            break
          }
          default:
            toast.error(t('error-without-status'), {
              position: 'bottom-right',
            })
            break
        }
      })
  }

  return (
    <>
      {!session && !loading && (
        <meta httpEquiv="refresh" content="0; /signin" />
      )}
      {session && curRequest && formData && (
        <>
          <Layout>
            <div className={classes.layout}>
              <Paper className={classes.paper} elevation={2}>
                <Button
                  startIcon={<ArrowBackIosIcon />}
                  href={`/${router.locale}/requesthistory`}
                >
                  {t('back')}
                </Button>
                <br />
                <br />
                <Box
                  display="flex"
                  alignItems="center"
                  justifyContent="center"
                  flexDirection="column"
                >
                  <Typography component="h1" variant="h4" gutterBottom>
                    {t('add-quotation')}
                  </Typography>
                </Box>
                <form className={classes.form} onSubmit={handleSubmit}>
                  <TextField
                    className={clsx(classes.textField, classes.margin)}
                    margin="normal"
                    required
                    disabled
                    fullWidth
                    id="invoice_number"
                    type="number"
                    label={t('invoice_number')}
                    name="invoice_number"
                    autoComplete="invoice_number"
                    defaultValue={formData.invoice_number}
                    onChange={handleChange}
                  />
                  <TextField
                    className={clsx(classes.textField, classes.margin)}
                    margin="normal"
                    required
                    disabled={session.level > 1}
                    fullWidth
                    id="from"
                    label={t('from')}
                    name="from"
                    autoComplete="from"
                    defaultValue={formData.from}
                    onChange={handleChange}
                  />
                  <TextField
                    className={clsx(classes.textField, classes.margin)}
                    margin="normal"
                    required
                    fullWidth
                    type="date"
                    id="date"
                    label={t('date')}
                    name="date"
                    autoComplete="date"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    onChange={handleChange}
                  />
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="project_name"
                    label={t('project_name')}
                    name="project_name"
                    autoComplete="project_name"
                    className={classes.margin}
                    onChange={handleChange}
                  />
                  <TextField
                    margin="normal"
                    fullWidth
                    variant="outlined"
                    name="remark"
                    label={t('remark')}
                    id="remark"
                    multiline
                    rows={10}
                    rowsMax={10}
                    autoComplete="remark"
                    className={classes.margin}
                    onChange={handleChange}
                  />
                  <ServiceItemForm
                    initialItems={curRequest.requested_items.filter(
                      (item) => item.needed,
                    )}
                    usage={ServiceItemFormUsage.QUOTATION_ITEMS}
                    onUpdate={(items: IServiceItem[]) => {
                      setFormData({
                        ...formData,
                        quotation_items: items,
                      })
                    }}
                  />
                  <Box mb={6} textAlign="center">
                    <Button
                      //   disabled={submitting}
                      type="submit"
                      fullWidth
                      variant="contained"
                      color="primary"
                      className={classes.submit}
                      // onClick={update}
                    >
                      {t('create')}
                    </Button>
                  </Box>
                </form>
                <ToastContainer />
              </Paper>
            </div>
          </Layout>
        </>
      )}
    </>
  )
}

export const getStaticProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
})

export const getStaticPaths = async () => {
  return {
    paths: [],
    fallback: 'blocking',
  }
}

// export const getServerSideProps: GetServerSideProps = async () => {
//   const res = await axios.get('/company')
//   res.data.companys.map((company: ICompany) => {
//     company.id = company._id
//   })
//   return { props: { list: res.data.companys } }
// }

export default Quotationadd
