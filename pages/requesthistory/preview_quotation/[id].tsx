import React from 'react'
import { IQuotationPdfProps } from '@/components/elements/QuotationPdf/CommonProps'
import { ICompany } from '@/server/api/Company/company.model'
import axios from 'axios'
import QuotationPdfGetter from '@/components/elements/QuotationPdf/QuotationPdfGetter'
import { useRouter } from 'next/router'
import { useState } from 'react'
import { IRequestHistory } from '@/server/api/RequestHistory/requesthistory.model'
import { IService, IServiceItem } from '@/server/api/Service/service.model'
import { IUser } from '@/server/api/User/user.model'

const PreviewQuotationPdf = () => {
  const router = useRouter()
  // const [session, loading] = useSession()
  const [quotation_props, set_quotation_props] = useState<IQuotationPdfProps>()
  const [width, setWidth] = React.useState(0)
  const [height, setHeight] = React.useState(0)
  React.useEffect(() => {
    async function fetchData() {
      if (!router.query.id) return
      try {
        const request_res = await axios.get(
          `/requesthistory/${router.query.id}`,
          { params: { withFullInfo: true, withVendorImages: true } },
        )
        const request_history_info: IRequestHistory =
          request_res.data.request_history
        let total_price = 0
        request_history_info.requested_items.forEach((item: IServiceItem) => {
          total_price = total_price + Number(item.price)
        })
        const company_info = request_history_info.company as ICompany
        const props: IQuotationPdfProps = {
          company_name: (request_history_info.requester as IUser)
            .normal_user_company_name,
          company_address: (request_history_info.requester as IUser)
            .normal_user_company_address,
          email: (request_history_info.requester as IUser).email,
          contact_person: (request_history_info.requester as IUser).username,
          project_name: (request_history_info.service as IService).service_name,
          quotation_items: request_history_info.requested_items,
          total_price: total_price,
          vendor: (request_history_info.company as ICompany).company_name,
          quotation_number: '',
          limited_info: request_res.data.user_level >= 4,
          vendor_image: company_info.image_data,
          chop_image: company_info.chop_image_data,
          signature_image: company_info.signature_image_data,
        }
        set_quotation_props(props)
      } catch (err) {
        // if (err.response) {
        //   // console.log(err.response)
        // } else if (err.request) {
        //   // console.log(err.request)
        // }
        // if (err.message) console.log(err.message)
      }
    }
    setWidth(window.innerWidth)
    setHeight(window.innerHeight)
    fetchData()
  }, [router.query.id])
  // const QuotationPdf = getQuotationPdf(props, width, height)
  return (
    <>
      {quotation_props && (
        <>
          <QuotationPdfGetter
            quotation_pdf_props={quotation_props}
            height={height}
            width={width}
          />
        </>
      )}
    </>
  )
}

export default PreviewQuotationPdf
