import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Box, Button, TextField, Card, Link } from '@material-ui/core'
import { signIn } from 'next-auth/client'
import { Fonts } from '@/components/constants/AppEnums'
import { CremaTheme } from '@/types/AppContextPropsType'
import { useTranslation } from 'next-i18next'
import HazedawnWhiteIcon from '@/components/icons/HazedawnWhite'
import { useRouter } from 'next/router'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

const useStyles = makeStyles((theme: CremaTheme) => ({
  cardRoot: {
    minWidth: '36rem',
    width: '100%',
    overflow: 'hidden',
    boxShadow: '0 10px 15px -3px rgba(0, 0, 0, 0.1)',
    textAlign: 'center',
    position: 'relative',
    paddingTop: 20,
    '&:before': {
      content: "''",
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      width: 130,
      height: 9,
      borderBottomRightRadius: 80,
      borderBottomLeftRadius: 80,
      marginRight: 'auto',
      marginLeft: 'auto',
      backgroundColor: theme.palette.primary.main,
    },
    [theme.breakpoints.down('xs')]: {
      minWidth: 'auto',
    },
  },
  textUppercase: {
    textTransform: 'uppercase',
  },
  formRoot: {
    textAlign: 'left',
  },
  myTextFieldRoot: {
    width: '100%',
  },
  checkboxRoot: {
    marginLeft: -12,
  },
  pointer: {
    cursor: 'pointer',
  },
  btnRoot: {
    // borderRadius: theme.overrides.MuiCard.root.borderRadius,
    width: '10rem',
    fontWeight: Fonts.REGULAR,
    fontSize: 16,
    textTransform: 'capitalize',
  },
  btnRootFull: {
    width: '100%',
  },
  textPrimary: {
    color: theme.palette.text.primary,
  },
  colorTextPrimary: {
    color: theme.palette.primary.main,
  },
  underlineNone: {
    textDecoration: 'none',
  },
  textGrey: {
    color: theme.palette.grey[500],
  },
}))

const SignIn = () => {
  const { t } = useTranslation()
  const classes = useStyles({})
  const router = useRouter()

  const showError = (msg: string) =>
    toast.error(msg, {
      position: 'bottom-right',
    })
  const showSuccess = () =>
    toast.success(t('login-success'), {
      position: 'bottom-right',
    })

  const [formData, setFormData] = useState({ username: '', password: '' })
  // const [isSubmitting, setIsSubmitting] = useState(false)

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault()
    const { username, password } = formData
    let res
    try {
      res = await signIn('credentials', {
        username,
        password,
        redirect: false,
      })
    } catch (err) {
      showError(err)
    }

    if (res?.error) showError(t('wrong-username-or-pw'))
    else {
      showSuccess()
      router.replace('/')
    }
  }

  return (
    <Box
      flex={1}
      display="flex"
      height="100vh"
      position="relative"
      bgcolor="#f3f4f6"
      flexDirection="column"
      justifyContent="center"
      alignItems="center"
      width="100%"
      margin="0 auto"
    >
      <Box textAlign="center">
        <HazedawnWhiteIcon size="140" />
      </Box>
      <Box
        display="flex"
        flexDirection="column"
        justifyContent="center"
        alignItems="center"
      >
        <Card className={classes.cardRoot}>
          <Box px={{ xs: 6, sm: 10 }}>
            <Box
              component="h2"
              mb={{ xs: 3 }}
              color="text.primary"
              fontWeight={Fonts.REGULAR}
              fontSize={{ xs: 24 }}
            >
              {t('login')}
            </Box>
          </Box>
          <Box flex={1} display="flex" flexDirection="column">
            <Box
              px={{ xs: 6, sm: 10 }}
              pt={6}
              flex={1}
              display="flex"
              flexDirection="column"
            >
              <form className={classes.formRoot} onSubmit={handleSubmit}>
                <Box mb={{ xs: 5 }}>
                  <TextField
                    placeholder={t('username') ?? 'Username'}
                    name="username"
                    label={t('username')}
                    variant="outlined"
                    className={classes.myTextFieldRoot}
                    type="text"
                    defaultValue={formData.username}
                    onChange={(e) =>
                      setFormData({ ...formData, username: e.target.value })
                    }
                  />
                </Box>

                <Box mb={{ xs: 5 }}>
                  <TextField
                    type="password"
                    placeholder={t('password') ?? 'Password'}
                    label={t('password')}
                    name="password"
                    variant="outlined"
                    className={classes.myTextFieldRoot}
                    defaultValue={formData.password}
                    onChange={(e) =>
                      setFormData({ ...formData, password: e.target.value })
                    }
                  />
                </Box>
                <Box mb={{ xs: 3 }} style={{ textAlign: 'center' }}>
                  <Button
                    variant="contained"
                    color="secondary"
                    type="submit"
                    // disabled={isSubmitting}
                    className={classes.btnRoot}
                  >
                    {t('login')}
                  </Button>
                </Box>

                <Box
                  mb={{ xs: 3 }}
                  display="flex"
                  flexDirection={{ xs: 'row', sm: 'row-reverse' }}
                  alignItems={{ sm: 'center' }}
                  justifyContent={{ sm: 'space-between' }}
                  fontSize={15}
                >
                  {/* <Box
                    color="primary.main"
                    component="span"
                    ml={{ sm: 4 }}
                    className={classes.pointer}
                    onClick={() => router.push('/forgetpassword')}
                    fontSize={15}
                  >
                    {t('Forget password ...')}
                  </Box> */}
                  <Box component="span">
                    <Link
                      href={`/${router.locale}/forgotpassword`}
                      className={`${classes.underlineNone} ${classes.colorTextPrimary}`}
                    >
                      {t('forget-password')}
                    </Link>
                  </Box>
                </Box>

                <Box
                  mb={6}
                  display="flex"
                  flexDirection={{ xs: 'column', sm: 'row' }}
                  alignItems={{ sm: 'center' }}
                  justifyContent={{ sm: 'space-between' }}
                >
                  <Box color="text.secondary" fontSize={15}>
                    <Box className={classes.textGrey} component="span" mr={2}>
                      {t('no-account')}
                    </Box>
                    <Box />
                    <Box component="span">
                      <Link
                        href={`/${router.locale}/companyregister`}
                        className={`${classes.underlineNone} ${classes.colorTextPrimary}`}
                      >
                        {t('register-as-company-admin')}
                      </Link>
                    </Box>
                    <Box />
                    <Box component="span">
                      <Link
                        href={`/${router.locale}/normalregister`}
                        className={`${classes.underlineNone} ${classes.colorTextPrimary}`}
                      >
                        {t('register-as-normal-user')}
                      </Link>
                    </Box>
                  </Box>
                </Box>
              </form>
              <ToastContainer />
            </Box>
          </Box>
        </Card>
      </Box>
    </Box>
  )
}

export const getStaticProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
})

export default SignIn
