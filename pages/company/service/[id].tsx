import React from 'react'
import axios from 'axios'
import { IService } from '../../../server/api/Service/service.model'
import { useRouter } from 'next/router'
import { useSession } from 'next-auth/client'
import { useEffect } from 'react'
import ServiceList from '@/components/elements/ServiceList'
import Layout from '@/components/layouts/navLayout'
import { IRequestHistory } from '@/server/api/RequestHistory/requesthistory.model'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

export default function Compservicelist() {
  const router = useRouter()
  const [renderedData, setRenderedData] = React.useState<IService[]>()
  const [request_history, set_request_history] =
    React.useState<IRequestHistory[]>()
  const [session, loading] = useSession()
  const vendor_id = router.query.id
  useEffect(() => {
    async function fetchData() {
      if (!session || !vendor_id) {
        return
      } else {
        const request_history_res = await axios.get(`requesthistory`)
        const user_request_histories: IRequestHistory[] =
          request_history_res.data.request_histories
        const filtered_requested_history: IRequestHistory[] = []
        const service_res = await axios.get(`/service/vendor/${vendor_id}`)
        service_res.data.services.forEach((service: IService) => {
          service.id = service._id
        })
        const service_ids: string[] = service_res.data.services.map(
          (service: IService) => service._id,
        )
        user_request_histories.forEach((history) => {
          if (
            service_ids.includes(history.service.toString()) &&
            (session.level as number) >= 4
          )
            filtered_requested_history.push(history)
        })
        setRenderedData(() => service_res.data.services)
        set_request_history(filtered_requested_history)
      }
    }
    fetchData()
  }, [vendor_id, session])

  return (
    <Layout>
      {!session && !loading && (
        <meta httpEquiv="refresh" content="0; /signin" />
      )}
      {session && renderedData && router && request_history && (
        <ServiceList
          company_id={router.query.id as string}
          user_level={session.level as number}
          user={session.user._id as string}
          router={router}
          servicelist={renderedData}
        />
      )}
    </Layout>
  )
}

export const getStaticProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
})

export const getStaticPaths = async () => {
  return {
    paths: [],
    fallback: 'blocking',
  }
}
