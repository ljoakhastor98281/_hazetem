import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import {
  Typography,
  Button,
  TextField,
  Paper,
  Box,
  Avatar,
  // FormControl,
  // InputLabel,
  // Select,
  // MenuItem,
} from '@material-ui/core'
import axios from 'axios'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { useTranslation } from 'next-i18next'
import Layout from '../../../../components/layouts/navLayout'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'
import { useRouter } from 'next/router'
import { useSession } from 'next-auth/client'
// import { IService } from '@/server/api/Service/service.model'
import { DropzoneDialog } from 'material-ui-dropzone'
import { toBase64 } from '@/server/utils/helper'
import { IServiceItem } from '@/server/api/Service/service.model'
import ServiceItemForm, {
  ServiceItemFormUsage,
} from '@/components/elements/ServiceItemForm'
// import { ICompany } from '@/server/api/Company/company.model'
// import clsx from 'clsx'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

const useStyles = makeStyles((theme) => ({
  layout: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    maxWidth: '768px',
    margin: '0 auto',
  },
  paper: {
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(8),
      padding: `${theme.spacing(6)}px ${theme.spacing(4)}px`,
    },
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    width: '31%',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  avatar: {
    width: theme.spacing(60),
    height: theme.spacing(40),
    // margin: theme.spacing(3),
    margin: 'auto',
    border: '1px solid rgba(0,0,0,0.2)',
    objectFit: 'contain',
  },
  // buttonProgress: {
  //   position: 'absolute',
  //   top: '50%',
  //   left: '50%',
  //   marginTop: -12,
  //   marginLeft: -12,
  // },
  // button: {
  //   display: 'block',
  //   marginTop: theme.spacing(2),
  // },
  // formControl: {
  //   // margin: theme.spacing(1),
  //   width: '100%',
  // },
  // textField: {
  //   width: '31%',
  //   [theme.breakpoints.down('sm')]: {
  //     width: '100%',
  //   },
  // },
  // margin: {
  //   margin: theme.spacing(3, 0, 2),
  // },
  card: {
    display: 'flex',
    height: '100%',
    // backgroundColor: cyan[100],
    width: '40%',
    // marginLeft: 330,
  },
  cardMedia: {
    width: 210,
    height: 297,
  },
  box: {
    display: 'flex',
    justifyContent: 'space-between',
    marginTop: theme.spacing(2),
  },
}))

interface IFormData {
  description: string
  service_name: string
  vendor: string
  service_items: IServiceItem[]
  image_data: string
}

export default function ServiceAdd() {
  const router = useRouter()
  const [session, loading] = useSession()
  const [formData, setFormData] = useState<IFormData>()

  useEffect(() => {
    async function fetchData() {
      if (!session) return
      setFormData({
        description: '',
        service_name: '',
        vendor: router.query.id as string,
        service_items: [],
        image_data: '',
      })
    }
    fetchData()
  }, [router.query.id, session])

  const classes = useStyles()
  const { t } = useTranslation()
  const [open, setOpen] = useState(false)
  const handleOpen = async () => {
    setOpen(true)
  }

  const handleClose = async () => {
    setOpen(false)
  }

  const handleSave = async (files: File[]) => {
    if (!formData) return
    setFormData({
      ...formData,
      image_data: (await toBase64(files[0])) as string,
    })
    setOpen(false)
  }

  const showError = () =>
    toast.error(t('create-fail'), {
      position: 'bottom-right',
    })
  const showSuccess = () =>
    toast.success(t('create-success'), {
      position: 'bottom-right',
    })

  const handleChange = (e: { target: { name: string; value: string } }) => {
    if (!formData) return
    setFormData({ ...formData, [e.target.name]: e.target.value })
  }

  const handleSubmit = async (e: { preventDefault: () => void }) => {
    e.preventDefault()
    // console.log(formData)
    let res
    try {
      res = await axios.post('/service', { ...formData, needed: true })
    } catch (err) {
      if (err) {
        switch (err.response.status) {
          case 400:
            // console.log(err.response.data)
            if (err.response.data.msg == 'missing service items') {
              toast.error(t('miss-item'), {
                position: 'bottom-right',
              })
              break
            } else if (err.response.data.msg == 'missing service name') {
              toast.error(t('miss-service-name'), {
                position: 'bottom-right',
              })
              break
            } else if (err.response.data.msg == 'missing item title') {
              toast.error(t('miss-item-title'), {
                position: 'bottom-right',
              })
              break
            } else {
              toast.error(t('error-without-status'), {
                position: 'bottom-right',
              })
              break
            }
          case 403:
            toast.error(t('user-level-cannot-access'), {
              position: 'bottom-right',
            })
            break
          case 409:
            if (err.response.data.msg == 'conflict-service-name') {
              toast.error(t('409-service-name'), {
                position: 'bottom-right',
              })
              break
            } else if (err.response.data.msg == 'conflict-item-title') {
              toast.error(t('409-service-item'), {
                position: 'bottom-right',
              })
              break
            } else {
              toast.error(t('error-without-status'), {
                position: 'bottom-right',
              })
              break
            }
          default:
            showError()
            break
        }
      }
    }
    if (res?.status == 200) {
      showSuccess()
      await new Promise((r) => setTimeout(r, 3000))
      router.push(`/company/service/${router.query.id}`)
    }
  }
  return (
    <Layout>
      {!session && !loading && (
        <meta httpEquiv="refresh" content="0; /signin" />
      )}
      {session && session?.level > 3 && (
        <meta httpEquiv="refresh" content="0; /levelerror" />
      )}
      {session && formData && (
        <>
          <div className={classes.layout}>
            <Paper className={classes.paper} elevation={2}>
              <Button
                startIcon={<ArrowBackIosIcon />}
                href={`/${router.locale}/company`}
              >
                {t('back')}
              </Button>
              <br />
              <br />
              <Box
                display="flex"
                alignItems="center"
                justifyContent="center"
                flexDirection="column"
              >
                <Typography component="h1" variant="h4" gutterBottom>
                  {t('add-service')}
                </Typography>
              </Box>
              <form className={classes.form} onSubmit={handleSubmit}>
                <Box
                  display="flex"
                  alignItems="center"
                  justifyContent="center"
                  flexDirection="column"
                >
                  <Button onClick={handleOpen}>
                    <Avatar
                      variant="square"
                      className={classes.avatar}
                      src={
                        formData.image_data
                          ? formData.image_data
                          : 'https://via.placeholder.com/500/ffffff/000000/?text=Click+To+Upload+Service+Icon'
                      }
                    />
                    {/* <Badge
                      color="primary"
                      overlap="circle"
                      anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'right',
                      }}
                      badgeContent={t('upload-logo')}
                    >
                      <Avatar
                        variant="square"
                        className={classes.avatar}
                        src={
                          formData.image_data
                            ? formData.image_data
                            : 'https://via.placeholder.com/1500/ffffff/000000/?text=Upload+Service+Icon'
                        }
                      />
                    </Badge> */}
                  </Button>
                  <DropzoneDialog
                    open={open}
                    onSave={handleSave}
                    acceptedFiles={['image/png', 'image/jpeg']}
                    showPreviews={true}
                    maxFileSize={5242880}
                    onClose={handleClose}
                    filesLimit={1}
                    onDrop={handleSave}
                    showAlerts={['error']}
                    getFileAddedMessage={(fileName) => {
                      toast.success(
                        t('file-added-success', { fileName: fileName }),
                        { position: 'bottom-left' },
                      )
                      return t('file-added-success', {
                        fileName: fileName,
                      })
                    }}
                    clearOnUnmount={false}
                  />
                </Box>
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="service_name"
                  label={t('service_name')}
                  name="service_name"
                  autoComplete="service_name"
                  // className={classes.margin}
                  onChange={handleChange}
                />
                <TextField
                  margin="normal"
                  fullWidth
                  variant="outlined"
                  name="description"
                  label={t('description')}
                  id="description"
                  multiline
                  rows={10}
                  rowsMax={10}
                  autoComplete="description"
                  // className={classes.margin}
                  onChange={handleChange}
                />
                <ServiceItemForm
                  initialItems={[]}
                  usage={ServiceItemFormUsage.SERVICE_ITEMS}
                  onUpdate={(items: IServiceItem[]) => {
                    setFormData({
                      ...formData,
                      service_items: items,
                    })
                  }}
                />
                <Box mb={6} textAlign="center">
                  <Button
                    //   disabled={submitting}
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                    // onClick={update}
                  >
                    {t('create')}
                  </Button>
                </Box>
              </form>
              <ToastContainer />
            </Paper>
          </div>
        </>
      )}
    </Layout>
  )
}

export const getStaticProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
})

export const getStaticPaths = async () => {
  return {
    paths: [],
    fallback: 'blocking',
  }
}
