import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { ICompany } from '../../../server/api/Company/company.model'
import { PDFDocument, StandardFonts, rgb } from 'pdf-lib'
import { format } from 'date-fns'
// import { Context } from 'vm'
import { useRouter } from 'next/router'
import { useSession } from 'next-auth/client'

export default function ViewPdf() {
  const router = useRouter()
  const [pdfDataUri, setPdfDataUri] = useState('')

  useEffect(() => {
    setWidth(window.innerWidth)
    setHeight(window.innerHeight)

    async function fetchData() {
      if(!router.query.id)
        return
      try {
        const request_url = `/company/${router.query.id}`
        const company_res = await axios.get(request_url)
        const company: ICompany = company_res.data.company
        const template_url = '/company/pdf-template'
        let arrayBuffer = new Uint8Array()
        const template_res = await axios.get(template_url)
        arrayBuffer = template_res.data
        const pdfDoc = await PDFDocument.load(arrayBuffer)
        const timesRomanFont = await pdfDoc.embedFont(
          StandardFonts.TimesRoman,
        )
        const pages = pdfDoc.getPages()
        const { width, height } = pages[0].getSize()
        pages[0].drawText(company.project_name, {
          x: width / 3 + 22,
          y: height / 2 + 72,
          size: 10,
          font: timesRomanFont,
          color: rgb(0.0, 0.0, 0.0),
        })
        pages[1].drawText(company.company_name, {
          x: width / 2 - 120,
          y: height / 2 - 22,
          size: 15,
          font: timesRomanFont,
          color: rgb(0.0, 0.0, 0.0),
        })
        // @ts-ignore
        const dateAdded = new Date(company.createdAt)
        const formatted_date =
          format(dateAdded, 'EEEE do ') +
          'of' +
          format(dateAdded, ' MMMM yyyy')
        pages[2].drawText(formatted_date, {
          x: width / 2,
          y: height / 4 - 20,
          size: 15,
          font: timesRomanFont,
          color: rgb(0.0, 0.0, 0.0),
        })
        pdfDoc.setTitle(company.company_name + '_' + company._id)
        const pdfDataUri = await pdfDoc.saveAsBase64({ dataUri: true })
        setPdfDataUri(pdfDataUri)

      } catch (err) {
        if (err.response) {
          console.log(err.response)
        }
      }
    }
      fetchData()
    }, [router.query])

  const [width, setWidth] = useState(0)
  const [height, setHeight] = useState(0)

  return (
    <>
      {pdfDataUri && (
        <>
          <div>
            <iframe src={pdfDataUri} width={width} height={height}></iframe>
          </div>
        </>
      )}
    </>
  )
}
