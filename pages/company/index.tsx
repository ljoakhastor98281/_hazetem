import React from 'react'
import Layout from '../../components/layouts/navLayout'
import { FormControlLabel, Switch, Typography } from '@material-ui/core'
import axios from 'axios'
import { ICompany } from '../../server/api/Company/company.model'
import { IService } from '../../server/api/Service/service.model'
import { useSession } from 'next-auth/client'
import { useEffect } from 'react'
import CompanyList from '../../components/elements/CompanyList'
import CompanyServiceList from '../../components/elements/CompanyServiceList'
import ServiceList from '../../components/elements/ServiceList'
import { useRouter } from 'next/router'
import { useTranslation } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

export default function Company() {
  const [companyList, setCompanyList] = React.useState<ICompany[]>()
  const [ownServiceData, setOnwServiceData] = React.useState<IService[]>()
  const [session, loading] = useSession()
  const router = useRouter()
  const { t } = useTranslation()

  useEffect(() => {
    async function fetchData() {
      if (!session) {
        return
      } else if (session.level == 2 || session.level == 3) {
        const res = await axios.get(
          `/service/vendor/${session.user.company_id}`,
        )
        res.data.services.map((service: IService) => {
          service.id = service._id
        })
        setOnwServiceData(() => res.data.services)
      } else {
        const res = await axios.get('/company')
        res.data.companys.forEach((company: ICompany) => {
          company.id = company._id
        })
        setCompanyList(() => res.data.companys)
      }
    }
    fetchData()
  }, [session])
  const [showServiceList, setShowServiceList] = React.useState(false)

  return (
    <Layout>
      {!session && !loading && (
        <meta httpEquiv="refresh" content="0; /signin" />
      )}
      {session && session.level < 2 && (
        <>
          <FormControlLabel
            control={
              <Switch
                checked={showServiceList}
                onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                  setShowServiceList(event.target.checked)
                }}
                name="checkShowServiceList"
              />
            }
            label={
              <Typography variant="body2" color="textPrimary">
                {t('switch-to-service')}
              </Typography>
            }
          />
        </>
      )}
      {session &&
        (session.level == 2 || session.level == 3) &&
        ownServiceData &&
        router && (
          <ServiceList
            company_id={session.user.company_id as string}
            user_level={session.level as number}
            user={session.user._id as string}
            router={router}
            servicelist={ownServiceData}
          />
        )}
      {session &&
        ((session.level < 2 && showServiceList) || session.level == 4) &&
        companyList && <CompanyServiceList companylist={companyList} />}
      {session &&
        session.level < 2 &&
        !showServiceList &&
        companyList &&
        router && (
          <>
            <CompanyList companylist={companyList} router={router} />
          </>
        )}
    </Layout>
  )
}

export const getStaticProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
})
