import React, { useEffect, useState } from 'react'
import clsx from 'clsx'
import { makeStyles } from '@material-ui/core/styles'
import { Typography, Button, TextField, Paper, Box } from '@material-ui/core'
import axios from 'axios'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { useTranslation } from 'next-i18next'
import { ICompany } from '@/server/api/Company/company.model'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'
import InputAdornment from '@material-ui/core/InputAdornment'
import Layout from '../../../components/layouts/navLayout'
import { useRouter } from 'next/router'
import { useSession } from 'next-auth/client'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import CheckCircleOutlineOutlinedIcon from '@material-ui/icons/CheckCircleOutlineOutlined'

const useStyles = makeStyles((theme) => ({
  layout: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    maxWidth: '768px',
    margin: '0 auto',
  },
  paper: {
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(8),
      padding: `${theme.spacing(6)}px ${theme.spacing(4)}px`,
    },
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  // buttonProgress: {
  //   position: 'absolute',
  //   top: '50%',
  //   left: '50%',
  //   marginTop: -12,
  //   marginLeft: -12,
  // },
  textField: {
    width: '48%',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  margin: {
    marginRight: theme.spacing(3),
  },
  textfieldLabel: {
    width: '100%',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
  },
}))

export default function CompanyEdit() {
  const router = useRouter()

  const classes = useStyles({})
  const { t } = useTranslation()

  const [renderedData, setRenderedData] = useState<ICompany>()
  const [session, loading] = useSession()

  useEffect(() => {
    async function fetchData() {
      if (!session) {
        return
      } else {
        await axios
          .get(`/company/${router.query.id}`)
          .then((res) => {
            setRenderedData(() => res.data.company)
          })
          .catch(() => {
            return
          })
      }
    }
    fetchData()
  }, [session, router.query])

  const showError = () =>
    toast.error(t('error'), {
      position: 'bottom-right',
    })
  const showSuccess = () =>
    toast.success(t('update-success'), {
      position: 'bottom-right',
    })

  const handleChange = (e: { target: { name: string; value: string } }) => {
    if (renderedData) {
      setRenderedData({ ...renderedData, [e.target.name]: e.target.value })
    } else {
      toast.error(t('error-changing-data'), {
        position: 'bottom-right',
      })
    }
  }

  const handleSubmit = async (e: { preventDefault: () => void }) => {
    // console.log(renderedData)
    e.preventDefault()
    let res
    try {
      res = await axios.put(`/company/${router.query.id}`, renderedData)
    } catch (err) {
      if (err) {
        // console.log(err.response.data)
        switch (err.response.status) {
          case 409:
            if (err.response.data.msg == 'conflict-company_name') {
              toast.error(t('register-409-company-fail'), {
                position: 'bottom-right',
              })
              break
            } else {
              toast.error(t('error-without-status'), {
                position: 'bottom-right',
              })
              break
            }
          case 403:
            toast.error(t('access-other-info'), {
              position: 'bottom-right',
            })
            break
          default:
            showError()
            break
        }
      }
    }
    if (res?.status == 200) {
      showSuccess()
      await new Promise((r) => setTimeout(r, 3000))
      router.push(`/${router.locale}/company`)
    }
  }

  return (
    <Layout>
      {!session && !loading && (
        <meta httpEquiv="refresh" content="0; /signin" />
      )}
      {session && session?.level > 2 && (
        <meta httpEquiv="refresh" content="0; /levelerror" />
      )}
      {session && session.level <= 2 && !renderedData && (
        <>
          <h1>company with id {router.query.id} not found</h1>
        </>
      )}
      {session && renderedData && (
        <>
          <div className={classes.layout}>
            <Paper className={classes.paper} elevation={2}>
              <Button
                startIcon={<ArrowBackIosIcon />}
                href={`/${router.locale}/company`}
              >
                {t('back')}
              </Button>
              <br />
              <br />
              <Box
                display="flex"
                alignItems="center"
                justifyContent="center"
                flexDirection="column"
              >
                <Typography component="h1" variant="h4" gutterBottom>
                  {t('company-edit')}
                </Typography>
              </Box>
              <form className={classes.form} onSubmit={handleSubmit}>
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="company_name"
                  label={t('company_name')}
                  name="company_name"
                  autoComplete="company_name"
                  autoFocus
                  defaultValue={renderedData.company_name}
                  onChange={handleChange}
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="project_name"
                  label={t('project_name')}
                  name="project_name"
                  autoComplete="project_name"
                  defaultValue={renderedData.project_name}
                  onChange={handleChange}
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="business_outline"
                  label={t('business_outline')}
                  name="Business Outline"
                  autoComplete="business_outline"
                  defaultValue={renderedData.business_outline}
                  onChange={handleChange}
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="status"
                  label={t('status')}
                  name="status"
                  autoComplete="status"
                  defaultValue={renderedData.status}
                  className={clsx(classes.margin, classes.textField)}
                  onChange={handleChange}
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  name="date_of_agreement_signed"
                  label={t('date_of_agreement_signed')}
                  type="date"
                  id="date_of_agreement_signed"
                  autoComplete="date_of_agreement_signed"
                  InputLabelProps={{
                    shrink: true,
                    className: classes.textfieldLabel,
                  }}
                  defaultValue={
                    renderedData.date_of_agreement_signed
                      .toString()
                      .split(`T`)[0]
                  }
                  className={classes.textField}
                  onChange={handleChange}
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="sales_lead"
                  label={t('sales_lead')}
                  name="sales_lead"
                  autoComplete="sales_lead"
                  defaultValue={renderedData.sales_lead}
                  className={clsx(classes.margin, classes.textField)}
                  onChange={handleChange}
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  type="number"
                  onKeyDown={(evt) =>
                    ['e', 'E', '+', '-'].includes(evt.key) &&
                    evt.preventDefault()
                  }
                  id="total_amount"
                  label={t('total_amount')}
                  name="total_amount"
                  autoComplete="total_amount"
                  defaultValue={renderedData.total_amount}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">$</InputAdornment>
                    ),
                  }}
                  className={classes.textField}
                  onChange={handleChange}
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="contact_person"
                  label={t('contact_person')}
                  name="contact_person"
                  autoComplete="contact_person"
                  defaultValue={renderedData.contact_person}
                  className={clsx(classes.margin, classes.textField)}
                  onChange={handleChange}
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="client_contact_number"
                  type="number"
                  onKeyDown={(evt) =>
                    ['e', 'E', '+', '-'].includes(evt.key) &&
                    evt.preventDefault()
                  }
                  label={t('client_contact_number')}
                  name="client_contact_number"
                  autoComplete="client_contact_number"
                  InputLabelProps={{
                    className: classes.textfieldLabel,
                  }}
                  defaultValue={renderedData.client_contact_number}
                  className={classes.textField}
                  onChange={handleChange}
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  type="email"
                  id="contact_email"
                  label={t('contact_email')}
                  name="contact_email"
                  autoComplete="contact_email"
                  error={
                    renderedData.contact_email.length > 0 &&
                    !/\S+@\S+\.\S+/.test(renderedData.contact_email)
                  }
                  helperText={
                    !/\S+@\S+\.\S+/.test(renderedData.contact_email) ? (
                      t('email-error')
                    ) : (
                      <CheckCircleOutlineOutlinedIcon
                        color="primary"
                        fontSize="small"
                      />
                    )
                  }
                  defaultValue={renderedData.contact_email}
                  onChange={handleChange}
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  type="number"
                  id="number_of_staff"
                  onKeyDown={(evt) =>
                    ['e', 'E', '+', '-'].includes(evt.key) &&
                    evt.preventDefault()
                  }
                  label={t('number_of_staff')}
                  name="number_of_staff"
                  autoComplete="number_of_staff"
                  defaultValue={renderedData.number_of_staff}
                  className={clsx(classes.margin, classes.textField)}
                  onChange={handleChange}
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="company_start_year"
                  label={t('company_start_year')}
                  name="company_start_year"
                  type="number"
                  onKeyDown={(evt) =>
                    ['e', 'E', '+', '-'].includes(evt.key) &&
                    evt.preventDefault()
                  }
                  autoComplete="company_start_year"
                  defaultValue={renderedData.company_start_year}
                  className={classes.textField}
                  onChange={handleChange}
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="company_outline"
                  label={t('company_outline')}
                  name="company_outline"
                  autoComplete="company_outline"
                  variant="outlined"
                  multiline
                  rows={10}
                  rowsMax={10}
                  defaultValue={renderedData.company_outline}
                  onChange={handleChange}
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  name="BR_expiry_date"
                  label={t('BR_expiry_date')}
                  type="date"
                  id="BR_expiry_date"
                  autoComplete="BR_expiry_date"
                  InputLabelProps={{
                    shrink: true,
                    className: classes.textfieldLabel,
                  }}
                  defaultValue={
                    renderedData.BR_expiry_date.toString().split(`T`)[0]
                  }
                  // className={clsx(classes.margin, classes.textField)}
                  onChange={handleChange}
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="company_address"
                  label={t('company_address')}
                  name="company_address"
                  autoComplete="company_address"
                  defaultValue={renderedData.company_address}
                  onChange={handleChange}
                />
                <TextField
                  margin="normal"
                  fullWidth
                  id="tvp_account_name"
                  label={t('tvp_account_name')}
                  name="tvp_account_name"
                  autoComplete="tvp_account_name"
                  defaultValue={renderedData.tvp_account_name}
                  className={clsx(classes.margin, classes.textField)}
                  onChange={handleChange}
                />
                <TextField
                  margin="normal"
                  fullWidth
                  id="tvp_password"
                  label={t('tvp_password')}
                  name="tvp_password"
                  autoComplete="tvp_password"
                  defaultValue={renderedData.tvp_password}
                  className={classes.textField}
                  onChange={handleChange}
                />
                <TextField
                  margin="normal"
                  fullWidth
                  id="remark"
                  label={t('remark')}
                  name="remark"
                  autoComplete="remark"
                  variant="outlined"
                  multiline
                  rows={10}
                  rowsMax={10}
                  defaultValue={renderedData.remark}
                  onChange={handleChange}
                />
                <Box mb={6} textAlign="center">
                  <Button
                    //   disabled={submitting}
                    type="submit"
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                  >
                    {/* {submitting && (
                    <CircularProgress
                      size={24}
                      className={classes.buttonProgress}
                    />
                  )} */}
                    {t('update')}
                  </Button>
                </Box>
              </form>
            </Paper>
            <ToastContainer />
          </div>
        </>
      )}
    </Layout>
  )
}

export const getStaticProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
})

export const getStaticPaths = async () => {
  return {
    paths: [],
    fallback: 'blocking',
  }
}
