import React from 'react'
import { DropzoneDialog } from 'material-ui-dropzone'
import { Box, Button } from '@material-ui/core'
import axios from 'axios'
import { toBase64 } from '@/server/utils/helper'

// remove later
// import mongoose from 'mongoose'

export default function FileUploader() {
  const [open, setOpen] = React.useState(false)

  // hard code company id
  // const company_id = new mongoose.Types.ObjectId()
  const service_id = '60cb11a15a9aa4490c291619'

  const handleOpen = async () => {
    setOpen(true)
  }

  const handleClose = async () => {
    setOpen(false)
  }

  const handleSave = async (files: File[]) => {
    // console.log(service_id.toHexString())
    let sus = true

    files.forEach(async (file) => {
      // console.log(await toBase64(file))
      // const res = await axios.post(`/company/uploadImage/${service_id.toHexString()}`, {
      const res = await axios.patch(`/service/uploadImage/${service_id}`, {
        image_data: await toBase64(file),
      })
      if (res.data.msg !== 'sus' || res.status !== 200) {
        sus = false
      }
    })

    if (sus) {
      window.alert('sus')
    } else {
      window.alert('not sus')
    }

    setOpen(false)
  }

  return (
    <Box>
      <Button onClick={handleOpen}>upload logos</Button>
      <DropzoneDialog
        open={open}
        onSave={handleSave}
        acceptedFiles={['image/png', 'image/jpeg']}
        showPreviews={true}
        maxFileSize={5000000}
        onClose={handleClose}
      />
    </Box>
  )
}
