/*
Import svg icon guide:
1. Go to https://iconify.design/icon-sets/ search icon
2. Click the chosen one and copy svg code(the first textarea)
3. Go to https://transform.tools/html-to-jsx, paste svg code on left side
4. Create tsx in icons folder, copy the following template to new tsx
5. Copy step 3 right side code
6. Paste to the new tsx but !!!remember change this two line!!!
    width="1em" height="1em"
   to
    width={width ?? size} height={height ?? size}
 */

import { IIconProps } from '@/types/props.interface'
import { Tooltip } from '@material-ui/core'

export default function SettingWheel({ size, width, height }: IIconProps) {
  return (
    <Tooltip title="Hazedawn">
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width={width ?? size}
        height={height ?? size}
        viewBox="0 0 89.44 53.58"
      >
        <defs>
          <style
            dangerouslySetInnerHTML={{ __html: '.cls-1{fill:#323296;}' }}
          />
        </defs>
        <g id="Layer_2" data-name="Layer 2">
          <g id="Layer_1-2" data-name="Layer 1">
            <path
              className="cls-1"
              d="M2,53.38H0V34.25H2v6.41A3,3,0,0,1,5,39c2.43,0,3.67,1.54,3.67,5.22v9.17h-2V44.44c0-2.31-.27-3.7-2.33-3.7-1.62,0-2.3.91-2.3,2.83Z"
            />
            <path
              className="cls-1"
              d="M11.17,49.1V43.42c0-3,1.47-4.43,3.7-4.43a3.43,3.43,0,0,1,3.07,1.64V39.16h1.9V53.38h-1.9V51.91a3.43,3.43,0,0,1-3.07,1.67C12.59,53.58,11.17,52.06,11.17,49.1ZM17.81,49v-5.5c0-1.82-.88-2.76-2.3-2.76s-2.31,1-2.31,2.84V49c0,1.9.84,2.86,2.31,2.86S17.81,50.85,17.81,49Z"
            />
            <path
              className="cls-1"
              d="M30,53.38H22L27.34,41h-5V39.16H30.2L24.93,51.53h5Z"
            />
            <path
              className="cls-1"
              d="M40.49,46.94H33.82v1.9c0,1.85.79,3,2.34,3a2.11,2.11,0,0,0,2.3-2.12v-.46h2v.23a4,4,0,0,1-4.33,4.1c-2.77,0-4.34-1.82-4.34-4.69V43.52c0-2.76,1.57-4.56,4.34-4.56s4.33,1.8,4.33,4.56Zm-4.33-6.2a2.24,2.24,0,0,0-2.34,2.5v2.21h4.64V43.3A2.27,2.27,0,0,0,36.16,40.74Z"
            />
            <path
              className="cls-1"
              d="M42.62,49.25V43.32c0-3,1.39-4.31,3.49-4.31a2.9,2.9,0,0,1,2.56,1.24v-6h2.84V53.38H48.88V52.16a3.12,3.12,0,0,1-2.77,1.42C44,53.58,42.62,52.29,42.62,49.25Zm6.08-.38V43.65c0-1.5-.63-2.1-1.62-2.1s-1.62.66-1.62,2.15v5.17c0,1.55.63,2.18,1.62,2.18S48.7,50.42,48.7,48.87Z"
            />
            <path
              className="cls-1"
              d="M53.74,49.22v-5.9c0-3,1.42-4.31,3.5-4.31A3.19,3.19,0,0,1,60,40.41V39.16h2.63V53.38H60V52.14a3.15,3.15,0,0,1-2.76,1.44C55.13,53.58,53.74,52.29,53.74,49.22Zm6.08-.35V43.42c0-1.32-.73-1.87-1.62-1.87s-1.62.63-1.62,2.15v5.17c0,1.55.61,2.15,1.62,2.15S59.82,50.39,59.82,48.87Z"
            />
            <path
              className="cls-1"
              d="M64.05,39.16h2.89l1.7,8.77,1.7-8.77h2.45L74.47,48l1.75-8.8h2.86L76,53.38H73.17l-1.62-8.51-1.62,8.51H67.14Z"
            />
            <path
              className="cls-1"
              d="M83.34,53.38H80.52V39.16h2.66v1.27A3.16,3.16,0,0,1,85.92,39c2.13,0,3.52,1.3,3.52,4.31V53.38H86.6V43.47c0-1.34-.68-1.9-1.64-1.9s-1.62.56-1.62,1.9Z"
            />
            <path
              className="cls-1"
              d="M31.63,11.89c4.09-1.55,4.95-3.38,3.21-7.23Z"
            />
            <path
              className="cls-1"
              d="M31.63,13.89l3.21,7.24C36.58,17.27,35.72,15.44,31.63,13.89Z"
            />
            <path
              className="cls-1"
              d="M36.28,22.57l7.24,3.21C42,21.68,40.14,20.83,36.28,22.57Z"
            />
            <path
              className="cls-1"
              d="M45.51,25.78l7.24-3.21C48.9,20.83,47.06,21.68,45.51,25.78Z"
            />
            <path
              className="cls-1"
              d="M54.2,21.13l3.21-7.24C53.31,15.44,52.46,17.27,54.2,21.13Z"
            />
            <path
              className="cls-1"
              d="M57.41,11.89,54.2,4.66C52.46,8.51,53.31,10.34,57.41,11.89Z"
            />
            <path
              className="cls-1"
              d="M52.75,3.21,45.51,0C47.06,4.1,48.9,5,52.75,3.21Z"
            />
            <path
              className="cls-1"
              d="M43.52,0,36.28,3.21C40.14,5,42,4.1,43.52,0Z"
            />
            <path
              className="cls-1"
              d="M44.52,7.59h-4v0h0A6.31,6.31,0,0,0,43.9,9.89v2.64l-3.13,1.81a6.42,6.42,0,0,0-3.63-1.73v0h0l2,3.43,2,3.43v0h0a6.25,6.25,0,0,0,.3-4.07l3.12-1.8,3,1.75a6.12,6.12,0,0,0,.28,4.12h0v0l2-3.43,2-3.43h0v0a6.44,6.44,0,0,0-3.59,1.69l-3.06-1.77V9.89A6.33,6.33,0,0,0,48.48,7.6h0l0,0Z"
            />
          </g>
        </g>
      </svg>
    </Tooltip>
  )
}
