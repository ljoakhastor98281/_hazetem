import React from 'react'
import MenuItem from '@material-ui/core/MenuItem'
import {
  Box,
  Card,
  CardContent,
  CardMedia,
  Container,
  Divider,
  FormControl,
  Grid,
  IconButton,
  InputAdornment,
  InputLabel,
  Link,
  OutlinedInput,
  Select,
  Toolbar,
  Typography,
} from '@material-ui/core'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import SearchIcon from '@material-ui/icons/Search'
import RotateLeftIcon from '@material-ui/icons/RotateLeft'
import { createStyles, makeStyles } from '@material-ui/core/styles'
import { Theme } from '@material-ui/core/styles'
import { lighten } from '@material-ui/core/styles'
import { ICompany } from '../../server/api/Company/company.model'
import lightBlue from '@material-ui/core/colors/lightBlue'
import cyan from '@material-ui/core/colors/cyan'
import { useTranslation } from 'next-i18next'
import { useRouter } from 'next/router'
import clsx from 'clsx'

const useListStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      backgroundColor: '#a2dffc',
    },
    insideListText: {
      maxWidth: 250,
      minWidth: 250,
    },
    cardGrid: {
      flexGrow: 1,
      paddingTop: 16,
    },
    card: {
      display: 'flex',
      height: '100%',
      backgroundColor: cyan[50],
      boxShadow: '0 2px 5px 2px rgba(190, 190, 190, 0.43)',
      '&:hover': {
        backgroundColor: lightBlue[50],
        boxShadow: '0 5px 10px 5px rgba(190, 190, 190, 0.43)',
      },
    },
    cardMedia: {
      width: 350,
      height: 300,
      objectFit: 'contain',
      margin: 'auto',
      display: 'flex',
    },
    cardActionArea: {
      flex: 1,
      height: '100%',
      backgroundColor: 'green',
    },
    cardContent: {
      flex: '1 0 auto',
      width: '50%',
      justiyContent: 'space-between',
      flexDirection: 'column',
      borderRadius: theme.shape.borderRadius,
      backgroundColor: cyan[50],
      boxShadow: '0 2px 5px 2px rgba(190, 190, 190, 0.43)',
      '&:hover': {
        backgroundColor: lightBlue[50],
        boxShadow: '0 5px 10px 5px rgba(190, 190, 190, 0.43)',
      },
    },
    cardContentTitle: {
      flex: 1,
      margin: theme.spacing(1),
    },
    cardButton: {
      display: 'flex',
      alignItems: 'center',
      flexDirection: 'column',
      justiyContent: 'space-between',
      paddingLeft: theme.spacing(1),
      paddingBottom: theme.spacing(1),
    },
    editbutton: {
      position: 'absolute',
      right: 60,
      marginright: theme.spacing(2),
    },
    deletebutton: {
      position: 'absolute',
      right: 130,
      marginright: theme.spacing(2),
    },
    section: {
      flexDirection: 'row',
      display: 'flex',
      [theme.breakpoints.down('md')]: {
        flexDirection: 'column',
        display: 'flex',
        justifyContent: 'center',
        textAlign: 'center',
      },
    },
    sectionMedia: {
      display: 'flex',
      [theme.breakpoints.down('sm')]: {
        display: 'none',
      },
    },
  }),
)

const useEnhancedSearchBarStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(1),
      [theme.breakpoints.down('sm')]: {
        display: 'block',
      },
    },
    highlight:
      theme.palette.type === 'light'
        ? {
            color: theme.palette.secondary.main,
            backgroundColor: lighten(theme.palette.secondary.light, 0.85),
            margin: theme.spacing(1),
          }
        : {
            color: theme.palette.text.primary,
            backgroundColor: theme.palette.secondary.dark,
          },
    filterBox: {
      margin: theme.spacing(1),
    },
    title: {
      flex: '1 1 100%',
      alignItems: 'center',
      justifyContent: 'center',
      textAlign: 'center',
      fontSize: 50,
      [theme.breakpoints.down('xs')]: {
        fontSize: 40,
      },
      fontFamily: 'Sintony, sans-serif',
    },
    subtitle: {
      flex: '1 1 100%',
      fontSize: 20,
      [theme.breakpoints.down('sm')]: {
        fontSize: 15,
      },
    },
    margin: {
      margin: theme.spacing(1),
    },
    button: {
      margin: theme.spacing(1),
      maxWidth: 200,
      width: '100%',
    },
    mobileButton: {
      margin: theme.spacing(1),
    },
    input: {
      marginLeft: theme.spacing(1),
      flex: 1,
    },
    iconButton: {
      padding: 10,
    },
    sectionDesktop: {
      display: 'flex',
      [theme.breakpoints.down('xs')]: {
        display: 'none',
      },
    },
    sectionMobile: {
      display: 'none',
      [theme.breakpoints.down('xs')]: {
        display: 'flex',
      },
    },
    details: {
      display: 'flex',
      flexDirection: 'column',
    },
    innerFormControl: {
      margin: theme.spacing(1),
      width: 200,
    },
  }),
)

interface EnhancedSearchBarProps {
  onRestoreClick: (e: React.MouseEvent<unknown>) => void
  onFilterClick: (e: React.MouseEvent<unknown>) => void
  textFieldData: { filteredData: string }
  setTextFieldData: React.Dispatch<
    React.SetStateAction<{
      filteredData: string
    }>
  >
  optionData: { selectedData: string }
  setOptionData: React.Dispatch<
    React.SetStateAction<{
      selectedData: string
    }>
  >
  onChangeFilter: (e: React.ChangeEvent<HTMLInputElement>) => void
}

const EnhancedSearchBar = (props: EnhancedSearchBarProps) => {
  const classes = useEnhancedSearchBarStyles()
  const { t } = useTranslation()
  const options = [
    t('company_name'),
    t('business_outline'),
    t('company_outline'),
  ]

  return (
    <>
      <Toolbar className={classes.root}>
        <Typography className={classes.title} id="tableTitle" component="div">
          {t('company-list')}
        </Typography>
      </Toolbar>
      <br />
      <Box className={classes.margin} textAlign="right">
        <FormControl fullWidth className={classes.filterBox} variant="outlined">
          <OutlinedInput
            id="search"
            key="Search..."
            type="text"
            onChange={props.onChangeFilter}
            startAdornment={
              <>
                <FormControl className={classes.innerFormControl}>
                  {/* <InputLabel
                    className={classes.sectionMobile}
                    id="select-search"
                  >
                    <PageviewIcon fontSize="medium" />
                  </InputLabel> */}
                  <InputLabel
                    className={classes.sectionDesktop}
                    id="select-search"
                  >
                    {t('search-by')}
                  </InputLabel>
                  <Select
                    MenuProps={{
                      anchorOrigin: {
                        vertical: 'bottom',
                        horizontal: 'left',
                      },
                      getContentAnchorEl: null,
                    }}
                    defaultValue=""
                    labelId="select-search"
                    id="select-search"
                    onChange={(e) =>
                      props.setOptionData({
                        ...props.optionData,
                        selectedData: String(e.target.value),
                      })
                    }
                  >
                    {options.map((option) => (
                      <MenuItem key={option} value={option}>
                        {option}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
                <Divider
                  className={classes.margin}
                  orientation="vertical"
                  flexItem
                />
                <IconButton
                  aria-label="restore"
                  onClick={props.onRestoreClick}
                  edge="start"
                >
                  <RotateLeftIcon />
                </IconButton>
              </>
            }
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="search"
                  onClick={props.onFilterClick}
                  edge="end"
                >
                  <SearchIcon />
                </IconButton>
              </InputAdornment>
            }
          />
        </FormControl>
      </Box>
    </>
  )
}

interface IListProps {
  companylist: ICompany[]
}

export default function CompanyServiceList({ companylist }: IListProps) {
  const [renderedData, setRenderedData] =
    React.useState<ICompany[]>(companylist)

  const [formData, setFormData] = React.useState({ filteredData: '' })
  const [option, setOption] = React.useState({ selectedData: '' })
  const serviceClasses = useListStyles()
  const { t } = useTranslation()
  const router = useRouter()

  const serviceFilterButtonHandler = async (e: React.MouseEvent<unknown>) => {
    e.preventDefault()
    const returnObj: ICompany[] = []
    const { filteredData } = formData
    const { selectedData } = option

    if (selectedData != '') {
      companylist.map((company: ICompany) => {
        if (selectedData == t('company_name')) {
          if (company.company_name.includes(filteredData)) {
            returnObj.push(company)
          }
        } else if (
          selectedData == t('business_outline') &&
          company.business_outline.includes(filteredData)
        ) {
          returnObj.push(company)
        } else if (
          selectedData == t('company_outline') &&
          company.company_outline.includes(filteredData)
        ) {
          returnObj.push(company)
        }
      })
      setRenderedData(returnObj)
    }
  }

  const onChangeFilterHandler = async (
    e: React.ChangeEvent<HTMLInputElement>,
  ) => {
    e.preventDefault()

    setFormData({
      ...formData,
      filteredData: e.target.value,
    })

    const { selectedData } = option
    const returnObj: ICompany[] = []

    if (selectedData != '') {
      companylist.map((company: ICompany) => {
        if (selectedData == 'Company Name') {
          if (company.company_name.includes(String(e.target.value))) {
            returnObj.push(company)
          }
        } else if (
          selectedData == 'Business Nature' &&
          company.business_outline.includes(String(e.target.value))
        ) {
          returnObj.push(company)
        } else if (
          selectedData == 'Business Outline' &&
          company.company_outline.includes(String(e.target.value))
        ) {
          returnObj.push(company)
        }
      })
      setRenderedData(returnObj)
    }
  }

  const serviceRestoreButtonHandler = async (e: React.MouseEvent<unknown>) => {
    e.preventDefault()
    setRenderedData(companylist)
  }

  return (
    <div style={{ height: 400, width: '100%' }}>
      <EnhancedSearchBar
        onFilterClick={serviceFilterButtonHandler}
        onRestoreClick={serviceRestoreButtonHandler}
        textFieldData={formData}
        setTextFieldData={setFormData}
        optionData={option}
        setOptionData={setOption}
        onChangeFilter={onChangeFilterHandler}
      />
      <Container className={serviceClasses.cardGrid} maxWidth="xl">
        <Grid
          container
          direction="row"
          justify="space-evenly"
          alignItems="stretch"
          spacing={2}
        >
          {renderedData.map((data) => (
            <Grid item key={`Company-${data._id}`} xs={12}>
              <Link
                underline="none"
                href={`/${router.locale}/company/service/${data._id}`}
              >
                <Card
                  className={serviceClasses.card}
                  key={`OverviewItem-${data.id}`}
                >
                  <CardMedia
                    className={clsx(
                      serviceClasses.cardMedia,
                      serviceClasses.sectionMedia,
                    )}
                    // style={{ display: 'flex', justifyContent: 'center' }}
                    image={
                      data.image_data
                        ? data.image_data
                        : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAS4AAACnCAMAAACYVkHVAAAAJ1BMVEX09PTa2trc3Nz29vbj4+Pg4ODv7+/Y2Njy8vLt7e3q6urm5ubn5+eHk7pVAAAEMklEQVR4nO2ci46jMAxFSZw3/P/3ru0QFtoppSPtUsn3rDSLmFaCIycxjplpAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPxzSLj7Ir4b8UPdU6lL8MvdF/TtiKjSQnQ5O+cClbsv6OvQEddDKrEo7xTPuAhdeySUivgqqc5xmBq6+MfdF/hVSFxRSYuEVGY93j/ocunuS/wCxujjsTeLKOe3aFJlfMKHJfDJXK2vjUVNldpCl7SLJvXlY2hJBmiSs4vlXGJM596tpvxel+OQSrt0S05Gk7aKGuhjT91sAbUeSUhNx7SUIv8i3nXFt8KiJKTyELVN5jz2ZhY1PSfwNMnk5S0mEhT6JL4be1kjqhZN4Pkj5dELURO5xeBopPnvbJXV1CKz+ekzIesSu8mirjrCSseemprerXmUOCDdbFAXJwWsK3ZR178VOXXdL4125jGJrjBUlbKWHd4QZSXYn7j3Hv4fPSn48HYpcHTtl8ZkJbxUlx/B5a+yJWcrVuZ9muXOR1LgfosZXVXutkLXNahwQp9HKfmQ20PXT4igsB6LrvgZIZrSJXO9H7VR0RXW1OBSRkHr5GdHVxBdayIgh+GTO2dhwZSuadHb7cerrpKuMpnTlTLPXm3U/USXrJZZGf+/pE7WdBWpR8xHXb6vkMctjWe8QV381Ohz/FlXCKfpg0ldUl5Yl8ajrkWWvvkkvizq4kyAh1xfGg+65t4XMUPXHi0l57407nR5N61Pkg+Zvl//GdXFSyPfb9PDva5RpiB/mO99f1Cyq0sfrHty+l4X/6ZuFWuTuki2GPu24WEw1j531d1glNJW5QefmvvOtkldum2oh4ep3nddx+abntDyfGdVF1GT7F3n9aMu30ppx4nLz9p2UihY1aVL41ohfEhTnxKHv8/fFMzqKnLDywVduz0Q0iK/RV0TbSbe6OLPbNGl+yA2dUlPiS6Np7rysYuEik1dWrLqj0Gnuh57biiZHIxEC1vSx6DXurzLz92oNVdz5cHRVyIJ1Ul0/dSMSnMzqEtmbacVwidd+cyWtv4a1CUVQu2oOejiTCGUIM3O2buXjc4GdfFTo1QIy0N0xUaFmqajy0sdFnVJhdCnH8qD8irLItu2L1tsLOpqWvKS9F51ScEwbsOP0nzSj2RRl75YMG+6qne7zdly2rRrUBcvjU4rhCO6fDv00Z991aIubbmMI7okni7fvkVda1/J0PXcS3/yVXu6aJr1DdjwcUuJSV3T1NzY3IGuCyS3vbf4WcOSSV2l9N0x+Rnef3yHxUdsJm4t4D58grVmyw6F0Goqr2r0J/QvGNM19TenPnW1YU1Xz7Sg6yL9di+/5PII/kwCeMsv/1JLmSy+aQwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABjiD11yHkXgwpzGAAAAAElFTkSuQmCC'
                    }
                    title="company image"
                  />
                  <CardContent className={serviceClasses.cardContent}>
                    <Typography
                      gutterBottom
                      variant="h5"
                      className={serviceClasses.cardContentTitle}
                    >
                      {data.company_name}
                    </Typography>
                    <Box p={2}>
                      <List className={serviceClasses.root}>
                        <ListItem className={serviceClasses.section}>
                          <ListItemText
                            className={serviceClasses.insideListText}
                            primary={t('business_outline')}
                            primaryTypographyProps={{
                              style: { fontWeight: 'bold' },
                            }}
                          />
                          <ListItemText primary={data.business_outline} />
                        </ListItem>
                        <Divider variant="middle" component="li" />
                        <ListItem className={serviceClasses.section}>
                          <ListItemText
                            className={serviceClasses.insideListText}
                            primary={t('company_outline')}
                            primaryTypographyProps={{
                              style: { fontWeight: 'bold' },
                            }}
                          />
                          <ListItemText primary={data.company_outline} />
                        </ListItem>
                        <Divider variant="middle" component="li" />
                        <ListItem className={serviceClasses.section}>
                          <ListItemText
                            className={serviceClasses.insideListText}
                            primary={t('client_contact_number')}
                            primaryTypographyProps={{
                              style: { fontWeight: 'bold' },
                            }}
                          />
                          <ListItemText primary={data.client_contact_number} />
                        </ListItem>
                        <Divider variant="middle" component="li" />
                        <ListItem className={serviceClasses.section}>
                          <ListItemText
                            className={serviceClasses.insideListText}
                            primary={t('contact_email')}
                            primaryTypographyProps={{
                              style: { fontWeight: 'bold' },
                            }}
                          />
                          <ListItemText primary={data.contact_email} />
                        </ListItem>
                      </List>
                    </Box>
                  </CardContent>
                </Card>
              </Link>
            </Grid>
          ))}
        </Grid>
      </Container>
    </div>
  )
}
