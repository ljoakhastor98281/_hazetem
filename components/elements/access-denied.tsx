import { Link } from '@material-ui/core'
import { signIn } from 'next-auth/client'
import { useTranslation } from 'next-i18next'

const AccessDenied = () => {
  const { t } = useTranslation()
  return (
    <>
      <h1>{t('access-denied')}</h1>
      <p>
        <Link
          href="/api/auth/signin"
          onClick={(e) => {
            e.preventDefault()
            signIn()
          }}
        >
          {t('must-signin')}
        </Link>
      </p>
    </>
  )
}

export default AccessDenied
