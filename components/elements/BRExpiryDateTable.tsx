import React from 'react'
import TableCell from '@material-ui/core/TableCell'
import TableRow from '@material-ui/core/TableRow'
import { createStyles, makeStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableHead from '@material-ui/core/TableHead'
import TableBody from '@material-ui/core/TableBody'
import {
  Box,
  Button,
  Menu,
  MenuItem,
  Theme,
  Typography,
  IconButton,
  Tooltip,
  Select,
  Dialog,
  TextField,
  Paper,
} from '@material-ui/core'
import { IBRCompanyTableRow } from '@components/interfaces/interfaces'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import axios from 'axios'
import ArrowLeftIcon from '@material-ui/icons/ArrowLeft'
import ArrowRightIcon from '@material-ui/icons/ArrowRight'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { useTranslation } from 'next-i18next'
import { BallSpinner } from 'react-spinners-kit'
import { Fonts, MonthLongEN, FontFamily } from '@AppEnums'

const useTableStyles = makeStyles(() => ({
  tableResponsiveMaterial: {
    minHeight: '.01%',
    overflowX: 'auto',
    '& > thead > tr > th, > tbody > tr > th, > tfoot > tr > th, thead > tr > td, tbody > tr > td, tfoot > tr > td':
      {
        whiteSpace: 'normal',
      },
    '@media (max-width: 767px)': {
      width: '100%',
      marginBottom: 15,
      overflowY: 'hidden',
    },
    '&::-webkit-scrollbar': {
      width: '12px',
    },
    '&::-webkit-scrollbar-track': {
      backgroundColor: '#e4e4e4',
      borderRadius: '100px',
    },
    '&::-webkit-scrollbar-thumb': {
      backgroundImage: 'linear-gradient(90deg, #D0368A 0%, #708AD4 99%)',
      borderRadius: '100px',
      boxShadow: 'inset 2px 2px 5px 0 rgba(#fff, 0.5)',
      borderColor: '#10B292',
      border: '1px solid rgba(0,0,0,0.2)',
    },
  },
}))

const AppTableContainer = (props: any) => {
  const classes = useTableStyles()

  return <Box className={classes.tableResponsiveMaterial}>{props.children}</Box>
}

interface AppMenuProps {
  id: string
}

const AppMenu: React.FC<AppMenuProps> = ({ id }) => {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
  const open = Boolean(anchorEl)
  const { t } = useTranslation()
  const [dialogOpen, setDialogOpen] = React.useState(false)
  const [mailData, setMailData] = React.useState({
    email_subject: '',
    email_message: '',
  })
  const [isSendingEmail, setIsSendingEmail] = React.useState(false)
  const [isSendingBREmail, setIsSendingBREmail] = React.useState(false)

  const options = [t('send-br-reminder-mail'), t('send-customized-mail')]

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  const handleDialogClose = () => {
    setDialogOpen(false)
  }

  const handleSendCustomizedMail = async (event: {
    preventDefault: () => void
  }) => {
    event.preventDefault()
    setIsSendingEmail(true)
    await axios
      .post(`/mail/send/company/${id}`, mailData)
      .then((res) => {
        toast.success(t('mail-sent-to') + res.data.email, {
          position: 'bottom-right',
        })
        setIsSendingEmail(false)
      })
      .catch((err) => {
        switch (err.response.status) {
          case 418: {
            toast.error(t('418-error'), {
              position: 'bottom-right',
            })
            break
          }
          case 404: {
            toast.error(t('404-company-error'), {
              position: 'bottom-right',
            })
            break
          }
          case 403: {
            toast.error(t('not-auth-msg'), {
              position: 'bottom-right',
            })
            break
          }
          default: {
            toast.error(t('error-without-status'), {
              position: 'bottom-right',
            })
            break
          }
        }
      })
    setDialogOpen(false)
  }

  return (
    <Box>
      <IconButton
        style={{ height: 30, width: 30 }}
        aria-label="more"
        onClick={handleClick}
      >
        <MoreVertIcon />
      </IconButton>
      <Menu
        id="long-menu"
        anchorEl={anchorEl}
        keepMounted
        open={open}
        onClose={handleClose}
      >
        {options.map((option) => (
          <MenuItem
            key={option}
            selected={option === 'Pyxis'}
            value={option}
            onClick={async () => {
              switch (option) {
                case t('send-br-reminder-mail'): {
                  setIsSendingBREmail(true)
                  await axios
                    .get(`/mail/sendbrremind/${id}`)
                    .then((res) => {
                      toast.success(t('mail-sent-to') + res.data.email, {
                        position: 'bottom-right',
                      })
                      setIsSendingBREmail(false)
                    })
                    .catch((err) => {
                      switch (err.response.status) {
                        case 418: {
                          toast.error(t('418-error'), {
                            position: 'bottom-right',
                          })
                          break
                        }
                        case 404: {
                          toast.error(t('404-company-error'), {
                            position: 'bottom-right',
                          })
                          break
                        }
                        case 403: {
                          toast.error(t('not-auth-msg'), {
                            position: 'bottom-right',
                          })
                          break
                        }
                        default: {
                          toast.error(t('error-without-status'), {
                            position: 'bottom-right',
                          })
                          break
                        }
                      }
                    })
                  break
                }
                case t('send-customized-mail'): {
                  setDialogOpen(true)
                  break
                }
                default: {
                  break
                }
              }
            }}
          >
            {option}
          </MenuItem>
        ))}
      </Menu>
      <Dialog open={dialogOpen} onClose={handleDialogClose} fullWidth>
        <Box p={2}>
          <Typography align="center" variant="h4">
            {t('send-customized-mail')}
          </Typography>
          <form onSubmit={handleSendCustomizedMail}>
            <Box p={2}>
              <TextField
                required
                fullWidth
                variant="outlined"
                label={t('email-title')}
                onChange={(event) => {
                  setMailData({
                    ...mailData,
                    email_subject: event.target.value,
                  })
                }}
              />
            </Box>
            <Box p={2}>
              <TextField
                required
                fullWidth
                variant="outlined"
                multiline
                rows={12}
                label={t('email-body')}
                onChange={(event) => {
                  setMailData({
                    ...mailData,
                    email_message: event.target.value,
                  })
                }}
              />
            </Box>
            <Box pt={2}>
              <Button
                variant="contained"
                color="primary"
                type="submit"
                disabled={isSendingEmail ? true : false}
                fullWidth
              >
                {isSendingEmail ? (
                  <Box margin-left="auto" margin-right="auto" py={1.5}>
                    <BallSpinner size={30} color="#FFFFFF" loading={true} />
                  </Box>
                ) : (
                  t('send-email')
                )}
              </Button>
            </Box>
          </form>
        </Box>
      </Dialog>
      <Box position="fixed" bottom={30} right={120}>
        {isSendingBREmail ? (
          <Paper
            elevation={3}
            variant="outlined"
            style={{
              backgroundImage:
                'linear-gradient(90deg, #D0368A 0%, #708AD4 99%)',
            }}
          >
            <Typography color="textSecondary">{t('sendemail')}</Typography>
            <br />
            <BallSpinner size={50} color="#17F9F2" loading={isSendingBREmail} />
          </Paper>
        ) : null}
      </Box>
    </Box>
  )
}

const useTableHeadingStyles = makeStyles(() => ({
  tableRowRoot: {
    '& th': {
      fontSize: 13,
      padding: 8,
      fontWeight: Fonts.BOLD,
      fontFamily: 'Titillium Web, sans-serif',
      color: 'red',
      '&:first-child': {
        paddingLeft: 20,
      },
      '&:last-child': {
        paddingRight: 20,
      },
    },
  },
}))

const TableHeading = () => {
  const classes = useTableHeadingStyles()
  const { t } = useTranslation()
  return (
    <TableRow className={classes.tableRowRoot}>
      <TableCell>{t('id')}</TableCell>
      <TableCell>{t('company_name')}</TableCell>
      <TableCell>{t('contact_person')}</TableCell>
      <TableCell>{t('contact_email')}</TableCell>
      <TableCell>{t('client_contact_number')}</TableCell>
      <TableCell>{t('BR_expiry_date')}</TableCell>
      <TableCell>{t('action')}</TableCell>
    </TableRow>
  )
}

const useTableItemStyles = makeStyles((theme: Theme) => ({
  tableCell: {
    fontSize: 13,
    padding: 8,
    fontFamily: 'Titillium Web, sans-serif',
    whiteSpace: 'normal',
    '&:first-child': {
      paddingLeft: 20,
    },
    '&:last-child': {
      paddingRight: 20,
    },
  },
  whiteSpace: {
    whiteSpace: 'normal',
  },
  anchar: {
    color: theme.palette.primary.main,
    borderBottom: `1px solid ${theme.palette.primary.main}`,
    display: 'inline-block',
  },
  badgeRoot: {
    padding: '3px 10px',
    borderRadius: 4,
    display: 'inline-block',
  },
}))

interface TableItemProps {
  data: IBRCompanyTableRow
}

const TableItem: React.FC<TableItemProps> = ({ data }) => {
  const classes = useTableItemStyles()
  const { t } = useTranslation()
  const dateString = data.BR_expiry_date.toString().split('T')[0].split('-')

  return (
    <TableRow key={data.id} className="item-hover">
      <TableCell component="th" scope="row" className={classes.tableCell}>
        {data.id}
      </TableCell>
      <TableCell align="left" className={classes.tableCell}>
        <Box display="flex" alignItems="center">
          <Box fontWeight={Fonts.BOLD}>{data.company_name}</Box>
        </Box>
      </TableCell>
      <TableCell align="left" className={classes.tableCell}>
        {data.contact_person}
      </TableCell>
      <TableCell align="left" className={classes.tableCell}>
        {data.contact_email}
      </TableCell>
      <TableCell align="left" className={classes.tableCell}>
        {data.client_contact_number}
      </TableCell>
      <TableCell align="left" className={classes.tableCell}>
        {/* {data.BR_expiry_date.toString().split('T')[0]} */}
        {t('date-long', {
          date: dateString[2],
          month: t(MonthLongEN[parseInt(dateString[1])]),
          year: dateString[0],
        })}
      </TableCell>
      <TableCell align="right">
        <AppMenu id={data.id} />
      </TableCell>
    </TableRow>
  )
}

const useBoxStyles = makeStyles(() =>
  createStyles({
    boxRoot: {
      '&::-webkit-scrollbar': {
        width: '12px',
      },
      '&::-webkit-scrollbar-track': {
        backgroundColor: '#e4e4e4',
        borderRadius: '100px',
      },
      '&::-webkit-scrollbar-thumb': {
        backgroundImage: 'linear-gradient(90deg, #D0368A 0%, #708AD4 99%)',
        borderRadius: '100px',
        boxShadow: 'inset 2px 2px 5px 0 rgba(#fff, 0.5)',
        borderColor: '#10B292',
        border: '1px solid rgba(0,0,0,0.2)',
      },
    },
  }),
)

interface BRExpiryDateTableProps {
  renderedData: IBRCompanyTableRow[]
  pageSizeOptions: number[]
}

const BRExpiryDateTable: React.FC<BRExpiryDateTableProps> = ({
  renderedData,
  pageSizeOptions,
}) => {
  const [_page, setPage] = React.useState(0)
  const [_pageSize, setPageSize] = React.useState(pageSizeOptions[0])
  const [_renderedData, setRenderedData] = React.useState<IBRCompanyTableRow[]>(
    [],
  )
  const [_rangeOfDataShown, setRangeOfDataShown] = React.useState<number[]>([])
  const classes = useBoxStyles()
  const { t } = useTranslation()

  React.useEffect(() => {
    async function fetchData() {
      if (renderedData.length > 0) {
        const renderedDataList = []
        for (
          let i = _page * _pageSize;
          i <
          (renderedData.length < _page * _pageSize + _pageSize
            ? renderedData.length
            : _page * _pageSize + _pageSize);
          i++
        ) {
          renderedDataList.push(renderedData[i])
        }
        setRenderedData(renderedDataList)
        setRangeOfDataShown([
          _page * _pageSize,
          renderedData.length < _page * _pageSize + _pageSize
            ? renderedData.length
            : _page * _pageSize + _pageSize,
        ])
      }
    }
    fetchData()
  }, [_page, _pageSize, renderedData])

  const handleSelectChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    // console.log(event.target.value)
    setPageSize(event.target.value as number)
  }

  const handlePreviousPageClick = async () => {
    const new_page = _page - 1
    if (new_page >= 0) {
      setPage(new_page)
    }
  }

  const handleNextPageClick = async () => {
    const new_page = _page + 1
    if (new_page <= (renderedData.length / _pageSize) >> 0) {
      setPage(new_page)
    }
  }

  return (
    <Box>
      <Typography
        component="div"
        id="tableTitle"
        align="center"
        style={{ fontFamily: FontFamily.TITLE, fontSize: 50 }}
      >
        {t('company-list')}
      </Typography>
      <Typography
        component="div"
        id="tableTitle"
        align="center"
        variant="h6"
        style={{ fontFamily: FontFamily.TITLE, color: 'grey' }}
      >
        {'(' + t('BR_expiry_date') + ' ≤ 1 ' + t('month') + ')'}
      </Typography>
      <br />
      <Box border={1} borderRadius={8} borderColor="error.main">
        <AppTableContainer>
          <Table className="table">
            <colgroup>
              <col style={{ width: '10%' }} />
              <col style={{ width: '40%' }} />
              <col style={{ width: '10%' }} />
              <col style={{ width: '10%' }} />
              <col style={{ width: '10%' }} />
              <col style={{ width: '19%' }} />
              <col style={{ width: '1%' }} />
            </colgroup>
            <TableHead>
              <TableHeading />
            </TableHead>
            <TableBody>
              {_renderedData.map((data) => (
                <TableItem data={data} key={data.id} />
              ))}
            </TableBody>
          </Table>
        </AppTableContainer>
        <Box
          component="div"
          overflow="auto"
          display="flex"
          flexDirection="row-reverse"
          className={classes.boxRoot}
        >
          <Box height="100%" alignItems="center" px={2} width={128}>
            <Tooltip title={t('previous-page').toString()} arrow>
              <IconButton
                aria-label="previous-page"
                onClick={handlePreviousPageClick}
              >
                <ArrowLeftIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title={t('next-page').toString()} arrow>
              <IconButton aria-label="next-page" onClick={handleNextPageClick}>
                <ArrowRightIcon />
              </IconButton>
            </Tooltip>
          </Box>
          <Box
            textAlign="center"
            display="flex"
            justifyContent="center"
            alignItems="center"
            px={2}
            width={128}
          >
            <Typography color="textSecondary">
              {`${_rangeOfDataShown[0] + 1}-${_rangeOfDataShown[1]} / ${
                renderedData.length
              }`}
            </Typography>
          </Box>
          <Box
            textAlign="center"
            display="flex"
            justifyContent="center"
            alignItems="center"
            px={2}
            width={200}
          >
            <Typography color="textSecondary">
              {t('rows-per-page') + ': '}
            </Typography>
            <Box pl={1}>
              <Select
                id="select-page-size"
                defaultValue={_pageSize}
                value={_pageSize}
                onChange={handleSelectChange}
                style={{ color: 'grey' }}
              >
                {pageSizeOptions.map((option: number) => (
                  <MenuItem key={option} value={option}>
                    {option}
                  </MenuItem>
                ))}
              </Select>
            </Box>
          </Box>
          <Box
            textAlign="center"
            display="flex"
            justifyContent="center"
            alignItems="center"
            px={2}
            width={128}
          >
            <Typography color="textSecondary">
              {t('page') + ': ' + (_page + 1)}
            </Typography>
          </Box>
        </Box>
      </Box>
    </Box>
  )
}

export default BRExpiryDateTable
