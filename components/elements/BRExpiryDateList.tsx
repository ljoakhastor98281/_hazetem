import React from 'react'
import clsx from 'clsx'
import {
  DataGrid,
  GridColDef,
  GridPageChangeParams,
  GridRowId,
  GridSelectionModelChangeParams,
  GridToolbarColumnsButton,
  GridToolbarContainer,
  GridToolbarDensitySelector,
  GridToolbarExport,
  GridToolbarFilterButton,
  GridValueGetterParams,
} from '@material-ui/data-grid'
import {
  Box,
  Button,
  ListItemIcon,
  ListItemText,
  Menu,
  MenuItem,
  Toolbar,
  Typography,
  IconButton,
} from '@material-ui/core'
import { createStyles, makeStyles, withStyles } from '@material-ui/core/styles'
import { Theme } from '@material-ui/core/styles'
import { lighten } from '@material-ui/core/styles'
import axios from 'axios'
import DehazeIcon from '@material-ui/icons/Dehaze'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import { useTranslation } from 'next-i18next'
import { IBRCompanyTableRow } from '@components/interfaces/interfaces'
import MailIcon from '@material-ui/icons/Mail'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import FadeButton from '@/components/elements/FadeButton'
import zalgo from '../../server/utils/zalgo'
// import DeleteIcon from '@material-ui/icons/Delete'
// import EditIcon from '@material-ui/icons/Edit'
// import PageviewIcon from '@material-ui/icons/Pageview'
// import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf'
// import RoomServiceIcon from '@material-ui/icons/RoomService'
// import { PDFDocument, StandardFonts, rgb } from 'pdf-lib'
// import JSZip from 'jszip'
// import FileSaver from 'file-saver'
// import { format } from 'date-fns'
// import { ICompany } from '@/server/api/Company/company.model'
// import { NextRouter } from 'next/router'

const useEnhancedTableToolbarStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(1),
      [theme.breakpoints.down('sm')]: {
        display: 'block',
      },
    },
    highlight:
      theme.palette.type === 'light'
        ? {
            color: theme.palette.secondary.main,
            backgroundColor: lighten(theme.palette.secondary.light, 0.85),
            margin: theme.spacing(1),
          }
        : {
            color: theme.palette.text.primary,
            backgroundColor: theme.palette.secondary.dark,
          },
    title: {
      flex: '1 1 100%',
      alignItems: 'center',
      justifyContent: 'center',
      textAlign: 'center',
      fontSize: 50,
      [theme.breakpoints.down('xs')]: {
        fontSize: 40,
      },
      fontFamily: 'Sintony, sans-serif',
      color: lighten(theme.palette.secondary.light, 0.15),
    },
    subtitle: {
      flex: '1 1 100%',
      fontSize: 20,
      [theme.breakpoints.down('xs')]: {
        fontSize: 15,
      },
      color: theme.palette.secondary.main,
      fontFamily: 'Sintony, sans-serif',
    },
    toolbar: {
      flex: '1 1 100%',
      fontSize: 20,
      [theme.breakpoints.down('sm')]: {
        fontSize: 15,
      },
      color: theme.palette.secondary.main,
    },
    buttonMarigin: {
      margin: theme.spacing(1),
    },
    button: {
      margin: theme.spacing(1),
      maxWidth: 200,
      width: '100%',
    },
    mobileButton: {
      margin: theme.spacing(1),
    },
    input: {
      marginLeft: theme.spacing(1),
      flex: 1,
    },
    iconButton: {
      padding: 10,
    },
    sectionDesktop: {
      display: 'none',
      [theme.breakpoints.up('md')]: {
        display: 'flex',
      },
    },
    sectionMobile: {
      display: 'flex',
      [theme.breakpoints.up('md')]: {
        display: 'none',
      },
    },
  }),
)

const StyledDataGrid = withStyles({
  root: {
    '& .MuiDataGrid-window': {
      overflowY: 'visible !important',
      // bottom: 1,
    },
    '& .MuiDataGrid-columnsHeader': {
      alignItems: 'center',
    },
    '& .MuiDataGrid-row': {
      maxHeight: 'none !important',
    },
    '& .MuiDataGrid-viewport': {
      maxHeight: 'max-content !important',
    },
    '& .MuiDataGrid-cell': {
      lineHeight: 'unset !important',
      verticalAlign: 'middle',
      position: 'relative',
      display: 'flex !important',
      alignItems: 'center',
      maxHeight: 'none !important',
      whiteSpace: 'normal !important',
      overflowWrap: 'break-word',
    },
  },
})(DataGrid)

interface EnhancedTableToolbarProps {
  numSelected: number
  textFieldData: { filteredData: string }
  setTextFieldData: React.Dispatch<
    React.SetStateAction<{
      filteredData: string
    }>
  >
  onSendBRMailClick: () => void
}

const EnhancedTableToolbar = (props: EnhancedTableToolbarProps) => {
  const classes = useEnhancedTableToolbarStyles()
  const { t } = useTranslation()
  const { numSelected } = props
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }
  const today = new Date()

  const menuItems: JSX.Element[] = []
  menuItems.push(
    <MenuItem
      key="send-br-remind-mail"
      onClick={props.onSendBRMailClick}
      disabled={props.numSelected == 0 ? false : true}
    >
      <ListItemIcon>
        <MailIcon fontSize="small" />
      </ListItemIcon>
      <ListItemText primary={t('send-br-remind-mail')} />
    </MenuItem>,
  )

  const handleClose = () => {
    setAnchorEl(null)
  }

  return (
    <>
      <Toolbar className={clsx(classes.root)}>
        <Typography className={classes.title} id="tableTitle" component="div">
          {t(zalgo.generate('company list (br expiration <= 1 month)'))}
        </Typography>
      </Toolbar>
      <Typography
        className={classes.subtitle}
        id="tableSubtitle"
        component="div"
      >
        {`${t('today')}: ${today.getDate()}-${
          today.getMonth() + 1
        }-${today.getFullYear()}`}
      </Typography>
      {numSelected > 0 ? (
        <>
          <Toolbar
            className={clsx(classes.buttonMarigin, classes.sectionDesktop, {
              [classes.highlight]: numSelected > 0,
            })}
          >
            <Typography
              className={classes.toolbar}
              color="inherit"
              component="div"
            >
              {t('selected')} {numSelected}
            </Typography>
            <FadeButton
              text={t('send-br-remind-mail')}
              onClick={props.onSendBRMailClick}
              ariaLabel={'send-br-remind-mail'}
              startIcon={<MailIcon />}
              disabled={numSelected == 1 ? false : true}
            />
          </Toolbar>
          <Toolbar
            className={clsx(classes.root, classes.sectionMobile, {
              [classes.highlight]: numSelected > 0,
            })}
          >
            <Typography
              className={classes.subtitle}
              color="inherit"
              component="div"
            >
              {t('selected')} {numSelected}
            </Typography>
            <IconButton
              aria-controls="action-menu"
              aria-haspopup="true"
              onClick={handleClick}
              color="primary"
            >
              <MoreVertIcon />
            </IconButton>
            <Menu
              id="action-menu"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleClose}
              getContentAnchorEl={null}
              anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
              transformOrigin={{ vertical: 'top', horizontal: 'center' }}
            >
              {menuItems}
            </Menu>
          </Toolbar>
        </>
      ) : (
        <></>
      )}
    </>
  )
}

interface IListProps {
  list: IBRCompanyTableRow[]
}

export default function BRExpiryDateList({ list }: IListProps) {
  const [page, setPage] = React.useState(0)
  const { t } = useTranslation()
  const [selected, setSelected] = React.useState<GridRowId[]>([])
  const [formData, setFormData] = React.useState({ filteredData: '' })
  const [pageSize, setPageSize] = React.useState(5)
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)

  const columns: GridColDef[] = [
    { field: 'id', headerName: 'ID', width: 250 },
    { field: 'company_name', headerName: t('company_name'), width: 250 },
    { field: 'contact_person', headerName: t('contact_person'), width: 250 },
    { field: 'contact_email', headerName: t('contact_email'), width: 300 },
    {
      field: 'client_contact_number',
      headerName: t('client_contact_number'),
      width: 250,
    },
    {
      field: 'BR_expiry_date',
      headerName: t('BR_expiry_date'),
      width: 250,
      valueGetter: (params: GridValueGetterParams) => {
        return params.row.BR_expiry_date.toString().split('T')[0]
      },
    },
  ]

  const handleSendBRMailButton = async () => {
    await axios
      .get(`/mail/sendbrremind/${selected[0]}`)
      .then((res) => {
        toast.success(t('br reminder email sent to ') + res.data.email, {
          position: 'bottom-right',
        })
      })
      .catch((err) => {
        // console.log(err.response)
        switch (err.response.status) {
          case 418: {
            toast.error(t('send-email-error'), {
              position: 'bottom-right',
            })
            break
          }
          case 404: {
            toast.error(t('404-company-error'), {
              position: 'bottom-right',
            })
            break
          }
          case 403: {
            toast.error(t('email-unauth'), {
              position: 'bottom-right',
            })
            break
          }
          default:
            break
        }
      })
  }

  const handlePageSizeChange = async (param: GridPageChangeParams) => {
    setPageSize(param.pageSize)
  }

  const handleSelectionChange = (params: GridSelectionModelChangeParams) => {
    setSelected(params.selectionModel)
  }

  const handleChangePage = (param: GridPageChangeParams) => {
    setPage(param.page)
  }

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  const buttonStyles = makeStyles((theme: Theme) =>
    createStyles({
      button: {
        margin: theme.spacing(1),
        padding: theme.spacing(1),
      },
      sectionDesktop: {
        display: 'none',
        [theme.breakpoints.up('md')]: {
          display: 'flex',
        },
      },
      sectionMobile: {
        display: 'flex',
        [theme.breakpoints.up('md')]: {
          display: 'none',
        },
      },
    }),
  )

  function CustomToolbar() {
    const classes = buttonStyles()
    return (
      <>
        <div className={classes.sectionDesktop}>
          <GridToolbarContainer>
            <GridToolbarColumnsButton />
            <GridToolbarFilterButton />
            <GridToolbarDensitySelector />
            <GridToolbarExport />
          </GridToolbarContainer>
        </div>
        <div className={classes.sectionMobile}>
          <Button
            aria-controls="grid-menu"
            aria-haspopup="true"
            onClick={handleClick}
            variant="contained"
            color="primary"
            startIcon={<DehazeIcon />}
            className={classes.button}
          >
            {t('filter')}
          </Button>
          <Menu
            id="grid-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
            getContentAnchorEl={null}
            anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
            transformOrigin={{ vertical: 'top', horizontal: 'center' }}
          >
            <MenuItem onClick={handleClose}>
              <GridToolbarColumnsButton />
            </MenuItem>
            <MenuItem onClick={handleClose}>
              <GridToolbarFilterButton />
            </MenuItem>
            <MenuItem>
              <GridToolbarDensitySelector />
            </MenuItem>
            <MenuItem>
              <GridToolbarExport />
            </MenuItem>
          </Menu>
        </div>
      </>
    )
  }

  return (
    <div style={{ height: 400, width: '100%' }}>
      <EnhancedTableToolbar
        numSelected={selected.length}
        textFieldData={formData}
        setTextFieldData={setFormData}
        onSendBRMailClick={handleSendBRMailButton}
      />
      <br />
      <br />
      <Box>
        <StyledDataGrid
          autoHeight
          // rowHeight={100}
          rows={list}
          page={page}
          columns={columns}
          pageSize={pageSize}
          onPageSizeChange={handlePageSizeChange}
          onPageChange={handleChangePage}
          checkboxSelection={true}
          rowsPerPageOptions={[5, 10, 15, 20, 25]}
          onSelectionModelChange={handleSelectionChange}
          components={{ Toolbar: CustomToolbar }}
          disableColumnMenu
          disableSelectionOnClick
        />
      </Box>
      <ToastContainer />
    </div>
  )
}
