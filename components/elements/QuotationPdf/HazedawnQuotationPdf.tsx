/* eslint-disable jsx-a11y/alt-text */
import React from 'react'
import {
  Page,
  Text,
  View,
  Document,
  StyleSheet,
  PDFViewer,
  Image,
} from '@react-pdf/renderer'
import { IQuotationPdfProps } from './CommonProps'
import Hazedawn_logo from '../../../server/assets/vendor_logo/HD_logo_V_white.png'
import Hazedawn_sig from '../../../server/assets/vendor_signature/hd_sign.png'
import Hazedawn_chop from '../../../server/assets/vendor_signature/HD_Chop.png'

// Create styles
const styles = StyleSheet.create({
  page: {
    flexDirection: 'column',
    backgroundColor: '#E4E4E4',
  },
  section: {
    margin: 10,
    padding: 10,
    flexGrow: 1,
  },
  header: {
    width: '100%',
    height: '25%',
    flexDirection: 'row',
  },
  header_right: {
    width: '40%',
    flexDirection: 'column',
  },
  header_left: {
    width: '60%',
    flexDirection: 'column',
  },
  project_name: {
    textDecoration: 'underline',
  },
  item: {
    flexDirection: 'row',
    fontSize: 12,
  },
  description: {
    fontSize: 12,
  },
  vendor_logo: {
    height: '100',
    width: '100',
  },
})

export const HazedawnQuotationPdfDoc = (props: IQuotationPdfProps) => {
  try {
    return (
      <Document title={props.quotation_number}>
        <Page size="A4" style={styles.page}>
          <View style={styles.header} fixed>
            <View style={styles.header_left}>
              <Image
                source={async () => Hazedawn_logo.src}
                style={styles.vendor_logo}
              />
              <Text>{props.company_name}</Text>
              <Text>{props.company_address}</Text>
              <Text>{props.email}</Text>
              <Text>{props.contact_person}</Text>
              <Text>{`Quotation#: ${props.quotation_number}`}</Text>
              <Text style={styles.project_name}>{props.project_name}</Text>
            </View>
            <View style={styles.header_right}>
              <View></View>
              <View>
                <Text>Quotation</Text>
                <Text>Date</Text>
                <Text
                  render={({ pageNumber, totalPages }) =>
                    `${pageNumber} / ${totalPages}`
                  }
                />
              </View>
            </View>
          </View>

          <View>
            {props.quotation_items.map((item, idx) => {
              return (
                <View key={idx} style={styles.item} wrap={false}>
                  <Text>{item.description}</Text>
                  <Text>{item.price}</Text>
                </View>
              )
            })}
          </View>
          <View>
            <Image
              source={async () => Hazedawn_sig.src}
              style={styles.vendor_logo}
            />
            <Image
              source={async () => Hazedawn_chop.src}
              style={styles.vendor_logo}
            />
            <Text
              style={{ fontSize: 15 }}
            >{`Total amount: ${props.total_price} `}</Text>
          </View>
        </Page>
      </Document>
    )
  } catch (err) {
    return <div>loading...</div>
  }
}

// Create Document Component
export default function HazedawnQuotationPdf(props: IQuotationPdfProps) {
  const [width, setWidth] = React.useState(0)
  const [height, setHeight] = React.useState(0)
  React.useEffect(() => {
    setWidth(window.innerWidth)
    setHeight(window.innerHeight)
  }, [])
  return (
    <div>
      <PDFViewer width={width} height={height}>
        <HazedawnQuotationPdfDoc {...props} />
      </PDFViewer>
    </div>
  )
}
