/* eslint-disable jsx-a11y/alt-text */
import React from 'react'
import {
  Page,
  Text,
  View,
  Document,
  StyleSheet,
  PDFViewer,
  Image,
  Font,
} from '@react-pdf/renderer'
import { IQuotationPdfProps } from './CommonProps'
import {
  Table,
  TableHeader,
  TableCell,
  TableBody,
  DataTableCell,
} from '@david.kucsai/react-pdf-table'

Font.register({
  family: 'PT Sans',
  fonts: [
    {
      src: 'https://fonts.gstatic.com/s/ptsans/v8/FUDHvzEKSJww3kCxuiAo2A.ttf',
    },
    {
      src: 'https://fonts.gstatic.com/s/ptsans/v8/0XxGQsSc1g4rdRdjJKZrNC3USBnSvpkopQaUR-2r7iU.ttf',
      fontWeight: 'bold',
    },
  ],
})

// Create styles
const styles = StyleSheet.create({
  page: {
    flexDirection: 'column',
    // backgroundColor: '#E4E4E4',
    margin: 30,
  },
  section: {
    // margin: 40,
    // padding: 40,
    // flexGrow: 1,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '90%',
  },
  subSection: {
    display: 'flex',
    flexDirection: 'column',
    margin: 10,
  },
  header: {
    width: '90%',
    // height: '25%',
    flexDirection: 'row',
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  // header_right: {
  //   width: '40%',
  //   flexDirection: 'column',
  // },
  // header_left: {
  //   width: '60%',
  //   flexDirection: 'column',
  // },
  project_name: {
    textDecoration: 'underline',
  },
  item: {
    flexDirection: 'row',
    fontSize: 12,
  },
  description: {
    fontSize: 12,
  },
  vendor_logo: {
    height: '150',
    width: '150',
  },
  vendor_chop: {
    height: '100',
    width: '100',
  },
  tableCell: {
    fontFamily: 'PT Sans',
    fontSize: 12,
    padding: 3,
  },
})

export const GenericQuotationPdfDoc = (props: IQuotationPdfProps) => {
  try {
    return (
      <Document title={props.quotation_number}>
        <Page size="A4" style={styles.page}>
          <View style={styles.header} fixed>
            <Image
              source={async () => props.vendor_image}
              style={styles.vendor_logo}
            />
            <Text>QUOTE</Text>
            {/* <View>
              <View></View>
              <View>
                <Text>Quotation</Text>
                <Text>Date</Text>
                <Text
                  render={({ pageNumber, totalPages }) =>
                    `${pageNumber} / ${totalPages}`
                  }
                />
              </View>
            </View> */}
          </View>
          <View>
            {!props.limited_info ? (
              <div style={styles.section}>
                <div style={styles.subSection}>
                  <Text
                    style={{
                      fontFamily: 'PT Sans',
                      fontWeight: 'bold',
                      fontSize: 14,
                    }}
                  >
                    From:
                  </Text>
                  {props.company_name ? (
                    <Text
                      style={{
                        fontFamily: 'PT Sans',
                        fontSize: 12,
                      }}
                    >
                      {props.company_name}
                    </Text>
                  ) : (
                    <div></div>
                  )}
                  {props.company_address ? (
                    <Text
                      style={{
                        fontFamily: 'PT Sans',
                        fontSize: 12,
                      }}
                    >
                      {props.company_address}
                    </Text>
                  ) : (
                    <div></div>
                  )}
                  <Text
                    style={{
                      fontFamily: 'PT Sans',
                      fontSize: 12,
                    }}
                  >
                    {props.email}
                  </Text>
                  <Text
                    style={{
                      fontFamily: 'PT Sans',
                      fontSize: 12,
                    }}
                  >
                    Tel: {props.contact_person}
                  </Text>
                </div>
                <div
                  style={{
                    width: '50%',
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                  }}
                >
                  <Table>
                    <TableHeader>
                      <TableCell
                        style={{
                          fontFamily: 'PT Sans',
                          fontSize: 12,
                          padding: 3,
                        }}
                      >
                        Quote Number
                      </TableCell>
                      <TableCell
                        style={{
                          fontFamily: 'PT Sans',
                          fontSize: 12,
                          padding: 3,
                        }}
                      >
                        {props.quotation_number}
                      </TableCell>
                    </TableHeader>
                  </Table>
                  <Table>
                    <TableHeader>
                      <TableCell
                        style={{
                          fontFamily: 'PT Sans',
                          fontSize: 12,
                          padding: 3,
                          marginTop: -1,
                        }}
                      >
                        Quote Date
                      </TableCell>
                      <TableCell
                        style={{
                          fontFamily: 'PT Sans',
                          fontSize: 12,
                          padding: 3,
                          marginTop: -1,
                        }}
                      ></TableCell>
                    </TableHeader>
                  </Table>
                  <Table>
                    <TableHeader>
                      <TableCell
                        style={{
                          fontFamily: 'PT Sans',
                          fontSize: 12,
                          backgroundColor: '#d6edff',
                          padding: 3,
                          marginTop: -1,
                        }}
                      >
                        Total
                      </TableCell>
                      <TableCell
                        style={{
                          fontFamily: 'PT Sans',
                          fontSize: 12,
                          backgroundColor: '#d6edff',
                          padding: 3,
                          marginTop: -1,
                        }}
                      >
                        {props.total_price}
                      </TableCell>
                    </TableHeader>
                  </Table>
                </div>
              </div>
            ) : (
              <div>
                <Text
                  style={{
                    fontFamily: 'PT Sans',
                    fontSize: 12,
                  }}
                >
                  * No peronsal information for preview
                </Text>
              </div>
            )}
            <View style={styles.section}>
              <Text
                style={{
                  fontFamily: 'PT Sans',
                  fontSize: 12,
                  margin: 10,
                }}
              >
                {props.project_name}
              </Text>
            </View>
          </View>

          <View break>
            <View style={{ width: '90%' }} fixed>
              <Table>
                <TableHeader>
                  <TableCell
                    weighting={0.15}
                    style={{
                      fontFamily: 'PT Sans',
                      fontWeight: 'bold',
                      fontSize: 14,
                      paddingTop: 5,
                      paddingLeft: 5,
                    }}
                  >
                    Hrs/Qty
                  </TableCell>
                  <TableCell
                    weighting={0.35}
                    style={{
                      fontFamily: 'PT Sans',
                      fontWeight: 'bold',
                      fontSize: 14,
                      padding: 5,
                    }}
                  >
                    Service
                  </TableCell>
                  <TableCell
                    weighting={0.25}
                    style={{
                      fontFamily: 'PT Sans',
                      fontWeight: 'bold',
                      fontSize: 14,
                      padding: 5,
                    }}
                  >
                    Rate/Price
                  </TableCell>
                  <TableCell
                    weighting={0.25}
                    style={{
                      fontFamily: 'PT Sans',
                      fontWeight: 'bold',
                      fontSize: 14,
                      padding: 5,
                    }}
                  >
                    Sub Total
                  </TableCell>
                </TableHeader>
              </Table>
            </View>
            <View style={{ width: '90%' }} break>
              {props.quotation_items.map((item, idx) => {
                return (
                  <Table
                    key={idx}
                    data={[
                      {
                        qty: item.order,
                        service: item.title + '\n\n' + item.description,
                        price: item.price,
                        subtotal: item.price,
                      },
                    ]}
                  >
                    {/* <TableHeader></TableHeader> */}
                    <TableBody>
                      <DataTableCell
                        weighting={0.15}
                        style={(styles.tableCell, { textAlign: 'center' })}
                        getContent={(r) => r.qty}
                      />
                      <DataTableCell
                        weighting={0.35}
                        style={styles.tableCell}
                        getContent={(r) => r.service}
                      />
                      <DataTableCell
                        weighting={0.25}
                        style={styles.tableCell}
                        getContent={(r) => r.price}
                      />
                      <DataTableCell
                        weighting={0.25}
                        style={styles.tableCell}
                        getContent={(r) => r.subtotal}
                      />
                    </TableBody>
                  </Table>
                )
              })}
            </View>
            <div
              style={{
                width: '50%',
                flexDirection: 'row',
                flexWrap: 'wrap',
                margin: 20,
                left: 215,
              }}
            >
              <Table>
                <TableHeader>
                  <TableCell
                    style={{
                      fontFamily: 'PT Sans',
                      fontSize: 12,
                      padding: 3,
                    }}
                  >
                    Subtotal
                  </TableCell>
                  <TableCell
                    style={{
                      fontFamily: 'PT Sans',
                      fontSize: 12,
                      padding: 3,
                    }}
                  >
                    {props.total_price}
                  </TableCell>
                </TableHeader>
              </Table>
              <Table>
                <TableHeader>
                  <TableCell
                    style={{
                      fontFamily: 'PT Sans',
                      fontSize: 12,
                      padding: 3,
                      marginTop: -1,
                    }}
                  >
                    Tax
                  </TableCell>
                  <TableCell
                    style={{
                      fontFamily: 'PT Sans',
                      fontSize: 12,
                      padding: 3,
                      marginTop: -1,
                    }}
                  >
                    $0.00
                  </TableCell>
                </TableHeader>
              </Table>
              <Table>
                <TableHeader>
                  <TableCell
                    style={{
                      fontFamily: 'PT Sans',
                      fontSize: 12,
                      backgroundColor: '#d6edff',
                      padding: 3,
                      marginTop: -1,
                    }}
                  >
                    Total Due
                  </TableCell>
                  <TableCell
                    style={{
                      fontFamily: 'PT Sans',
                      fontSize: 12,
                      backgroundColor: '#d6edff',
                      padding: 3,
                      marginTop: -1,
                    }}
                  >
                    {props.total_price}
                  </TableCell>
                </TableHeader>
              </Table>
            </div>
          </View>

          {!props.limited_info ? (
            <View
              style={{
                display: 'flex',
                flexDirection: 'row',
              }}
            >
              <Image
                source={async () => props.chop_image}
                style={styles.vendor_chop}
              />
              <Image
                source={async () => props.signature_image}
                style={styles.vendor_chop}
              />
            </View>
          ) : (
            <div></div>
          )}
          <Text
            style={{
              fontFamily: 'PT Sans',
              fontSize: 12,
              position: 'absolute',
              bottom: 70,
              textAlign: 'center',
              color: 'grey',
            }}
            render={({ pageNumber, totalPages }) =>
              `Page ${pageNumber} / ${totalPages}`
            }
            fixed
          />
        </Page>
      </Document>
    )
  } catch (err) {
    // console.log(err.response)
    return <div>loading...</div>
  }
}

// Create Document Component
export default function GenericQuotationPdf(props: IQuotationPdfProps) {
  const [width, setWidth] = React.useState(0)
  const [height, setHeight] = React.useState(0)
  React.useEffect(() => {
    setWidth(window.innerWidth)
    setHeight(window.innerHeight)
  }, [])
  return (
    <div>
      <PDFViewer width={width} height={height}>
        <GenericQuotationPdfDoc {...props} />
      </PDFViewer>
    </div>
  )
}
