import { IServiceItem } from '@/server/api/Service/service.model'

export interface IQuotationPdfProps {
  company_name: string | null
  company_address: string | null
  email: string
  contact_person: string
  project_name: string
  quotation_items: IServiceItem[]
  total_price: number
  vendor: string
  quotation_number: string
  limited_info: boolean
  vendor_image: string
  chop_image: string
  signature_image: string
}

const vendor_options = [
  'Jedar',
  'Hazedawn',
  'Midas',
  'Find2Meals',
  'Avancer',
  'Sunergos',
  'AdAsia',
  'Fovea',
  'Starlight',
]

export { vendor_options }
