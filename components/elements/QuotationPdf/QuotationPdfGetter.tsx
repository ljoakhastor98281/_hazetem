import React from 'react'
import { IQuotationPdfProps } from '@/components/elements/QuotationPdf/CommonProps'
import { pdf, PDFViewer } from '@react-pdf/renderer'
import { StarlightQuotationPdfDoc } from './StarlightQuotationPdf'
import { GenericQuotationPdfDoc } from './GenericQuotationPdf'
import { AdAsiaQuotationPdfDoc } from './AdAsiaQuotationPdf'
import { AvancerQuotationPdfDoc } from './AvancerQuotationPdf'
import { Find2MealsQuotationPdfDoc } from './Find2MealsQuotationPdf'
import { FoveaQuotationPdfDoc } from './FoveaQuotationPdf'
import { HazedawnQuotationPdfDoc } from './HazedawnQuotationPdf'
import { JedarQuotationPdfDoc } from './JedarQuotationPdf'
import { MidasQuotationPdfDoc } from './MidasQuotationPdf'
import { SunergosQuotationPdfDoc } from './SunergosQuotationPdf'
import axios from 'axios'
import { IInvoice } from '@/server/api/Invoice/invoice.model'
import { IRequestHistory } from '@/server/api/RequestHistory/requesthistory.model'
import { IUser } from '@/server/api/User/user.model'

const QuotationPdfDocGetter = (props: IQuotationPdfProps) => {
  switch (props.vendor) {
    case 'AdAsia':
      return <AdAsiaQuotationPdfDoc {...props} />
    case 'Avancer':
      return <AvancerQuotationPdfDoc {...props} />
    case 'Find2Meals':
      return <Find2MealsQuotationPdfDoc {...props} />
    case 'Fovea':
      return <FoveaQuotationPdfDoc {...props} />
    case 'Hazedawn':
      return <HazedawnQuotationPdfDoc {...props} />
    case 'Jedar':
      return <JedarQuotationPdfDoc {...props} />
    case 'Midas':
      return <MidasQuotationPdfDoc {...props} />
    case 'Starlight':
      return <StarlightQuotationPdfDoc {...props} />
    case 'Sunergos':
      return <SunergosQuotationPdfDoc {...props} />
    default:
      return <GenericQuotationPdfDoc {...props} />
  }
}

interface IQuotationPdfViewerProps {
  quotation_pdf_props: IQuotationPdfProps
  width: number
  height: number
}

const QuotationPdfGetter = ({
  quotation_pdf_props,
  width,
  height,
}: IQuotationPdfViewerProps) => {
  return (
    <PDFViewer width={width} height={height}>
      <QuotationPdfDocGetter {...quotation_pdf_props} />
    </PDFViewer>
  )
}

export const getQuotationPdfUrl = async (
  id: string,
  user_level: number,
): Promise<string> => {
  const quotation_res = await axios.get(`/invoice/${id}`, {
    params: {
      byRequestId: user_level < 4,
    },
  })
  const quotation_info: IInvoice = quotation_res.data.invoice
  const request_history_res = await axios.get(
    `/requesthistory/${quotation_info.request_history}`,
    { params: { withFullInfo: true } },
  )
  const request_history_info: IRequestHistory =
    request_history_res.data.request_history
  const props: IQuotationPdfProps = {
    company_name: (request_history_info.requester as IUser)
      .normal_user_company_name,
    company_address: (request_history_info.requester as IUser)
      .normal_user_company_address,
    email: (request_history_info.requester as IUser).email,
    contact_person: (request_history_info.requester as IUser).username,
    project_name: quotation_info.project_name,
    quotation_items: quotation_info.quotation_items,
    total_price: quotation_info.price,
    vendor: quotation_info.from,
    quotation_number: quotation_info.invoice_number,
    limited_info: false,
  }
  const blob = await pdf(<QuotationPdfDocGetter {...props} />).toBlob()
  // FileSaver.saveAs(blob, 'test.pdf') this is to test blob is valid
  return new Promise<string>((resolve) => {
    const reader = new FileReader()
    reader.onloadend = () => {
      resolve(reader.result as string)
    }
    reader.readAsDataURL(blob)
  })
}

export default QuotationPdfGetter
