import React from 'react'
import clsx from 'clsx'
import {
  DataGrid,
  GridColDef,
  GridPageChangeParams,
  GridRowId,
  GridSelectionModelChangeParams,
  GridToolbarColumnsButton,
  GridToolbarContainer,
  GridToolbarDensitySelector,
  GridToolbarExport,
  GridToolbarFilterButton,
  GridValueGetterParams,
  GridValueFormatterParams,
  GridCellParams,
} from '@material-ui/data-grid'
import {
  Button,
  ListItemIcon,
  ListItemText,
  Menu,
  MenuItem,
  Toolbar,
  Tooltip,
  Typography,
  IconButton,
} from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/Delete'
import EditIcon from '@material-ui/icons/Edit'
import PageviewIcon from '@material-ui/icons/Pageview'
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf'
import RoomServiceIcon from '@material-ui/icons/RoomService'
import {
  createStyles,
  fade,
  makeStyles,
  withStyles,
} from '@material-ui/core/styles'
import { Theme } from '@material-ui/core/styles'
// import { lighten } from '@material-ui/core/styles'
import { PDFDocument, StandardFonts, rgb } from 'pdf-lib'
import JSZip from 'jszip'
import FileSaver from 'file-saver'
import { format } from 'date-fns'
import axios from 'axios'
import { ICompany } from '@/server/api/Company/company.model'
import { NextRouter } from 'next/router'
import DehazeIcon from '@material-ui/icons/Dehaze'
import { useTranslation } from 'next-i18next'
import MoreVertIcon from '@material-ui/icons/MoreVert'

const useEnhancedTableToolbarStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(1),
      [theme.breakpoints.down('sm')]: {
        display: 'block',
      },
    },
    highlight:
      theme.palette.type === 'light'
        ? {
            color: '#004369',
            backgroundColor: '#d8e9ef',
            margin: theme.spacing(1),
          }
        : {
            color: theme.palette.text.primary,
            backgroundColor: theme.palette.secondary.dark,
          },
    title: {
      flex: '1 1 100%',
      alignItems: 'center',
      justifyContent: 'center',
      textAlign: 'center',
      fontSize: 50,
      [theme.breakpoints.down('xs')]: {
        fontSize: 40,
      },
      fontFamily: 'Sintony, sans-serif',
    },
    subtitle: {
      flex: '1 1 100%',
      fontSize: 20,
      [theme.breakpoints.down('sm')]: {
        fontSize: 15,
      },
    },
    buttonMarigin: {
      margin: theme.spacing(1),
    },
    button: {
      margin: theme.spacing(1),
      maxWidth: 200,
      width: '100%',
      backgroundColor: '#4ea1d3',
      color: 'white',
    },
    button2: {
      margin: theme.spacing(1),
      maxWidth: 200,
      width: '100%',
      backgroundColor: '#e85a71',
      color: 'white',
    },
    mobileButton: {
      margin: theme.spacing(1),
    },
    input: {
      marginLeft: theme.spacing(1),
      flex: 1,
    },
    iconButton: {
      padding: 10,
    },
    sectionDesktop: {
      display: 'none',
      [theme.breakpoints.up('md')]: {
        display: 'flex',
      },
    },
    sectionMobile: {
      display: 'flex',
      [theme.breakpoints.up('md')]: {
        display: 'none',
      },
    },
    tooltip: {
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
    },
  }),
)

// const StyledDataGrid = withStyles({
//   root: {
//     '& .MuiDataGrid-window': {
//       overflowY: 'visible !important',
//     },
//     '& .MuiDataGrid-columnsHeader': {
//       alignItems: 'center',
//     },
//     '& .MuiDataGrid-row': {
//       maxHeight: 'none !important',
//     },
//     '& .MuiDataGrid-viewport': {
//       maxHeight: 'max-content !important',
//     },
//     '& .MuiDataGrid-cell': {
//       lineHeight: 'unset !important',
//       verticalAlign: 'middle',
//       position: 'relative',
//       display: 'flex !important',
//       alignItems: 'center',
//       maxHeight: 'none !important',
//       whiteSpace: 'normal !important',
//       overflowWrap: 'break-word',
//     },
//   },
// })(DataGrid)

interface EnhancedTableToolbarProps {
  numSelected: number
  onEditclick: () => void
  onDeleteClick: () => void
  onGenPdfClick: () => void
  onShowPdfClick: () => void
  onServiceClick: () => void
  textFieldData: { filteredData: string }
  setTextFieldData: React.Dispatch<
    React.SetStateAction<{
      filteredData: string
    }>
  >
}

const EnhancedTableToolbar = (props: EnhancedTableToolbarProps) => {
  const classes = useEnhancedTableToolbarStyles()
  const { t } = useTranslation()
  const { numSelected } = props
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  return (
    <>
      <Toolbar className={clsx(classes.root)}>
        <Typography className={classes.title} id="tableTitle" component="div">
          {t('company-list')}
        </Typography>
      </Toolbar>
      {numSelected > 0 ? (
        <>
          <Toolbar
            className={clsx(classes.buttonMarigin, classes.sectionDesktop, {
              [classes.highlight]: numSelected > 0,
            })}
          >
            <Typography
              className={classes.subtitle}
              color="inherit"
              component="div"
            >
              {t('selected')} {numSelected}
            </Typography>
            <Button
              variant="contained"
              aria-label="edit"
              color="primary"
              disabled={numSelected == 1 ? false : true}
              className={classes.button}
              onClick={props.onEditclick}
              startIcon={<EditIcon />}
            >
              {t('edit')}
            </Button>
            <Button
              variant="contained"
              aria-label="edit"
              color="primary"
              disabled={numSelected == 1 ? false : true}
              className={classes.button}
              onClick={props.onServiceClick}
              startIcon={<RoomServiceIcon />}
            >
              {t('service')}
            </Button>

            <Button
              variant="contained"
              color="primary"
              aria-label="gen-pdf"
              className={classes.button}
              onClick={props.onGenPdfClick}
              startIcon={<PictureAsPdfIcon />}
            >
              {t('download-pdf')}
            </Button>
            <Button
              variant="contained"
              color="primary"
              aria-label="show-pdf"
              className={classes.button}
              onClick={props.onShowPdfClick}
              startIcon={<PageviewIcon />}
            >
              {t('view-pdf')}
            </Button>
            <Button
              variant="contained"
              aria-label="delete"
              color="secondary"
              className={classes.button2}
              onClick={props.onDeleteClick}
              startIcon={<DeleteIcon />}
            >
              {t('delete')}
            </Button>
          </Toolbar>
          <Toolbar
            className={clsx(classes.root, classes.sectionMobile, {
              [classes.highlight]: numSelected > 0,
            })}
          >
            <Typography
              className={classes.subtitle}
              color="inherit"
              component="div"
            >
              {t('selected')} {numSelected}
            </Typography>
            <IconButton
              aria-controls="action-menu"
              aria-haspopup="true"
              onClick={handleClick}
              color="primary"
            >
              <MoreVertIcon />
            </IconButton>
            <Menu
              id="action-menu"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleClose}
              getContentAnchorEl={null}
              anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
              transformOrigin={{ vertical: 'top', horizontal: 'center' }}
            >
              <MenuItem
                onClick={props.onEditclick}
                disabled={numSelected == 1 ? false : true}
              >
                <ListItemIcon>
                  <EditIcon fontSize="small" />
                </ListItemIcon>
                <ListItemText primary={t('edit')} />
              </MenuItem>
              <MenuItem
                onClick={props.onServiceClick}
                disabled={numSelected == 1 ? false : true}
              >
                <ListItemIcon>
                  <RoomServiceIcon fontSize="small" />
                </ListItemIcon>
                <ListItemText primary={t('service')} />
              </MenuItem>
              <MenuItem onClick={props.onDeleteClick}>
                <ListItemIcon>
                  <DeleteIcon fontSize="small" />
                </ListItemIcon>
                <ListItemText primary={t('delete')} />
              </MenuItem>
              <MenuItem onClick={props.onGenPdfClick}>
                <ListItemIcon>
                  <PictureAsPdfIcon fontSize="small" />
                </ListItemIcon>
                <ListItemText primary={t('download-pdf')} />
              </MenuItem>
              <MenuItem onClick={props.onShowPdfClick}>
                <ListItemIcon>
                  <PageviewIcon fontSize="small" />
                </ListItemIcon>
                <ListItemText primary={t('view-pdf')} />
              </MenuItem>
            </Menu>
          </Toolbar>
        </>
      ) : (
        <></>
      )}
    </>
  )
}

interface IListProps {
  companylist: ICompany[]
  router: NextRouter
}

const CellTooltip = withStyles({
  arrow: {
    color: fade('#282c37', 0.9),
  },
  tooltip: {
    fontSize: '1em',
    color: 'white',
    backgroundColor: fade('#282c37', 0.9),
    whiteSpace: 'pre-line',
  },
})(Tooltip)

export default function CompanyList({ companylist, router }: IListProps) {
  const [renderedData, setRenderedData] =
    React.useState<ICompany[]>(companylist)
  const [page, setPage] = React.useState(0)
  const { t } = useTranslation()
  const [selected, setSelected] = React.useState<GridRowId[]>([])
  const [formData, setFormData] = React.useState({ filteredData: '' })
  const [pageSize, setPageSize] = React.useState(5)
  const classes = useEnhancedTableToolbarStyles()

  const columns: GridColDef[] = [
    { field: '_id', headerName: 'ID', width: 250, hide: true },
    {
      field: 'company_name',
      headerName: t('company_name'),
      width: 250,
      renderCell: (params: GridCellParams) => (
        <CellTooltip title={params.row.company_name} arrow placement="top">
          <div className={classes.tooltip}>{params.row.company_name}</div>
        </CellTooltip>
      ),
    },
    {
      field: 'business_outline',
      headerName: t('business_outline'),
      width: 250,
      renderCell: (params: GridCellParams) => (
        <CellTooltip title={params.row.business_outline} arrow placement="top">
          <div className={classes.tooltip}>{params.row.business_outline}</div>
        </CellTooltip>
      ),
    },
    {
      field: 'company_outline',
      headerName: t('company_outline'),
      width: 250,
      renderCell: (params: GridCellParams) => (
        <CellTooltip title={params.row.company_outline} arrow placement="top">
          <div className={classes.tooltip}>{params.row.company_outline}</div>
        </CellTooltip>
      ),
    },
    {
      field: 'company_start_year',
      headerName: t('company_start_year'),
      width: 190,
    },
    { field: 'number_of_staff', headerName: t('number_of_staff'), width: 170 },
    {
      field: 'company_address',
      headerName: t('company_address'),
      width: 250,
      renderCell: (params: GridCellParams) => (
        <CellTooltip title={params.row.company_address} arrow placement="top">
          <div className={classes.tooltip}>{params.row.company_address}</div>
        </CellTooltip>
      ),
    },
    {
      field: 'contact_person',
      headerName: t('contact_person'),
      width: 250,
      renderCell: (params: GridCellParams) => (
        <CellTooltip title={params.row.contact_person} arrow placement="top">
          <div className={classes.tooltip}>{params.row.contact_person}</div>
        </CellTooltip>
      ),
    },
    {
      field: 'client_contact_number',
      headerName: t('client_contact_number'),
      width: 220,
    },
    {
      field: 'contact_email',
      headerName: t('contact_email'),
      width: 300,
      renderCell: (params: GridCellParams) => (
        <CellTooltip title={params.row.contact_email} arrow placement="top">
          <div className={classes.tooltip}>{params.row.contact_email}</div>
        </CellTooltip>
      ),
    },
    {
      field: 'BR_expiry_date',
      headerName: t('BR_expiry_date'),
      width: 170,
      valueGetter: (params: GridValueGetterParams) => {
        return params.row.BR_expiry_date.toString().split('T')[0]
      },
    },
    {
      field: 'tvp_account_name',
      headerName: t('tvp_account_name'),
      width: 250,
      renderCell: (params: GridCellParams) => (
        <CellTooltip title={params.row.tvp_account_name} arrow placement="top">
          <div className={classes.tooltip}>{params.row.tvp_account_name}</div>
        </CellTooltip>
      ),
    },
    {
      field: 'tvp_password',
      headerName: t('tvp_password'),
      width: 250,
      renderCell: (params: GridCellParams) => (
        <CellTooltip title={params.row.tvp_password} arrow placement="top">
          <div className={classes.tooltip}>{params.row.tvp_password}</div>
        </CellTooltip>
      ),
    },
    {
      field: 'project_name',
      headerName: t('project_name'),
      width: 250,
      renderCell: (params: GridCellParams) => (
        <CellTooltip title={params.row.project_name} arrow placement="top">
          <div className={classes.tooltip}>{params.row.project_name}</div>
        </CellTooltip>
      ),
    },
    {
      field: 'date_of_agreement_signed',
      headerName: t('date_of_agreement_signed'),
      width: 250,
      valueGetter: (params: GridValueGetterParams) => {
        return params.row.date_of_agreement_signed.toString().split('T')[0]
      },
    },
    {
      field: 'status',
      headerName: t('status'),
      width: 250,
      renderCell: (params: GridCellParams) => (
        <CellTooltip title={params.row.status} arrow placement="top">
          <div className={classes.tooltip}>{params.row.status}</div>
        </CellTooltip>
      ),
    },
    { field: 'sales_lead', headerName: t('sales_lead'), width: 250 },
    {
      field: 'total_amount',
      headerName: t('total_amount'),
      width: 170,
      valueFormatter: (params: GridValueFormatterParams) => {
        if (params.row.total_amount) {
          return `$ ${params.row.total_amount}`
        } else {
          return '-'
        }
      },
    },
    {
      field: 'remark',
      headerName: t('remark'),
      width: 250,
      renderCell: (params: GridCellParams) => (
        <CellTooltip title={params.row.remark} arrow placement="top">
          <div className={classes.tooltip}>{params.row.remark}</div>
        </CellTooltip>
      ),
    },
    {
      field: 'admin_added',
      headerName: t('admin_added'),
      width: 250,
      hide: true,
      valueGetter: (params: GridValueGetterParams) => {
        if (params.row.admin_added) {
          return params.row.admin_added.toString().split('T')[0]
        } else {
          return '-'
        }
      },
    },
    {
      field: 'admin_modified',
      headerName: t('admin_modified'),
      width: 250,
      hide: true,
      valueGetter: (params: GridValueGetterParams) => {
        if (params.row.admin_modified) {
          return params.row.admin_modified.toString().split('T')[0]
        } else {
          return '-'
        }
      },
    },
  ]

  const handlePageSizeChange = async (param: GridPageChangeParams) => {
    setPageSize(param.pageSize)
  }

  const handleSelectionChange = (params: GridSelectionModelChangeParams) => {
    setSelected(params.selectionModel)
  }

  const updateRenderedData = async () => {
    const res = await axios.get('/company/')
    res.data.companys.map((company: ICompany) => {
      company.id = company._id
    })
    setRenderedData(res.data.companys)
    setSelected([])
  }

  const handleChangePage = (param: GridPageChangeParams) => {
    setPage(param.page)
  }

  const editButtonHandler = async () => {
    selected.map((item: GridRowId) => {
      router.push(`/company/edit/${item}`)
    })
  }

  const deleteButtonHandler = async () => {
    const decision = window.confirm(t('delete-alert'))
    if (!decision) {
      return
    }
    selected.map(async (item: GridRowId) => {
      await axios.delete(`/company/${item}`)
      updateRenderedData()
      router.reload()
    })
  }

  const fillSingleTemplate = async (id: GridRowId, arrayBuffer: Uint8Array) => {
    const request_url = 'company/' + id
    const res = await axios.get(request_url)
    const company: ICompany = res.data.company
    const pdfDoc = await PDFDocument.load(arrayBuffer)
    const timesRomanFont = await pdfDoc.embedFont(StandardFonts.TimesRoman)
    const pages = pdfDoc.getPages()
    const { width, height } = pages[0].getSize()
    pages[0].drawText(company.project_name, {
      x: width / 3 + 22,
      y: height / 2 + 72,
      size: 10,
      font: timesRomanFont,
      color: rgb(0.0, 0.0, 0.0),
    })
    pages[1].drawText(company.company_name, {
      x: width / 2 - 120,
      y: height / 2 - 22,
      size: 15,
      font: timesRomanFont,
      color: rgb(0.0, 0.0, 0.0),
    })
    const dateAdded = new Date(company.createdAt)
    const formatted_date =
      format(dateAdded, 'EEEE do ') + 'of' + format(dateAdded, ' MMMM yyyy')
    pages[2].drawText(formatted_date, {
      x: width / 2,
      y: height / 4 - 20,
      size: 15,
      font: timesRomanFont,
      color: rgb(0.0, 0.0, 0.0),
    })
    pdfDoc.setTitle(company.company_name + '_' + company._id)
    return pdfDoc
  }
  const genPdfButtonHandler = async () => {
    const decision = window.confirm(`${t('gen-pdf-alert')}`)
    if (!decision) return
    const template_url = '/company/pdf-template'
    let arrayBuffer: Uint8Array
    await axios.get(template_url).then(async (res) => {
      arrayBuffer = res.data
      if (selected.length == 1) {
        const pdfDoc = await fillSingleTemplate('' + selected[0], arrayBuffer)
        const filename = pdfDoc.getTitle() + '.pdf'
        const pdfData = await pdfDoc.save()
        const file = new Blob([pdfData], { type: 'application/pdf' })
        FileSaver.saveAs(file, filename)
      }
      if (selected.length > 1) {
        const zip = new JSZip()
        Promise.all(
          selected.map(async (id: GridRowId) => {
            const pdfDoc = await fillSingleTemplate(id, arrayBuffer)
            const filename = pdfDoc.getTitle() + '.pdf'
            const pdfData = await pdfDoc.save()
            zip.file(filename, pdfData)
          }),
        ).then(() => {
          zip.generateAsync({ type: 'blob' }).then(function (array) {
            FileSaver.saveAs(array, 'generated_pdf.zip')
          })
        })
      }
    })
  }

  const serviceButtonHandler = async () => {
    selected.map((item: GridRowId) => {
      router.push(`/company/service/${item}`)
    })
  }

  const showPdfButtonHandler = async () => {
    if (selected.length > 5) {
      window.alert(t('view-pdf-alert-excess'))
      return
    }
    const decision = window.confirm(`${t('view-pdf-alert')}`)
    if (!decision) return
    selected.map((id: GridRowId) => {
      router.push(`/company/viewpdf/${id}`)
    })
  }

  const buttonStyles = makeStyles((theme: Theme) =>
    createStyles({
      button: {
        margin: theme.spacing(1),
        padding: theme.spacing(1),
      },
      sectionDesktop: {
        display: 'none',
        [theme.breakpoints.up('md')]: {
          display: 'flex',
        },
      },
      sectionMobile: {
        display: 'flex',
        [theme.breakpoints.up('md')]: {
          display: 'none',
        },
      },
    }),
  )

  function CustomToolbar() {
    const classes = buttonStyles()
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
    const handleClick = (event: React.MouseEvent<HTMLElement>) => {
      setAnchorEl(event.currentTarget)
    }

    const handleClose = () => {
      setAnchorEl(null)
    }
    return (
      <>
        <div className={classes.sectionDesktop}>
          <GridToolbarContainer>
            <GridToolbarColumnsButton />
            <GridToolbarFilterButton />
            <GridToolbarDensitySelector />
            <GridToolbarExport />
          </GridToolbarContainer>
        </div>
        <div className={classes.sectionMobile}>
          <Button
            aria-controls="grid-menu"
            aria-haspopup="true"
            onClick={handleClick}
            variant="contained"
            color="primary"
            // eslint-disable-next-line react/jsx-no-undef
            startIcon={<DehazeIcon />}
            className={classes.button}
          >
            {t('filter')}
          </Button>
          <Menu
            id="grid-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
            getContentAnchorEl={null}
            anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
            transformOrigin={{ vertical: 'top', horizontal: 'center' }}
          >
            <MenuItem onClick={handleClose}>
              <GridToolbarColumnsButton />
            </MenuItem>
            <MenuItem onClick={handleClose}>
              <GridToolbarFilterButton />
            </MenuItem>
            <MenuItem>
              <GridToolbarDensitySelector />
            </MenuItem>
            <MenuItem>
              <GridToolbarExport />
            </MenuItem>
          </Menu>
        </div>
      </>
    )
  }

  return (
    <>
      {renderedData && (
        <>
          <div style={{ height: 400, width: '100%' }}>
            <EnhancedTableToolbar
              numSelected={selected.length}
              onEditclick={editButtonHandler}
              onDeleteClick={deleteButtonHandler}
              onGenPdfClick={genPdfButtonHandler}
              onShowPdfClick={showPdfButtonHandler}
              onServiceClick={serviceButtonHandler}
              textFieldData={formData}
              setTextFieldData={setFormData}
            />
            <br />
            <br />
            <DataGrid
              autoHeight
              page={page}
              rows={companylist}
              columns={columns}
              pageSize={pageSize}
              onPageSizeChange={handlePageSizeChange}
              onPageChange={handleChangePage}
              checkboxSelection={true}
              rowsPerPageOptions={[5, 10, 15, 20, 25]}
              onSelectionModelChange={handleSelectionChange}
              components={{ Toolbar: CustomToolbar }}
              disableColumnMenu
              disableSelectionOnClick
            />
          </div>
        </>
      )}
    </>
  )
}
