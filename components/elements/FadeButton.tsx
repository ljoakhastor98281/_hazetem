import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'

const useStyles = makeStyles({
  root: {
    background:
      'radial-gradient(circle, rgba(249,254,42,1) 0%, rgba(41,208,153,1) 84%)',
    borderRadius: 3,
    border: 0,
    color: 'white',
    height: 48,
    padding: '0 30px',
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    width: '1000px',
  },
  label: {
    textTransform: 'capitalize',
  },
})

interface IButtonProps {
  text: string
  onClick: (event: React.MouseEvent<HTMLElement>) => void
  ariaLabel?: string
  startIcon?: React.ReactNode
  disabled?: boolean
}

export default function FadeButton({
  text,
  onClick,
  ariaLabel,
  startIcon,
  disabled,
}: IButtonProps) {
  const classes = useStyles()

  return (
    <Button
      classes={{
        root: classes.root, // class name, e.g. `classes-nesting-root-x`
        label: classes.label, // class name, e.g. `classes-nesting-label-x`
      }}
      variant="contained"
      onClick={onClick}
      aria-label={ariaLabel}
      startIcon={startIcon}
      disabled={disabled}
    >
      {text}
    </Button>
  )
}
