import React, { useRef } from 'react'
import Button from '@material-ui/core/Button'
import SignaturePad from 'react-signature-canvas'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import { useTranslation } from 'next-i18next'

export interface ISignatureDialogProps {
  open: boolean
  title: string
  onClose: () => void
  handleSave: (data: string) => void
}

export default function SignatureDialog(props: ISignatureDialogProps) {
  const { t } = useTranslation()
  const padRef = useRef({}) as React.MutableRefObject<any>
  return (
    <Dialog open={props.open} onClose={props.onClose}>
      <DialogTitle>{props.title}</DialogTitle>
      <DialogContent>
        <SignaturePad ref={padRef} />
      </DialogContent>
      <DialogActions>
        <Button onClick={props.onClose}>{t('cancel')}</Button>
        <Button
          onClick={() => {
            padRef.current.clear()
          }}
        >
          {t('clear')}
        </Button>
        <Button
          onClick={() => {
            const data = padRef.current.getCanvas().toDataURL('image/png')
            props.handleSave(data)
            props.onClose()
          }}
        >
          {t('save')}
        </Button>
      </DialogActions>
    </Dialog>
  )
}
