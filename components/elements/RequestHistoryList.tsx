import React from 'react'
import clsx from 'clsx'
// import { useSession } from 'next-auth/client'
import {
  DataGrid,
  GridColDef,
  GridPageChangeParams,
  GridRowId,
  GridSelectionModelChangeParams,
  GridToolbarContainer,
  GridToolbarColumnsButton,
  GridToolbarFilterButton,
  GridToolbarExport,
  GridToolbarDensitySelector,
  GridCellParams,
} from '@material-ui/data-grid'
import {
  ListItemIcon,
  ListItemText,
  Toolbar,
  Tooltip,
  Typography,
  IconButton,
} from '@material-ui/core'
import Button from '@material-ui/core/Button'
import ArchiveIcon from '@material-ui/icons/Archive'
import UnarchiveIcon from '@material-ui/icons/Unarchive'
import ReceiptIcon from '@material-ui/icons/Receipt'
import DoneIcon from '@material-ui/icons/Done'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf'
import {
  createStyles,
  fade,
  makeStyles,
  withStyles,
} from '@material-ui/core/styles'
import { Theme, useTheme } from '@material-ui/core/styles'
import axios from 'axios'
import { IRequestHistory } from '../../server/api/RequestHistory/requesthistory.model'
import { NextRouter } from 'next/router'
import DehazeIcon from '@material-ui/icons/Dehaze'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import { useTranslation } from 'next-i18next'
import SwipeableViews from 'react-swipeable-views'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Box from '@material-ui/core/Box'
import { IServiceItem } from '@/server/api/Service/service.model'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import CancelIcon from '@material-ui/icons/Cancel'
import DirectionsRunIcon from '@material-ui/icons/DirectionsRun'
import CheckCircleIcon from '@material-ui/icons/CheckCircle'
import BlockIcon from '@material-ui/icons/Block'
import AlarmIcon from '@material-ui/icons/Alarm'

interface TabPanelProps {
  children?: React.ReactNode
  dir?: string
  index: number
  value: number
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3} height={700}>
          {children}
        </Box>
      )}
    </div>
  )
}

function a11yProps(index: number) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  }
}

const useToolbarStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(1),
      [theme.breakpoints.down('sm')]: {
        display: 'block',
      },
    },
    highlight:
      theme.palette.type === 'light'
        ? {
            color: '#004369',
            backgroundColor: '#d8e9ef',
            margin: theme.spacing(1),
          }
        : {
            color: theme.palette.text.primary,
            backgroundColor: theme.palette.secondary.dark,
          },
    title: {
      flex: '1 1 100%',
      alignItems: 'center',
      justifyContent: 'center',
      textAlign: 'center',
      fontSize: 50,
      [theme.breakpoints.down('xs')]: {
        fontSize: 40,
      },
      fontFamily: 'Sintony, sans-serif',
    },
    subtitle: {
      flex: '1 1 100%',
      fontSize: 20,
      [theme.breakpoints.down('sm')]: {
        fontSize: 15,
      },
    },
    buttonMarigin: {
      margin: theme.spacing(1),
    },
    button: {
      margin: theme.spacing(1),
      display: 'flex',
      maxWidth: 220,
      width: '100%',
      backgroundColor: '#4ea1d3',
      color: 'white',
    },
    button2: {
      margin: theme.spacing(1),
      maxWidth: 220,
      width: '100%',
      backgroundColor: '#e85a71',
      color: 'white',
    },
    mobileButton: {
      margin: theme.spacing(1),
    },
    input: {
      marginLeft: theme.spacing(1),
      flex: 1,
    },
    iconButton: {
      padding: 10,
    },
    sectionDesktop: {
      display: 'none',
      [theme.breakpoints.up('md')]: {
        display: 'flex',
      },
    },
    sectionMobile: {
      display: 'flex',
      [theme.breakpoints.up('md')]: {
        display: 'none',
      },
    },
    tooltip: {
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
    },
    status0: {
      color: '#2AB67B',
      borderColor: '#2AB67B',
      display: 'flex',
      width: '100%',
      justifyContent: 'center',
    },
    status1: {
      color: '#530021',
      borderColor: '#530021',
      display: 'flex',
      width: '100%',
      justifyContent: 'center',
    },
    status2: {
      color: '#ED2461',
      borderColor: '#ED2461',
      display: 'flex',
      width: '100%',
      justifyContent: 'center',
    },
    status3: {
      color: '#398170',
      borderColor: '#398170',
      display: 'flex',
      width: '100%',
      justifyContent: 'center',
    },
    status4: {
      color: '#444F59',
      borderColor: '#444F59',
      display: 'flex',
      width: '100%',
      justifyContent: 'center',
    },
    marginright: {
      marginRight: 3,
    },
  }),
)

// const StyledDataGrid = withStyles({
//   root: {
//     '& .MuiDataGrid-window': {
//       overflowY: 'visible !important',
//     },
//     '& .MuiDataGrid-columnsHeader': {
//       alignItems: 'center',
//     },
//     '& .MuiDataGrid-row': {
//       maxHeight: 'none !important',
//     },
//     '& .MuiDataGrid-viewport': {
//       maxHeight: 'max-content !important',
//     },
//     '& .MuiDataGrid-cell': {
//       lineHeight: 'unset !important',
//       verticalAlign: 'middle',
//       position: 'relative',
//       display: 'flex !important',
//       alignItems: 'center',
//       maxHeight: 'none !important',
//       whiteSpace: 'normal !important',
//       overflowWrap: 'break-word',
//     },
//   },
// })(DataGrid)

interface EnhancedTableToolbarProps {
  level: number
  showUnArchive: number
  numSelected: number
  selectsDeleted: boolean
  onArchiveClick: () => void
  onUnArchiveClick: () => void
  onAddQuotationClick: () => void
  onApproveClick: () => void
  onDeclineClick: () => void
  onViewPdfClick: () => void
  textFieldData: { filteredData: string }
  setTextFieldData: React.Dispatch<
    React.SetStateAction<{
      filteredData: string
    }>
  >
}

const EnhancedTableToolbar = (props: EnhancedTableToolbarProps) => {
  const classes = useToolbarStyles()
  const { numSelected } = props
  // const [session, loading] = useSession()
  const { t } = useTranslation()
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  const menuItems: JSX.Element[] = []
  if (props.level < 4) {
    menuItems.push(
      <MenuItem
        key="approve"
        onClick={props.onApproveClick}
        disabled={props.selectsDeleted}
      >
        <ListItemIcon>
          <DoneIcon fontSize="small" />
        </ListItemIcon>
        <ListItemText primary={t('approve')} />
      </MenuItem>,
      <MenuItem
        key="decline"
        onClick={props.onDeclineClick}
        disabled={props.selectsDeleted}
      >
        <ListItemIcon>
          <BlockIcon fontSize="small" />
        </ListItemIcon>
        <ListItemText primary={t('decline')} />
      </MenuItem>,
      <MenuItem
        key="add-quotation"
        onClick={props.onAddQuotationClick}
        disabled={props.numSelected != 1 || props.selectsDeleted}
      >
        <ListItemIcon>
          <ReceiptIcon fontSize="small" />
        </ListItemIcon>
        <ListItemText primary={t('add-quotation')} />
      </MenuItem>,
    )
  }
  menuItems.push(
    <MenuItem
      key="view-pdf"
      onClick={props.onViewPdfClick}
      disabled={props.selectsDeleted}
    >
      <ListItemIcon>
        <PictureAsPdfIcon fontSize="small" />
      </ListItemIcon>
      <ListItemText
        primary={
          props.level >= 4 ? t('view-quotation') : t('preview-quotation')
        }
      />
    </MenuItem>,
  )
  if (props.showUnArchive == 0) {
    menuItems.push(
      <MenuItem onClick={props.onArchiveClick} key="archive">
        <ListItemIcon>
          <ArchiveIcon fontSize="small" />
        </ListItemIcon>
        <ListItemText primary={t('archive')} />
      </MenuItem>,
    )
  } else {
    menuItems.push(
      <MenuItem onClick={props.onUnArchiveClick} key="unarchive">
        <ListItemIcon>
          <ArchiveIcon fontSize="small" />
        </ListItemIcon>
        <ListItemText primary={t('unarchive')} />
      </MenuItem>,
    )
  }

  return (
    <>
      {menuItems && (
        <>
          <Toolbar className={clsx(classes.root)}>
            {props.level > 3 ? (
              <Typography
                className={classes.title}
                // variant="h3"
                id="tableTitle"
                component="div"
              >
                {t('request-history')}
              </Typography>
            ) : (
              <Typography
                className={classes.title}
                variant="h3"
                id="tableTitle"
                component="div"
              >
                {t('client-list')}
              </Typography>
            )}
          </Toolbar>
          {numSelected > 0 ? (
            <>
              <Toolbar
                className={clsx(classes.buttonMarigin, classes.sectionDesktop, {
                  [classes.highlight]: numSelected > 0,
                })}
              >
                <Typography
                  className={classes.subtitle}
                  color="inherit"
                  component="div"
                >
                  {t('selected')} {numSelected}
                </Typography>
                {props.level < 4 ? (
                  <>
                    <Button
                      variant="contained"
                      aria-label="approve"
                      color="primary"
                      className={classes.button}
                      onClick={props.onApproveClick}
                      startIcon={<DoneIcon />}
                      disabled={props.selectsDeleted}
                    >
                      {t('approve')}
                    </Button>
                    <Button
                      variant="contained"
                      aria-label="decline"
                      color="secondary"
                      className={classes.button2}
                      onClick={props.onDeclineClick}
                      startIcon={<BlockIcon />}
                      disabled={props.selectsDeleted}
                    >
                      {t('decline')}
                    </Button>
                    <Button
                      variant="contained"
                      aria-label="edit"
                      color="primary"
                      className={classes.button}
                      onClick={props.onAddQuotationClick}
                      disabled={
                        (props.numSelected == 1 ? false : true) ||
                        props.selectsDeleted
                      }
                      startIcon={<ReceiptIcon />}
                    >
                      {t('add-quotation')}
                    </Button>
                  </>
                ) : (
                  <div></div>
                )}
                <Button
                  variant="contained"
                  aria-label="view-pdf"
                  color="primary"
                  className={classes.button}
                  onClick={props.onViewPdfClick}
                  startIcon={<PictureAsPdfIcon />}
                  disabled={props.selectsDeleted}
                >
                  {props.level >= 4
                    ? t('view-quotation')
                    : t('preview-quotation')}
                </Button>
                {props.showUnArchive == 0 ? (
                  <Button
                    variant="contained"
                    aria-label="edit"
                    color="secondary"
                    className={classes.button2}
                    onClick={props.onArchiveClick}
                    startIcon={<ArchiveIcon />}
                  >
                    {t('archive')}
                  </Button>
                ) : (
                  <Button
                    variant="contained"
                    aria-label="edit"
                    color="secondary"
                    className={classes.button}
                    onClick={props.onUnArchiveClick}
                    startIcon={<UnarchiveIcon />}
                  >
                    {t('unarchive')}
                  </Button>
                )}
              </Toolbar>
              <Toolbar
                className={clsx(classes.root, classes.sectionMobile, {
                  [classes.highlight]: numSelected > 0,
                })}
              >
                <Typography
                  className={classes.subtitle}
                  color="inherit"
                  component="div"
                >
                  {t('selected')} {numSelected}
                </Typography>
                <IconButton
                  aria-controls="action-menu"
                  aria-haspopup="true"
                  onClick={handleClick}
                  color="primary"
                >
                  <MoreVertIcon />
                </IconButton>
                <Menu
                  id="action-menu"
                  anchorEl={anchorEl}
                  keepMounted
                  open={Boolean(anchorEl)}
                  onClose={handleClose}
                  getContentAnchorEl={null}
                  anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
                  transformOrigin={{ vertical: 'top', horizontal: 'center' }}
                >
                  {menuItems}
                </Menu>
              </Toolbar>
            </>
          ) : (
            <div></div>
          )}
        </>
      )}
    </>
  )
}

interface IListProps {
  requesthistorylist: IRequestHistory[]
  router: NextRouter
  user_level: number
}

const CellTooltip = withStyles({
  arrow: {
    color: fade('#282c37', 0.9),
  },
  tooltip: {
    fontSize: '1em',
    color: 'white',
    backgroundColor: fade('#282c37', 0.9),
    whiteSpace: 'pre-line',
  },
})(Tooltip)

export default function RequestHistoryList({
  requesthistorylist,
  router,
  user_level,
}: IListProps) {
  const [page, setPage] = React.useState(0)
  const [selected, setSelected] = React.useState<GridRowId[]>([])
  const [formData, setFormData] = React.useState({ filteredData: '' })
  const [archivedData, setArchivedData] = React.useState<IRequestHistory[]>([])
  const [unarchivedData, setUnarchivedData] = React.useState<IRequestHistory[]>(
    [],
  )
  const { t } = useTranslation()
  const classes = useToolbarStyles()
  const [pageSize, setPageSize] = React.useState(5)
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
  const [showUnArchive, setShowUnArchive] = React.useState(0)
  const theme = useTheme()

  React.useEffect(() => {
    async function fetchData() {
      const archived: IRequestHistory[] = []
      const unarchived: IRequestHistory[] = []

      requesthistorylist.map((requesthistory) => {
        requesthistory.archive_status == 0
          ? unarchived.push(requesthistory)
          : archived.push(requesthistory)
      })

      setArchivedData(() => archived)
      setUnarchivedData(() => unarchived)
    }
    fetchData()
  }, [requesthistorylist])

  const columns: GridColDef[] = [
    {
      field: 'service_name',
      headerName: t('service_name'),
      width: 250,
      renderCell: (params: GridCellParams) => (
        <CellTooltip
          title={params.row.service ? params.row.service.service_name : ''}
          arrow
          placement="top"
        >
          <div className={classes.tooltip}>
            {!params.row.service
              ? `*${t('deleted-service')}`
              : params.row.service?.service_name}
          </div>
        </CellTooltip>
      ),
      valueGetter: (params) => {
        return !params.row.service
          ? `*${t('deleted-service')}`
          : params.row.service?.service_name
      },
    },
    {
      field: 'company_name',
      headerName: t('company_name'),
      width: 250,
      hide: user_level == 2 || user_level == 3,
      renderCell: (params: GridCellParams) => (
        <CellTooltip
          title={params.row.company?.company_name}
          arrow
          placement="top"
        >
          <div className={classes.tooltip}>
            {!params.row.company
              ? `*${t('deleted-company')}`
              : params.row.company?.company_name}
          </div>
        </CellTooltip>
      ),
      valueGetter: (params) => {
        return !params.row.company
          ? `*${t('deleted-company')}`
          : params.row.company?.company_name
      },
    },
    {
      field: 'requester',
      headerName: t('requester'),
      width: 250,
      hide: user_level == 4,
      renderCell: (params: GridCellParams) => (
        <CellTooltip
          title={params.row.requester?.username}
          arrow
          placement="top"
        >
          <div className={classes.tooltip}>
            {!params.row.requester
              ? `*${t('deleted-requester')}`
              : params.row.requester?.username}
          </div>
        </CellTooltip>
      ),
      valueGetter: (params) => {
        return !params.row.requester
          ? `*${t('deleted-requester')}`
          : params.row.requester?.username
      },
    },
    {
      field: 'status',
      headerName: t('request_status'),
      width: 175,
      renderCell: (params: GridCellParams) => {
        switch (params.row.status) {
          case 0:
            return (
              <Box
                border={1}
                borderRadius={8}
                padding={1}
                className={classes.status0}
              >
                <Typography className={classes.status0}>
                  <DirectionsRunIcon
                    className={classes.marginright}
                  ></DirectionsRunIcon>
                  {t('ongoing')}
                </Typography>
              </Box>
            )
          case 1:
            return (
              <Box
                border={1}
                borderRadius={8}
                padding={1}
                className={classes.status1}
              >
                <Typography className={classes.status1}>
                  <BlockIcon className={classes.marginright}></BlockIcon>
                  {t('cancelled')}
                </Typography>
              </Box>
            )
          case 2:
            return (
              <Box
                border={1}
                borderRadius={8}
                padding={1}
                className={classes.status2}
              >
                <Typography className={classes.status2}>
                  <CancelIcon className={classes.marginright}></CancelIcon>
                  {t('declined')}
                </Typography>
              </Box>
            )
          case 3:
            return (
              <Box
                border={1}
                borderRadius={8}
                padding={1}
                className={classes.status3}
              >
                <Typography className={classes.status3}>
                  <CheckCircleIcon
                    className={classes.marginright}
                  ></CheckCircleIcon>
                  {t('approved')}
                </Typography>
              </Box>
            )
          case 4:
            return (
              <Box
                border={1}
                borderRadius={8}
                padding={1}
                className={classes.status4}
              >
                <Typography className={classes.status4}>
                  <AlarmIcon className={classes.marginright}></AlarmIcon>
                  {t('expired')}
                </Typography>
              </Box>
            )
          default:
            return (
              <Box border={1} borderRadius={8} padding={1}>
                <Typography>{t('unknown')}</Typography>
              </Box>
            )
        }
      },
      valueGetter: (params) => {
        switch (params.row.status) {
          case 0:
            return t('ongoing')
          case 1:
            return t('cancelled')
          case 2:
            return t('declined')
          case 3:
            return t('approved')
          case 4:
            return t('expired')
          default:
            return t('unknown')
        }
      },
    },
    {
      field: 'requested_items',
      headerName: t('requested-items'),
      width: 190,
      renderCell: (params) => (
        <strong>
          <Button
            variant="contained"
            color="primary"
            size="small"
            style={{ marginLeft: 16, backgroundColor: '#4ea1d3' }}
            onClick={() => {
              router.push(`/requesthistory/item/${params.row._id}`)
            }}
          >
            {
              (params.row.requested_items as IServiceItem[]).filter(
                (item: IServiceItem) => item.needed,
              ).length
            }
          </Button>
        </strong>
      ),
    },
    {
      field: 'request_date',
      headerName: t('request_date'),
      width: 170,
      valueGetter: (params) => {
        return params.row.request_date?.toString().split('T')[0]
      },
    },
    {
      field: 'contact_person',
      headerName: t('contact_person'),
      width: 250,
      hide: user_level == 2 || user_level == 3,
      renderCell: (params: GridCellParams) => (
        <CellTooltip
          title={params.row.company?.contact_person}
          arrow
          placement="top"
        >
          <div className={classes.tooltip}>
            {!params.row.company
              ? `*${t('deleted-company')}`
              : params.row.company?.contact_person}
          </div>
        </CellTooltip>
      ),
      valueGetter: (params) => {
        return !params.row.company
          ? `*${t('deleted-company')}`
          : params.row.company?.contact_person
      },
    },
    {
      field: 'client_contact_number',
      headerName: t('client_contact_number'),
      width: 250,
      hide: user_level == 2 || user_level == 3,
      renderCell: (params: GridCellParams) => (
        <CellTooltip
          title={params.row.company?.client_contact_number}
          arrow
          placement="top"
        >
          <div className={classes.tooltip}>
            {!params.row.company
              ? `*${t('deleted-company')}`
              : params.row.company?.contact_number}
          </div>
        </CellTooltip>
      ),
      valueGetter: (params) => {
        return !params.row.company
          ? `*${t('deleted-company')}`
          : params.row.company?.contact_number
      },
    },
    {
      field: 'contact_email',
      headerName: t('contact_email'),
      width: 250,
      hide: user_level == 2 || user_level == 3,
      renderCell: (params: GridCellParams) => (
        <CellTooltip
          title={params.row.company?.contact_email}
          arrow
          placement="top"
        >
          <div className={classes.tooltip}>
            {!params.row.company
              ? `*${t('deleted-company')}`
              : params.row.company?.contact_email}
          </div>
        </CellTooltip>
      ),
      valueGetter: (params) => {
        return !params.row.company
          ? `*${t('deleted-company')}`
          : params.row.company?.contact_email
      },
    },
  ]

  const handleShowUnArchiveChange = (
    event: React.ChangeEvent<unknown>,
    newValue: number,
  ) => {
    setShowUnArchive(newValue)
    setSelected([])
  }

  const handleShowUnArchiveChangeIndex = (index: number) => {
    setShowUnArchive(index)
    setSelected([])
  }

  const handlePagSizeChange = async (param: GridPageChangeParams) => {
    setPageSize(param.pageSize)
  }

  const handleSelectionChange = (params: GridSelectionModelChangeParams) => {
    setSelected(params.selectionModel)
  }

  const handleChangePage = (param: GridPageChangeParams) => {
    setPage(param.page)
  }

  const addQuotationButtonHandler = async () => {
    selected.map((item) => {
      let curHistory = unarchivedData.find(
        (val) => val._id?.toString() == item.toString(),
      )
      if (!curHistory)
        curHistory = archivedData.find(
          (val) => val._id?.toString() == item.toString(),
        )
      if (!curHistory) {
        toast.error(t('Cannot find data, load again'), {
          position: 'bottom-right',
        })
        return
      }
      if (curHistory.status != 3) {
        toast.info(t('request-status-must-approved'), {
          position: 'bottom-right',
        })
        return
      }
      router.push(`/requesthistory/add/${item.toString()}`)
    })
  }

  const updateRenderedData = async () => {
    await axios
      .get('/requesthistory', {
        params: {
          withFullInfo: true,
        },
      })
      .then((res) => {
        const archived: IRequestHistory[] = []
        const unarchived: IRequestHistory[] = []

        res.data.request_histories.map((requesthistory: IRequestHistory) => {
          requesthistory.id = requesthistory._id
          requesthistory.archive_status == 0
            ? unarchived.push(requesthistory)
            : archived.push(requesthistory)
        })

        setArchivedData(() => archived)
        setUnarchivedData(() => unarchived)
      })
  }

  const archiveButtonHandler = async () => {
    const decision = window.confirm(t('archive-alert'))
    if (!decision) {
      return
    }
    selected.map(async (item: GridRowId) => {
      await axios.get(`/requesthistory/archive/${item}`)
      updateRenderedData()
      setSelected([])
    })
  }

  const unArchiveButtonHandler = async () => {
    const decision = window.confirm(t('unarchive-alert'))
    if (!decision) {
      return
    }
    selected.map(async (item: GridRowId) => {
      await axios.get(`/requesthistory/unarchive/${item}`)
      updateRenderedData()
      setSelected([])
    })
  }

  const approveButtonHandler = async () => {
    const approve_failed: GridRowId[] = []
    let approve_not_ongoing = false
    const decision = window.confirm(t('approve-message'))
    if (!decision) {
      return
    }
    const approves = selected.map(async (id: GridRowId) => {
      try {
        await axios.get(`/requesthistory/approve/${id}`)
      } catch (err) {
        if (err.response.status == 400) approve_not_ongoing = true
        approve_failed.push(id)
        return
      }
    })
    const showError = (msg: string) =>
      toast.error(t(msg), {
        position: 'bottom-right',
      })

    Promise.all(approves).then(() => {
      if (approve_not_ongoing) showError(t('approve-error-message'))
      updateRenderedData()
      setSelected(approve_failed)
    })
  }

  const declineButtonHandler = async () => {
    const decline_failed: GridRowId[] = []
    let decline_not_ongoing = false
    const decision = window.confirm(t('decline-message'))
    if (!decision) {
      return
    }
    const declines = selected.map(async (id: GridRowId) => {
      try {
        await axios.get(`/requesthistory/decline/${id}`)
      } catch (err) {
        if (err.response.status == 400) decline_not_ongoing = true
        decline_failed.push(id)
        return
      }
    })
    const showError = (msg: string) =>
      toast.error(t(msg), {
        position: 'bottom-right',
      })
    Promise.all(declines).then(() => {
      if (decline_not_ongoing) showError(t('decline-error-message'))
      updateRenderedData()
      setSelected(decline_failed)
    })
  }

  const viewPdfButtonHandler = async () => {
    if (selected.length > 5) {
      alert(t('pdf-error'))
      return
    }
    selected.map((id: GridRowId) => {
      const cur_request_history = requesthistorylist.find(
        (history: IRequestHistory) => history?._id?.toString() == id,
      )
      //two situatons to preview pdf based on request history:
      //1. normal user, request not approved yet
      //2. company or super user, just wanna preview pdf before adding a real quotation.
      if ((cur_request_history?.status as number) < 3 || user_level < 4)
        window.open(`/requesthistory/preview_quotation/${id}`)
      else window.open(`/requesthistory/quotation/${id}`)
    })
  }

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  const buttonStyles = makeStyles((theme: Theme) =>
    createStyles({
      button: {
        margin: theme.spacing(1),
        padding: theme.spacing(1),
      },
      sectionDesktop: {
        display: 'none',
        [theme.breakpoints.up('md')]: {
          display: 'flex',
        },
      },
      sectionMobile: {
        display: 'flex',
        [theme.breakpoints.up('md')]: {
          display: 'none',
        },
      },
    }),
  )

  function CustomToolbar() {
    const classes = buttonStyles()
    return (
      <>
        <div className={classes.sectionDesktop}>
          <GridToolbarContainer>
            <GridToolbarColumnsButton />
            <GridToolbarFilterButton />
            <GridToolbarDensitySelector />
            <GridToolbarExport />
          </GridToolbarContainer>
        </div>
        <div className={classes.sectionMobile}>
          <Button
            aria-controls="grid-menu"
            aria-haspopup="true"
            onClick={handleClick}
            variant="contained"
            color="primary"
            startIcon={<DehazeIcon />}
            className={classes.button}
          >
            {t('filter')}
          </Button>
          <Menu
            id="grid-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
            getContentAnchorEl={null}
            anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
            transformOrigin={{ vertical: 'top', horizontal: 'center' }}
          >
            {[
              <MenuItem onClick={handleClose} key="GridToolbarColumnsButton">
                <GridToolbarColumnsButton />
              </MenuItem>,
              <MenuItem onClick={handleClose} key="GridToolbarFilterButton">
                <GridToolbarFilterButton />
              </MenuItem>,
              <MenuItem key="GridToolbarDensitySelector">
                <GridToolbarDensitySelector />
              </MenuItem>,
              <MenuItem key="GridToolbarExport">
                <GridToolbarExport />
              </MenuItem>,
            ]}
            {/* <MenuItem onClick={handleClose}>
              <GridToolbarColumnsButton />
            </MenuItem>
            <MenuItem onClick={handleClose}>
              <GridToolbarFilterButton />
            </MenuItem>
            <MenuItem>
              <GridToolbarDensitySelector />
            </MenuItem>
            <MenuItem>
              <GridToolbarExport />
            </MenuItem> */}
          </Menu>
        </div>
      </>
    )
  }

  return (
    <div background-color={theme.palette.background.paper} max-width={500}>
      <AppBar position="static" color="default">
        <Tabs
          value={showUnArchive}
          onChange={handleShowUnArchiveChange}
          indicatorColor="primary"
          textColor="primary"
          variant="fullWidth"
          aria-label="full width tabs example"
        >
          <Tab label={t('request-history')} {...a11yProps(0)} />
          <Tab label={t('archived-request-history')} {...a11yProps(1)} />
        </Tabs>
      </AppBar>
      <SwipeableViews
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={showUnArchive}
        onChangeIndex={handleShowUnArchiveChangeIndex}
      >
        <TabPanel value={showUnArchive} index={0} dir={theme.direction}>
          <div style={{ height: 400, width: '100%' }}>
            <EnhancedTableToolbar
              showUnArchive={showUnArchive}
              numSelected={selected.length}
              selectsDeleted={(() => {
                let result = false
                unarchivedData.forEach((data: IRequestHistory) => {
                  if (
                    (!data.service || !data.requester || !data.company) &&
                    selected.includes(data._id.toString())
                  )
                    result = true
                })
                return result
              })()}
              onArchiveClick={archiveButtonHandler}
              onUnArchiveClick={unArchiveButtonHandler}
              onAddQuotationClick={addQuotationButtonHandler}
              onApproveClick={approveButtonHandler}
              onDeclineClick={declineButtonHandler}
              onViewPdfClick={viewPdfButtonHandler}
              textFieldData={formData}
              setTextFieldData={setFormData}
              level={user_level}
            />
            <DataGrid
              autoHeight
              rows={unarchivedData}
              page={page}
              columns={columns}
              pageSize={pageSize}
              onPageSizeChange={handlePagSizeChange}
              onPageChange={handleChangePage}
              checkboxSelection={true}
              rowsPerPageOptions={[5, 10, 15, 20, 25]}
              onSelectionModelChange={handleSelectionChange}
              selectionModel={selected}
              disableSelectionOnClick
              components={{
                Toolbar: CustomToolbar,
              }}
              disableColumnMenu
            />
          </div>
        </TabPanel>
        <TabPanel value={showUnArchive} index={1} dir={theme.direction}>
          <div style={{ height: 400, width: '100%' }}>
            <EnhancedTableToolbar
              showUnArchive={showUnArchive}
              numSelected={selected.length}
              selectsDeleted={(() => {
                let result = false
                archivedData.forEach((data: IRequestHistory) => {
                  if (
                    (!data.service || !data.requester || !data.company) &&
                    selected.includes(data._id.toString())
                  )
                    result = true
                })
                return result
              })()}
              onArchiveClick={archiveButtonHandler}
              onUnArchiveClick={unArchiveButtonHandler}
              onAddQuotationClick={addQuotationButtonHandler}
              onApproveClick={approveButtonHandler}
              onDeclineClick={declineButtonHandler}
              onViewPdfClick={viewPdfButtonHandler}
              textFieldData={formData}
              setTextFieldData={setFormData}
              level={user_level}
            />
            <DataGrid
              autoHeight
              rows={archivedData}
              page={page}
              columns={columns}
              pageSize={pageSize}
              onPageSizeChange={handlePagSizeChange}
              onPageChange={handleChangePage}
              checkboxSelection={true}
              rowsPerPageOptions={[5, 10, 15, 20, 25]}
              onSelectionModelChange={handleSelectionChange}
              selectionModel={selected}
              disableSelectionOnClick
              components={{
                Toolbar: CustomToolbar,
              }}
              disableColumnMenu
            />
          </div>
        </TabPanel>
      </SwipeableViews>
      <ToastContainer />
    </div>
  )
}
