import React from 'react'
import clsx from 'clsx'
import {
  Card,
  CardHeader,
  CardContent,
  CardMedia,
  Collapse,
  Container,
  Grid,
  Toolbar,
  Typography,
  Box,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  IconButton,
} from '@material-ui/core'
import Button from '@material-ui/core/Button'
import AddCircleIcon from '@material-ui/icons/AddCircle'
import { createStyles, makeStyles } from '@material-ui/core/styles'
import { Theme } from '@material-ui/core/styles'
import axios from 'axios'
import { IService, IServiceItem } from '../../server/api/Service/service.model'
import cyan from '@material-ui/core/colors/cyan'
import lightBlue from '@material-ui/core/colors/lightBlue'
import { NextRouter } from 'next/router'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { IUser } from '@/server/api/User/user.model'
import {
  IRequestHistory,
  IRequestStatus,
} from '@/server/api/RequestHistory/requesthistory.model'
import { useState, useEffect } from 'react'
import { Fonts } from '@/components/constants/AppEnums'
import { useTranslation } from 'next-i18next'
import { useRouter } from 'next/router'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'
import ArrowRightIcon from '@material-ui/icons/ArrowRight'
import SentimentVeryDissatisfiedIcon from '@material-ui/icons/SentimentVeryDissatisfied'
import Accordion from '@material-ui/core/Accordion'
import AccordionSummary from '@material-ui/core/AccordionSummary'
import AccordionDetails from '@material-ui/core/AccordionDetails'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

const useListStyles = makeStyles((theme: Theme) =>
  createStyles({
    cardGrid: {
      flexGrow: 1,
      paddingTop: 16,
    },
    card: {
      display: 'flex',
      height: '100%',
      backgroundColor: cyan[50],
      boxShadow: '0 2px 5px 2px rgba(190, 190, 190, 0.43)',
      '&:hover': {
        backgroundColor: lightBlue[50],
        boxShadow: '0 5px 10px 5px rgba(190, 190, 190, 0.43)',
      },
    },
    cardMedia: {
      width: 350,
      height: 300,
      objectFit: 'contain',
      display: 'flex',
      margin: 'auto',
    },
    cardActionArea: {
      flex: 1,
      height: '100%',
      backgroundColor: 'green',
    },
    cardContent: {
      flex: '1 0 auto',
      width: '50%',
      justiyContent: 'space-between',
      flexDirection: 'column',
      borderRadius: theme.shape.borderRadius,
      padding: theme.spacing(3),
      backgroundColor: cyan[50],
      boxShadow: '0 2px 5px 2px rgba(190, 190, 190, 0.43)',
      '&:hover': {
        backgroundColor: lightBlue[50],
        boxShadow: '0 4px 8px 4px rgba(190, 190, 190, 0.43)',
      },
    },
    cardContentTitle: {
      flex: 1,
    },
    cardContentWord: {
      alignItems: 'start',
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
    },
    cardButton: {
      display: 'flex',
      alignItems: 'center',
      flexDirection: 'column',
      justiyContent: 'space-between',
      paddingLeft: theme.spacing(1),
      paddingBottom: theme.spacing(1),
    },
    editbutton: {
      position: 'absolute',
      right: 60,
      marginright: theme.spacing(2),
    },
    deletebutton: {
      position: 'absolute',
      right: 130,
      marginright: theme.spacing(2),
    },
    status_ongoing: {
      color: '#2AB67B',
      borderColor: '#2AB67B',
      height: 40,
      display: 'flex',
      justifyContent: 'center',
    },
    status_cancelled: {
      color: '#530021',
      borderColor: '#530021',
      height: 40,
      display: 'flex',
      justifyContent: 'center',
    },
    status_declined: {
      color: '#ED2461',
      borderColor: '#ED2461',
      height: 40,
      display: 'flex',
      justifyContent: 'center',
    },
    status_approved: {
      color: '#398170',
      borderColor: '#398170',
      height: 40,
      display: 'flex',
      justifyContent: 'center',
    },
    status_expired: {
      color: '#444F59',
      borderColor: '#444F59',
      height: 40,
      display: 'flex',
      justifyContent: 'center',
    },
    margin: {
      margin: theme.spacing(1),
    },
    sectionMedia: {
      display: 'flex',
      [theme.breakpoints.down('sm')]: {
        display: 'none',
      },
    },
    listItem: {
      flexDirection: 'row',
      display: 'flex',
    },
    insideListText: {
      // minWidth: 100,
      // display: 'flex',
      // width: '100%',
      // maxWidth: 100,
    },
    item_card_header_root: {
      width: '100%',
    },
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: 'rotate(180deg)',
    },
  }),
)

const useEnhancedVenderBarStyle = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(1),
      [theme.breakpoints.down('sm')]: {
        display: 'block',
      },
    },
    title: {
      flex: '1 1 100%',
      alignItems: 'center',
      justifyContent: 'center',
      textAlign: 'center',
      fontSize: 50,
      [theme.breakpoints.down('xs')]: {
        fontSize: 40,
      },
    },
    addbutton: {
      position: 'absolute',
      right: 40,
    },
  }),
)

interface EnhancedVendorBarProps {
  vendor: string
  level: number
}

const EnhancedVendorBar = (props: EnhancedVendorBarProps) => {
  const classes = useEnhancedVenderBarStyle()
  const { t } = useTranslation()
  const router = useRouter()

  return (
    <>
      {props.level != 2 && props.level != 3 ? (
        <Button
          startIcon={<ArrowBackIosIcon />}
          href={`/${router.locale}/company`}
        >
          {t('back')}
        </Button>
      ) : (
        <></>
      )}
      <Toolbar className={clsx(classes.root)}>
        <Typography className={classes.title} id="tableTitle" component="div">
          {t('comp-service-list')}
        </Typography>
      </Toolbar>
      {props.level < 4 ? (
        <Button
          endIcon={<AddCircleIcon color="primary" />}
          href={`/${router.locale}/company/service/add/${props.vendor}`}
          className={classes.addbutton}
        >
          {t('add-service')}
        </Button>
      ) : (
        <></>
      )}
    </>
  )
}

interface IListProps {
  company_id: string
  user_level: number
  router: NextRouter
  servicelist: IService[]
  user: IUser | string
}

export default function ServiceList({
  company_id,
  user_level,
  user,
  router,
  servicelist,
}: IListProps) {
  const { t } = useTranslation()
  const showError = (msg: string) =>
    toast.error(t(msg), {
      position: 'bottom-right',
    })
  const showSuccess = (msg: string) =>
    toast.success(t(msg), {
      position: 'bottom-right',
    })
  const serviceClasses = useListStyles()

  // By default no item selected. If this is changed, pay attention to default checked of checkboxes for items
  const initial_requested_items: IServiceItem[][] = servicelist.map(
    (service: IService) => {
      return service.service_items.map((item: IServiceItem) => {
        return { ...item, needed: false }
      })
    },
  )
  const [requested_items, set_requested_items] = useState<IServiceItem[][]>(
    initial_requested_items,
  )

  const [cur_request_history, set_cur_request_history] =
    useState<IRequestHistory[]>()

  useEffect(() => {
    async function fetchData() {
      const res = await axios.get('/requesthistory', {
        params: {
          withFullInfo: true,
        },
      })
      set_cur_request_history(res.data.request_histories)
    }
    fetchData()
  }, [])

  const getServiceHistory = (service_id: string) => {
    if (!cur_request_history) return undefined
    return cur_request_history.find(
      (history: IRequestHistory) =>
        history.service?.toString() == service_id ||
        (history.service as IService)?._id?.toString() == service_id,
    )
  }

  const getServiceHistoryIdx = (service_id: string) => {
    if (!cur_request_history) return undefined
    return cur_request_history.findIndex(
      (history: IRequestHistory) =>
        history.service?.toString() == service_id ||
        (history.service as IService)?._id?.toString() == service_id,
    )
  }

  const updateRequestHistory = async () => {
    await axios
      .get('/requesthistory')
      .then((res) => {
        set_cur_request_history(res.data.request_histories)
      })
      .catch(() => {
        showError(t('request-history-error'))
      })
  }

  const statusStr = (status: IRequestStatus | undefined) => {
    switch (status) {
      case 0:
        return t('ongoing')
      case 1:
        return t('cancelled')
      case 2:
        return t('declined')
      case 3:
        return t('approved')
      case 4:
        return t('expired')
      default:
        return t('unknown')
    }
  }

  // const isModifiedAfterRequest = (history: IRequestHistory | undefined) => {
  //   if (history == undefined) return false
  //   const serviceUpdatedTime = new Date((history.service as IService).updatedAt)
  //   const requestTime = new Date(history.request_date)
  //   return serviceUpdatedTime.getTime() > requestTime.getTime()
  // }

  return (
    <>
      {cur_request_history && (
        <div style={{ height: 400, width: '100%' }}>
          <EnhancedVendorBar vendor={company_id} level={user_level} />
          <br />
          <br />
          <Container className={serviceClasses.cardGrid} maxWidth="xl">
            <Grid
              container
              direction="row"
              justify="space-evenly"
              alignItems="stretch"
              spacing={2}
            >
              {servicelist.length == 0 ? (
                <>
                  <Box
                    display="flex"
                    flexDirection="column"
                    textAlign="center"
                    justifyContent="center"
                    alignItems="center"
                  >
                    <SentimentVeryDissatisfiedIcon
                      style={{ fontSize: 90, color: '#6a7eb4' }}
                    />
                    <Box fontSize={20}>{t('no-service-shown')}</Box>
                  </Box>
                </>
              ) : (
                <>
                  {servicelist.map((data, service_index) => (
                    <Grid item xs={12} key={`OverviewItem-${data.id}`}>
                      <Card className={serviceClasses.card}>
                        <CardMedia
                          className={clsx(
                            serviceClasses.cardMedia,
                            serviceClasses.sectionMedia,
                          )}
                          // style={{
                          //   display: 'flex',
                          //   justifyContent: 'space-between',
                          //   alignItems: 'space-between',
                          // }}
                          image={
                            data.image_data
                              ? data.image_data
                              : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAS4AAACnCAMAAACYVkHVAAAAJ1BMVEX09PTa2trc3Nz29vbj4+Pg4ODv7+/Y2Njy8vLt7e3q6urm5ubn5+eHk7pVAAAEMklEQVR4nO2ci46jMAxFSZw3/P/3ru0QFtoppSPtUsn3rDSLmFaCIycxjplpAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPxzSLj7Ir4b8UPdU6lL8MvdF/TtiKjSQnQ5O+cClbsv6OvQEddDKrEo7xTPuAhdeySUivgqqc5xmBq6+MfdF/hVSFxRSYuEVGY93j/ocunuS/wCxujjsTeLKOe3aFJlfMKHJfDJXK2vjUVNldpCl7SLJvXlY2hJBmiSs4vlXGJM596tpvxel+OQSrt0S05Gk7aKGuhjT91sAbUeSUhNx7SUIv8i3nXFt8KiJKTyELVN5jz2ZhY1PSfwNMnk5S0mEhT6JL4be1kjqhZN4Pkj5dELURO5xeBopPnvbJXV1CKz+ekzIesSu8mirjrCSseemprerXmUOCDdbFAXJwWsK3ZR178VOXXdL4125jGJrjBUlbKWHd4QZSXYn7j3Hv4fPSn48HYpcHTtl8ZkJbxUlx/B5a+yJWcrVuZ9muXOR1LgfosZXVXutkLXNahwQp9HKfmQ20PXT4igsB6LrvgZIZrSJXO9H7VR0RXW1OBSRkHr5GdHVxBdayIgh+GTO2dhwZSuadHb7cerrpKuMpnTlTLPXm3U/USXrJZZGf+/pE7WdBWpR8xHXb6vkMctjWe8QV381Ohz/FlXCKfpg0ldUl5Yl8ajrkWWvvkkvizq4kyAh1xfGg+65t4XMUPXHi0l57407nR5N61Pkg+Zvl//GdXFSyPfb9PDva5RpiB/mO99f1Cyq0sfrHty+l4X/6ZuFWuTuki2GPu24WEw1j531d1glNJW5QefmvvOtkldum2oh4ep3nddx+abntDyfGdVF1GT7F3n9aMu30ppx4nLz9p2UihY1aVL41ohfEhTnxKHv8/fFMzqKnLDywVduz0Q0iK/RV0TbSbe6OLPbNGl+yA2dUlPiS6Np7rysYuEik1dWrLqj0Gnuh57biiZHIxEC1vSx6DXurzLz92oNVdz5cHRVyIJ1Ul0/dSMSnMzqEtmbacVwidd+cyWtv4a1CUVQu2oOejiTCGUIM3O2buXjc4GdfFTo1QIy0N0xUaFmqajy0sdFnVJhdCnH8qD8irLItu2L1tsLOpqWvKS9F51ScEwbsOP0nzSj2RRl75YMG+6qne7zdly2rRrUBcvjU4rhCO6fDv00Z991aIubbmMI7okni7fvkVda1/J0PXcS3/yVXu6aJr1DdjwcUuJSV3T1NzY3IGuCyS3vbf4WcOSSV2l9N0x+Rnef3yHxUdsJm4t4D58grVmyw6F0Goqr2r0J/QvGNM19TenPnW1YU1Xz7Sg6yL9di+/5PII/kwCeMsv/1JLmSy+aQwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABjiD11yHkXgwpzGAAAAAElFTkSuQmCC'
                          }
                          title="company image"
                        />
                        <CardContent className={serviceClasses.cardContent}>
                          <Box display="flex" flexDirection="row">
                            <Typography
                              gutterBottom
                              variant="h5"
                              className={serviceClasses.cardContentTitle}
                            >
                              {data.service_name}
                            </Typography>
                            {(() => {
                              const status = getServiceHistory(data.id)?.status
                              if (status != undefined && user_level >= 4)
                                return (
                                  <Box
                                    border={1}
                                    borderRadius={8}
                                    padding={1}
                                    className={clsx({
                                      [serviceClasses.status_ongoing]:
                                        status == 0,
                                      [serviceClasses.status_cancelled]:
                                        status == 1,
                                      [serviceClasses.status_declined]:
                                        status == 2,
                                      [serviceClasses.status_approved]:
                                        status == 3,
                                      [serviceClasses.status_expired]:
                                        status == 4,
                                    })}
                                  >
                                    <Typography
                                      style={{ backgroundColor: 'transparent' }}
                                      className={clsx({
                                        [serviceClasses.status_ongoing]:
                                          status == 0,
                                        [serviceClasses.status_cancelled]:
                                          status == 1,
                                        [serviceClasses.status_declined]:
                                          status == 2,
                                        [serviceClasses.status_approved]:
                                          status == 3,
                                        [serviceClasses.status_expired]:
                                          status == 4,
                                      })}
                                    >
                                      {statusStr(status)}
                                    </Typography>
                                  </Box>
                                )
                              else return <></>
                            })()}
                          </Box>
                          <Typography>{data.description}</Typography>
                          <br />
                          {data.service_items.map(
                            (item: IServiceItem, item_index: number) => {
                              return (
                                <Accordion key={`${data.id}-${item_index}`}>
                                  <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="item1a-content"
                                    id="item1a-header"
                                    style={{ backgroundColor: '#a2dffc' }}
                                  >
                                    <FormControlLabel
                                      // className={serviceClasses.cardContentWord}
                                      onClick={(event) =>
                                        event.stopPropagation()
                                      }
                                      onFocus={(event) =>
                                        event.stopPropagation()
                                      }
                                      control={
                                        user_level == 4 ? (
                                          <Checkbox
                                            disabled={
                                              getServiceHistory(data.id) !=
                                              undefined
                                            }
                                            defaultChecked={(() => {
                                              const curServiceIndex =
                                                getServiceHistoryIdx(data.id)
                                              if (curServiceIndex === -1)
                                                return false
                                              else {
                                                return cur_request_history[
                                                  curServiceIndex as number
                                                ].requested_items[item_index]
                                                  ?.needed
                                              }
                                            })()}
                                            color="primary"
                                            value={item.needed}
                                            name="requested_items"
                                            onChange={(e) => {
                                              const new_requested_items = [
                                                ...requested_items,
                                              ]
                                              if (e.target.checked)
                                                new_requested_items[
                                                  service_index
                                                ][item_index].needed = true
                                              else
                                                new_requested_items[
                                                  service_index
                                                ][item_index].needed = false
                                              set_requested_items(
                                                new_requested_items,
                                              )
                                            }}
                                          />
                                        ) : (
                                          <></>
                                        )
                                      }
                                      label={
                                        <Typography
                                          style={{
                                            fontSize: 20,
                                            fontWeight: Fonts.BOLD,
                                          }}
                                        >
                                          {t('service-item') +
                                            ' ' +
                                            (item_index + 1)}
                                        </Typography>
                                      }
                                    />
                                  </AccordionSummary>
                                  <AccordionDetails>
                                    <>
                                      <List>
                                        <ListItem
                                          className={serviceClasses.listItem}
                                        >
                                          <ListItemText
                                            primary={
                                              <Typography
                                                style={{ fontWeight: 'bold' }}
                                              >
                                                {t('service-title')}:{' '}
                                              </Typography>
                                            }
                                            secondary={
                                              <Typography>
                                                {item.title}
                                              </Typography>
                                            }
                                          />
                                        </ListItem>
                                        <ListItem>
                                          <ListItemText
                                            primary={
                                              <Typography
                                                style={{ fontWeight: 'bold' }}
                                              >
                                                {t('description')}:{' '}
                                              </Typography>
                                            }
                                            secondary={
                                              <Typography>
                                                {item.description}
                                              </Typography>
                                            }
                                          />
                                        </ListItem>
                                        <ListItem>
                                          <ListItemText
                                            primary={
                                              <Typography
                                                style={{ fontWeight: 'bold' }}
                                              >
                                                {t('price')}:{' '}
                                              </Typography>
                                            }
                                            secondary={
                                              <Typography>
                                                {item.price}
                                              </Typography>
                                            }
                                          />
                                        </ListItem>
                                      </List>
                                    </>
                                  </AccordionDetails>
                                </Accordion>
                              )
                            },
                          )}
                          <br />

                          {user_level == 4 ? (
                            <>
                              <Button
                                variant="contained"
                                color="primary"
                                className={serviceClasses.margin}
                                disabled={
                                  getServiceHistory(data.id) != undefined ||
                                  requested_items[service_index].every(
                                    (item: IServiceItem) => {
                                      return !item.needed
                                    },
                                  )
                                }
                                onClick={async (e: {
                                  preventDefault: () => void
                                }) => {
                                  e.preventDefault()
                                  const decision = window.confirm(
                                    t('request-confirm'),
                                  )
                                  if (!decision) return
                                  let res
                                  const new_requesthistory = {
                                    service: servicelist[service_index]._id,
                                    requester: user,
                                    requested_items:
                                      requested_items[service_index],
                                    company: company_id,
                                  }
                                  try {
                                    if (
                                      getServiceHistory(data.id) == undefined
                                    ) {
                                      res = await axios.post(
                                        `/requesthistory/`,
                                        new_requesthistory,
                                      )
                                    } else {
                                      res = await axios.put(
                                        `/requesthistory/${
                                          getServiceHistory(data.id)?._id
                                        }`,
                                        new_requesthistory,
                                      )
                                    }
                                  } catch (err) {
                                    if (err) {
                                      showError(
                                        `${t('request-fail')} ${
                                          err.response.status
                                        }`,
                                      )
                                    }
                                  }
                                  if (res?.status == 200) {
                                    showSuccess(t('create-success'))
                                    updateRequestHistory()
                                  }
                                }}
                              >
                                {t('request')}
                              </Button>
                              <Button
                                variant="contained"
                                color="secondary"
                                className={serviceClasses.margin}
                                disabled={
                                  getServiceHistory(data.id) == undefined ||
                                  (getServiceHistory(data.id)
                                    ?.status as IRequestStatus) != 0
                                }
                                onClick={async (e: {
                                  preventDefault: () => void
                                }) => {
                                  e.preventDefault()
                                  const decision = window.confirm(
                                    t('cancel-confirm'),
                                  )
                                  if (!decision) return
                                  let res
                                  try {
                                    res = await axios.get(
                                      `/requesthistory/cancel/${getServiceHistory(
                                        data.id,
                                      )?._id?.toString()}`,
                                    )
                                  } catch (err) {
                                    if (err) {
                                      showError(
                                        `${t('cancel-fail')} ${
                                          err.response.status
                                        }`,
                                      )
                                    }
                                  }
                                  if (res?.status == 200) {
                                    showSuccess(t('cancel-success'))
                                    updateRequestHistory()
                                  }
                                }}
                              >
                                {t('cancel')}
                              </Button>
                            </>
                          ) : (
                            <>
                              <Button
                                variant="contained"
                                color="primary"
                                onClick={() =>
                                  router.push(
                                    `/company/service/edit/${data._id}`,
                                  )
                                }
                                className={serviceClasses.editbutton}
                              >
                                {t('edit')}
                              </Button>
                              <Button
                                variant="contained"
                                color="secondary"
                                onClick={async () => {
                                  const decision = window.confirm(
                                    t('delete-service-confirm'),
                                  )
                                  if (!decision) {
                                    return
                                  }
                                  await axios.delete(`/service/${data._id}`)
                                  router.reload()
                                }}
                                className={serviceClasses.deletebutton}
                              >
                                {t('delete')}
                              </Button>
                              <br />
                              <br />
                            </>
                          )}
                        </CardContent>
                      </Card>
                    </Grid>
                  ))}
                </>
              )}
            </Grid>
            <ToastContainer />
          </Container>
        </div>
      )}
    </>
  )
}
