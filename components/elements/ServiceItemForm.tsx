import React, { useState } from 'react'
import {
  FormControl,
  List,
  TextField,
  ListItem,
  Box,
  Button,
  Checkbox,
  Card,
  CardHeader,
  Collapse,
  CardContent,
  IconButton,
  Fab,
} from '@material-ui/core'
import { IServiceItem } from '@/server/api/Service/service.model'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import { makeStyles } from '@material-ui/core/styles'
import clsx from 'clsx'
import { useTranslation } from 'next-i18next'
import InputAdornment from '@material-ui/core/InputAdornment'
import AddIcon from '@material-ui/icons/Add'
import DeleteForeverIcon from '@material-ui/icons/DeleteForever'
import SortIcon from '@material-ui/icons/Sort'

const overrdingStyle = makeStyles((theme) => ({
  formControl: {
    // margin: theme.spacing(1),
    width: '100%',
  },
  list: {
    width: '100%',
  },
  listItem: {
    width: '100%',
  },
  accordian: {
    width: '100%',
  },
  card: {
    width: '100%',
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  textField: {
    width: '47%',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  margin: {
    margin: theme.spacing(1),
  },
  button: {
    margin: theme.spacing(1),
    width: '25%',
  },
  card_header_title: {
    width: '60%',
  },
  card_header_action: {
    width: '40%',
  },
  item_card_header_root: {
    width: '100%',
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
}))

export enum ServiceItemFormUsage {
  SERVICE_ITEMS = 0,
  QUOTATION_ITEMS,
}

export interface IServiceItemFormProps {
  initialItems: IServiceItem[]
  usage: ServiceItemFormUsage
  onUpdate: (items: IServiceItem[]) => void
}

export default function ServiceItemForm(props: IServiceItemFormProps) {
  const classes = overrdingStyle()
  const [curItems, setCurItems] = useState<IServiceItem[]>(props.initialItems)
  const initialExpanded: boolean[] = new Array(props.initialItems.length)
  initialExpanded.fill(false, 0)
  const [expanded, setExpanded] = useState<boolean[]>(initialExpanded)
  const initialSelected: boolean[] = new Array(props.initialItems.length)
  initialSelected.fill(false, 0)
  const [selected, setSelected] = useState<number[]>([])
  const { t } = useTranslation()

  const usageStr = (usage: ServiceItemFormUsage) => {
    switch (usage) {
      case ServiceItemFormUsage.SERVICE_ITEMS:
        return t('service')
      case ServiceItemFormUsage.QUOTATION_ITEMS:
        return t('quotation')
      default:
        return 'Unknown'
    }
  }

  const addNewServiceItem = () => {
    setCurItems([
      ...curItems,
      {
        title: '',
        description: '',
        price: 0,
        order: curItems.length + 1,
        needed: false,
      },
    ])
    setExpanded([...expanded, false])
  }

  //does not update, only return array
  const getSortedSelectedServiceItem = (items: IServiceItem[]) => {
    const newServiceItems = [...items]
    newServiceItems.sort((item1, item2) => item1.order - item2.order)
    newServiceItems.forEach((item, index) => (item.order = index + 1))
    return newServiceItems
  }

  const removeSelectedServiceItem = () => {
    const decision = window.confirm(t('remove-selected-confirm'))

    if (!decision) return
    //assumes expanded and curItems have same length
    const newExpanded: boolean[] = []
    const newItemList = curItems.filter((_val, index) => {
      if (!selected.includes(index)) newExpanded.push(expanded[index])
      return !selected.includes(index)
    })
    props.onUpdate(newItemList)
    setCurItems(newItemList)
    setExpanded(newExpanded)
    setSelected([])
  }

  const handleItemUpdate = (
    e: { target: { name: string; value: string } },
    index: number,
  ) => {
    const newItemList = curItems
    newItemList[index] = { ...curItems[index], [e.target.name]: e.target.value }
    setCurItems(newItemList)
  }

  const handleExpandedUpdate = (index: number) => {
    //this is to trigger the hook, plz use this to update expanded
    const newExpanded = [...expanded]
    newExpanded[index] = !expanded[index]
    setExpanded(newExpanded)
  }

  const handleCheckboxToggle = (index: number) => () => {
    const currentIndex = selected.indexOf(index)
    const newSelected = [...selected]

    if (currentIndex === -1) {
      newSelected.push(index)
    } else {
      newSelected.splice(currentIndex, 1)
    }

    setSelected(newSelected)
  }

  return (
    <FormControl className={classes.formControl}>
      <List
        className={classes.list}
        onChange={() => {
          props.onUpdate(curItems)
        }}
      >
        <Box mb={6} justifyContent="center" className={classes.sectionDesktop}>
          {props.usage != ServiceItemFormUsage.QUOTATION_ITEMS && (
            <Button
              className={classes.button}
              fullWidth
              variant="contained"
              color="primary"
              onClick={addNewServiceItem}
              startIcon={<AddIcon />}
            >
              {t('add-item')}
            </Button>
          )}
          <Button
            className={classes.button}
            fullWidth
            variant="contained"
            color="primary"
            onClick={() => {
              const newServiceItems = getSortedSelectedServiceItem(curItems)
              setCurItems(newServiceItems)
              setExpanded(expanded.map(() => false))
              props.onUpdate(newServiceItems)
            }}
            disabled={curItems.length === 0}
            startIcon={<SortIcon />}
          >
            {t('sort-item')}
          </Button>
          <Button
            className={classes.button}
            fullWidth
            variant="contained"
            color="secondary"
            onClick={removeSelectedServiceItem}
            disabled={selected.length === 0}
            startIcon={<DeleteForeverIcon />}
          >
            {t('remove-item')}
            {/* {t('Remove selected Service Item')} */}
          </Button>
        </Box>
        <Box mb={6} justifyContent="center" className={classes.sectionMobile}>
          {props.usage != ServiceItemFormUsage.QUOTATION_ITEMS && (
            <Fab
              style={{ margin: 10 }}
              color="primary"
              onClick={addNewServiceItem}
            >
              <AddIcon />
            </Fab>
          )}
          <Fab
            style={{ margin: 10 }}
            color="primary"
            onClick={() => {
              const newServiceItems = getSortedSelectedServiceItem(curItems)
              setCurItems(newServiceItems)
              setExpanded(expanded.map(() => false))
              props.onUpdate(newServiceItems)
            }}
            disabled={curItems.length === 0}
          >
            <SortIcon />
          </Fab>
          <Fab
            style={{ margin: 10 }}
            color="secondary"
            onClick={removeSelectedServiceItem}
            disabled={selected.length === 0}
          >
            <DeleteForeverIcon />
            {/* {t('Remove selected Service Item')} */}
          </Fab>
        </Box>
        {curItems.map((item: IServiceItem, index: number) => {
          return (
            <ListItem key={index} className={classes.listItem}>
              {
                <Card className={classes.card}>
                  <Box display="flex" flexDirection="row">
                    <Checkbox
                      onClick={handleCheckboxToggle(index)}
                      checked={selected.includes(index)}
                    />
                    <CardHeader
                      aria-controls="service_item_header"
                      id="service_item_heade r"
                      className={classes.item_card_header_root}
                      title={`${usageStr(props.usage)} ${t('item')} #${
                        index + 1
                      }`}
                      onClick={(event: React.MouseEvent) => {
                        event.stopPropagation()
                        handleExpandedUpdate(index)
                      }}
                      action={
                        <IconButton
                          className={clsx(classes.expand, {
                            [classes.expandOpen]: expanded[index],
                          })}
                          aria-expanded={expanded[index]}
                          aria-label="show more"
                        >
                          <ExpandMoreIcon />
                        </IconButton>
                      }
                    />
                  </Box>
                  <Collapse in={expanded[index]} timeout="auto" unmountOnExit>
                    <CardContent>
                      <TextField
                        className={classes.margin}
                        margin="normal"
                        required
                        fullWidth
                        id={`title${index}`}
                        label={t('service-title')}
                        name="title"
                        autoComplete="title"
                        value={item.title}
                        onChange={(e: {
                          target: { name: string; value: string }
                        }) => {
                          handleItemUpdate(e, index)
                        }}
                      />
                      <TextField
                        className={classes.margin}
                        margin="normal"
                        fullWidth
                        id={`description${index}`}
                        label={t('description')}
                        name="description"
                        autoComplete="description"
                        multiline
                        rows={10}
                        variant="outlined"
                        value={item.description}
                        onChange={(e: {
                          target: { name: string; value: string }
                        }) => {
                          handleItemUpdate(e, index)
                        }}
                      />
                      <TextField
                        className={clsx(classes.margin, classes.textField)}
                        margin="normal"
                        required
                        id={`price${index}`}
                        label={t('price')}
                        name="price"
                        type="number"
                        onKeyDown={(evt) =>
                          ['e', 'E', '+', '-'].includes(evt.key) &&
                          evt.preventDefault()
                        }
                        autoComplete="price"
                        value={item.price}
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">$</InputAdornment>
                          ),
                        }}
                        onChange={(e: {
                          target: { name: string; value: string }
                        }) => {
                          handleItemUpdate(e, index)
                        }}
                      />
                      <TextField
                        className={clsx(classes.margin, classes.textField)}
                        margin="normal"
                        required
                        id={`order${index}`}
                        label={t('order')}
                        name="order"
                        type="number"
                        onKeyDown={(evt) =>
                          ['e', 'E', '+', '-'].includes(evt.key) &&
                          evt.preventDefault()
                        }
                        autoComplete="order"
                        value={item.order}
                        onChange={(e: {
                          target: { name: string; value: string }
                        }) => {
                          handleItemUpdate(e, index)
                        }}
                      />
                    </CardContent>
                  </Collapse>
                </Card>
              }
            </ListItem>
          )
        })}
      </List>
    </FormControl>
  )
}
