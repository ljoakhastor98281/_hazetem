import React, { useState } from 'react'
import clsx from 'clsx'
import {
  DataGrid,
  GridPageChangeParams,
  GridToolbarContainer,
  GridToolbarColumnsButton,
  GridToolbarFilterButton,
  GridToolbarExport,
  GridToolbarDensitySelector,
  GridColDef,
  GridValueFormatterParams,
  GridCellParams,
} from '@material-ui/data-grid'
import { Toolbar, Typography, Tooltip } from '@material-ui/core'
import Button from '@material-ui/core/Button'
import {
  createStyles,
  makeStyles,
  withStyles,
  fade,
} from '@material-ui/core/styles'
import { Theme } from '@material-ui/core/styles'
import { lighten } from '@material-ui/core/styles'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import DehazeIcon from '@material-ui/icons/Dehaze'
import { IRequestedItemTableRow } from '@/components/interfaces/interfaces'
import { useTranslation } from 'next-i18next'
import { NextRouter } from 'next/router'
// import ArchiveIcon from '@material-ui/icons/Archive'
// import Layout from '@/components/layouts/navLayout'
// import axios from 'axios'
// import { NextRouter } from 'next/router'
// import { format } from 'date-fns'
// import ListIcon from '@material-ui/icons/List'
// import ListItemIcon from '@material-ui/core/ListItemIcon'
// import ListItemText from '@material-ui/core/ListItemText'
// import { IServiceItem } from '@/server/api/Service/service.model'

const useToolbarStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(1),
    },
    highlight:
      theme.palette.type === 'light'
        ? {
            color: theme.palette.secondary.main,
            backgroundColor: lighten(theme.palette.secondary.light, 0.85),
          }
        : {
            color: theme.palette.text.primary,
            backgroundColor: theme.palette.secondary.dark,
          },
    title: {
      flex: '1 1 100%',
      alignItems: 'center',
      justifyContent: 'center',
      textAlign: 'center',
      fontSize: 50,
      [theme.breakpoints.down('xs')]: {
        fontSize: 40,
      },
    },
    subtitle: {
      flex: '1 1 100%',
      fontSize: 20,
      [theme.breakpoints.down('sm')]: {
        fontSize: 15,
      },
    },
    button: {
      margin: theme.spacing(1),
      width: 200,
    },
    addButton: {
      margin: theme.spacing(1),
    },
    input: {
      marginLeft: theme.spacing(1),
      flex: 1,
    },
    iconButton: {
      padding: 10,
    },
    divider: {
      height: 28,
      margin: 4,
    },
    sectionDesktop: {
      display: 'none',
      [theme.breakpoints.up('md')]: {
        display: 'flex',
      },
    },
    sectionMobile: {
      display: 'flex',
      [theme.breakpoints.up('md')]: {
        display: 'none',
      },
    },
  }),
)

// const StyledDataGrid = withStyles({
//   root: {
//     '& .MuiDataGrid-window': {
//       overflowY: 'visible !important',
//     },
//     '& .MuiDataGrid-columnsHeader': {
//       alignItems: 'center',
//     },
//     '& .MuiDataGrid-row': {
//       maxHeight: 'none !important',
//     },
//     '& .MuiDataGrid-viewport': {
//       maxHeight: 'max-content !important',
//     },
//     '& .MuiDataGrid-renderingZone': {
//       maxHeight: 'max-content !important',
//     },
//     '& .MuiDataGrid-cell': {
//       lineHeight: 'unset !important',
//       verticalAlign: 'middle',
//       position: 'relative',
//       display: 'flex !important',
//       alignItems: 'center',
//       maxHeight: 'none !important',
//       whiteSpace: 'normal !important',
//       overflowWrap: 'break-word',
//     },
//   },
// })(DataGrid)

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    tooltip: {
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
    },
  }),
)

const EnhancedTableToolbar = ({ router }: IListProps) => {
  const classes = useToolbarStyles()
  const { t } = useTranslation()

  return (
    <>
      <Button
        startIcon={<ArrowBackIosIcon />}
        href={`/${router.locale}/requesthistory`}
      >
        {t('back')}
      </Button>
      <br />
      <br />
      <Toolbar className={clsx(classes.root)}>
        <Typography className={classes.title} id="tableTitle" component="div">
          {t('requested-items')}
        </Typography>
      </Toolbar>
    </>
  )
}

interface IListProps {
  list: IRequestedItemTableRow[]
  router: NextRouter
}

const CellTooltip = withStyles({
  arrow: {
    color: fade('#282c37', 0.9),
  },
  tooltip: {
    fontSize: '1em',
    color: 'white',
    backgroundColor: fade('#282c37', 0.9),
    whiteSpace: 'pre-line',
  },
})(Tooltip)

export default function RequestedItemList({ list, router }: IListProps) {
  const [page, setPage] = useState(0)
  const { t } = useTranslation()
  const [pageSize, setPageSize] = useState(5)
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
  // const [selected, setSelected] = useState<GridRowId[]>([])
  const classes = useStyles()

  const columns: GridColDef[] = [
    {
      field: 'title',
      headerName: t('service-title'),
      width: 300,
      renderCell: (params: GridCellParams) => (
        <CellTooltip title={params.row.title} arrow placement="top">
          <div className={classes.tooltip}>{params.row.title}</div>
        </CellTooltip>
      ),
    },
    {
      field: 'description',
      headerName: t('description'),
      width: 300,
      renderCell: (params: GridCellParams) => (
        <CellTooltip title={params.row.description} arrow placement="top">
          <div className={classes.tooltip}>{params.row.description}</div>
        </CellTooltip>
      ),
    },
    {
      field: 'price',
      headerName: t('price'),
      width: 250,
      renderCell: (params: GridCellParams) => (
        <CellTooltip
          title={params.row.price ? `$ ${params.row.price}` : '-'}
          arrow
          placement="top"
        >
          <div className={classes.tooltip}>
            {params.row.price ? `$ ${params.row.price}` : '-'}
          </div>
        </CellTooltip>
      ),
    },
    {
      field: 'order',
      headerName: t('order'),
      width: 100,
      renderCell: (params: GridCellParams) => (
        <CellTooltip title={params.row.order} arrow placement="top">
          <div className={classes.tooltip}>{params.row.order}</div>
        </CellTooltip>
      ),
    },
  ]

  const handlePagSizeChange = async (param: GridPageChangeParams) => {
    setPageSize(param.pageSize)
  }

  const handleChangePage = (param: GridPageChangeParams) => {
    setPage(param.page)
  }

  // const handleSelectionChange = (params: GridSelectionModelChangeParams) => {
  //   setSelected(params.selectionModel)
  // }

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  const buttonStyles = makeStyles((theme: Theme) =>
    createStyles({
      button: {
        margin: theme.spacing(1),
        padding: theme.spacing(1),
      },
      sectionDesktop: {
        display: 'none',
        [theme.breakpoints.up('md')]: {
          display: 'flex',
        },
      },
      sectionMobile: {
        display: 'flex',
        [theme.breakpoints.up('md')]: {
          display: 'none',
        },
      },
    }),
  )

  function CustomToolbar() {
    const classes = buttonStyles()
    const { t } = useTranslation()

    return (
      <>
        <div className={classes.sectionDesktop}>
          <GridToolbarContainer>
            <GridToolbarColumnsButton />
            <GridToolbarFilterButton />
            <GridToolbarDensitySelector />
            <GridToolbarExport />
          </GridToolbarContainer>
        </div>
        <div className={classes.sectionMobile}>
          <Button
            aria-controls="grid-menu"
            aria-haspopup="true"
            onClick={handleClick}
            variant="contained"
            color="primary"
            startIcon={<DehazeIcon />}
            className={classes.button}
          >
            {t('filter')}
          </Button>
          <Menu
            id="grid-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
            getContentAnchorEl={null}
            anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
            transformOrigin={{ vertical: 'top', horizontal: 'center' }}
          >
            <MenuItem onClick={handleClose}>
              <GridToolbarColumnsButton />
            </MenuItem>
            <MenuItem onClick={handleClose}>
              <GridToolbarFilterButton />
            </MenuItem>
            <MenuItem>
              <GridToolbarDensitySelector />
            </MenuItem>
            <MenuItem>
              <GridToolbarExport />
            </MenuItem>
          </Menu>
        </div>
      </>
    )
  }

  return (
    <div style={{ height: 400, width: '100%' }}>
      <EnhancedTableToolbar router={router} list={list} />
      <DataGrid
        rows={list}
        columns={columns}
        page={page}
        pageSize={pageSize}
        onPageSizeChange={handlePagSizeChange}
        onPageChange={handleChangePage}
        checkboxSelection={false}
        rowsPerPageOptions={[5, 10, 15, 20, 25]}
        // onSelectionModelChange={handleSelectionChange}
        disableSelectionOnClick
        components={{
          Toolbar: CustomToolbar,
        }}
        disableColumnMenu
      />
    </div>
  )
}
