export interface IRequestedItemTableRow {
  id: string
  title: string
  description: string
  price: number
  order: number
  needed: boolean
}

export interface IBRCompanyTableRow {
  id: string
  company_name: string
  contact_person: string
  contact_email: string
  client_contact_number: string
  BR_expiry_date: Date
}
