import React, { useState } from 'react'
import clsx from 'clsx'
import {
  createStyles,
  makeStyles,
  useTheme,
  Theme,
} from '@material-ui/core/styles'
import Drawer from '@material-ui/core/Drawer'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import List from '@material-ui/core/List'
import Collapse from '@material-ui/core/Collapse'
import CssBaseline from '@material-ui/core/CssBaseline'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'
import ChevronRightIcon from '@material-ui/icons/ChevronRight'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart'
import FileCopyIcon from '@material-ui/icons/FileCopy'
import AccountBoxIcon from '@material-ui/icons/AccountBox'
import MenuItem from '@material-ui/core/MenuItem'
import Menu from '@material-ui/core/Menu'
import Divider from '@material-ui/core/Divider'
import HazedawnWhiteIcon from '@/components/icons/HazedawnWhite'
import ExpandLess from '@material-ui/icons/ExpandLess'
import ExpandMore from '@material-ui/icons/ExpandMore'
import { signOut, useSession } from 'next-auth/client'
import { useTranslation } from 'next-i18next'
import { useRouter } from 'next/router'
import { Link, Tooltip } from '@material-ui/core'
import Button from '@material-ui/core/Button'
import LanguageIcon from '@material-ui/icons/Language'

const drawerWidth = 260

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
    },
    grow: {
      flexGrow: 1,
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
    appBarShift: {
      marginLeft: drawerWidth,
      width: `calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      display: 'none',
      [theme.breakpoints.up('sm')]: {
        display: 'block',
      },
    },
    // search: {
    //   position: 'relative',
    //   borderRadius: theme.shape.borderRadius,
    //   boxShadow: '0 2px 5px 2px rgba(172, 182, 229, 0.43)',
    //   backgroundColor: theme.palette.common.white,
    //   '&:hover': {
    //     backgroundColor: fade(theme.palette.common.white, 0.9),
    //   },
    //   marginRight: theme.spacing(2),
    //   marginLeft: 0,
    //   width: '100%',
    //   [theme.breakpoints.up('sm')]: {
    //     marginLeft: theme.spacing(3),
    //     width: 'auto',
    //   },
    // },
    // searchIcon: {
    //   padding: theme.spacing(0, 2),
    //   height: '100%',
    //   position: 'absolute',
    //   pointerEvents: 'none',
    //   display: 'flex',
    //   alignItems: 'center',
    //   justifyContent: 'center',
    // },
    // inputRoot: {
    //   color: 'primary',
    // },
    // inputInput: {
    //   padding: theme.spacing(1, 1, 1, 0),
    //   // vertical padding + font size from searchIcon
    //   paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    //   transition: theme.transitions.create('width'),
    //   width: '100%',
    //   [theme.breakpoints.up('md')]: {
    //     width: '20ch',
    //   },
    // },
    sectionDesktop: {
      display: 'none',
      [theme.breakpoints.up('sm')]: {
        display: 'flex',
      },
    },
    sectionMobile: {
      display: 'flex',
      [theme.breakpoints.up('sm')]: {
        display: 'none',
      },
    },
    hide: {
      display: 'none',
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
      whiteSpace: 'nowrap',
    },
    drawerOpen: {
      width: drawerWidth,
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    drawerClose: {
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      overflowX: 'hidden',
      width: 0,
      [theme.breakpoints.up('sm')]: {
        width: theme.spacing(8) + 1,
      },
    },
    toolbar: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      padding: theme.spacing(0, 1),
      ...theme.mixins.toolbar,
      backgroundColor: '#b9b9b91f',
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      marginTop: 60,
      [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(7) + 1,
      },
    },
    contentShift: {
      [theme.breakpoints.up('sm')]: {
        transition: theme.transitions.create('margin', {
          easing: theme.transitions.easing.easeOut,
          duration: theme.transitions.duration.enteringScreen,
        }),
        marginLeft: drawerWidth,
      },
    },
    nested: {
      paddingLeft: theme.spacing(4),
    },
    appbarButton: {
      marginLeft: theme.spacing(2),
      marginRight: theme.spacing(2),
    },
  }),
)

export default function Navbar({ children }: any) {
  const { t, i18n } = useTranslation()
  const [session, loading] = useSession()
  const classes = useStyles()
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null)
  const [anchorElLng, setAnchorElLng] = useState<null | HTMLElement>(null)
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] =
    useState<null | HTMLElement>(null)
  const [drawerAnchorEl, setDrawerAnchorEl] = useState<null | HTMLElement>(null)
  const theme = useTheme()
  const [list1Open, setList1Open] = useState(false)
  const [list2Open, setList2Open] = useState(false)
  const [list3Open, setList3Open] = useState(false)

  const router = useRouter()

  const isMenuOpen = Boolean(anchorEl)
  const isLngMenuOpen = Boolean(anchorElLng)
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl)
  const isDrawerOpen = Boolean(drawerAnchorEl)

  const handleProfileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }

  const handleLngMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElLng(event.currentTarget)
  }

  const handleLngMenuClose = () => {
    setAnchorElLng(null)
  }

  const handleMenuClose = () => {
    setAnchorEl(null)
    handleMobileMenuClose()
  }

  const handleMobileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setMobileMoreAnchorEl(event.currentTarget)
  }

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null)
  }

  const handleClickList1 = () => {
    setList1Open(!list1Open)
  }

  const handleClickList2 = () => {
    setList2Open(!list2Open)
  }

  const handleClickList3 = () => {
    setList3Open(!list3Open)
  }

  const handleCloseList = () => {
    setList1Open(false)
    setList2Open(false)
    setList3Open(false)
  }

  const handleDrawerOpen = (event: React.MouseEvent<HTMLElement>) => {
    setDrawerAnchorEl(event.currentTarget)
  }

  const handleDrawerClose = () => {
    setDrawerAnchorEl(null)
    handleCloseList()
  }

  const menuId = 'primary-search-account-menu'
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      getContentAnchorEl={null}
      anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem
        onClick={async () => {
          router.push('/profile')
        }}
      >
        {t('profile')}
      </MenuItem>
      {session?.level == 0 ? (
        <MenuItem
          onClick={async () => {
            router.push('/superstafflist')
          }}
        >
          {t('staff-list')}
        </MenuItem>
      ) : (
        <div></div>
      )}
      {session?.level == 2 ? (
        <MenuItem
          onClick={async () => {
            router.push('/companystafflist')
          }}
        >
          {t('staff-list')}
        </MenuItem>
      ) : (
        <div></div>
      )}
      <MenuItem onClick={() => signOut()}>{t('sign-out')}</MenuItem>
    </Menu>
  )

  const mobileMenuId = 'primary-search-account-menu-mobile'
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      getContentAnchorEl={null}
      anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem
        onClick={async () => {
          router.push('/profile')
        }}
      >
        {t('profile')}
      </MenuItem>
      {session?.level == 0 ? (
        <MenuItem
          onClick={async () => {
            router.push('/superstafflist')
          }}
        >
          {t('staff-list')}
        </MenuItem>
      ) : (
        <div></div>
      )}
      {session?.level == 2 ? (
        <MenuItem
          onClick={async () => {
            router.push('/companystafflist')
          }}
        >
          {t('staff-list')}
        </MenuItem>
      ) : (
        <div></div>
      )}
      <MenuItem onClick={() => signOut()}>{t('sign-out')}</MenuItem>
    </Menu>
  )

  if (!loading && !session) {
    router.replace(`${router.locale}/signin`)
  }

  return (
    <>
      {session && (
        <>
          <nav>
            <CssBaseline />
            <AppBar
              position="fixed"
              style={{ backgroundColor: '#d3f8ff' }}
              className={clsx(classes.appBar, {
                [classes.appBarShift]: drawerAnchorEl,
              })}
            >
              <Toolbar>
                <div className={classes.sectionDesktop}>
                  <IconButton
                    color="primary"
                    edge="start"
                    onMouseOver={handleDrawerOpen}
                    className={clsx(classes.menuButton, {
                      [classes.hide]: drawerAnchorEl,
                    })}
                  >
                    <MenuIcon />
                  </IconButton>
                </div>
                <div className={classes.sectionMobile}>
                  <IconButton
                    color="primary"
                    edge="start"
                    onClick={handleDrawerOpen}
                    className={clsx(classes.menuButton, {
                      [classes.hide]: drawerAnchorEl,
                    })}
                  >
                    <MenuIcon />
                  </IconButton>
                </div>
                {/* <IconButton
                  color="primary"
                  edge="start"
                  onMouseOver={handleDrawerOpen}
                  className={clsx(classes.menuButton, {
                    [classes.hide]: drawerAnchorEl,
                  })}
                >
                  <MenuIcon />
                </IconButton> */}
                <Link
                  component="button"
                  onClick={() => {
                    router.push('/')
                  }}
                >
                  <HazedawnWhiteIcon size="50" />
                </Link>

                {/* <div className={classes.search}>
              <div className={classes.searchIcon}>
                <SearchIcon color="primary" />
              </div>
              <InputBase
                placeholder={t('search')}
                classes={{
                  root: classes.inputRoot,
                  input: classes.inputInput,
                }}
                inputProps={{ 'aria-label': 'search' }}
              />
            </div> */}
                <div className={classes.grow} />
                <div className={classes.sectionDesktop}>
                  <Button
                    color="primary"
                    startIcon={<LanguageIcon />}
                    className={classes.appbarButton}
                    onClick={handleLngMenuOpen}
                    aria-haspopup="true"
                  >
                    {t('change-language')}
                  </Button>
                  <Menu
                    anchorEl={anchorElLng}
                    anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                    keepMounted
                    getContentAnchorEl={null}
                    transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                    open={isLngMenuOpen}
                    onClose={handleLngMenuClose}
                  >
                    <MenuItem component={Link} href={`/zh${router.asPath}`}>
                      {t('chinese')}
                    </MenuItem>
                    <MenuItem component={Link} href={`/en${router.asPath}`}>
                      {t('english')}
                    </MenuItem>
                  </Menu>
                  <Divider orientation="vertical" flexItem />
                  <IconButton
                    edge="end"
                    aria-label="account of current user"
                    className={classes.appbarButton}
                    aria-controls={menuId}
                    aria-haspopup="true"
                    onClick={handleProfileMenuOpen}
                    color="primary"
                  >
                    <AccountBoxIcon />
                  </IconButton>
                </div>
                <div className={classes.sectionMobile}>
                  <IconButton
                    color="primary"
                    onClick={handleLngMenuOpen}
                    aria-haspopup="true"
                  >
                    <LanguageIcon />
                  </IconButton>
                  <Menu
                    anchorEl={anchorElLng}
                    anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                    keepMounted
                    getContentAnchorEl={null}
                    transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                    open={isLngMenuOpen}
                    onClose={handleLngMenuClose}
                  >
                    <MenuItem
                      onClick={() => i18n.changeLanguage('zh')}
                      component={Link}
                      href={`/zh${router.asPath}`}
                    >
                      {t('chinese')}
                    </MenuItem>
                    <MenuItem
                      onClick={() => i18n.changeLanguage('en')}
                      component={Link}
                      href={`/en${router.asPath}`}
                    >
                      {t('english')}
                    </MenuItem>
                  </Menu>
                  <IconButton
                    aria-label="show more"
                    aria-controls={mobileMenuId}
                    aria-haspopup="true"
                    onClick={handleMobileMenuOpen}
                    color="primary"
                  >
                    <AccountBoxIcon />
                  </IconButton>
                </div>
              </Toolbar>
            </AppBar>
            {renderMobileMenu}
            {renderMenu}
            <div className={classes.sectionDesktop}>
              <Drawer
                variant="permanent"
                id="drawerItem"
                aria-owns={isDrawerOpen ? 'drawerItem' : undefined}
                aria-haspopup="true"
                onMouseOver={handleDrawerOpen}
                open={isDrawerOpen}
                onClose={handleDrawerClose}
                onMouseLeave={handleDrawerClose}
                // disableRestoreFocus={true}
                className={clsx(classes.drawer, {
                  [classes.drawerOpen]: isDrawerOpen,
                  [classes.drawerClose]: !isDrawerOpen,
                })}
                classes={{
                  paper: clsx({
                    [classes.drawerOpen]: isDrawerOpen,
                    [classes.drawerClose]: !isDrawerOpen,
                  }),
                }}
              >
                <div className={classes.toolbar}>
                  <Tooltip
                    title={
                      session.user.username
                        ? t('specific-user-profile', {
                            user: session.user.username,
                          }).toString()
                        : t('profile').toString()
                    }
                  >
                    <Button
                      color="primary"
                      startIcon={<AccountBoxIcon />}
                      href={`/${router.locale}/profile`}
                    >
                      {session.user.username}
                    </Button>
                  </Tooltip>
                  <IconButton>
                    {theme.direction === 'rtl' ? (
                      <ChevronRightIcon />
                    ) : (
                      <ChevronLeftIcon />
                    )}
                  </IconButton>
                </div>
                <Divider variant="middle" />
                {session.level < 4 ? (
                  <List>
                    <ListItem button onClick={handleClickList1}>
                      <ListItemIcon onClick={handleDrawerOpen}>
                        <AccountBoxIcon />
                      </ListItemIcon>
                      <ListItemText primary={t('client-management')} />
                      {list1Open ? <ExpandLess /> : <ExpandMore />}
                    </ListItem>
                    <Collapse in={list1Open} timeout="auto" unmountOnExit>
                      <List component="div" disablePadding>
                        {session.level < 2 ? (
                          <ListItem
                            button
                            className={classes.nested}
                            onClick={async () => {
                              router.push('/company')
                            }}
                          >
                            <ListItemIcon></ListItemIcon>
                            <ListItemText primary={t('all-company')} />
                          </ListItem>
                        ) : null}
                        {session.level < 2 ? (
                          <ListItem
                            button
                            className={classes.nested}
                            onClick={async () => {
                              router.push('/companybedlte1m')
                            }}
                          >
                            <ListItemIcon></ListItemIcon>
                            <ListItemText
                              primary={t('all-company')}
                              secondary={
                                t('BR_expiry_date') + ' ≤ 1 ' + t('month')
                              }
                              secondaryTypographyProps={{ variant: 'caption' }}
                            />
                          </ListItem>
                        ) : null}
                        <ListItem
                          button
                          className={classes.nested}
                          onClick={async () => {
                            router.push('/quotation')
                          }}
                        >
                          <ListItemIcon></ListItemIcon>
                          <ListItemText primary={t('company-quotation')} />
                        </ListItem>
                      </List>
                    </Collapse>
                  </List>
                ) : null}
                <List>
                  <ListItem button onClick={handleClickList2}>
                    <ListItemIcon onClick={handleDrawerOpen}>
                      <ShoppingCartIcon />
                    </ListItemIcon>
                    <ListItemText primary={t('e-commence')} />
                    {list2Open ? <ExpandLess /> : <ExpandMore />}
                  </ListItem>
                  <Collapse in={list2Open} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                      {/* <ListItem button className={classes.nested}>
                  <ListItemText primary={t('dashboard')} />
                </ListItem> */}
                      <ListItem
                        button
                        className={classes.nested}
                        onClick={async () => {
                          router.push('/company')
                        }}
                      >
                        <ListItemIcon></ListItemIcon>
                        <ListItemText primary={t('service')} />
                      </ListItem>
                      <ListItem
                        button
                        className={classes.nested}
                        onClick={async () => {
                          router.push('/requesthistory')
                        }}
                      >
                        <ListItemIcon></ListItemIcon>
                        <ListItemText primary={t('request-history')} />
                      </ListItem>
                    </List>
                  </Collapse>
                </List>
                {session.level < 4 ? (
                  <List>
                    <ListItem button onClick={handleClickList3}>
                      <ListItemIcon onClick={handleDrawerOpen}>
                        <FileCopyIcon />
                      </ListItemIcon>
                      <ListItemText primary={t('erp')} />
                      {list3Open ? <ExpandLess /> : <ExpandMore />}
                    </ListItem>
                    <Collapse in={list3Open} timeout="auto" unmountOnExit>
                      <List component="div" disablePadding>
                        <ListItem button className={classes.nested}>
                          <ListItemIcon></ListItemIcon>
                          <ListItemText primary={t('acct-system')} />
                        </ListItem>
                        <ListItem button className={classes.nested}>
                          <ListItemIcon></ListItemIcon>
                          <ListItemText primary={t('hr-system')} />
                        </ListItem>
                      </List>
                    </Collapse>
                  </List>
                ) : (
                  <div></div>
                )}
              </Drawer>
            </div>
            <div className={classes.sectionMobile}>
              <Drawer
                variant="permanent"
                id="drawerItem"
                anchor="left"
                aria-owns={isDrawerOpen ? 'drawerItem' : undefined}
                aria-haspopup="true"
                open={isDrawerOpen}
                onClose={handleDrawerClose}
                // disableRestoreFocus={true}
                className={clsx(classes.drawer, {
                  [classes.drawerOpen]: isDrawerOpen,
                  [classes.drawerClose]: !isDrawerOpen,
                })}
                classes={{
                  paper: clsx({
                    [classes.drawerOpen]: isDrawerOpen,
                    [classes.drawerClose]: !isDrawerOpen,
                  }),
                }}
              >
                <div className={classes.toolbar}>
                  <Tooltip
                    title={
                      session.user.username
                        ? t('specific-user-profile', {
                            user: session.user.username,
                          }).toString()
                        : t('profile').toString()
                    }
                  >
                    <Button
                      color="primary"
                      startIcon={<AccountBoxIcon />}
                      href={`/${router.locale}/profile`}
                    >
                      {session.user.username}
                    </Button>
                  </Tooltip>
                  <IconButton onClick={handleDrawerClose}>
                    {theme.direction === 'rtl' ? (
                      <ChevronRightIcon />
                    ) : (
                      <ChevronLeftIcon />
                    )}
                  </IconButton>
                </div>
                <Divider variant="middle" />
                {session.level < 4 ? (
                  <List>
                    <ListItem button onClick={handleClickList1}>
                      <ListItemIcon>
                        <AccountBoxIcon />
                      </ListItemIcon>
                      <ListItemText primary={t('client-management')} />
                      {list1Open ? <ExpandLess /> : <ExpandMore />}
                    </ListItem>
                    <Collapse in={list1Open} timeout="auto" unmountOnExit>
                      <List component="div" disablePadding>
                        {session.level < 2 ? (
                          <ListItem
                            button
                            className={classes.nested}
                            onClick={async () => {
                              router.push('/company')
                            }}
                          >
                            <ListItemIcon></ListItemIcon>
                            <ListItemText primary={t('all-company')} />
                          </ListItem>
                        ) : null}
                        {session.level < 2 ? (
                          <ListItem
                            button
                            className={classes.nested}
                            onClick={async () => {
                              router.push('/companybedlte1m')
                            }}
                          >
                            <ListItemIcon></ListItemIcon>
                            <ListItemText
                              primary={t('all-company')}
                              secondary={
                                t('BR_expiry_date') + ' ≤ 1 ' + t('month')
                              }
                              secondaryTypographyProps={{ variant: 'caption' }}
                            />
                          </ListItem>
                        ) : null}
                        <ListItem
                          button
                          className={classes.nested}
                          onClick={async () => {
                            router.push('/quotation')
                          }}
                        >
                          <ListItemIcon></ListItemIcon>
                          <ListItemText primary={t('company-quotation')} />
                        </ListItem>
                      </List>
                    </Collapse>
                  </List>
                ) : null}
                <List>
                  <ListItem button onClick={handleClickList2}>
                    <ListItemIcon>
                      <ShoppingCartIcon />
                    </ListItemIcon>
                    <ListItemText primary={t('e-commence')} />
                    {list2Open ? <ExpandLess /> : <ExpandMore />}
                  </ListItem>
                  <Collapse in={list2Open} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                      {/* <ListItem button className={classes.nested}>
                  <ListItemText primary={t('dashboard')} />
                </ListItem> */}
                      <ListItem
                        button
                        className={classes.nested}
                        onClick={async () => {
                          router.push('/company')
                        }}
                      >
                        <ListItemIcon></ListItemIcon>
                        <ListItemText primary={t('service')} />
                      </ListItem>
                      <ListItem
                        button
                        className={classes.nested}
                        onClick={async () => {
                          router.push('/requesthistory')
                        }}
                      >
                        <ListItemIcon></ListItemIcon>
                        <ListItemText primary={t('request-history')} />
                      </ListItem>
                    </List>
                  </Collapse>
                </List>
                {session.level < 4 ? (
                  <List>
                    <ListItem button onClick={handleClickList3}>
                      <ListItemIcon>
                        <FileCopyIcon />
                      </ListItemIcon>
                      <ListItemText primary={t('erp')} />
                      {list3Open ? <ExpandLess /> : <ExpandMore />}
                    </ListItem>
                    <Collapse in={list3Open} timeout="auto" unmountOnExit>
                      <List component="div" disablePadding>
                        <ListItem button className={classes.nested}>
                          <ListItemIcon></ListItemIcon>
                          <ListItemText primary={t('acct-system')} />
                        </ListItem>
                        <ListItem button className={classes.nested}>
                          <ListItemIcon></ListItemIcon>
                          <ListItemText primary={t('hr-system')} />
                        </ListItem>
                      </List>
                    </Collapse>
                  </List>
                ) : (
                  <div></div>
                )}
              </Drawer>
            </div>
          </nav>
          <main
            className={clsx(classes.content, {
              [classes.contentShift]: isDrawerOpen,
            })}
          >
            {children}
          </main>
        </>
      )}
    </>
  )
}
