export interface RequestStatusObject {
  ongoing: number
  cancelled: number
  declined: number
  approved: number
  expired: number
}
