const isDev = process.env.NODE_ENV !== 'production'

const dbUrl =
  // 'mongodb+srv://hazedawndev:MPrrxxRR6uUy4EIz@cluster0.m4jpp.mongodb.net/hazetem?authSource=admin&replicaSet=atlas-pz9rbb-shard-0&w=majority&readPreference=primary&appname=MongoDB%20Compass&retryWrites=true&ssl=true'
  'mongodb+srv://joeleung:joeleung@cluster0.s6bfl.mongodb.net/myFirstDatabase?retryWrites=true&w=majority'

const webInfo = {
  devUrl: 'http://localhost:3000/',
  baseUrl: isDev ? 'http://localhost:3000/' : 'https://hazetem.vercel.app/',
  // baseUrl: isDev ? 'http://localhost:3000/' : 'http://hazetem.com/',
}

const corsOptions = {
  credentials: true,
  origin: [
    'https://hazetem.vercel.app',
    'https://hazetem.com',
    'http://localhost:3000',
    'http://localhost:8088',
    'https://mail.google.com',
  ],
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
  allowedHeaders: ['Content-Type', 'Authorization'],
}

const jwtConfig = {
  authSecret: 'this is auth jwt',
  userSecret: 'this is user jwt',
  secret: '0a5e90f2b35894134611120648be9dce',
  expirationTime: 86400000,
}

const nodemailerConfig = {
  secret: 'emaillllllllllllllllllllllllllllllllll',
  user: 'support@hazedawn.com',
  pass: 'hazedawncool',
}

// delete later
const validateUser = true

export {
  isDev,
  dbUrl,
  webInfo,
  corsOptions,
  jwtConfig,
  validateUser,
  nodemailerConfig,
}
