import { Request, Response } from 'express'
import nodemailer from 'nodemailer'
import User from '../User/user.model'
import { nodemailerConfig } from '../../config'
import jwt from 'jsonwebtoken'
import Company, { ICompany } from '../Company/company.model'
import RequestHistory, {
  IRequestHistory,
} from '../RequestHistory/requesthistory.model'
import { requestRemindMail, dailyRequestRemindMail } from '../../utils/email'
import { IServiceItem } from '../Service/service.model'
import { RequestStatusObject } from '../../types/appEnums'

function send(
  receiver_email: string,
  sender_email: string,
  sender_password: string,
  email_subject: string,
  email_message: string,
  res: Response,
) {
  try {
    const transporter = nodemailer.createTransport({
      host: 'smtp.gmail.com',
      port: 465,
      secure: true,
      auth: {
        user: sender_email,
        pass: sender_password,
      },
    })

    const mailOptions = {
      from: sender_email,
      to: receiver_email,
      subject: email_subject,
      text: email_message,
    }

    transporter.sendMail(mailOptions, (err, response) => {
      if (err) {
        console.error(`mailing error: ${err}`)
        return res.status(418).json({
          msg: "i'm a teapot: mailing error",
          err: err,
        })
      } else {
        return res.status(200).json({
          msg: 'sus',
          res: response,
          email: receiver_email,
        })
      }
    })
  } catch (err) {
    return res.status(500).json({
      msg: err,
    })
  }
}

export function send_request_remind_mail_to_vendor(
  receiver_email: string,
  requester: string,
  service_name: string,
  requested_items: IServiceItem[],
  company_id: string,
  request_history_id: string,
) {
  try {
    const sender_email = nodemailerConfig.user
    const sender_password = nodemailerConfig.pass
    const itemList: IServiceItem[] = []

    requested_items.map((item: IServiceItem) => {
      if (item.needed) {
        itemList.push(item)
      }
    })

    const transporter = nodemailer.createTransport({
      host: 'smtp.gmail.com',
      port: 465,
      secure: true,
      auth: {
        user: sender_email,
        pass: sender_password,
      },
    })

    transporter.sendMail(
      {
        from: sender_email,
        to: receiver_email,
        subject: 'Request Remind Mail',
        text: `seems like ${requester} just requested your service: ${service_name} with items [ ${itemList.map(
          (item: IServiceItem, index: number) => {
            if (index < itemList.length - 1) {
              return `${item.title}, `
            } else {
              return item.title
            }
          },
        )} ], click here to approve the request`,
        html: requestRemindMail(
          requester,
          service_name,
          itemList,
          company_id,
          request_history_id,
        ),
        attachments: [
          {
            filename: 'hazedawn.png',
            path: './public/images/hazedawn.png',
            cid: 'hazedawn_logo',
          },
          {
            filename: 'clock.png',
            path: './public/images/clock.png',
            cid: 'clock',
          },
        ],
      },
      (err) => {
        if (err) {
          console.error(`mailing error: ${err}`)
        }
      },
    )
  } catch (err) {
    console.error(err)
  }
}

export async function sendMail_BR_remind() {
  try {
    const currentDate = new Date()
    const day29beforeExpiry = new Date(
      currentDate.getTime() + 29 * 24 * 60 * 60 * 1000,
    )
    const day31beforeExpiry = new Date(
      currentDate.getTime() + 31 * 24 * 60 * 60 * 1000,
    )
    await Company.find({
      BR_expiry_date: {
        $gte: day29beforeExpiry,
        $lte: day31beforeExpiry,
      },
    }).then((companys: ICompany[]) => {
      companys.map((company: ICompany) => {
        const sender_email = nodemailerConfig.user
        const sender_password = nodemailerConfig.pass

        const transporter = nodemailer.createTransport({
          host: 'smtp.gmail.com',
          port: 465,
          secure: true,
          auth: {
            user: sender_email,
            pass: sender_password,
          },
        })

        transporter.sendMail(
          {
            from: sender_email,
            to: company.contact_email,
            subject: 'BR Expiry Date Reminder',
            text: `Dear ${
              company.contact_person
            },\n\nThis is a reminder for your BR expiry date. The BR expiry date is on: ${
              company.BR_expiry_date.toString().split('T')[0]
            }. Please do not forget to submit all the necessary forms.\n\nBest Regard,\nHazedawn`,
          },
          (err) => {
            if (err) {
              console.error(`mailing error: ${err}`)
            } else {
              console.log(
                `br expiry reminder mailed to ${company.contact_email}`,
              )
            }
          },
        )
      })
    })
  } catch (err) {
    // console.error(err)
  }
}

export async function sendMail_request_remind() {
  try {
    const currentDate = new Date()
    const yesterday = new Date(currentDate.getTime() - 86400000)

    // list of companies to notify
    const companies_to_notify: string[] = []
    // list of number of unapproved/ongoing requests corresponding to the company list above
    const num_requests: RequestStatusObject[] = []
    await RequestHistory.find({
      createdAt: { $gte: yesterday },
    })
      .sort({ createdAt: -1 })
      .populate({ path: 'company', select: ['company_name', 'contact_email'] })
      .select(['company', 'status', 'createdAt'])
      .then(async (result: IRequestHistory[]) => {
        result.map((request_history: IRequestHistory) => {
          if (
            !companies_to_notify.includes(
              (request_history.company as ICompany).contact_email,
            )
          ) {
            companies_to_notify.push(
              (request_history.company as ICompany).contact_email,
            )
            num_requests.push({
              ongoing: 0,
              cancelled: 0,
              declined: 0,
              approved: 0,
              expired: 0,
            })
          }

          const index = companies_to_notify.indexOf(
            (request_history.company as ICompany).contact_email,
          )
          switch (request_history.status) {
            case 0: {
              num_requests[index].ongoing += 1
              break
            }
            case 1: {
              num_requests[index].cancelled += 1
              break
            }
            case 2: {
              num_requests[index].declined += 1
              break
            }
            case 3: {
              num_requests[index].approved += 1
              break
            }
            case 4: {
              num_requests[index].expired += 1
              break
            }
            default: {
              console.log('sendMail_request_remind error')
              break
            }
          }
        })
      })

    companies_to_notify.map((email: string, index: number) => {
      const sender_email = nodemailerConfig.user
      const sender_password = nodemailerConfig.pass

      const transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        secure: true,
        auth: {
          user: sender_email,
          pass: sender_password,
        },
      })

      transporter.sendMail(
        {
          from: sender_email,
          to: email,
          subject: 'BR Expiry Date Reminder',
          html: dailyRequestRemindMail(num_requests[index]),
          attachments: [
            {
              filename: 'hazedawn.png',
              path: './public/images/hazedawn.png',
              cid: 'hazedawn_logo',
            },
            {
              filename: 'background.png',
              path: './public/images/background.png',
              cid: 'email_bg',
            },
          ],
        },
        (err) => {
          if (err) {
            console.error(`mailing error: ${err}`)
          }
        },
      )
    })
  } catch (err) {
    console.error(err)
  }
}

const mailController = {
  async sendMailWithAddress(req: Request, res: Response) {
    const { user } = req
    //@ts-ignore
    if (user.level != 0 && user.level != 1) {
      return res.status(403).json({
        msg: 'you are not super admin nor super staff',
      })
    } else {
      const {
        receiver_email,
        sender_email,
        sender_password,
        email_subject,
        email_message,
      } = req.body
      send(
        receiver_email,
        sender_email,
        sender_password,
        email_subject,
        email_message,
        res,
      )
    }
  },
  async sendMailDefault(req: Request, res: Response) {
    const { user } = req
    //@ts-ignore
    if (user.level != 0 && user.level != 1) {
      return res.status(403).json({
        msg: 'you are not super admin nor super staff',
      })
    } else {
      const { receiver_email, email_subject, email_message } = req.body

      // default email to send email
      // less secure enabled email
      const sender_email = nodemailerConfig.user
      const sender_password = nodemailerConfig.pass

      send(
        receiver_email,
        sender_email,
        sender_password,
        email_subject,
        email_message,
        res,
      )
    }
  },
  async sendMailDefaultByUserId(req: Request, res: Response) {
    const { user } = req
    //@ts-ignore
    if (user.level != 0 && user.level != 1) {
      return res.status(403).json({
        msg: 'you are not super admin nor super staff',
      })
    } else {
      try {
        const user = await User.findById(req.params.id)
        const { email_subject, email_message } = req.body

        // default email to send email
        // less secure enabled email
        const sender_email = nodemailerConfig.user
        const sender_password = nodemailerConfig.pass

        if (!user) {
          return res.status(404).json({
            msg: 'user not found',
          })
        } else {
          send(
            user.email,
            sender_email,
            sender_password,
            email_subject,
            email_message,
            res,
          )
        }
      } catch (err) {
        return res.status(500).json({
          msg: err,
        })
      }
    }
  },
  async sendPW(req: Request, res: Response) {
    try {
      const { email } = req.body

      const found_user = await User.findOne({ email })
      const ver_code = Math.random().toString(36).substr(2, 5).toUpperCase()

      if (found_user) {
        const message = `Dear ${found_user.username}, \n\nemail recovery verification code: ${ver_code}\n\nGoodLuck,\nHazedawn`

        const sender_email = nodemailerConfig.user
        const sender_password = nodemailerConfig.pass

        const currentDate = new Date()
        const token = jwt.sign(
          JSON.stringify({
            ver_code: ver_code,
            email: email,
          }),
          nodemailerConfig.secret,
        )
        res.cookie('email-jwt', token, {
          httpOnly: true,
          expires: new Date(currentDate.getTime() + 65000),
        })

        send(
          email,
          sender_email,
          sender_password,
          'User password recovery',
          message,
          res,
        )
      } else {
        return res.status(404).json({
          msg: 'user with this email not found',
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async sendMailDefaultByCompanyId(req: Request, res: Response) {
    const { user } = req
    //@ts-ignore
    if (user.level != 0 && user.level != 1) {
      return res.status(403).json({
        msg: 'you are not super admin nor super staff',
      })
    } else {
      try {
        const company = await Company.findById(req.params.id)
        const { email_subject, email_message } = req.body

        // default email to send email
        // less secure enabled email
        const sender_email = nodemailerConfig.user
        const sender_password = nodemailerConfig.pass

        if (!company) {
          return res.status(404).json({
            msg: 'company not found',
          })
        } else {
          send(
            company.contact_email,
            sender_email,
            sender_password,
            email_subject,
            email_message,
            res,
          )
        }
      } catch (err) {
        return res.status(500).json({
          msg: err,
        })
      }
    }
  },
  async sendBRRemindMailById(req: Request, res: Response) {
    const { user } = req

    try {
      // @ts-ignore
      if (user.level < 2) {
        await Company.findById(req.params.id).then((company) => {
          const sender_email = nodemailerConfig.user
          const sender_password = nodemailerConfig.pass

          const transporter = nodemailer.createTransport({
            host: 'smtp.gmail.com',
            port: 465,
            secure: true,
            auth: {
              user: sender_email,
              pass: sender_password,
            },
          })

          if (company) {
            const { user } = req

            transporter.sendMail(
              {
                from: sender_email,
                to: company.contact_email,
                subject: 'BR Expiry Date Reminder',
                text: `Dear ${
                  company.contact_person
                },\n\nThis is a reminder for your BR expiry date. The BR expiry date is on: ${
                  company.BR_expiry_date.toString().split('T')[0]
                }. Please do not forget to submit all the necessary forms.\n\nBest Regard,\n${
                  // @ts-ignore
                  user.username
                }\nHazedawn`,
              },
              (err) => {
                if (err) {
                  console.error(`mailing error: ${err}`)
                  return res.status(418).json({
                    msg: `mailing error: ${err}`,
                  })
                } else {
                  return res.status(200).json({
                    msg: `br expiry reminder mailed to ${company.contact_email}`,
                    email: company.contact_email,
                  })
                }
              },
            )
          } else {
            return res.status(404).json({
              msg: 'company not found',
            })
          }
        })
      } else {
        return res.status(403).json({
          msg: 'you are not authorized to send email',
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
}

export default mailController
