import { Router } from 'express'
import mailController from './nodemailer.controller'
import { authMiddleware } from '../../middlewares/auth.middleware'

const router = Router()

// forogt password mail to user email
router.post('/forgotpw', mailController.sendPW)

router.post('/send_default', authMiddleware, mailController.sendMailDefault)
router.post('/send_address', authMiddleware, mailController.sendMailWithAddress)
router.post(
  '/send/user/:id',
  authMiddleware,
  mailController.sendMailDefaultByUserId,
)
router.post(
  '/send/company/:id',
  authMiddleware,
  mailController.sendMailDefaultByCompanyId,
)
router.get(
  '/sendbrremind/:id',
  authMiddleware,
  mailController.sendBRRemindMailById,
)

export default router
