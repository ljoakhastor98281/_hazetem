import mongoose from 'mongoose'
import { ICompany } from '../Company/company.model'

export interface IServiceItem {
  title: string
  description: string
  price: number
  order: number
  needed: boolean
}

export interface IService extends Partial<mongoose.Document> {
  _id: mongoose.Types.ObjectId
  vendor: mongoose.Types.ObjectId | ICompany
  service_name: string
  description: string
  image_data: string
  service_items: IServiceItem[]
  createdAt: Date
  updatedAt: Date
}

const ServiceSchema = new mongoose.Schema(
  {
    vendor: {
      type: mongoose.Types.ObjectId,
      required: true,
      trim: true,
      ref: 'companys',
    },
    service_name: {
      type: String,
      required: true,
      trim: true,
    },
    description: {
      type: String,
      required: false,
      trim: true,
      default: '',
    },
    image_data: {
      type: String,
      required: false,
      trim: true,
    },
    service_items: {
      type: Array,
      required: true,
      trim: true,
    },
  },
  {
    timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' },
    optimisticConcurrency: true,
    strict: false,
  },
)

// vendor_name deprecated
// get vendor name from company model instead

// ServiceSchema.pre('validate', async function (this: IService, next) {
//   const vendor = await Company.findById(this.vendor_id)
//   this.vendor_name = (vendor as ICompany).company_name
//   next()
// })

// ServiceSchema.pre('updateOne', async function (this, next) {
//   const vendor = await Company.findById(this.get('vendor_id'))
//   this.set({ vendor_name: (vendor as ICompany).company_name })
//   next()
// })

const Service: mongoose.Model<IService> =
  mongoose.models.services ||
  mongoose.model<IService>('services', ServiceSchema)

export default Service
