import { Request, Response } from 'express'
import { validationResult } from 'express-validator'
import Company from '../Company/company.model'
import { IUser } from '../User/user.model'
import Service, { IService, IServiceItem } from './service.model'

const serviceController = {
  async getServiceByVenderId(req: Request, res: Response) {
    await Service.find({ vendor: req.params.id })
      .exec()
      .then((result) => {
        return res.status(200).json({
          msg: 'sus',
          count: result.length,
          services: result,
        })
      })
      .catch((err) => {
        return res.status(500).json({
          msg: err,
        })
      })
  },
  async uploadImage(req: Request, res: Response) {
    try {
      const service = await Service.findById(req.params.id)
      if (service) {
        await Service.findByIdAndUpdate(
          req.params.id,
          {
            $set: { image_data: req.body.image_data },
          },
          { new: true },
        )
          .exec()
          .then(async () => {
            return res.status(200).json({
              msg: 'sus uploaded image',
            })
          })
          .catch((err) => {
            return res.status(500).json({
              msg: err,
            })
          })
      } else {
        return res.status(404).json({
          msg: 'service not found',
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },

  // deployment
  async getAllServices(req: Request, res: Response) {
    try {
      const { user } = req
      // @ts-ignore
      if (user.level != 0 && user.level != 1) {
        return res.status(403).json({
          msg: 'you are not super admin nor super staff',
        })
      } else {
        let result: IService[] = []
        if (
          req.query.newlyCreated?.toString().toLowerCase() === 'true' &&
          req.query.days_ago
        ) {
          const currentDate = new Date()
          const days_ago = new Date(
            currentDate.getTime() -
              parseInt(req.query.days_ago.toString()) * 86400000,
          )

          result = await Service.find({
            createdAt: {
              $gte: days_ago,
              $lte: currentDate,
            },
          })
            .sort({ createdAt: -1 })
            .select(['service_name', 'vendor'])
        } else {
          result = await Service.find()
        }
        if (req.query.withVendorInfo?.toString().toLowerCase() === 'true') {
          await Service.populate(result, 'vendor')
        }
        return res.status(200).json({
          msg: 'sus',
          count: result.length,
          services: result,
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async getServiceById(req: Request, res: Response) {
    const { user } = req
    try {
      const service =
        // @ts-ignore
        user.level == 2 || user.level == 3
          ? // @ts-ignore
            await Service.findOne({
              _id: req.params.id,
              vendor: (user as IUser)?.company_id,
            })
          : await Service.findById(req.params.id)
      if (service) {
        return res.status(200).json({
          msg: 'sus',
          service: service,
        })
      } else {
        return res.status(404).json({
          msg: 'service not found',
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async getServiceByIdWithVendorInfo(req: Request, res: Response) {
    const { user } = req
    try {
      const service =
        // @ts-ignore
        user.level == 2 || user.level == 3
          ? await Service.find({
              _id: req.params.id,
              // @ts-ignore
              vendor: user.company_id,
            }).populate({
              path: 'vendor',
              select: [
                '_id',
                'company_name',
                'contact_person',
                'contact_email',
                'client_contact_number',
              ],
            })
          : await Service.findById(req.params.id).populate({
              path: 'vendor',
              select: [
                '_id',
                'company_name',
                'contact_person',
                'contact_email',
                'client_contact_number',
              ],
            })
      if (service) {
        return res.status(200).json({
          msg: 'sus',
          service: service,
        })
      } else {
        return res.status(404).json({
          msg: 'service not found',
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async addService(req: Request, res: Response) {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      const status_code = errors.array()[0].msg.includes('conflict') ? 409 : 400
      return res.status(status_code).json({ msg: errors.array()[0].msg })
    }
    const { user } = req
    const {
      vendor,
      service_name,
      description,
      image_data,
      service_items,
    }: IService = req.body

    if (service_name == '') {
      return res.status(400).json({
        msg: 'missing service name',
      })
    }

    if ((user as IUser).level > 3) {
      return res.status(403).json({
        msg: 'you are normie',
      })
    } else if ((user as IUser).level > 1) {
      try {
        const same_vendor = await Company.findById((user as IUser).company_id)
        if (same_vendor) {
          const same_service = await Service.findOne({
            vendor: (user as IUser).company_id,
            service_name: service_name,
          })
          if (same_service) {
            res.status(409).json({
              msg: 'conflict-service-name',
            })
          } else {
            const postService: IService = new Service({
              vendor: (user as IUser).company_id,
              service_name,
              description,
              image_data,
              service_items,
            })
            await postService?.save?.()
            return res.status(200).json({
              msg: 'sus',
              service: postService,
            })
          }
        } else {
          return res
            .status(404)
            .json({ msg: 'The vendor with this id does not exist' })
        }
      } catch (err) {
        // console.error(err.message)
        return res.status(500).json({ msg: err })
      }
    } else {
      try {
        const same_vendor = await Company.findById(vendor)
        if (same_vendor) {
          const same_service = await Service.findOne({
            vendor: vendor,
            service_name: service_name,
          })
          if (same_service) {
            res.status(409).json({
              msg: 'conflict-service-name',
            })
          } else {
            const postService: IService = new Service({
              vendor,
              service_name,
              description,
              image_data,
              service_items,
            })
            await postService?.save?.()
            return res.status(200).json({
              msg: 'sus',
              service: postService,
            })
          }
        } else {
          return res
            .status(404)
            .json({ msg: 'The vendor with this id does not exist' })
        }
      } catch (err) {
        // console.error(err.message)
        return res.status(500).json({ msg: err })
      }
    }
  },
  async changeServiceById(req: Request, res: Response) {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      const status_code = errors.array()[0].msg.includes('conflict') ? 409 : 400
      return res.status(status_code).json({ msg: errors.array()[0].msg })
    }
    const { user } = req
    const {
      vendor,
      service_name,
      description,
      image_data,
      service_items,
    }: IService = req.body

    if (service_name == '') {
      return res.status(400).json({
        msg: 'missing service name',
      })
    }

    if ((user as IUser).level > 3) {
      return res.status(403).json({
        msg: 'you are normie',
      })
    } else {
      try {
        const same_service_name = await Service.find({
          _id: { $ne: req.params.id },
          service_name: service_name,
        })

        if (same_service_name.length > 0) {
          // console.log(same_service_name)
          return res.status(409).json({
            msg: 'conflict-service-name',
          })
        }

        const service = await Service.findById(req.params.id)
        if (service) {
          if ((user as IUser).level < 2) {
            await Service.updateOne(
              { _id: req.params.id },
              {
                vendor,
                service_name,
                description,
                image_data,
                service_items,
              },
            )
          } else {
            if ((user as IUser).company_id != service.vendor) {
              return res.status(403).json({
                msg: 'not your company service',
              })
            } else {
              await Service.updateOne(
                { _id: req.params.id },
                {
                  vendor: (user as IUser).company_id,
                  service_name,
                  description,
                  image_data,
                  service_items,
                },
              )
            }
          }
          const newService = await Service.findOne({ _id: req.params.id })
          return res.status(200).json({
            msg: 'sus',
            new_service: newService,
          })
        } else {
          return res.status(404).json({
            msg: 'service not found',
          })
        }
      } catch (err) {
        return res.status(500).json({
          msg: err,
        })
      }
    }
  },
  async deleteServiceById(req: Request, res: Response) {
    const { user } = req
    // @ts-ignore
    if (user.level > 3) {
      return res.status(403).json({
        msg: 'you are normie',
      })
    }
    // @ts-ignore
    else if (user.level > 1) {
      // @ts-ignore
      try {
        const service = await Service.findById(req.params.id)
        if (service) {
          if ((user as IUser).company_id != service.vendor) {
            return res.status(400).json({
              msg: 'not your company service',
            })
          }
          await Service.findByIdAndDelete(req.params.id)
            .exec()
            .then((result) => {
              return res.status(200).json({
                msg: 'sus',
                service: result,
              })
            })
            .catch((err) => {
              return res.status(500).json({
                msg: err,
              })
            })
        } else {
          return res.status(404).json({
            msg: 'service not found',
          })
        }
      } catch (err) {
        return res.status(500).json({
          msg: err,
        })
      }
    } else {
      try {
        const service = await Service.findById(req.params.id)
        if (service) {
          await Service.findByIdAndDelete(req.params.id)
            .exec()
            .then((result) => {
              return res.status(200).json({
                msg: 'sus',
                service: result,
              })
            })
            .catch((err) => {
              return res.status(500).json({
                msg: err,
              })
            })
        } else {
          return res.status(404).json({
            msg: 'service not found',
          })
        }
      } catch (err) {
        return res.status(500).json({
          msg: err,
        })
      }
    }
  },

  // development
  async getAllServicesIsDev(req: Request, res: Response) {
    try {
      const result = await Service.find()
      if (req.query.withVendorInfo?.toString().toLowerCase() === 'true')
        await Service.populate(result, 'vendor')
      return res.status(200).json({
        msg: 'sus',
        count: result.length,
        services: result,
      })
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async getServiceByIdIsDev(req: Request, res: Response) {
    try {
      const service = await Service.findById(req.params.id)
      if (service) {
        if (req.query.withVendorInfo?.toString().toLowerCase() === 'true')
          await Service.populate(service, 'vendor')
        return res.status(200).json({
          msg: 'sus',
          service: service,
        })
      } else {
        return res.status(404).json({
          msg: 'service not found',
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async getServiceByVendorIdIsDev(req: Request, res: Response) {
    try {
      const service = await Service.find({ vendor: req.params.id })
      if (service) {
        if (req.query.withVendorInfo?.toString().toLowerCase() === 'true') {
          const vendor = await Company.find({ _id: req.params.id })
          return res.status(200).json({
            msg: 'sus',
            count: service.length,
            services: service,
            vendor: vendor,
          })
        }
        return res.status(200).json({
          msg: 'sus',
          count: service.length,
          services: service,
        })
      } else {
        return res.status(404).json({
          msg: 'service with this vendor not found',
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async addServiceIsDev(req: Request, res: Response) {
    const {
      vendor,
      service_name,
      description,
      image_data,
      service_items,
    }: IService = req.body
    try {
      const same_vendor = await Company.findById(vendor)
      if (same_vendor) {
        const same_service = await Service.findOne({
          vendor: vendor,
          service_name: service_name,
        })
        if (same_service) {
          res
            .status(409)
            .json({ msg: 'Service with the same name exists for this vendor' })
        } else {
          const postService: IService = new Service({
            vendor,
            service_name,
            description,
            image_data,
            service_items,
          })
          await postService?.save?.()
          return res.status(200).json({
            msg: 'sus',
            service: postService,
          })
        }
      } else {
        return res
          .status(404)
          .json({ msg: 'The vendor with this id does not exist' })
      }
    } catch (err) {
      // console.error(err.message)
      return res.status(500).json({ msg: err })
    }
  },
  async changeServiceByIdIsDev(req: Request, res: Response) {
    try {
      const {
        vendor,
        service_name,
        description,
        image_data,
        service_items,
      }: IService = req.body
      const service = await Service.findById(req.params.id)
      if (!service) {
        return res.status(404).json({
          msg: 'No service with this id can be found',
        })
      }
      await Service.updateOne(
        { _id: req.params.id },
        {
          vendor,
          service_name,
          description,
          image_data,
          service_items,
        },
      )
        .exec()
        .then(async () => {
          const newService = await Service.findOne({ _id: req.params.id })
          return res.status(200).json({
            msg: 'sus',
            new_service: newService,
          })
        })
        .catch((err) => {
          return res.status(500).json({
            msg: err,
          })
        })
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async deleteServiceByIdIsDev(req: Request, res: Response) {
    try {
      const service = await Service.findById(req.params.id)
      if (service) {
        await Service.findByIdAndDelete(req.params.id)
          .exec()
          .then((result) => {
            return res.status(200).json({
              msg: 'sus',
              service: result,
            })
          })
          .catch((err) => {
            return res.status(500).json({
              msg: err,
            })
          })
      } else {
        return res.status(404).json({
          msg: 'service not found',
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
}

export default serviceController
