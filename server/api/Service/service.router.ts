import { Router } from 'express'
import serviceController from './service.controller'

import { isDev, validateUser } from '../../config'
import { authMiddleware } from '../../middlewares/auth.middleware'
import { check } from 'express-validator'
import { validateServiceItems } from '../../middlewares/service_items.middleware'

const router = Router()

// if (!isDev) {
if (validateUser) {
  // deployment
  router.get('/', authMiddleware, serviceController.getAllServices)
  router.get('/:id', authMiddleware, serviceController.getServiceById)
  router.post(
    '/',
    authMiddleware,
    [check('service_items').custom((items) => validateServiceItems(items))],
    serviceController.addService,
  )
  router.put(
    '/:id',
    authMiddleware,
    [check('service_items').custom((items) => validateServiceItems(items))],
    serviceController.changeServiceById,
  )
  router.delete('/:id', authMiddleware, serviceController.deleteServiceById)
  router.get(
    '/vendor/:id',
    authMiddleware,
    serviceController.getServiceByVenderId,
  )
  router.patch(
    '/uploadImage/:id',
    authMiddleware,
    serviceController.uploadImage,
  )
} else {
  // development
  router.get('/', serviceController.getAllServicesIsDev)
  router.get('/:id', serviceController.getServiceByIdIsDev)
  router.post('/', serviceController.addServiceIsDev)
  router.put('/:id', serviceController.changeServiceByIdIsDev)
  router.delete('/:id', serviceController.deleteServiceByIdIsDev)
  router.get('/vendor/:id', serviceController.getServiceByVenderId)
  router.patch('/uploadImage/:id', serviceController.uploadImage)
}

export default router
