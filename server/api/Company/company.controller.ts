import { Request, Response } from 'express'
import Company, { ICompany } from './company.model'
import { validationResult } from 'express-validator'
import fs from 'fs'

import { getToken } from 'next-auth/jwt'
import path from 'path'
const secret = process.env.SECRET

const companyController = {
  // default
  async addCompany(req: Request, res: Response) {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(400).json({ msg: errors.array()[0].msg })
    }

    const { user } = req
    // @ts-ignore
    const isSuper = user?.level < 2 ? true : false

    const {
      company_name,
      business_outline,
      status,
      date_of_agreement_signed,
      first_payment_amount,
      sales_lead,
      deposit,
      total_amount,
      contact_person,
      contact_email,
      client_contact_number,
      number_of_staff,
      company_start_year,
      company_outline,
      company_address,
      tvp_account_name,
      tvp_password,
      remark,
      approved_case,
      sort_order,
      follow_up,
      project_name,
      BR_expiry_date,
    }: ICompany = req.body

    try {
      const company = await Company.findOne({ company_name })

      if (company) {
        return res.status(409).json({ msg: 'company already exists' })
      } else {
        const postCompany: ICompany = new Company({
          company_name,
          business_outline,
          status,
          date_of_agreement_signed,
          first_payment_amount,
          sales_lead,
          deposit,
          total_amount,
          contact_person,
          contact_email,
          client_contact_number,
          number_of_staff,
          company_start_year,
          company_outline,
          company_address,
          tvp_account_name,
          tvp_password,
          remark,
          approved_case,
          admin_added: isSuper ? Date() : null,
          sort_order,
          follow_up,
          project_name,
          service_paid: '0123',
          eshop_paid: '0123',
          BR_expiry_date,
        })
        await postCompany?.save?.()
        return res.status(200).json({
          msg: 'sus',
          company: postCompany,
        })
      }
    } catch (err) {
      // console.error(err.message)
      return res.status(500).json({ msg: err })
    }
  },

  // deployment
  async getAllCompanys(req: Request, res: Response) {
    const { user } = req

    // @ts-ignore
    if (user.level > 1) {
      try {
        const companys = await Company.find({ service_paid: '0123' }).select([
          'company_name',
          'business_outline',
          'company_outline',
          'contact_person',
          'contact_email',
          'client_contact_number',
          'image_data',
        ])

        return res.status(200).json({
          msg: 'sus',
          count: companys.length,
          companys: companys,
        })
      } catch (err) {
        return res.status(500).json({
          msg: err,
        })
      }
    } else {
      try {
        let companys: ICompany[] = []
        if (req.query.latest) {
          const num = parseInt(req.query.latest.toString())
          if (num > 0) {
            companys = await Company.find()
              .sort({ createdAt: -1 })
              .limit(num)
              .select('company_name')
          } else {
            return res.status(400).json({
              msg: `invalid query number for latest: ${req.query.latest}`,
            })
          }
        } else if (
          req.query.newlyCreated?.toString().toLowerCase() === 'true' &&
          req.query.days_ago
        ) {
          const currentDate = new Date()
          const days_ago = new Date(
            currentDate.getTime() -
              parseInt(req.query.days_ago.toString()) * 86400000,
          )

          companys = await Company.find({
            createdAt: {
              $gte: days_ago,
              $lte: currentDate,
            },
          })
            .sort({ createdAt: -1 })
            .select('company_name')
        } else if (req.query.br_expiry?.toString().toLowerCase() === 'true') {
          const currentDate = new Date()
          const day31beforeExpiry = new Date(
            currentDate.getTime() + 31 * 86400000,
          )
          companys = await Company.find({
            BR_expiry_date: {
              $gte: currentDate,
              $lte: day31beforeExpiry,
            },
          })
        } else {
          companys = await Company.find()
        }

        return res.status(200).json({
          msg: 'sus',
          count: companys.length,
          companys: companys,
        })
      } catch (err) {
        return res.status(500).json({
          msg: err,
        })
      }
    }
  },
  getPdfTemplateBinary(req: Request, res: Response) {
    const { user } = req
    //@ts-ignore
    if (user?.level != 0 && user?.level != 1) {
      return res.status(403).json({
        msg: 'you are not super admin nor super staff',
      })
    } else {
      try {
        fs.readFile('./server/assets/templates/template.pdf', (err, data) => {
          return res.status(200).send(data.toString('base64'))
        })
      } catch (err) {
        return res.status(500).json({
          msg: err,
        })
      }
    }
  },
  async getCompanyById(req: Request, res: Response) {
    const { user } = req
    //@ts-ignore
    if (user?.level != 0 && user?.level != 1) {
      return res.status(403).json({
        msg: 'you are not super admin nor super staff',
      })
    } else {
      try {
        const company = await Company.findById(req.params.id)
        if (company) {
          return res.status(200).json({
            msg: 'sus',
            company: company,
          })
        } else {
          return res.status(404).json({
            msg: 'company not found',
          })
        }
      } catch (err) {
        return res.status(500).json({
          msg: err,
        })
      }
    }
  },
  async changeCompanyById(req: Request, res: Response) {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(400).json({ msg: errors.array()[0].msg })
    }

    const { user } = req
    //@ts-ignore
    if (user?.level > 3) {
      return res.status(403).json({
        msg: 'you are not super admin nor super staff nor admin nor staff',
      })
    } else {
      try {
        // @ts-ignore
        const isSuper = user?.level < 2 ? true : false
        const company = await Company.findById(req.params.id)
        if (company) {
          const {
            company_name,
            business_outline,
            status,
            date_of_agreement_signed,
            first_payment_amount,
            sales_lead,
            deposit,
            total_amount,
            contact_person,
            contact_email,
            client_contact_number,
            number_of_staff,
            company_start_year,
            company_outline,
            company_address,
            tvp_account_name,
            tvp_password,
            remark,
            approved_case,
            sort_order,
            follow_up,
            project_name,
            BR_expiry_date,
            image_data,
            chop_image_data,
            signature_image_data,
          } = req.body

          const other_company_company_name = await Company.find({
            company_name: company_name,
          })

          if (other_company_company_name.length > 0) {
            if (company.company_name != company_name) {
              return res.status(409).json({
                msg: 'conflict-company_name',
              })
            }
          }

          await Company.findByIdAndUpdate(req.params.id, {
            company_name,
            business_outline,
            status,
            date_of_agreement_signed,
            first_payment_amount,
            sales_lead,
            deposit,
            total_amount,
            contact_person,
            contact_email,
            client_contact_number,
            number_of_staff,
            company_start_year,
            company_outline,
            company_address,
            tvp_account_name,
            tvp_password,
            remark,
            approved_case,
            admin_modified: company?.admin_modified
              ? isSuper
                ? new Date()
                : company.admin_modified
              : null,
            sort_order,
            follow_up,
            project_name,
            BR_expiry_date,
            image_data,
            chop_image_data,
            signature_image_data,
          })
            .exec()
            .then((result) => {
              return res.status(200).json({
                msg: 'sus',
                company_original: result,
              })
            })
            .catch((err) => {
              return res.status(500).json({
                msg: err,
              })
            })
        } else {
          return res.status(404).json({
            msg: 'company not found',
          })
        }
      } catch (err) {
        return res.status(500).json({
          msg: err,
        })
      }
    }
  },
  async deleteCompanyById(req: Request, res: Response) {
    const { user } = req
    //@ts-ignore
    if (user?.level != 0 && user?.level != 1) {
      return res.status(403).json({
        msg: 'you are not super admin nor super staff',
      })
    } else {
      try {
        const company = await Company.findById(req.params.id)
        if (company) {
          await Company.findByIdAndDelete(req.params.id)
            .exec()
            .then((result) => {
              return res.status(200).json({
                msg: 'sus',
                company: result,
              })
            })
            .catch((err) => {
              return res.status(500).json({
                msg: err,
              })
            })
        } else {
          return res.status(404).json({
            msg: 'company not found',
          })
        }
      } catch (err) {
        return res.status(500).json({
          msg: err,
        })
      }
    }
  },
  async getOwnCompany(req: Request, res: Response) {
    const { user } = req
    //@ts-ignore
    if (user?.level > 3) {
      return res.status(403).json({
        msg: 'you are normie',
      })
    } else {
      await Company.findById(req.params.id)
        .exec()
        .then((result) => {
          return res.status(200).json({
            msg: 'sus',
            company: result,
          })
        })
        .catch((err) => {
          return res.status(500).json({
            msg: err,
          })
        })
    }
  },
  async uploadImage(req: Request, res: Response) {
    try {
      const company = await Company.findById(req.params.id)
      if (company) {
        await Company.findByIdAndUpdate(
          req.params.id,
          {
            $set: { image_data: req.body.image_data },
          },
          { new: true },
        )
          .exec()
          .then(async () => {
            return res.status(200).json({
              msg: 'sus uploaded image',
            })
          })
          .catch((err) => {
            return res.status(500).json({
              msg: err,
            })
          })
      } else {
        return res.status(404).json({
          msg: 'company not found',
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },

  async getAllCompanysIsDev(req: Request, res: Response) {
    await Company.find()
      .exec()
      .then((result) => {
        return res.status(200).json({
          msg: 'sus',
          count: result.length,
          companys: result,
        })
      })
      .catch((err) => {
        return res.status(500).json({
          msg: err,
        })
      })
  },
  getPdfTemplateBinaryIsDev(req: Request, res: Response) {
    try {
      fs.readFile('./server/assets/templates/template.pdf', (err, data) => {
        return res.status(200).send(data.toString('base64'))
      })
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async getCompanyByIdIsDev(req: Request, res: Response) {
    try {
      const company = await Company.findById(req.params.id)
      if (company) {
        return res.status(200).json({
          msg: 'sus',
          company: company,
        })
      } else {
        return res.status(404).json({
          msg: 'company not found',
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async changeCompanyByIdIsDev(req: Request, res: Response) {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(400).json({ msg: errors.array()[0].msg })
    }

    try {
      const company = await Company.findById(req.params.id)
      if (company) {
        const {
          company_name,
          business_outline,
          status,
          date_of_agreement_signed,
          first_payment_amount,
          sales_lead,
          deposit,
          total_amount,
          contact_person,
          contact_email,
          client_contact_number,
          number_of_staff,
          company_start_year,
          company_outline,
          company_address,
          tvp_account_name,
          tvp_password,
          remark,
          approved_case,
          sort_order,
          follow_up,
          project_name,
          BR_expiry_date,
        } = req.body
        await Company.findByIdAndUpdate(req.params.id, {
          company_name,
          business_outline,
          status,
          date_of_agreement_signed,
          first_payment_amount,
          sales_lead,
          deposit,
          total_amount,
          contact_person,
          contact_email,
          client_contact_number,
          number_of_staff,
          company_start_year,
          company_outline,
          company_address,
          tvp_account_name,
          tvp_password,
          remark,
          approved_case,
          admin_modified: new Date(),
          sort_order,
          follow_up,
          project_name,
          BR_expiry_date,
        })
          .exec()
          .then((result) => {
            return res.status(200).json({
              msg: 'sus',
              company_original: result,
            })
          })
          .catch((err) => {
            return res.status(500).json({
              msg: err,
            })
          })
      } else {
        return res.status(404).json({
          msg: 'company not found',
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async deleteCompanyByIdIsDev(req: Request, res: Response) {
    try {
      const company = await Company.findById(req.params.id)
      if (company) {
        await Company.findByIdAndDelete(req.params.id)
          .exec()
          .then((result) => {
            return res.status(200).json({
              msg: 'sus',
              company: result,
            })
          })
          .catch((err) => {
            return res.status(500).json({
              msg: err,
            })
          })
      } else {
        return res.status(404).json({
          msg: 'company not found',
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async getOwnCompanyIsDev(req: Request, res: Response) {
    await Company.findById(req.params.id)
      .exec()
      .then((result) => {
        return res.status(200).json({
          msg: 'sus',
          company: result,
        })
      })
      .catch((err) => {
        return res.status(500).json({
          msg: err,
        })
      })
  },
}

export default companyController
