import { Router } from 'express'
import companyController from './company.controller'
import { check } from 'express-validator'

import { isDev, validateUser } from '../../config'
import { authMiddleware } from '../../middlewares/auth.middleware'

const router = Router()

// if (!isDev) {
if (validateUser) {
  // deveployment
  router.get('/', authMiddleware, companyController.getAllCompanys)
  router.delete('/:id', authMiddleware, companyController.deleteCompanyById)
  router.put(
    '/:id',
    authMiddleware,
    [check('contact_email', 'Please enter an email').isEmail()],
    companyController.changeCompanyById,
  )
  router.get(
    '/pdf-template',
    authMiddleware,
    companyController.getPdfTemplateBinary,
  )
  // router.get(
  //   '/br_expiry',
  //   authMiddleware,
  //   companyController.getAllCompanysOneMonthBRExpiry,
  // )
  router.get('/:id', authMiddleware, companyController.getCompanyById)
  router.get('/own/:id', authMiddleware, companyController.getOwnCompany)
  router.patch(
    '/uploadImage/:id',
    authMiddleware,
    companyController.uploadImage,
  )
} else {
  // development
  router.get('/', companyController.getAllCompanysIsDev)
  router.delete('/:id', companyController.deleteCompanyByIdIsDev)
  router.put(
    '/:id',
    [check('contact_email', 'Please enter an email').isEmail()],
    companyController.changeCompanyByIdIsDev,
  )
  router.get('/pdf-template', companyController.getPdfTemplateBinaryIsDev)
  router.get('/:id', companyController.getCompanyByIdIsDev)
  router.get('/own/:id', companyController.getOwnCompanyIsDev)
  router.patch('/uploadImage/:id', companyController.uploadImage)
}

router.post(
  '/',
  [check('contact_email', 'Please enter an email').isEmail()],
  companyController.addCompany,
)

export default router
