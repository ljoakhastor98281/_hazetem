import mongoose from 'mongoose'

export interface ICompany extends Partial<mongoose.Document> {
  _id: mongoose.Types.ObjectId
  company_name: string
  business_outline: string
  status: string
  service_paid: string
  eshop_paid: string
  date_of_agreement_signed: Date
  first_payment_amount: string
  sales_lead: string
  deposit: string
  total_amount: string
  contact_person: string
  contact_email: string
  client_contact_number: string
  number_of_staff: string
  company_start_year: string
  company_outline: string
  company_address: string
  tvp_account_name: string
  tvp_password: string
  remark: string
  approved_case: string
  admin_added: Date | null
  admin_modified: Date | null
  sort_order: string
  follow_up: string
  project_name: string
  createdAt: Date
  updatedAt: Date
  BR_expiry_date: Date
  image_data: string
  chop_image_data: string
  signature_image_data: string
}

const CompanySchema = new mongoose.Schema(
  {
    company_name: {
      type: String,
      required: true,
      index: { unique: true },
      trim: true,
    },
    business_outline: {
      type: String,
      required: false,
      trim: true,
    },
    status: {
      type: String,
      required: false,
      trim: true,
    },
    service_paid: {
      type: String,
      required: false,
      trim: true,
    },
    eshop_paid: {
      type: String,
      required: false,
      trim: true,
    },
    date_of_agreement_signed: {
      type: Date,
      required: false,
      trim: true,
    },
    first_payment_amount: {
      type: String,
      required: false,
      trim: true,
    },
    sales_lead: {
      type: String,
      required: false,
      trim: true,
    },
    deposit: {
      type: String,
      required: false,
      trim: true,
    },
    total_amount: {
      type: String,
      required: false,
      trim: true,
    },
    contact_person: {
      type: String,
      required: true,
      trim: true,
    },
    contact_email: {
      type: String,
      required: true,
      trim: true,
    },
    client_contact_number: {
      type: String,
      required: true,
      trim: true,
    },
    number_of_staff: {
      type: String,
      required: false,
      trim: true,
    },
    company_start_year: {
      type: String,
      required: false,
      trim: true,
    },
    company_outline: {
      type: String,
      required: false,
      trim: true,
    },
    company_address: {
      type: String,
      required: true,
      trim: true,
    },
    tvp_account_name: {
      type: String,
      required: false,
      trim: true,
    },
    tvp_password: {
      type: String,
      required: false,
      trim: true,
    },
    remark: {
      type: String,
      required: false,
      trim: true,
    },
    approved_case: {
      type: String,
      required: false,
      trim: true,
    },
    admin_added: {
      type: Date,
      required: false,
      trim: true,
    },
    admin_modified: {
      type: Date,
      required: false,
      trim: true,
    },
    sort_order: {
      type: String,
      required: false,
      trim: true,
    },
    follow_up: {
      type: String,
      required: false,
      trim: true,
    },
    project_name: {
      type: String,
      required: false,
      trim: true,
    },
    BR_expiry_date: {
      type: Date,
      required: false,
      trim: true,
    },
    image: {
      type: String,
      required: false,
      trim: true,
    },
    chop: {
      type: String,
      required: false,
      trim: true,
    },
    signature: {
      type: String,
      required: false,
      trimu: true,
    },
  },
  {
    timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' },
    optimisticConcurrency: true,
    strict: false,
  },
)

const Company: mongoose.Model<ICompany> =
  mongoose.models.companys ||
  mongoose.model<ICompany>('companys', CompanySchema)
export default Company
