import { Request, Response } from 'express'
import { validationResult } from 'express-validator'
import User from '../User/user.model'
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import { jwtConfig } from '../../config'

const authController = {
  async test(req: Request, res: Response) {
    return res.status(200).json({
      msg: 'auth api',
    })
  },
  async authenticate(req: Request, res: Response) {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(400).json({ error: errors.array()[0].msg })
    }

    const { username, password } = req.body

    try {
      const user = await User.findOne({ username })

      if (!user) {
        return res.status(404).json({
          err: `No user named ${username} :(`,
        })
      } else {
        const isMatch = await bcrypt.compare(password, user.password!)

        if (!isMatch) {
          return res.status(401).json({
            err: 'Incorrect password :(',
          })
        } else {
          const payload = {
            user: {
              _id: user._id,
            },
          }

          jwt.sign(JSON.stringify(payload), jwtConfig.secret, (err, token) => {
            if (err) throw err
            return res.status(200).json({
              msg: 'sus',
              token: token,
            })
          })
        }
      }
    } catch (err) {
      // console.error(err.message)
      return res.status(500).json({
        err: 'Server Error',
      })
    }
  },

  // postman
  async getSuperAdmin(req: Request, res: Response) {
    return res
      .cookie(
        'next-auth.callback-url',
        'http%3A%2F%2Flocalhost%3A3000%2Fsignin',
      )
      .cookie(
        'next-auth.csrf-token',
        '035eeff6535ac1c6decb6969f55348c95a5538c67980cc454ec7295fb73ae64d%7Ce89a0b49146ef3057cba4c16064f19d9a2bad32f53ba3f8e367b4c9936a42786',
      )
      .cookie(
        'next-auth.session-token',
        'eyJhbGciOiJIUzUxMiJ9.eyJlbWFpbCI6ImFkbWluY3ZAdGVzdC5jb20iLCJ1c2VybmFtZSI6InN1cGVyYWRtaW5fZ3V5IiwibGV2ZWwiOjAsImRhdGEiOnsiX2lkIjoiNjBjOTY5YTI4ZDhlMGMwMGVhZmQ1ZTliIiwiZW1haWwiOiJhZG1pbmN2QHRlc3QuY29tIiwidXNlcm5hbWUiOiJzdXBlcmFkbWluX2d1eSIsInBhc3N3b3JkIjoiJDJiJDEwJDh4S0hnSVRHcVpIc21ac3Rza0hVbGU2a05PNGtqdnZJZlNuQXZLeHFWL1J1a2VNNWZKcU1pIiwibGV2ZWwiOjAsImNvbXBhbnlfaWQiOm51bGwsImNyZWF0ZWRBdCI6IjIwMjEtMDYtMTZUMDM6MDE6NTQuNzgxWiIsInVwZGF0ZWRBdCI6IjIwMjEtMDctMTNUMDY6NTI6NTkuNjE5WiIsIl9fdiI6MH0sImlhdCI6MTYyNjkyMjQyOCwiZXhwIjoxNjI5NTE0NDI4fQ.qrPJcRsxH4094SB0ok4HI2Nod--zdRwDRR0ELazCqQFyF15ul0AQ8XYQFtL9OEZ2rXpggXLaVmr-IbrPASenpA',
      )
      .status(200)
      .json({
        msg: 'token has been added',
      })
  },
  async getSuperStaff(req: Request, res: Response) {
    return res
      .cookie(
        'next-auth.callback-url',
        'http%3A%2F%2Flocalhost%3A3000%2Fsignin',
      )
      .cookie(
        'next-auth.csrf-token',
        '36bcde8be28bb23077cc4cc2650e9aed635a6f9c8b21cba44c24ce10f7b87a53%7Cc7280c49a297849ed0fd33d3711e5f13e3e609914df36574b92f19cc63be7300',
      )
      .cookie(
        'next-auth.session-token',
        'eyJhbGciOiJIUzUxMiJ9.eyJlbWFpbCI6InN1cGVyc3RhZmZfZ3V5QHN1cGVyLmNvbSIsInVzZXJuYW1lIjoic3VwZXJzdGFmZl9ndXkiLCJsZXZlbCI6MSwiZGF0YSI6eyJfaWQiOiI2MGQxNTVkYTBlOTFmZDA1YWQwZTJlZmYiLCJlbWFpbCI6InN1cGVyc3RhZmZfZ3V5QHN1cGVyLmNvbSIsInVzZXJuYW1lIjoic3VwZXJzdGFmZl9ndXkiLCJsZXZlbCI6MSwiY29tcGFueV9pZCI6bnVsbCwiY3JlYXRlZEF0IjoiMjAyMS0wNi0yMlQwMzoxNTozOC42OTFaIiwidXBkYXRlZEF0IjoiMjAyMS0wNi0yMlQwMzoxNTozOC42OTFaIiwiX192IjowfSwiaWF0IjoxNjI0MzMxNzkyLCJleHAiOjE2MjY5MjM3OTJ9.61z9pkg6OneV6dKbKEEjS_oH_hmzdvDZ2ktsPApe21x3GlQ7NOrvLNH-9EDoeyF9VvoAXko_IvYDgLUJVw38TA',
      )
      .status(200)
      .json({
        msg: 'token has been added',
      })
  },
  async getAdmin(req: Request, res: Response) {
    return res
      .cookie(
        'next-auth.callback-url',
        'http%3A%2F%2Flocalhost%3A3000%2Fsignin',
      )
      .cookie(
        'next-auth.csrf-token',
        '7740d15d41f7c1c213d2b73afc1ce2898a3cbbb2f1556d77f60a0e4a6fd89b50%7C5afd254578b7ded83dac249fa355846289a8e4c61dbd95aac04493b8fd970aa4',
      )
      .cookie(
        'next-auth.session-token',
        'eyJhbGciOiJIUzUxMiJ9.eyJlbWFpbCI6InRlc3R0ZXN0MkB0ZXN0LmNvbSIsInVzZXJuYW1lIjoidGVzdHRlc3QyIiwibGV2ZWwiOjIsImRhdGEiOnsiX2lkIjoiNjBkMTlhMTBlM2Y0Y2QxNGRiODc0NjlmIiwiZW1haWwiOiJ0ZXN0dGVzdDJAdGVzdC5jb20iLCJ1c2VybmFtZSI6InRlc3R0ZXN0MiIsImxldmVsIjoyLCJjb21wYW55X2lkIjoiNjBkMTlhMTBlM2Y0Y2QxNGRiODc0NjliIiwiY3JlYXRlZEF0IjoiMjAyMS0wNi0yMlQwODowNjo0MC4zMDJaIiwidXBkYXRlZEF0IjoiMjAyMS0wNi0yMlQwODowNjo0MC4zMDJaIiwiX192IjowfSwiaWF0IjoxNjI0MzQ5MjMzLCJleHAiOjE2MjY5NDEyMzN9.fd6zoStgjgylTqEPerN-pP5eBELhz2BrN76tNPGzcAY81Ny605hDUfgNPzDl4TKwL8QXTLwLRDhfYu4uRE_DSw',
      )
      .status(200)
      .json({
        msg: 'token has been added',
      })
  },
  async getStaff(req: Request, res: Response) {
    return res
      .cookie(
        'next-auth.callback-url',
        'http%3A%2F%2Flocalhost%3A3000%2Fsignin',
      )
      .cookie(
        'next-auth.csrf-token',
        'b5d2d7e2e6a11bb8680897b8161e27163d1fc8a931a94c602433876c6ce57cd3%7C67fe5d6959e544b198a8d3e62446205497eda2cba8ca08000bbcee488ed977b0',
      )
      .cookie(
        'next-auth.session-token',
        'eyJhbGciOiJIUzUxMiJ9.eyJlbWFpbCI6InRlc3R0ZXN0MUBlbWFpbC5jb20iLCJ1c2VybmFtZSI6InRlc3R0ZXN0MSIsImxldmVsIjozLCJkYXRhIjp7Il9pZCI6IjYwYzlhMGQ5YTY4ZjA1MDRkMDZmMGEzZCIsImVtYWlsIjoidGVzdHRlc3QxQGVtYWlsLmNvbSIsInVzZXJuYW1lIjoidGVzdHRlc3QxIiwibGV2ZWwiOjMsImNvbXBhbnlfaWQiOm51bGwsImNyZWF0ZWRBdCI6IjIwMjEtMDYtMTZUMDY6NTc6MjkuMTgwWiIsInVwZGF0ZWRBdCI6IjIwMjEtMDYtMTZUMDY6NTc6MjkuMTgwWiIsIl9fdiI6MH0sImlhdCI6MTYyNDMzMTk1OCwiZXhwIjoxNjI2OTIzOTU4fQ.aRQTN4WSJvoTaWw_UWj4cNyFJU-UvWMdGn0CHGJW1CcueYbjGXLrerR8y8TGY4dncokH8NP2d5fYSgUp_bCg5A',
      )
      .status(200)
      .json({
        msg: 'token has been added',
      })
  },
  async getNormal(req: Request, res: Response) {
    return res
      .cookie(
        'next-auth.callback-url',
        'http%3A%2F%2Flocalhost%3A3000%2Fsignin',
      )
      .cookie(
        'next-auth.csrf-token',
        '758d656978efcd9fc4ecfdf2f3ea1134de811c506a96243de675ac2cdc8c6750%7C292b385cede28a07e751e7f6b9b815c1517237c7fb503fbca6876d6319fbefc7',
      )
      .cookie(
        'next-auth.session-token',
        'eyJhbGciOiJIUzUxMiJ9.eyJlbWFpbCI6Im5vcm1hbF9ndXlAbm9ybWFsLmNvbSIsInVzZXJuYW1lIjoibm9ybWFsX2d1eSIsImxldmVsIjo0LCJkYXRhIjp7Il9pZCI6IjYwZDA2MDI5MmM1NWY0MTUzYTk0OTY1NiIsImVtYWlsIjoibm9ybWFsX2d1eUBub3JtYWwuY29tIiwidXNlcm5hbWUiOiJub3JtYWxfZ3V5IiwibGV2ZWwiOjQsImNvbXBhbnlfaWQiOm51bGwsImNyZWF0ZWRBdCI6IjIwMjEtMDYtMjFUMDk6NDc6MjEuOTk1WiIsInVwZGF0ZWRBdCI6IjIwMjEtMDYtMjFUMDk6NDc6MjEuOTk1WiIsIl9fdiI6MH0sImlhdCI6MTYyNDMzMjAxMCwiZXhwIjoxNjI2OTI0MDEwfQ.iMpDaoki-ElZnO6qT0jWhnCL-Bu9_CP9A6kOZdQcW0zoT3WIahSxMPN7jCh6mbmajzPfcOAWz2hVys47YlKvnQ',
      )
      .status(200)
      .json({
        msg: 'token has been added',
      })
  },
  async signout(req: Request, res: Response) {
    return res
      .clearCookie('next-auth.callback-url')
      .clearCookie('next-auth.csrf-token')
      .clearCookie('next-auth.session-token')
      .status(200)
      .json({ msg: 'cleared tokens' })
  },
}

export default authController
