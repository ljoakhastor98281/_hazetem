import { Router } from 'express'
import authController from './auth.controller'
import { check } from 'express-validator'

const router = Router()

router.get('/test', authController.test)
router.post(
  '/',
  [
    check('username', 'username length: 8 ~ 30').isLength({ min: 8, max: 30 }),
    check('username', 'please include username').exists(),
    check('password', 'password length: 8 ~ 30').isLength({ min: 8, max: 30 }),
    check('password', 'please include password').exists(),
  ],
  authController.authenticate,
)

// for postman tokens
router.get('/super_admin', authController.getSuperAdmin)
router.get('/super_staff', authController.getSuperStaff)
router.get('/admin', authController.getAdmin)
router.get('/staff', authController.getStaff)
router.get('/normal', authController.getNormal)
router.get('/out', authController.signout)

export default router
