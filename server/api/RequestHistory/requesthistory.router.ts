import { Router } from 'express'
import requestHistoryController from './requesthistory.controller'

import { isDev, validateUser } from '../../config'
import { authMiddleware } from '../../middlewares/auth.middleware'
import { check } from 'express-validator'
import { validateServiceItems } from '../../middlewares/service_items.middleware'

const router = Router()

// if (!isDev) {
if (validateUser) {
  // deployment
  router.get(
    '/',
    authMiddleware,
    requestHistoryController.getAllRequestHistories,
  )
  router.get(
    '/most_requested/:requested',
    authMiddleware,
    requestHistoryController.getMostRequested,
  )
  router.get(
    '/vendor/popular_service/:id',
    authMiddleware,
    requestHistoryController.getPopularServiceByVendorId,
  )
  router.post(
    '/',
    authMiddleware,
    [check('requested_items').custom((items) => validateServiceItems(items))],
    requestHistoryController.addRequestHistory,
  )
  router.get(
    '/archive/:id',
    authMiddleware,
    requestHistoryController.archiveRequestHistory,
  )
  router.get(
    '/unarchive/:id',
    authMiddleware,
    requestHistoryController.unarchiveRequestHistory,
  )
  router.get(
    '/cancel/:id',
    authMiddleware,
    requestHistoryController.cancelRequestHistory,
  )
  router.get(
    '/decline/:id',
    authMiddleware,
    requestHistoryController.declineRequestHistory,
  )
  router.get(
    '/approve/:id',
    authMiddleware,
    requestHistoryController.approveRequestHistory,
  )
  router.get(
    '/approval_company_id/:id/:company_id',
    requestHistoryController.approveRequestHistoryWithId,
  )
  router.get(
    '/expire/:id',
    authMiddleware,
    requestHistoryController.expireRequestHistory,
  )
  router.get(
    '/:id',
    authMiddleware,
    requestHistoryController.getRequestHistoryById,
  )
  router.put(
    '/:id',
    authMiddleware,
    requestHistoryController.changeRequestHistoryById,
  )
  router.delete(
    '/:id',
    authMiddleware,
    requestHistoryController.deleteRequestHistoryById,
  )
} else {
  // development
  router.get('/', requestHistoryController.getAllRequestHistoriesIsDev)
  router.get('/:id', requestHistoryController.getRequestHistoryByIdIsDev)
  router.post('/', requestHistoryController.addRequestHistory)
  router.put('/:id', requestHistoryController.changeRequestHistoryById)
  router.delete('/:id', requestHistoryController.deleteRequestHistoryById)
}

export default router
