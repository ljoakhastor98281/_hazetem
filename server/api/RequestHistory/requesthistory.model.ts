import mongoose from 'mongoose'
import { IService, IServiceItem } from '../Service/service.model'
import { IUser } from '../User/user.model'
import { ICompany } from '../Company/company.model'

export enum IArchive {
  notArchived,
  userArchived,
  vendorArchived,
  allArchived,
}

export enum IRequestStatus {
  ongoing,
  cancelled,
  declined,
  approved,
  expired,
}

export interface IRequestStat {
  _id?: mongoose.Types.ObjectId
  name: string
  count: number
}

export interface IRequestHistory extends Partial<mongoose.Document> {
  _id: mongoose.Types.ObjectId
  requester: mongoose.Types.ObjectId | IUser
  service: mongoose.Types.ObjectId | IService
  requested_items: IServiceItem[]
  company: mongoose.Types.ObjectId | ICompany
  request_date: Date
  status: IRequestStatus
  archive_status: IArchive
  createdAt: Date
  updatedAt: Date
}

const RequestHistorySchema = new mongoose.Schema(
  {
    requester: {
      type: mongoose.Types.ObjectId,
      required: true,
      trim: true,
      ref: 'users',
    },
    service: {
      type: mongoose.Types.ObjectId,
      required: true,
      trim: true,
      ref: 'services',
    },
    requested_items: {
      type: Array,
      required: true,
      trim: true,
    },
    company: {
      type: mongoose.Types.ObjectId,
      required: true,
      trim: true,
      ref: 'companys',
    },
    request_date: {
      type: Date,
      required: true,
      trim: true,
    },
    status: {
      type: IRequestStatus,
      required: true,
      trim: true,
    },
    archive_status: {
      type: IArchive,
      required: true,
      trim: true,
    },
  },
  {
    timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' },
    optimisticConcurrency: true,
    strict: false,
  },
)

RequestHistorySchema.post('save', function (this: IRequestHistory) {
  setTimeout(async () => {
    if (this.status == 0) {
      await mongoose.models.requesthisotries.findByIdAndUpdate(this._id, {
        status: 4,
      })
    }
  }, 86400000)
})

const RequestHistory: mongoose.Model<IRequestHistory> =
  mongoose.models.requesthisotries ||
  mongoose.model<IRequestHistory>('requesthisotries', RequestHistorySchema)
export default RequestHistory
