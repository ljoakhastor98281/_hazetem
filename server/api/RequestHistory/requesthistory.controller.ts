import RequestHistory, {
  IRequestHistory,
  IRequestStat,
} from './requesthistory.model'
import { Request, Response } from 'express'
import User, { IUser } from '../User/user.model'
import Service, { IService } from '../Service/service.model'
import Company, { ICompany } from '../Company/company.model'
import { validationResult } from 'express-validator'
import mongoose from 'mongoose'
import { send_request_remind_mail_to_vendor } from '../nodemailer/nodemailer.controller'
import path from 'path'
import { request_approval } from '../../utils/approval_templates'

const getMostRequestedRanking = async (
  requested: string,
  numberWanted: number,
  include_id: boolean,
  user: IUser,
) => {
  let populateQuery
  switch (requested) {
    case 'company': {
      const companys: IRequestStat[] = await RequestHistory.aggregate([
        { $project: { company: 1 } },
        {
          $lookup: {
            from: 'companys',
            let: { company: '$company' },
            pipeline: [
              { $match: { $expr: { $eq: ['$_id', '$$company'] } } },
              { $project: { _id: 1, company_name: 1 } },
            ],
            as: 'company',
          },
        },
        { $match: { company: { $not: { $size: 0 } } } },
        { $replaceRoot: { newRoot: { $arrayElemAt: ['$company', 0] } } },
        {
          $group: {
            _id: '$_id',
            count: { $sum: 1 },
            name: { $first: '$company_name' },
          },
        },

        { $sort: { count: -1, name: 1 } },
        { $project: { _id: include_id, name: 1 } },
      ])
      const leadingCompanys = companys.slice(0, numberWanted)
      populateQuery = {
        path: 'company',
        select: [
          '_id',
          'company_name',
          'contact_person',
          'contact_email',
          'client_contact_number',
        ],
      }
      return {
        requested: requested,
        rank: leadingCompanys,
        populateQuery: populateQuery,
      }
    }
    case 'service': {
      console.log(user.company_id)
      const services: IRequestStat[] = await RequestHistory.aggregate([
        { $project: { service: 1, company: 1 } },
        {
          $match:
            user.level == 2 || user.level == 3
              ? {
                  company: mongoose.Types.ObjectId(user.company_id?.toString()),
                }
              : {},
        },
        {
          $lookup: {
            from: 'services',
            let: { service: '$service' },
            pipeline: [
              { $match: { $expr: { $eq: ['$_id', '$$service'] } } },
              { $project: { _id: 1, service_name: 1 } },
            ],
            as: 'service',
          },
        },
        { $match: { service: { $not: { $size: 0 } } } },
        { $replaceRoot: { newRoot: { $arrayElemAt: ['$service', 0] } } },
        {
          $group: {
            _id: '$_id',
            count: { $sum: 1 },
            name: { $first: '$service_name' },
          },
        },

        { $sort: { count: -1, name: 1 } },
        { $project: { _id: include_id, name: 1 } },
      ])
      const leadingServices = services.slice(0, numberWanted)
      populateQuery = {
        path: 'service',
        select: ['_id', 'service_name', 'description', 'updatedAt'],
      }
      return {
        requested: requested,
        rank: leadingServices,
        populateQuery: populateQuery,
      }
    }
    case 'requester': {
      const requesters: IRequestStat[] = await RequestHistory.aggregate([
        { $project: { requester: 1, company: 1 } },
        {
          $match:
            user.level == 2 || user.level == 3
              ? {
                  company: mongoose.Types.ObjectId(user.company_id?.toString()),
                }
              : {},
        },
        {
          $lookup: {
            from: 'users',
            let: { requester: '$requester' },
            pipeline: [
              { $match: { $expr: { $eq: ['$_id', '$$requester'] } } },
              { $project: { _id: 1, username: 1 } },
            ],
            as: 'requester',
          },
        },
        { $match: { requester: { $not: { $size: 0 } } } },
        { $replaceRoot: { newRoot: { $arrayElemAt: ['$requester', 0] } } },
        {
          $group: {
            _id: '$_id',
            count: { $sum: 1 },
            name: { $first: '$username' },
          },
        },

        { $sort: { count: -1, name: 1 } },
        { $project: { _id: include_id, name: 1 } },
      ])
      const leadingRequesters = requesters.slice(0, numberWanted)
      populateQuery = {
        path: 'requester',
        select: [
          '_id',
          'username',
          'email',
          'level',
          'normal_user_company_name',
          'normal_user_company_address',
        ],
      }
      return {
        requested: requested,
        rank: leadingRequesters,
        populateQuery: populateQuery,
      }
    }
    default: {
      return { requested: undefined, rank: undefined, populateQuery: undefined }
    }
  }
}

const requestHistoryController = {
  // deployment
  async getAllRequestHistories(req: Request, res: Response) {
    try {
      const { user } = req
      let result: IRequestHistory[] = []
      const mostRequestedRank: IRequestStat[] = []
      let searchQuery = {}
      let populateQuery
      //when searching for most requsted, not limited by user level
      if (req.query.mostRequested && (user as IUser).level <= 1) {
        let numberWanted = 1
        if (req.query.topMostWanted) {
          numberWanted = Number(req.query.topMostWanted.toString())
          if (isNaN(numberWanted))
            return res
              .status(400)
              .json({ msg: 'topMostWanted should be a number' })
        }
        if (numberWanted <= 0) {
          return res.status(400).json({
            msg: 'number of most requested items must be greater than 0',
          })
        }
        const rankResult = await getMostRequestedRanking(
          req.query.mostRequested.toString().toLowerCase(),
          numberWanted,
          true,
          user as IUser,
        )
        switch (rankResult.requested) {
          case 'company': {
            searchQuery = { ...searchQuery, company: { $in: rankResult.rank } }
            break
          }
          case 'service': {
            searchQuery = { ...searchQuery, service: { $in: rankResult.rank } }
            break
          }
          case 'requester': {
            searchQuery = {
              ...searchQuery,
              requester: { $in: rankResult.rank },
            }
            break
          }
          default: {
            return res.status(400).json({
              msg: `invalid query string mostRequested: ${req.query.mostRequested
                .toString()
                .toLowerCase()}`,
            })
          }
        }
        populateQuery = rankResult.populateQuery
        mostRequestedRank.push(...rankResult.rank)
      }
      // @ts-ignore
      if (user.level == 4) {
        // @ts-ignore
        searchQuery = { ...searchQuery, requester: user._id }
      }
      // @ts-ignore
      else if (user.level == 2 || user.level == 3) {
        // @ts-ignore
        searchQuery = { ...searchQuery, company: user.company_id }
      }
      if (
        req.query.getRHByDays?.toString().toLowerCase() === 'true' &&
        req.query.days_ago
      ) {
        const currentDate = new Date()
        const days_ago = new Date(
          currentDate.getTime() -
            parseInt(req.query.days_ago.toString()) * 86400000,
        )
        searchQuery = {
          ...searchQuery,
          createdAt: {
            $gte: days_ago,
            $lte: currentDate,
          },
        }
      }
      result = await RequestHistory.find(searchQuery)
      if (populateQuery) {
        await RequestHistory.populate(result, populateQuery)
        switch (populateQuery.path) {
          case 'company': {
            result.sort(
              (request1: IRequestHistory, request2: IRequestHistory) => {
                return (
                  mostRequestedRank.findIndex((val) => {
                    return val._id?.equals((request1.company as ICompany)._id)
                  }) -
                  mostRequestedRank.findIndex((val) => {
                    return val._id?.equals((request2.company as ICompany)._id)
                  })
                )
              },
            )
            break
          }
          case 'service': {
            result.sort(
              (request1: IRequestHistory, request2: IRequestHistory) => {
                return (
                  mostRequestedRank.findIndex((val) => {
                    return val._id?.equals((request1.service as IService)._id)
                  }) -
                  mostRequestedRank.findIndex((val) => {
                    return val._id?.equals((request2.service as IService)._id)
                  })
                )
              },
            )
            break
          }
          case 'requester': {
            result.sort(
              (request1: IRequestHistory, request2: IRequestHistory) => {
                return (
                  mostRequestedRank.findIndex((val) => {
                    return val._id?.equals((request1.requester as IUser)._id)
                  }) -
                  mostRequestedRank.findIndex((val) => {
                    return val._id?.equals((request2.requester as IUser)._id)
                  })
                )
              },
            )
            break
          }
          default:
            return res.status(400).json({
              msg: `invalid query string mostRequested: ${populateQuery.path
                .toString()
                .toLowerCase()}`,
            })
        }
      }
      if (
        req.query.withFullInfo?.toString().toLowerCase() === 'true' &&
        result.length > 0
      ) {
        if (typeof result[0]?.requester == 'object')
          await RequestHistory.populate(result, {
            path: 'requester',
            select: [
              '_id',
              'username',
              'email',
              'level',
              'normal_user_company_name',
              'normal_user_company_address',
            ],
          })
        if (typeof result[0]?.service == 'object')
          await RequestHistory.populate(result, {
            path: 'service',
            select: ['_id', 'service_name', 'description', 'updatedAt'],
          })
        if (typeof result[0]?.company == 'object')
          await RequestHistory.populate(result, {
            path: 'company',
            select: [
              '_id',
              'company_name',
              'contact_person',
              'contact_email',
              'client_contact_number',
            ],
          })
      }
      return res.status(200).json({
        msg: 'sus',
        count: result.length,
        request_histories: result,
      })
    } catch (err) {
      // console.log(err)
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async getMostRequested(req: Request, res: Response) {
    try {
      const { user } = req
      const requested = req.params.requested
      let numberWanted = 1
      if (req.query.topMostWanted) {
        numberWanted = Number(req.query.topMostWanted.toString())
        if (isNaN(numberWanted))
          return res
            .status(400)
            .json({ msg: 'topMostWanted should be a number' })
      }
      if (numberWanted <= 0) {
        return res.status(400).json({
          msg: 'number of most requested items must be greater than 0',
        })
      }
      if (
        requested == 'company' &&
        ((user as IUser).level == 2 || (user as IUser).level == 3)
      ) {
        return res
          .status(403)
          .json({ msg: 'You cannot see ranking of other companies' })
      }
      const rankResult = await getMostRequestedRanking(
        requested,
        numberWanted,
        requested == 'company',
        user as IUser,
      )
      return res.status(200).json({ msg: 'sus', rank: rankResult.rank })
    } catch (err) {
      console.log(err)
      return res.status(500).json({ msg: err })
    }
  },
  async getRequestHistoryById(req: Request, res: Response) {
    const { user } = req

    try {
      let result: IRequestHistory | null
      // @ts-ignore
      if (user.level == 4) {
        result = await RequestHistory.findOne({
          // @ts-ignore
          requester: user._id,
          _id: req.params.id,
        })
      }
      // @ts-ignore
      else if (user.level == 2 || user.level == 3) {
        result = await RequestHistory.findOne({
          // @ts-ignore
          company: user.company_id,
          _id: req.params.id,
        })
      } else {
        // @ts-ignore
        result = await RequestHistory.findById(req.params.id)
      }
      if (req.query.withFullInfo?.toString().toLowerCase() === 'true') {
        await RequestHistory.populate(result, {
          path: 'requester',
          select: [
            '_id',
            'username',
            'email',
            'level',
            'normal_user_company_name',
            'normal_user_company_address',
          ],
        })
        await RequestHistory.populate(result, {
          path: 'service',
          select: ['_id', 'service_name', 'description', 'updatedAt'],
        })
        const companyInfoList = [
          '_id',
          'company_name',
          'contact_person',
          'contact_email',
          'client_contact_number',
        ]
        if (req.query.withVendorImages?.toString().toLowerCase() === 'true') {
          companyInfoList.push(
            'image_data',
            'chop_image_data',
            'signature_image_data',
          )
        }
        await RequestHistory.populate(result, {
          path: 'company',
          select: companyInfoList,
        })
      }
      if (result) {
        return res.status(200).json({
          msg: 'sus',
          request_history: result,
          user_level: (user as IUser).level,
        })
      } else {
        return res.status(404).json({
          msg: 'request history not found',
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async getPopularServiceByVendorId(req: Request, res: Response) {
    const { user } = req

    if ((user as IUser).level < 2) {
      if (req.query.numOfServices) {
        const num = parseInt(req.query.numOfServices.toString())
        if (num > 0) {
          const company_popular_services = await RequestHistory.aggregate([
            { $match: { company: mongoose.Types.ObjectId(req.params.id) } },
            { $sortByCount: '$service' },
            {
              $lookup: {
                from: 'services',
                localField: '_id',
                foreignField: '_id',
                as: 'service',
              },
            },
            {
              $project: {
                count: 1,
                service: {
                  $arrayElemAt: ['$service', 0],
                },
              },
            },
            { $limit: num },
          ])
          return res.status(200).json({
            msg: 'sus',
            count: company_popular_services.length,
            services: company_popular_services,
          })
        } else {
          return res.status(400).json({
            msg: `invalid query string numOfServices: ${req.query.numOfServices.toString()}`,
          })
        }
      } else {
        return res.status(400).json({
          msg: 'missing query string numOfServices',
        })
      }
    } else {
      return res.status(403).json({
        msg: 'you are not super user',
      })
    }
  },
  async addRequestHistory(req: Request, res: Response) {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      const status_code = errors.array()[0].msg.includes('conflict') ? 409 : 400
      return res.status(status_code).json({ msg: errors.array()[0].msg })
    }
    const { requester, service, requested_items, company }: IRequestHistory =
      req.body

    try {
      const muser = await User.findById(requester)
      const mservice = await Service.findById(service)
      const mcompany = await Company.findById(company)

      if (!muser || !mservice || !mcompany) {
        return res.status(404).json({
          msg: 'user or service or company not found',
        })
      } else {
        const mrequesthistory = await RequestHistory.findOne({
          requester,
          service,
          company,
        })

        // @ts-ignore
        if (mservice.vendor.toHexString() != mcompany._id.toHexString()) {
          return res.status(400).json({
            msg: 'company here is not the same in service',
          })
        } else if (mrequesthistory) {
          return res.status(409).json({
            msg: 'request history already exists',
          })
        } else {
          const postRequestHistory: IRequestHistory = new RequestHistory({
            requester,
            service,
            requested_items,
            company,
            request_date: new Date(),
            status: 0,
            archive_status: 0,
          })

          await postRequestHistory?.save?.()

          send_request_remind_mail_to_vendor(
            mcompany.contact_email,
            muser.username,
            mservice.service_name,
            requested_items,
            mcompany._id.toHexString(),
            (postRequestHistory._id as mongoose.Types.ObjectId).toHexString(),
          )

          return res.status(200).json({
            msg: 'sus',
            request_history: postRequestHistory,
          })
        }
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async changeRequestHistoryById(req: Request, res: Response) {
    try {
      const mrequesthistory = await RequestHistory.findById(req.params.id)
      if (mrequesthistory) {
        const {
          requester,
          service,
          requested_items,
          company,
        }: IRequestHistory = req.body

        const mrequester = await User.findById(requester)
        const mservice = await Service.findById(service)
        const mcompany = await Company.findById(company)

        if (!mrequester || !mservice || !mcompany) {
          return res.status(404).json({
            msg: 'user or service or company not found',
          })
        }
        // @ts-ignore
        else if (mservice.vendor.toHexString() != mcompany._id.toHexString()) {
          return res.status(400).json({
            msg: 'company here is not the same in service',
          })
        } else {
          await RequestHistory.findByIdAndUpdate(req.params.id, {
            requester,
            service,
            requested_items,
            company,
          })
            .exec()
            .then((result) => {
              return res.status(200).json({
                msg: 'sus',
                request_history_original: result,
              })
            })
            .catch((err) => {
              return res.status(500).json({
                msg: err,
              })
            })
        }
      } else {
        return res.status(404).json({
          msg: 'request history not found',
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async deleteRequestHistoryById(req: Request, res: Response) {
    try {
      const requesthistory = await RequestHistory.findById(req.params.id)

      if (requesthistory) {
        await RequestHistory.findByIdAndDelete(req.params.id)
          .exec()
          .then((result) => {
            return res.status(200).json({
              msg: 'sus',
              request_history: result,
            })
          })
          .catch((err) => {
            return res.status(500).json({
              msg: err,
            })
          })
      } else {
        return res.status(404).json({
          msg: 'request history not found',
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async archiveRequestHistory(req: Request, res: Response) {
    const { user } = req

    try {
      const rh = await RequestHistory.findById(req.params.id)

      if (rh) {
        // @ts-ignore
        if (user.level < 2) {
          await RequestHistory.findByIdAndUpdate(req.params.id, {
            archive_status: 3,
          })
        }
        // @ts-ignore
        else if (user.level < 4) {
          await RequestHistory.findByIdAndUpdate(req.params.id, {
            archive_status: 2,
          })
        } else {
          await RequestHistory.findByIdAndUpdate(req.params.id, {
            archive_status: 1,
          })
        }
        const changed_rh = await RequestHistory.findById(req.params.id)
        return res.status(200).json({
          msg: 'sus',
          new_archive_status: changed_rh?.archive_status,
          request_history: changed_rh,
        })
      } else {
        return res.status(404).json({
          msg: 'request history not found',
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async unarchiveRequestHistory(req: Request, res: Response) {
    try {
      const rh = await RequestHistory.findById(req.params.id)

      if (rh) {
        await RequestHistory.findByIdAndUpdate(req.params.id, {
          archive_status: 0,
        })
        const changed_rh = await RequestHistory.findById(req.params.id)
        return res.status(200).json({
          msg: 'sus',
          new_archive_status: changed_rh?.archive_status,
          request_history: changed_rh,
        })
      } else {
        return res.status(404).json({
          msg: 'request history not found',
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async cancelRequestHistory(req: Request, res: Response) {
    const { user } = req

    try {
      const mrequesthistory = await RequestHistory.findById(req.params.id)

      if (mrequesthistory) {
        if (mrequesthistory.status < 1) {
          // @ts-ignore
          if (user.level < 2) {
            await RequestHistory.findByIdAndUpdate(req.params.id, { status: 1 })
            return res.status(200).json({
              msg: 'sus',
              status: 'cancelled',
            })
          } else {
            if (
              (user as IUser)._id.toString() ==
              (
                mrequesthistory.requester as mongoose.Types.ObjectId
              ).toHexString()
            ) {
              await RequestHistory.findByIdAndUpdate(req.params.id, {
                status: 1,
              })
              return res.status(200).json({
                msg: 'sus',
                status: 'cancelled',
              })
            } else {
              return res.status(403).json({
                msg: 'you cannot change other user request history',
              })
            }
          }
        } else {
          return res.status(400).json({
            msg: 'can only cancel ongoing request history',
          })
        }
      } else {
        return res.status(404).json({
          msg: 'request history not found',
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async declineRequestHistory(req: Request, res: Response) {
    const { user } = req

    try {
      const mrequesthistory = await RequestHistory.findById(req.params.id)

      if (mrequesthistory) {
        if (mrequesthistory.status < 1) {
          // @ts-ignore
          if (user.level < 2) {
            await RequestHistory.findByIdAndUpdate(req.params.id, { status: 2 })
            return res.status(200).json({
              msg: 'sus',
              status: 'declined',
            })
          } else {
            if (
              // @ts-ignore
              user.company_id.toString() ==
              // @ts-ignore
              mrequesthistory.company.toHexString()
            ) {
              await RequestHistory.findByIdAndUpdate(req.params.id, {
                status: 2,
              })
              return res.status(200).json({
                msg: 'sus',
                status: 'declined',
              })
            } else {
              return res.status(403).json({
                msg: 'you cannot change other user request history',
              })
            }
          }
        } else {
          return res.status(400).json({
            msg: 'can only decline ongoing request history',
          })
        }
      } else {
        return res.status(404).json({
          msg: 'request history not found',
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async approveRequestHistory(req: Request, res: Response) {
    const { user } = req

    try {
      const mrequesthistory = await RequestHistory.findById(req.params.id)
      if (mrequesthistory) {
        if (mrequesthistory.status < 1) {
          // @ts-ignore
          if (user.level < 2) {
            await RequestHistory.findByIdAndUpdate(req.params.id, { status: 3 })
            return res.status(200).json({
              msg: 'sus',
              status: 'approved',
            })
          } else {
            if (
              // @ts-ignore
              user.company_id.toString() ==
              // @ts-ignore
              mrequesthistory.company.toString()
            ) {
              await RequestHistory.findByIdAndUpdate(req.params.id, {
                status: 3,
              })
              return res.status(200).json({
                msg: 'sus',
                status: 'approved',
              })
            } else {
              return res.status(403).json({
                msg: 'you cannot change other user request history',
              })
            }
          }
        } else {
          return res.status(400).json({
            msg: 'can only approve ongoing request history',
          })
        }
      } else {
        return res.status(404).json({
          msg: 'request history not found',
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async approveRequestHistoryWithId(req: Request, res: Response) {
    try {
      const mrequesthistory = await RequestHistory.findById(req.params.id)
      if (mrequesthistory) {
        switch (mrequesthistory.status) {
          case 0: {
            if (
              req.params.company_id ==
              (mrequesthistory.company as mongoose.Types.ObjectId).toHexString()
            ) {
              await RequestHistory.findByIdAndUpdate(req.params.id, {
                status: 3,
              })
              return res.status(400).send(request_approval(0))
            } else {
              return res
                .status(403)
                .send('unauthorized to approval other company request history')
            }
          }
          case 1: {
            return res.status(400).send(request_approval(1))
          }
          case 2: {
            return res.status(400).send(request_approval(2))
          }
          case 3: {
            return res.status(400).send(request_approval(3))
          }
          case 4: {
            return res.status(400).send(request_approval(4))
          }
          default: {
            return res.status(500).send(request_approval(5))
          }
        }
      } else {
        // return res.status(404).json({
        //   msg: 'request history not found',
        // })
        return res.status(404).send('request history not found')
      }
    } catch (err) {
      // return res.status(500).json({
      //   msg: err,
      // })
      return res.status(500).send(`Server error: ${err}`)
    }
  },
  async expireRequestHistory(req: Request, res: Response) {
    const { user } = req

    try {
      const mrequesthistory = await RequestHistory.findById(req.params.id)

      if (mrequesthistory) {
        if (mrequesthistory.status < 1) {
          if ((user as IUser).level < 2) {
            await RequestHistory.findByIdAndUpdate(req.params.id, { status: 4 })
            return res.status(200).json({
              msg: 'sus',
              status: 'expired',
            })
          } else if ((user as IUser).level < 4) {
            if (
              (user as IUser).company_id?.toString() ==
              (mrequesthistory.company as mongoose.Types.ObjectId).toHexString()
            ) {
              await RequestHistory.findByIdAndUpdate(req.params.id, {
                status: 4,
              })
              return res.status(200).json({
                msg: 'sus',
                status: 'expired',
              })
            } else {
              return res.status(403).json({
                msg: 'you cannot change other user request history',
              })
            }
          } else {
            return res.status(403).json({
              msg: 'you cannot change other user request history',
            })
          }
        } else {
          return res.status(400).json({
            msg: 'can only cancel ongoing request history',
          })
        }
      } else {
        return res.status(404).json({
          msg: 'request history not found',
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },

  // development
  async getAllRequestHistoriesIsDev(req: Request, res: Response) {
    try {
      const result = await RequestHistory.find()
      if (req.query.withFullInfo?.toString().toLowerCase() === 'true') {
        await RequestHistory.populate(result, {
          path: 'requester',
          select: [
            '_id',
            'username',
            'email',
            'level',
            'normal_user_company_name',
            'normal_user_company_address',
          ],
        })
        await RequestHistory.populate(result, {
          path: 'service',
          select: ['_id', 'service_name', 'description', 'updatedAt'],
        })
        await RequestHistory.populate(result, {
          path: 'company',
          select: [
            '_id',
            'company_name',
            'contact_person',
            'contact_email',
            'client_contact_number',
          ],
        })
      }
      return res.status(200).json({
        msg: 'sus',
        count: result.length,
        request_histories: result,
      })
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async getRequestHistoryByIdIsDev(req: Request, res: Response) {
    try {
      const result = await RequestHistory.findById(req.params.id)

      if (result) {
        if (req.query.withFullInfo?.toString().toLowerCase() === 'true') {
          await RequestHistory.populate(result, {
            path: 'requester',
            select: [
              '_id',
              'username',
              'email',
              'level',
              'normal_user_company_name',
              'normal_user_company_address',
            ],
          })
          await RequestHistory.populate(result, {
            path: 'service',
            select: ['_id', 'service_name', 'description', 'updatedAt'],
          })
          await RequestHistory.populate(result, {
            path: 'company',
            select: [
              '_id',
              'company_name',
              'contact_person',
              'contact_email',
              'client_contact_number',
            ],
          })
        }
        return res.status(200).json({
          msg: 'sus',
          request_history: result,
        })
      } else {
        return res.status(404).json({
          msg: 'request history not found',
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
}

export default requestHistoryController
