import { NextFunction, Request, Response } from 'express'
import User, { IUserLevel, IUser } from './user.model'
import jwt from 'jsonwebtoken'
import { validationResult } from 'express-validator'
import bcrypt from 'bcrypt'
import { isDev, jwtConfig } from '../../config'
import mongoose from 'mongoose'
import Company from '../Company/company.model'

async function signup(
  email: string,
  username: string,
  password: string,
  res: Response,
  user_level: IUserLevel,
  company_id: mongoose.Types.ObjectId | null,
  staff_signup: boolean,
) {
  try {
    const found_user_with_email = await User.findOne({ email })
    const found_user_with_username = await User.findOne({ username })

    if (found_user_with_email || found_user_with_username) {
      if (company_id && !staff_signup) {
        await Company.findByIdAndDelete(company_id)
      }
      return res.status(409).json({ msg: 'user already exists' })
    } else {
      const new_user = new User({
        email: email,
        username: username,
        password: password,
        level: user_level,
        company_id: company_id,
      })

      const salt = await bcrypt.genSalt(10)

      new_user.password = await bcrypt.hash(password, salt)

      await new_user.save()

      const payload = {
        user: {
          _id: new_user._id,
          expiration: Date.now() + jwtConfig.expirationTime,
        },
      }

      const token = jwt.sign(JSON.stringify(payload), jwtConfig.secret)
      res
        .cookie('jwt', token, {
          httpOnly: true,
          secure: !isDev,
        })
        .status(200)
        .json({
          msg: 'sus',
          user: {
            _id: new_user._id,
            username: new_user.username,
          },
        })
    }
  } catch (err) {
    if (company_id && !staff_signup) {
      await Company.findByIdAndDelete(company_id)
    }
    console.error(`sign up error: ${err}`)
    return res.status(500).json({ msg: err })
  }
}

const userController = {
  async checkUser(req: Request, res: Response, next: NextFunction) {
    const { username, password } = req.body
    try {
      const user = await User.findOne({ username })
      if (!user) {
        return res.status(400).json({
          msg: 'invalid username / password',
        })
      } else {
        const isMatch = await bcrypt.compare(password, user.password!)
        if (!isMatch) {
          return res.status(400).json({
            msg: 'invalid username / password',
          })
        } else {
          delete user.password
          res.locals.user = user
          next()
        }
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async getAllUsers(req: Request, res: Response) {
    const { user } = req
    // @ts-ignore
    if (user.level > 2) {
      return res.status(403).json({
        msg: 'you are normie or staff',
      })
    }
    // @ts-ignore
    else if (user.level == 2) {
      // @ts-ignore
      await User.find({ level: 3, company_id: user.company_id })
        .exec()
        .then((result) => {
          return res.status(200).json({
            msg: 'sus',
            count: result.length,
            users: result,
          })
        })
        .catch((err) => {
          return res.status(500).json({
            msg: err,
          })
        })
    } else {
      try {
        let result: IUser[]
        if (req.query.latest && req.query.level) {
          const num = parseInt(req.query.latest.toString())
          const user_level = parseInt(req.query.level.toString())
          if (num > 0 && user_level >= 0 && user_level < 5) {
            result = await User.find({
              level: user_level,
            })
              .sort({ createdAt: -1 })
              .limit(num)
              .select('username')
          } else {
            return res.status(400).json({
              msg: `invalid query number for latest or level: ${req.query.latest}, ${req.query.level}`,
            })
          }
        } else if (
          req.query.newlyCreated?.toString().toLowerCase() === 'true' &&
          req.query.days_ago &&
          req.query.level
        ) {
          const user_level = parseInt(req.query.level.toString())
          if (user_level >= 0 && user_level < 5) {
            const currentDate = new Date()
            const days_ago = new Date(
              currentDate.getTime() -
                parseInt(req.query.days_ago.toString()) * 86400000,
            )

            result = await User.find({
              createdAt: {
                $gte: days_ago,
                $lte: currentDate,
              },
              level: user_level,
            })
              .sort({ createdAt: -1 })
              .select('username')
          } else {
            return res.status(400).json({
              msg: `invalid query number for level: ${req.query.level}`,
            })
          }
        } else {
          result = await User.find()
        }
        return res.status(200).json({
          msg: 'sus',
          count: result.length,
          users: result,
        })
      } catch (err) {
        return res.status(500).json({
          msg: err,
        })
      }
    }
  },
  async signup_admin(req: Request, res: Response) {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(400).json({ msg: errors.array()[0].msg })
    }

    const { email, username, password, company_id } = req.body
    try {
      if (company_id) {
        const company = await Company.findById(company_id)
        if (company) {
          await signup(
            email,
            username,
            password,
            res,
            IUserLevel.admin,
            company_id,
            false,
          )
        } else {
          return res.status(404).json({
            msg: 'company not found',
          })
        }
      } else {
        return res.status(400).json({
          msg: 'missing company id',
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async signup_staff(req: Request, res: Response) {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(400).json({ msg: errors.array()[0].msg })
    }

    const { email, username, password, company_id } = req.body

    try {
      if (company_id) {
        const company = await Company.findById(company_id)
        if (company) {
          await signup(
            email,
            username,
            password,
            res,
            IUserLevel.staff,
            company_id,
            true,
          )
        } else {
          return res.status(404).json({
            msg: 'company not found',
          })
        }
      } else {
        return res.status(400).json({
          msg: 'missing company id',
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async signup_super_admin(req: Request, res: Response) {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(400).json({ msg: errors.array()[0].msg })
    }

    const { email, username, password, company_id } = req.body

    if (!company_id) {
      await signup(
        email,
        username,
        password,
        res,
        IUserLevel.super_admin,
        null,
        false,
      )
    } else {
      return res.status(400).json({
        msg: 'super admin has company id',
      })
    }
  },
  async signup_normal(req: Request, res: Response) {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(400).json({ msg: errors.array()[0].msg })
    }

    const { email, username, password, company_id } = req.body

    if (!company_id) {
      await signup(
        email,
        username,
        password,
        res,
        IUserLevel.normal,
        null,
        false,
      )
    } else {
      return res.status(400).json({
        msg: 'normal user has company id',
      })
    }
  },
  async signup_super_staff(req: Request, res: Response) {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(400).json({ msg: errors.array()[0].msg })
    }

    const { email, username, password, company_id } = req.body

    if (!company_id) {
      await signup(
        email,
        username,
        password,
        res,
        IUserLevel.super_staff,
        null,
        true,
      )
    } else {
      return res.status(400).json({
        msg: 'super staff has company id',
      })
    }
  },
  signin(req: Request, res: Response) {
    if (!res.locals.user) {
      return res.status(404).json({
        msg: 'user not found',
      })
    } else {
      try {
        // console.log('signin', res.locals.user)
        const payload = {
          _id: res.locals.user._id,
          username: res.locals.user.username,
          level: res.locals.user.level,
          expiration: Date.now() + jwtConfig.expirationTime,
        }

        const token = jwt.sign(JSON.stringify(payload), jwtConfig.secret)
        res
          .cookie('jwt', token, {
            httpOnly: true,
            secure: !isDev,
          })
          .status(200)
          .json(res.locals.user)
      } catch (err) {
        return res.status(500).json({
          msg: err,
        })
      }
    }
  },
  signout(req: Request, res: Response) {
    if (req.cookies.jwt) {
      return res.clearCookie('jwt').status(200).json({
        msg: 'you are out',
      })
    } else {
      return res.status(400).json({
        msg: 'invalid token',
      })
    }
  },
  async sync(req: Request, res: Response, next: NextFunction) {
    // const _id = req.user?._id
    next()
  },
  async upload(req: Request, res: Response) {
    const { uploaded } = req.body
    if (uploaded == 'false') {
      return res.status(500).json({
        msg: 'upload error',
      })
    } else {
      return res.status(200).json({
        msg: 'sus',
        saved_path: req.file?.path.replace(/\\/g, '/'),
      })
    }
  },
  async deleteUserById(req: Request, res: Response) {
    const { user } = req
    //@ts-ignore
    if (user?.level != 0 && user?.level != 1) {
      return res.status(403).json({
        msg: 'you are not super admin nor super staff',
      })
    } else {
      try {
        const user = await User.findById(req.params.id)

        if (user) {
          await User.findByIdAndDelete(req.params.id)
            .exec()
            .then((result) => {
              return res.status(200).json({
                msg: 'sus',
                user: result,
              })
            })
            .catch((err) => {
              return res.status(500).json({
                msg: err,
              })
            })
        } else {
          return res.status(404).json({
            msg: 'user not found',
          })
        }
      } catch (err) {
        return res.status(500).json({
          msg: err,
        })
      }
    }
  },
  async getUserById(req: Request, res: Response) {
    const { user } = req
    try {
      //@ts-ignore
      if (user.level < 2) {
        const found_user = await User.findById(req.params.id)

        if (found_user) {
          return res.status(200).json({
            msg: 'sus',
            user: found_user,
          })
        } else {
          return res.status(404).json({
            msg: 'user not found',
          })
        }
      } else {
        //@ts-ignore
        if (user._id.toString() == req.params.id) {
          const found_user = await User.findById(req.params.id)

          if (found_user) {
            return res.status(200).json({
              msg: 'sus',
              user: found_user,
            })
          } else {
            return res.status(404).json({
              msg: 'user not found',
            })
          }
        } else {
          return res.status(403).json({
            msg: 'you cannot access other users info',
          })
        }
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async changeUserById(req: Request, res: Response) {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(400).json({ msg: errors.array()[0].msg })
    }

    const { user } = req

    const {
      username,
      email,
      normal_user_company_name,
      normal_user_company_address,
    } = req.body

    try {
      const found_user = await User.findById(req.params.id)

      if (found_user) {
        const other_user_username = await User.find({ username: username })
        const other_user_email = await User.find({ email: email })
        if (other_user_username.length > 0) {
          if (found_user.username != username) {
            return res.status(409).json({
              msg: 'conflict-username',
            })
          }
        }
        if (other_user_email.length > 0) {
          if (found_user.email != email) {
            return res.status(409).json({
              msg: 'conflict-email',
            })
          }
        }
        // @ts-ignore
        if (user.level < 2) {
          await User.findByIdAndUpdate(req.params.id, {
            email,
            username,
            normal_user_company_name,
            normal_user_company_address,
          })
            .then((result) => {
              return res.status(200).json({ msg: 'sus', user: result })
            })
            .catch((err) => {
              return res.status(500).json({
                msg: err,
              })
            })
        } else {
          // @ts-ignore
          if (user._id.toString() == req.params.id) {
            await User.findByIdAndUpdate(req.params.id, {
              email,
              username,
              normal_user_company_name,
              normal_user_company_address,
            })
              .then((result) => {
                return res.status(200).json({ msg: 'sus', user: result })
              })
              .catch((err) => {
                return res.status(500).json({
                  msg: err,
                })
              })
          } else {
            return res.status(403).json({
              msg: 'you cannot access other user account info',
            })
          }
        }
      } else {
        return res.status(404).json({
          msg: 'user not found',
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async changeUserpwById(req: Request, res: Response) {
    const { user } = req

    const { old_password, new_password } = req.body

    if (new_password.length < 8 || new_password.length > 30) {
      return res.status(400).json({
        msg: 'invalid-new',
      })
    }

    try {
      const found_user = await User.findById(req.params.id)

      if (found_user) {
        const isMatch = await bcrypt.compare(old_password, found_user.password!)
        if (isMatch) {
          const salt = await bcrypt.genSalt(10)
          const encrypted_pw = await bcrypt.hash(new_password, salt)

          // @ts-ignore
          if (user.level < 2) {
            await User.findByIdAndUpdate(req.params.id, {
              password: encrypted_pw,
            })
              .then((result) => {
                return res.status(200).json({ msg: 'sus', user: result })
              })
              .catch((err) => {
                return res.status(500).json({
                  msg: err,
                })
              })
          } else {
            // @ts-ignore
            if (user._id.toString() == req.params.id) {
              await User.findByIdAndUpdate(req.params.id, {
                password: encrypted_pw,
              })
                .then((result) => {
                  return res.status(200).json({ msg: 'sus', user: result })
                })
                .catch((err) => {
                  return res.status(500).json({
                    msg: err,
                  })
                })
            } else {
              return res.status(403).json({
                msg: 'you cannot access other user account info',
              })
            }
          }
        } else {
          return res.status(400).json({
            msg: 'invalid-old',
          })
        }
      } else {
        return res.status(404).json({
          msg: 'user not found',
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async sendCode(req: Request, res: Response) {
    const { ver_code } = req.body

    try {
      const token = jwt.decode(req.cookies['email-jwt'])
      if (token) {
        // @ts-ignore
        const found_user = await User.findOne({ email: token.email })

        if (found_user) {
          // @ts-ignore
          if (token.ver_code == ver_code) {
            const token_changepw = jwt.sign(
              JSON.stringify({
                user_id: found_user._id,
              }),
              jwtConfig.secret,
            )

            const currentDate = new Date()
            return res
              .cookie('change-pw-jwt', token_changepw, {
                httpOnly: true,
                expires: new Date(
                  currentDate.getTime() + jwtConfig.expirationTime,
                ),
              })
              .status(200)
              .json({
                msg: 'sus',
              })
          } else {
            return res.status(400).json({
              msg: 'incorrect verification code',
            })
          }
        } else {
          return res.status(404).json({
            msg: 'user not found',
          })
        }
      } else {
        return res.status(410).json({
          msg: 'expired token or no token',
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async forgotPW(req: Request, res: Response) {
    const { new_password } = req.body

    try {
      const token = jwt.decode(req.cookies['change-pw-jwt'])
      if (token) {
        // @ts-ignore
        const found_user = await User.findById(token.user_id)
        if (found_user) {
          const salt = await bcrypt.genSalt(10)
          const encrypted_pw = await bcrypt.hash(new_password, salt)

          // @ts-ignore
          await User.findByIdAndUpdate(token.user_id, {
            password: encrypted_pw,
          }).then(async () => {
            return res.status(200).json({
              msg: 'sus',
            })
          })
        } else {
          return res.status(404).json({
            msg: 'user not found',
          })
        }
      } else {
        return res.status(410).json({
          msg: 'expired token or no token',
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },

  // isDev
  // delete after deployment
  async getAllUsersIsDev(req: Request, res: Response) {
    await User.find()
      .exec()
      .then((result) => {
        return res.status(200).json({
          msg: 'sus',
          count: result.length,
          users: result,
        })
      })
      .catch((err) => {
        return res.status(500).json({
          msg: err,
        })
      })
  },
  async deleteUserByIdIsDev(req: Request, res: Response) {
    try {
      const user = await User.findById(req.params.id)

      if (user) {
        await User.findByIdAndDelete(req.params.id)
          .exec()
          .then((result) => {
            return res.status(200).json({
              msg: 'sus',
              user: result,
            })
          })
          .catch((err) => {
            return res.status(500).json({
              msg: err,
            })
          })
      } else {
        return res.status(404).json({
          msg: 'user not found',
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
}

export default userController
