import { Router } from 'express'
import userController from './user.controller'
import { check } from 'express-validator'
import upload from '../../middlewares/upload'

import { isDev, validateUser } from '../../config'
import { authMiddleware } from '../../middlewares/auth.middleware'

const router = Router()

// if (!isDev) {
if (validateUser) {
  // deployment
  router.get('/', authMiddleware, userController.getAllUsers)
  router.get('/:id', authMiddleware, userController.getUserById)
  router.delete(
    '/delete/:id',
    authMiddleware,
    userController.deleteUserByIdIsDev,
  )
  router.put(
    '/change/:id',
    authMiddleware,
    [
      check('email', 'invalid-email').isEmail(),
      check('username', 'invalid-username-length').isLength({
        min: 8,
        max: 30,
      }),
      check('username', 'please include username').exists(),
    ],
    userController.changeUserById,
  )
  router.put('/changepw/:id', authMiddleware, userController.changeUserpwById)
} else {
  // development
  router.get('/', userController.getAllUsersIsDev)
  router.delete('/delete/:id', userController.deleteUserById)
}

router.post(
  '/signup/super_admin',
  [
    check('email', 'invalid email').isEmail(),
    check('username', 'invalid-username-length').isLength({ min: 8, max: 30 }),
    check('username', 'please include username').exists(),
    check('password', 'invalid-password-length').isLength({ min: 8, max: 30 }),
    check('password', 'please include password').exists(),
  ],
  userController.signup_super_admin,
)
router.post(
  '/signup/super_staff',
  [
    check('email', 'invalid email').isEmail(),
    check('username', 'invalid-username-length').isLength({ min: 8, max: 30 }),
    check('username', 'please include username').exists(),
    check('password', 'invalid-password-length').isLength({ min: 8, max: 30 }),
    check('password', 'please include password').exists(),
  ],
  userController.signup_super_staff,
)
router.post(
  '/signup/normal',
  [
    check('email', 'invalid email').isEmail(),
    check('username', 'invalid-username-length').isLength({ min: 8, max: 30 }),
    check('username', 'please include username').exists(),
    check('password', 'invalid-password-length').isLength({ min: 8, max: 30 }),
    check('password', 'please include password').exists(),
  ],
  userController.signup_normal,
)
router.post(
  '/signup/admin',
  [
    check('email', 'invalid email').isEmail(),
    check('username', 'invalid-username-length').isLength({ min: 8, max: 30 }),
    check('username', 'please include username').exists(),
    check('password', 'invalid-password-length').isLength({ min: 8, max: 30 }),
    check('password', 'please include password').exists(),
  ],
  userController.signup_admin,
)
router.post(
  '/signup/staff',
  [
    check('email', 'invalid email').isEmail(),
    check('username', 'invalid-username-length').isLength({ min: 8, max: 30 }),
    check('username', 'please include username').exists(),
    check('password', 'invalid-password-length').isLength({ min: 8, max: 30 }),
    check('password', 'please include password').exists(),
  ],
  userController.signup_staff,
)
router.post('/signin', userController.checkUser, userController.signin)
router.get(
  '/signout',
  authMiddleware,
  userController.sync,
  userController.signout,
)
router.post('/upload', upload, userController.upload)
router.put('/sendcode', userController.sendCode)
router.put('/forgotpw', userController.forgotPW)

export default router
