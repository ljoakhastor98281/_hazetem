import mongoose from 'mongoose'

export enum IUserLevel {
  super_admin,
  super_staff,
  admin,
  staff,
  normal,
}

export interface IUser extends Partial<mongoose.Document> {
  _id: mongoose.Types.ObjectId
  email: string
  username: string
  password?: string
  level: IUserLevel
  company_id: mongoose.Types.ObjectId | undefined
  normal_user_company_name: string
  normal_user_company_address: string
  createdAt: Date
  updatedAt: Date
}

const UserSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      required: true,
      index: { unique: true },
      trim: true,
    },
    username: {
      type: String,
      required: true,
      index: { unique: true },
      minLength: 8,
      maxLength: 30,
      trim: true,
    },
    password: {
      type: String,
      required: true,
      trim: true,
    },
    level: {
      type: IUserLevel,
      required: true,
      trim: true,
    },
    company_id: {
      type: mongoose.Types.ObjectId,
      required: false,
      trim: true,
    },
    normal_user_company_name: {
      type: String,
      required: false,
      trim: true,
    },
    normal_user_company_address: {
      type: String,
      required: false,
      trim: true,
    },
  },
  {
    timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' },
    optimisticConcurrency: true,
  },
)

const User: mongoose.Model<IUser> =
  mongoose.models.users || mongoose.model<IUser>('users', UserSchema)
export default User
