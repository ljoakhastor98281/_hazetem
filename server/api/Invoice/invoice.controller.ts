import mongoose from 'mongoose'
import { Request, Response } from 'express'
import Invoice, { IInvoice } from './invoice.model'
import Company, { ICompany } from '../Company/company.model'
import { validationResult } from 'express-validator'
import RequestHistory from '../RequestHistory/requesthistory.model'
import { IUser } from '../User/user.model'
import { nanoid } from 'nanoid'

async function generateShareId() {
  let result = nanoid()
  while (await Invoice.findOne({ share_id: result })) result = nanoid() // repeated id
  return result
}

const invoiceController = {
  // deployment
  async getAllInvoices(req: Request, res: Response) {
    try {
      const { user } = req
      // @ts-ignore
      if (user.level > 3) {
        return res.status(403).json({
          msg: 'you are normie',
        })
      }
      // @ts-ignore
      else if (user.level > 1) {
        // @ts-ignore
        const result = await Invoice.find({ client: user.company_id })
        if (req.query.withClientInfo?.toString().toLowerCase() === 'true') {
          await Invoice.populate(result, {
            path: 'client',
            select: [
              '_id',
              'company_name',
              'contact_person',
              'contact_email',
              'client_contact_number',
              'project_name',
            ],
          })
        }
        return res.status(200).json({
          msg: 'sus',
          count: result.length,
          invoices: result,
        })
      } else {
        // @ts-ignore
        const result = await Invoice.find()
        if (req.query.withClientInfo?.toString().toLowerCase() === 'true') {
          await Invoice.populate(result, {
            path: 'client',
            select: [
              '_id',
              'company_name',
              'contact_person',
              'contact_email',
              'client_contact_number',
              'project_name',
            ],
          })
        }
        return res.status(200).json({
          msg: 'sus',
          count: result.length,
          invoices: result,
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async addInvoice(req: Request, res: Response) {
    const { user } = req
    // @ts-ignore
    if (user.level > 3) {
      return res.status(403).json({
        msg: 'you are normie',
      })
    } else {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        const status_code = errors.array()[0].msg.includes('conflict')
          ? 409
          : 400
        return res.status(status_code).json({ msg: errors.array()[0].msg })
      }

      const {
        request_history,
        client_id_old,
        client,
        from,
        project_name,
        quotation_items,
        remark,
        date,
        invoice_number,
      }: IInvoice = req.body
      // @ts-ignore
      if (
        (user as IUser).level > 1 &&
        (user as IUser).company_id != client &&
        (user as IUser).company_id != (client as ICompany)._id
      ) {
        return res.status(403).json({
          msg: 'you cannot access other company invoices',
        })
      }

      try {
        const company_id = await Company.findById(client)

        if (!company_id) {
          return res.status(401).json({
            msg: 'client (company) not found',
          })
        } else {
          const invoice = await Invoice.findOne({
            request_history,
            // client_id_old,
            // client,
            // from,
            // project_name,
            // quotation_items,
            // remark,
            // date,
            // invoice_number,
          })
          if (invoice) {
            return res.status(400).json({
              msg: 'invoice already exists',
            })
          } else {
            const rh = await RequestHistory.findById(request_history)
            if (rh) {
              const postInvoice: IInvoice = new Invoice({
                request_history,
                client_id_old,
                client,
                from,
                project_name,
                quotation_items,
                remark,
                date,
                invoice_number,
                admin_added:
                  // @ts-ignore
                  user.level < 2 ? new Date() : null,
                archive_status: 0,
              })

              await postInvoice?.save?.()
              return res.status(200).json({
                msg: 'sus',
                invoice: postInvoice,
              })
            } else {
              return res.status(404).json({
                msg: 'request history not found',
              })
            }
          }
        }
      } catch (err) {
        // console.error(err.message)
        return res.status(500).json({
          msg: err,
        })
      }
    }
  },
  async getInvoiceById(req: Request, res: Response) {
    const { user } = req
    if (req.query.byShareId?.toString().toLowerCase() == 'true') {
      try {
        const invoice = await Invoice.findOne({
          share_id: req.params.id,
        })
          .select([
            'project_name',
            'quotation_items',
            'price',
            'from',
            'invoice_number',
            'request_history',
            'client',
          ])
          .populate({
            path: 'client',
            select: {
              _id: 0,
              image_data: 1,
              chop_image_data: 1,
              signature_image_data: 1,
            },
          })
          .select('-_id')
        if (invoice) {
          const request_info = await RequestHistory.findById({
            _id: invoice.request_history,
          }).populate({
            path: 'requester',
            select: {
              _id: 0,
              email: 1,
              username: 1,
              normal_user_company_name: 1,
              normal_user_company_address: 1,
            },
          })
          return res.status(200).json({
            msg: 'sus',
            invoice: {
              project_name: invoice.project_name,
              quotation_items: invoice.quotation_items,
              price: invoice.price,
              from: invoice.from,
              invoice_number: invoice.invoice_number,
            },
            user_info: request_info?.requester,
            images: invoice.client,
          })
        } else {
          return res.status(404).json({ msg: 'invoice not found' })
        }
      } catch (err) {
        // console.error(err.message)
        return res.status(500).json({
          msg: err,
        })
      }
    }
    // @ts-ignore
    if (user.level > 3) {
      try {
        if (req.query.byRequestId?.toString().toLowerCase() != 'true')
          return res.status(403).json({
            msg: 'As ordinary user you can only get quotation by your request id',
          })
        const request_history = await RequestHistory.findById(req.params.id)
        if (request_history) {
          if (request_history.requester == (user as IUser)._id) {
            const invoice = await Invoice.findOne({
              request_history: mongoose.Types.ObjectId(req.params.id),
            }).select([
              'project_name',
              'quotation_items',
              'price',
              'from',
              'invoice_number',
              'client',
            ])
            if (invoice) {
              if (
                req.query.withVendorImages?.toString().toLowerCase() ===
                  'true' &&
                request_history.status == 3
              )
                await Invoice.populate(invoice, {
                  path: 'client',
                  select: [
                    'image_data',
                    'chop_image_data',
                    'signature_image_data',
                  ],
                })
              return res.status(200).json({
                msg: 'sus',
                invoice: invoice,
              })
            }
            return res.status(404).json({
              msg: 'Invoice of this request not created yet',
            })
          }
          return res.status(403).json({
            msg: 'You can only get your own quotation',
          })
        }
        return res.status(404).json({
          msg: 'Request history does not exist',
        })
      } catch (err) {
        // console.log(err)
      }
    } else {
      try {
        const invoice =
          // @ts-ignore
          user.level < 2
            ? await Invoice.findById(req.params.id)
            : await Invoice.findOne({
                _id: req.params.id,
                //@ts-ignore
                client: user.company_id,
              })
        if (invoice) {
          if (req.query.withClientInfo?.toString().toLowerCase() === 'true') {
            const companyInfoList = [
              '_id',
              'company_name',
              'contact_person',
              'contact_email',
              'client_contact_number',
              'project_name',
            ]
            if (
              req.query.withVendorImages?.toString().toLowerCase() === 'true'
            ) {
              companyInfoList.push(
                'image_data',
                'chop_image_data',
                'signature_image_data',
              )
            }
            await Invoice.populate(invoice, {
              path: 'client',
              select: companyInfoList,
            })
          }
          return res.status(200).json({
            msg: 'sus',
            invoice: invoice,
          })
        } else {
          return res.status(404).json({
            msg: 'invoice not found',
          })
        }
      } catch (err) {
        return res.status(500).json({
          msg: err,
        })
      }
    }
  },
  async getInvoiceWithCompanyId(req: Request, res: Response) {
    try {
      const result = await Invoice.find({ client: req.params.id })
      if (req.query.withClientInfo?.toString().toLowerCase() === 'true') {
        await Invoice.populate(result, {
          path: 'client',
          select: [
            '_id',
            'company_name',
            'contact_person',
            'contact_email',
            'client_contact_number',
            'project_name',
          ],
        })
      }
      res.status(200).json({
        msg: 'sus',
        count: result.length,
        invoices: result,
      })
    } catch (err) {
      res.status(500).json({
        msg: err,
      })
    }
  },
  async changeInvoiceById(req: Request, res: Response) {
    const { user } = req
    // @ts-ignore
    const isSuper = user.level < 2 ? true : false
    // @ts-ignore
    if (user.level != 0 && user.level != 1) {
      return res.status(403).json({
        msg: 'you are not super admin nor super staff',
      })
    } else {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        const status_code = errors.array()[0].msg.includes('conflict')
          ? 409
          : 400
        return res.status(status_code).json({ msg: errors.array()[0].msg })
      }

      const {
        client,
        from,
        project_name,
        quotation_items,
        remark,
        date,
        invoice_number,
      }: IInvoice = req.body
      const invoice = await Invoice.findById(req.params.id)
      if (!invoice) {
        res.status(500).json({
          msg: 'No Invoice with this id can be found',
        })
      }
      await Invoice.updateOne(
        { _id: req.params.id },
        {
          client,
          from,
          project_name,
          quotation_items,
          remark,
          date,
          invoice_number,
          admin_modified: invoice?.admin_modified
            ? isSuper
              ? new Date()
              : invoice.admin_modified
            : null,
        },
      )
        .exec()
        .then(async () => {
          const newInvoice = await Invoice.findOne({ _id: req.params.id })
          return res.status(200).json({
            msg: 'sus',
            new_invoice: newInvoice,
          })
        })
        .catch((err) => {
          return res.status(500).json({
            msg: err,
          })
        })
    }
  },
  async deleteInvoiceById(req: Request, res: Response) {
    const { user } = req
    // @ts-ignore
    if (user.level != 0 && user.level != 1) {
      return res.status(403).json({
        msg: 'you are not super admin nor super staff',
      })
    } else {
      await Invoice.findByIdAndDelete(req.params.id)
        .exec()
        .then((result) => {
          return res.status(200).json({
            msg: 'sus',
            invoice: result,
          })
        })
        .catch((err) => {
          return res.status(500).json({
            msg: err,
          })
        })
    }
  },
  async archiveInvoice(req: Request, res: Response) {
    const { user } = req

    try {
      const cur_invoice = await Invoice.findById(req.params.id)

      if (cur_invoice) {
        // @ts-ignore
        if (user.level < 2) {
          await Invoice.findByIdAndUpdate(req.params.id, {
            archive_status: 3,
          })
        }
        // @ts-ignore
        else if (user.level < 4) {
          await Invoice.findByIdAndUpdate(req.params.id, {
            archive_status: 2,
          })
        } else {
          await Invoice.findByIdAndUpdate(req.params.id, {
            archive_status: 1,
          })
        }
        const changed_invoice = await Invoice.findById(req.params.id)
        return res.status(200).json({
          msg: 'sus',
          new_archive_status: changed_invoice?.archive_status,
          invoice: changed_invoice,
        })
      } else {
        return res.status(404).json({
          msg: 'invoice not found',
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async unarchiveInvoice(req: Request, res: Response) {
    try {
      const cur_invoice = await Invoice.findById(req.params.id)

      if (cur_invoice) {
        await Invoice.findByIdAndUpdate(req.params.id, {
          archive_status: 0,
        })
        const changed_invoice = await Invoice.findById(req.params.id)
        return res.status(200).json({
          msg: 'sus',
          new_archive_status: changed_invoice?.archive_status,
          invoice: changed_invoice,
        })
      } else {
        return res.status(404).json({
          msg: 'invoice not found',
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async getShareId(req: Request, res: Response) {
    try {
      const { user } = req
      if ((user as IUser).level >= 4) {
        return res.status(403).json({
          msg: 'As ordinary user you can only get quotation by your request id',
        })
      }
      const invoice = await Invoice.findById(req.params.id)
      if (invoice) {
        if (invoice.share_id) {
          return res.status(200).json({
            msg: 'sus',
            share_id: invoice.share_id,
          })
        }
        const new_share_id = await generateShareId()
        await Invoice.updateOne(
          { _id: req.params.id },
          { quotation_items: invoice.quotation_items, share_id: new_share_id },
        )
          .exec()
          .then(() => {
            return res.status(200).json({
              msg: 'sus',
              share_id: new_share_id,
            })
          })
          .catch((err) => {
            // console.log(err)
            return res.status(500).json({
              msg: err,
            })
          })
      } else {
        return res.status(404).json({
          msg: 'invoice not found',
        })
      }
    } catch (err) {
      // console.log(err)
      return res.status(500).json({
        msg: err,
      })
    }
  },
  // development
  async getAllInvoicesIsDev(req: Request, res: Response) {
    try {
      const result = await Invoice.find()
      if (req.query.withClientInfo?.toString().toLowerCase() === 'true') {
        await Invoice.populate(result, {
          path: 'client',
          select: [
            '_id',
            'company_name',
            'contact_person',
            'contact_email',
            'client_contact_number',
            'project_name',
          ],
        })
      }
      return res.status(200).json({
        msg: 'sus',
        count: result.length,
        invoices: result,
      })
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async addInvoiceIsDev(req: Request, res: Response) {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      const status_code = errors.array()[0].msg.includes('conflict') ? 409 : 400
      return res.status(status_code).json({ msg: errors.array()[0].msg })
    }

    const {
      request_history,
      client_id_old,
      client,
      from,
      project_name,
      quotation_items,
      remark,
      date,
      invoice_number,
    }: IInvoice = req.body

    try {
      const company_id = await Company.findById(client)

      if (!company_id) {
        return res.status(401).json({
          msg: 'client (company) not found',
        })
      } else {
        const invoice = await Invoice.findOne({
          request_history,
          client_id_old,
          client,
          from,
          project_name,
          quotation_items,
          remark,
          date,
          invoice_number,
        })
        if (invoice) {
          return res.status(400).json({
            msg: 'invoice already exists',
          })
        } else {
          const rh = await RequestHistory.findById(request_history)
          if (rh) {
            const postInvoice: IInvoice = new Invoice({
              request_history,
              client_id_old,
              client,
              from,
              project_name,
              quotation_items,
              remark,
              date,
              invoice_number,
              admin_added: Date(),
              archive_status: 0,
            })

            await postInvoice?.save?.()
            return res.status(200).json({
              msg: 'sus',
              invoice: postInvoice,
            })
          } else {
            return res.status(404).json({
              msg: 'request history not found',
            })
          }
        }
      }
    } catch (err) {
      // console.error(err.message)
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async getInvoiceByIdIsDev(req: Request, res: Response) {
    try {
      const invoice = await Invoice.findById(req.params.id)

      if (invoice) {
        if (req.query.withClientInfo?.toString().toLowerCase() === 'true') {
          await Invoice.populate(invoice, {
            path: 'client',
            select: [
              '_id',
              'company_name',
              'contact_person',
              'contact_email',
              'client_contact_number',
              'project_name',
            ],
          })
        }
        return res.status(200).json({
          msg: 'sus',
          invoice: invoice,
        })
      } else {
        return res.status(404).json({
          msg: 'invoice not found',
        })
      }
    } catch (err) {
      return res.status(500).json({
        msg: err,
      })
    }
  },
  async getInvoiceWithCompanyIdIsDev(req: Request, res: Response) {
    try {
      const result = await Invoice.find({ client: req.params.id })
      if (req.query.withClientInfo?.toString().toLowerCase() === 'true') {
        await Invoice.populate(result, {
          path: 'client',
          select: [
            '_id',
            'company_name',
            'contact_person',
            'contact_email',
            'client_contact_number',
            'project_name',
          ],
        })
      }
      res.status(200).json({
        msg: 'sus',
        count: result.length,
        invoices: result,
      })
    } catch (err) {
      res.status(500).json({
        msg: err,
      })
    }
  },
  async changeInvoiceByIdIsDev(req: Request, res: Response) {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      const status_code = errors.array()[0].msg.includes('conflict') ? 409 : 400
      return res.status(status_code).json({ msg: errors.array()[0].msg })
    }

    const {
      client,
      from,
      project_name,
      quotation_items,
      remark,
      date,
      invoice_number,
    }: IInvoice = req.body
    const invoice = await Invoice.findById(req.params.id)
    if (!invoice) {
      res.status(500).json({
        msg: 'No Invoice with this id can be found',
      })
    }
    await Invoice.updateOne(
      { _id: req.params.id },
      {
        client,
        from,
        project_name,
        quotation_items,
        remark,
        date,
        invoice_number,
        admin_modified: invoice?.admin_modified ? new Date() : null,
      },
    )
      .exec()
      .then(async () => {
        const newInvoice = await Invoice.findOne({ _id: req.params.id })
        return res.status(200).json({
          msg: 'sus',
          new_invoice: newInvoice,
        })
      })
      .catch((err) => {
        return res.status(500).json({
          msg: err,
        })
      })
  },
  async deleteInvoiceByIdIsDev(req: Request, res: Response) {
    await Invoice.findByIdAndDelete(req.params.id)
      .exec()
      .then((result) => {
        return res.status(200).json({
          msg: 'sus',
          invoice: result,
        })
      })
      .catch((err) => {
        return res.status(500).json({
          msg: err,
        })
      })
  },
}

export default invoiceController
