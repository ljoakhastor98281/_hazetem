import mongoose from 'mongoose'
import { ICompany } from '../Company/company.model'
import {
  IRequestHistory,
  IArchive,
} from '../RequestHistory/requesthistory.model'
import { IServiceItem } from '../Service/service.model'

const totalQuotationItemPrice = (quotation_items: IServiceItem[]): number => {
  let result = 0
  quotation_items.map((item) => {
    result += Number(item.price)
  })
  return result
}

export interface IInvoice extends Partial<mongoose.Document> {
  _id: mongoose.Types.ObjectId
  request_history: mongoose.Types.ObjectId | IRequestHistory
  price: number
  client_id_old: string
  client: mongoose.Types.ObjectId | ICompany
  from: string
  project_name: string
  quotation_items: IServiceItem[]
  remark: string
  date: Date
  invoice_number: string
  archive_status: IArchive
  share_id: string
  admin_added: Date | null
  admin_modified: Date | null
}

const InvoiceSchema = new mongoose.Schema(
  {
    request_history: {
      type: mongoose.Types.ObjectId,
      required: true,
      trim: true,
      ref: 'requesthistories',
    },
    price: {
      type: Number,
      required: true,
      trim: true,
    },
    client_id_old: {
      type: String,
      required: false,
      trim: true,
    },
    client: {
      type: mongoose.Types.ObjectId,
      required: true,
      trim: true,
      ref: 'companys',
    },
    from: {
      type: String,
      required: true,
      trim: true,
    },
    project_name: {
      type: String,
      required: true,
      trim: true,
    },
    quotation_items: {
      type: Array,
      required: true,
      trim: true,
    },
    remark: {
      type: String,
      required: false,
      trim: true,
    },
    date: {
      type: Date,
      required: true,
      trim: true,
    },
    invoice_number: {
      type: String,
      required: true,
      trim: true,
    },
    admin_added: {
      type: Date,
      required: false,
      trim: true,
    },
    admin_modified: {
      type: Date,
      required: false,
      trim: true,
    },
    archive_status: {
      type: IArchive,
      required: true,
      trim: true,
    },
    share_id: {
      type: String,
      required: false,
      trim: true,
      unique: true,
    },
  },
  {
    timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' },
    optimisticConcurrency: true,
    strict: false,
  },
)

const itemComparator = (item1: IServiceItem, item2: IServiceItem) => {
  if (item1.order < item2.order) return -1
  else if (item1.order > item2.order) return 1
  else return 0
}

InvoiceSchema.pre('validate', function (this: IInvoice, next) {
  this.price = totalQuotationItemPrice(this.quotation_items)
  this.quotation_items.sort(itemComparator)
  this.quotation_items.map((item, index) => {
    item.order = index + 1
  })
  next()
})

InvoiceSchema.pre('updateOne', function (this, next) {
  this.set({ price: totalQuotationItemPrice(this.get('quotation_items')) })
  this.get('quotation_items').sort(itemComparator)
  this.get('quotation_items').map((item: IServiceItem, index: number) => {
    item.order = index + 1
  })
  next()
})

const Invoice: mongoose.Model<IInvoice> =
  mongoose.models.invoices ||
  mongoose.model<IInvoice>('invoices', InvoiceSchema)
export default Invoice
