import { Router } from 'express'
import invoiceController from './invoice.controller'
import { body, check } from 'express-validator'
import { isDev, validateUser } from '../../config'
import { authMiddleware } from '../../middlewares/auth.middleware'
import { validateServiceItems } from '../../middlewares/service_items.middleware'

const router = Router()
// if (!isDev) {
if (validateUser) {
  // deployment
  router.get('/', authMiddleware, invoiceController.getAllInvoices)
  router.get('/:id', authMiddleware, invoiceController.getInvoiceById)
  router.get('/share_id/:id', authMiddleware, invoiceController.getShareId)
  router.post(
    '/',
    authMiddleware,
    [
      check('invoice_number', 'please input nonempty invoice number').isLength({
        min: 1,
      }),
      check('from', 'please input nonempty vendor(from)').isLength({
        min: 1,
      }),
      check('project_name', 'please input nonempty project name').isLength({
        min: 1,
      }),
      check('quotation_items').custom((items) => validateServiceItems(items)),
    ],
    invoiceController.addInvoice,
  )
  router.get('/archive/:id', authMiddleware, invoiceController.archiveInvoice)
  router.get(
    '/unarchive/:id',
    authMiddleware,
    invoiceController.unarchiveInvoice,
  )
  router.put(
    '/:id',
    authMiddleware,
    [
      check('invoice_number', 'please input nonempty invoice number').isLength({
        min: 1,
      }),
      check('from', 'please input nonempty vendor(from)').isLength({
        min: 1,
      }),
      check('project_name', 'please input nonempty project name').isLength({
        min: 1,
      }),
      check('quotation_items').custom((items) => validateServiceItems(items)),
    ],
    invoiceController.changeInvoiceById,
  )
  router.delete('/:id', authMiddleware, invoiceController.deleteInvoiceById)
} else {
  // development
  router.get('/', invoiceController.getAllInvoicesIsDev)
  router.get('/:id', invoiceController.getInvoiceByIdIsDev)
  router.post('/', invoiceController.addInvoiceIsDev)
  router.put('/:id', invoiceController.changeInvoiceByIdIsDev)
  router.delete('/:id', invoiceController.deleteInvoiceByIdIsDev)
}

export default router
