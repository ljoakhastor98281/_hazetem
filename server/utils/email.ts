// html templates for nodemailer
import { webInfo } from '../config'
import { IServiceItem } from '../api/Service/service.model'
import { RequestStatusObject } from '../types/appEnums'

export const requestRemindMail = (
  requester: string,
  service_name: string,
  itemList: IServiceItem[],
  company_id: string,
  request_history_id: string,
) => {
  let items = ``
  itemList.map((item: IServiceItem) => {
    items += `
                                  <p>
                                    <a style="box-sizing: border-box;display: inline-block;font-family:'Varela Round',sans-serif;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;color: #000000;background-color:#ffffff; border-radius: 0px; -webkit-border-radius: 0px; -moz-border-radius: 0px; width:100%; max-width:100%; overflow-wrap: break-word; word-break: break-word; word-wrap:break-word; mso-border-alt: none;">
                                      <strong>
                                        <span style="display:block;padding:12px;line-height: 18px; font-size: 14px;">
                                          ${item.title}
                                        </span>
                                      </strong>
                                    </a>
                                  </p>
    `
  })

  return (
    `
      <!DOCTYPE HTML>
    
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="x-apple-disable-message-reformatting">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Hazedawn approve request email</title>
        <style type="text/css">
          table,
          td {
            color: #000000;
          }
      
          a {
            color: #57578a;
            text-decoration: none;
          }
      
          @media only screen and (min-width: 620px) {
            .u-row {
              width: 600px !important;
            }
      
            .u-row .u-col {
              vertical-align: top;
            }
      
            .u-row .u-col-100 {
              width: 600px !important;
            }
          }
      
          @media (max-width: 620px) {
            .u-row-container {
              max-width: 100% !important;
              padding-left: 0px !important;
              padding-right: 0px !important;
            }
      
            .u-row .u-col {
              min-width: 320px !important;
              max-width: 100% !important;
              display: block !important;
            }
      
            .u-row {
              width: calc(100% - 40px) !important;
            }
      
            .u-col {
              width: 100% !important;
            }
      
            .u-col>div {
              margin: 0 auto;
            }
          }
      
          body {
            margin: 0;
            padding: 0;
          }
      
          tr,
          td {
            vertical-align: top;
            border-collapse: collapse;
          }
      
          p {
            margin: 0;
          }
      
          .ie-container table,
          .mso-container table {
            table-layout: fixed;
          }
      
          * {
            line-height: inherit;
          }
      
          a[x-apple-data-detectors='true'] {
            color: inherit !important;
            text-decoration: none !important;
          }
        </style>
        <link href="https://fonts.googleapis.com/css2?family=Varela+Round&display=swap" rel="stylesheet" type="text/css"> 
      </head>
      
      <body class="clean-body"
        style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #e7e7e7;color: #000000;">
        <table
          style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;width:100%"
          cellpadding="0" cellspacing="0">
          <tbody>
            <tr style="vertical-align: top">
              <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                <div class="u-row-container" style="padding: 0px;background-color: transparent">
                  <div class="u-row"
                    style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;">
                    <div
                      style="border-collapse: collapse;display: table;width: 100%;background-color:#ffffff;background-repeat: no-repeat;background-position: center top;">
                      <div class="u-col u-col-100"
                        style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                        <div style="width: 100% !important;">
                          <div
                            style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                            <table style="font-family:'Varela Round',sans-serif;" role="presentation" cellpadding="0"
                              cellspacing="0" width="100%" border="0">
                              <tbody>
                                <tr>
                                  <td
                                    style="overflow-wrap:break-word;word-break:break-word;padding:60px 10px 20px;font-family:'Varela Round',sans-serif;"
                                    align="left">
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                      <tr>
                                        <td style="padding-right: 0px;padding-left: 0px;" align="center">
                                          <img align="center" border="0" src="cid:hazedawn_logo" alt="hazedawn" title="hazedawn"
                                            style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: flex !important;border: none;height: auto;float: none;margin: 20px;width: 23%;max-width: 162.4px;"
                                            width="162.4" />
                                          <img align="center" border="0" src="cid:clock" alt="hazedawn" title="hazedawn"
                                            style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: flex !important;border: none;height: auto;float: none;width: 26%;max-width: 162.4px;"
                                            width="162.4" />
                                        </td>
                                    </table>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
      
                            <table style="font-family:'Varela Round',sans-serif;" role="presentation" cellpadding="0"
                              cellspacing="0" width="100%" border="0">
                              <tbody>
                                <tr>
                                  <td
                                    style="overflow-wrap:break-word;word-break:break-word;padding:0px 10px 10px;font-family:'Varela Round',sans-serif;"
                                    align="left">
                                    <div
                                      style="color: #323296; line-height: 140%; text-align: center; word-wrap: break-word;">
                                      <p style="line-height: 140%; font-size: 14px;"><span
                                          style="font-size: 34px; line-height: 47.6px;"><strong>Reminder Mail</strong></span>
                                      </p>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
      
                            <table style="font-family:'Varela Round',sans-serif;" role="presentation" cellpadding="0"
                              cellspacing="0" width="100%" border="0">
                              <tbody>
                                <tr>
                                  <td
                                    style="overflow-wrap:break-word;word-break:break-word;padding:0px 50px;font-family:'Varela Round',sans-serif;"
                                    align="left">
                                    <div align="center">
                                      <a target="_blank"
                                        style="box-sizing: border-box;display: inline-block;font-family:'Varela Round',sans-serif;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;color: #000000; border-radius: 0px; -webkit-border-radius: 0px; -moz-border-radius: 0px; width:100%; max-width:100%; overflow-wrap: break-word; word-break: break-word; word-wrap:break-word; mso-border-alt: none;">
                                        <span style="display:block;padding:12px;line-height:200%;"><span
                                            style="font-size: 18px; line-height: 36px;"><strong><span style="line-height: 36px; font-size: 18px;">
                                                ${requester} has just requested your service: ${service_name}
                                                </span></strong></span></span>
                                      </a>
                                    </div>
                                    <div style="border-style:solid;border-color:#37bec5;">
                                      <p>
                                        <a style="box-sizing: border-box;display: inline-block;font-family:'Varela Round',sans-serif;font-weight:700;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;color: #000000;background-color: #ffffff;border-radius: 0px; -webkit-border-radius: 0px; -moz-border-radius: 0px; width:100%; max-width:100%; overflow-wrap: break-word; word-break: break-word; word-wrap:break-word; mso-border-alt: none;">
                                          <strong>
                                            <span style="display:block;padding:12px;line-height: 20px; font-size: 14px;">
                                              Service item(s) requested:
                                            </span>
                                          </strong>
                                        </a>
                                      </p>
                                      ` +
    items +
    `
                                  </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
      
                            <table style="font-family:'Varela Round',sans-serif;" role="presentation" cellpadding="0"
                              cellspacing="0" width="100%" border="0">
                              <tbody>
                                <tr>
                                  <td
                                    style="overflow-wrap:break-word;word-break:break-word;padding:15px 10px 10px;font-family:'Varela Round',sans-serif;"
                                    align="left">
                                    <div align="center">
                                      <strong>
                                          <a href="${webInfo.baseUrl}api/requesthistory/approval_company_id/${request_history_id}/${company_id}" style="box-sizing: border-box;display: inline-block;text-decoration: none;-webkit-text-size-adjust: none;text-align: center; width:auto; max-width:100%; overflow-wrap: break-word; word-break: break-word; word-wrap:break-word; mso-border-alt: none;">
                                            <button type="submit" style="display:block;padding:10px 20px;font-family:'Varela Round',sans-serif;line-height:120%;font-size: 14px; line-height: 16.8px;color: #ffffff; background-color: #37bec5; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; width:auto; max-width:100%; overflow-wrap: break-word; word-break: break-word; word-wrap:break-word;border: 0px;">APPROVE</button>
                                          </a>
                                      </strong>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
      
                            <table style="font-family:'Varela Round',sans-serif;" role="presentation" cellpadding="0"
                              cellspacing="0" width="100%" border="0">
                              <tbody>
                                <tr>
                                  <td
                                    style="overflow-wrap:break-word;word-break:break-word;padding:20px 10px 29px;font-family:'Varela Round',sans-serif;"
                                    align="left">
                                    <a href="${webInfo.baseUrl}">
                                      <div
                                          style="color: #000000; line-height: 200%; text-align: center; word-wrap: break-word;">
                                          <p style="font-size: 14px; line-height: 200%;">hazetem</p>
                                      </div>
                                    </a>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
      
                <div class="u-row-container" style="padding: 0px;background-color: transparent">
                  <div class="u-row"
                    style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">
                    <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
      
                      <div class="u-col u-col-100"
                        style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                        <div style="width: 100% !important;">
                          <div
                            style="padding: 9px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
      
                            <table style="font-family:'Varela Round',sans-serif;" role="presentation" cellpadding="0"
                              cellspacing="0" width="100%" border="0">
                              <tbody>
                                <tr>
                                  <td
                                    style="overflow-wrap:break-word;word-break:break-word;padding:19px 10px 10px;font-family:'Varela Round',sans-serif;"
                                    align="left">
                                    <div style="color: #444444; line-height: 140%; text-align: left; word-wrap: break-word;">
                                      <p style="line-height: 140%; text-align: center; font-size: 14px;"><strong>HAZEDAWN
                                          CORPORATION</strong></p>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
      
                            <table style="font-family:'Varela Round',sans-serif;" role="presentation" cellpadding="0"
                              cellspacing="0" width="100%" border="0">
                              <tbody>
                                <tr>
                                  <td
                                    style="overflow-wrap:break-word;word-break:break-word;padding:10px 40px;font-family:'Varela Round',sans-serif;"
                                    align="left">
                                    <div
                                      style="color: #666666; line-height: 140%; text-align: center; word-wrap: break-word;">
                                      <p style="font-size: 14px; line-height: 140%;"><span
                                          style="font-size: 12px; line-height: 16.8px;">Address line, email address , phone
                                          number, etc goes here.</span></p>
                                      <p style="font-size: 14px; line-height: 140%;"><span
                                          style="font-size: 12px; line-height: 16.8px;">Contact Us&nbsp; /&nbsp; Privacy
                                          Policy&nbsp; /&nbsp; Unsubscribe&nbsp; /&nbsp; Terms</span></p>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
      
                            <table style="font-family:'Varela Round',sans-serif;" role="presentation" cellpadding="0"
                              cellspacing="0" width="100%" border="0">
                              <tbody>
                                <tr>
                                  <td
                                    style="overflow-wrap:break-word;word-break:break-word;padding:10px 40px;font-family:'Varela Round',sans-serif;"
                                    align="left">
                                    <div style="line-height: 140%; text-align: center; word-wrap: break-word;">
                                      <p style="font-size: 14px; line-height: 140%;"><strong><span
                                            style="font-size: 12px; line-height: 16.8px;">&copy; Hazedawn&nbsp; -&nbsp; All
                                            Rights Reserved </span></strong></p>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
      
                            <table style="font-family:'Varela Round',sans-serif;" role="presentation" cellpadding="0"
                              cellspacing="0" width="100%" border="0">
                              <tbody>
                                <tr>
                                  <td
                                    style="overflow-wrap:break-word;word-break:break-word;padding:10px;font-family:'Varela Round',sans-serif;"
                                    align="left">
                                    <table height="0px" align="center" border="0" cellpadding="0" cellspacing="0" width="100%"
                                      style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;border-top: 1px solid #f8f8f8;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                      <tbody>
                                        <tr style="vertical-align: top">
                                          <td
                                            style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;font-size: 0px;line-height: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                            <span>&#160;</span>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
      </html>
    `
  )
}

export const dailyRequestRemindMail = (num_requests: RequestStatusObject) => {
  const today = new Date()
  const yesterday = new Date(today.getTime() - 86400000)
  const num_total =
    num_requests.ongoing +
    num_requests.cancelled +
    num_requests.declined +
    num_requests.approved +
    num_requests.expired

  let items = ``
  if (num_requests.ongoing) {
    items += `
                                <p>
                                  <a style="box-sizing: border-box;display: inline-block;font-family:'Montserrat',sans-serif;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;color: #FFFFFF; border-radius: 0px; -webkit-border-radius: 0px; -moz-border-radius: 0px; width:100%; max-width:100%; overflow-wrap: break-word; word-break: break-word; word-wrap:break-word; mso-border-alt: none;">
                                    <strong>
                                      <span style="display:block;padding:12px;line-height: 18px; font-size: 14px;">
                                        Number of ongoing request(s): ${num_requests.ongoing}
                                      </span>
                                    </strong>
                                  </a>
                                </p>
    `
  }
  if (num_requests.cancelled) {
    items += `
                                <p>
                                  <a style="box-sizing: border-box;display: inline-block;font-family:'Montserrat',sans-serif;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;color: #FFFFFF; border-radius: 0px; -webkit-border-radius: 0px; -moz-border-radius: 0px; width:100%; max-width:100%; overflow-wrap: break-word; word-break: break-word; word-wrap:break-word; mso-border-alt: none;">
                                    <strong>
                                      <span style="display:block;padding:12px;line-height: 18px; font-size: 14px;">
                                        Number of cancelled request(s): ${num_requests.cancelled}
                                      </span>
                                    </strong>
                                  </a>
                                </p>
    `
  }
  if (num_requests.declined) {
    items += `
                                <p>
                                  <a style="box-sizing: border-box;display: inline-block;font-family:'Montserrat',sans-serif;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;color: #FFFFFF; border-radius: 0px; -webkit-border-radius: 0px; -moz-border-radius: 0px; width:100%; max-width:100%; overflow-wrap: break-word; word-break: break-word; word-wrap:break-word; mso-border-alt: none;">
                                    <strong>
                                      <span style="display:block;padding:12px;line-height: 18px; font-size: 14px;">
                                        Number of declined request(s): ${num_requests.declined}
                                      </span>
                                    </strong>
                                  </a>
                                </p>
    `
  }
  if (num_requests.approved) {
    items += `
                                <p>
                                  <a style="box-sizing: border-box;display: inline-block;font-family:'Montserrat',sans-serif;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;color: #FFFFFF; border-radius: 0px; -webkit-border-radius: 0px; -moz-border-radius: 0px; width:100%; max-width:100%; overflow-wrap: break-word; word-break: break-word; word-wrap:break-word; mso-border-alt: none;">
                                    <strong>
                                      <span style="display:block;padding:12px;line-height: 18px; font-size: 14px;">
                                        Number of approved request(s): ${num_requests.approved}
                                      </span>
                                    </strong>
                                  </a>
                                </p>
    `
  }
  if (num_requests.expired) {
    items += `
                                <p>
                                  <a style="box-sizing: border-box;display: inline-block;font-family:'Montserrat',sans-serif;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;color: #FFFFFF; border-radius: 0px; -webkit-border-radius: 0px; -moz-border-radius: 0px; width:100%; max-width:100%; overflow-wrap: break-word; word-break: break-word; word-wrap:break-word; mso-border-alt: none;">
                                    <strong>
                                      <span style="display:block;padding:12px;line-height: 18px; font-size: 14px;">
                                        Number of expired request(s): ${num_requests.expired}
                                      </span>
                                    </strong>
                                  </a>
                                </p>
    `
  }
  return (
    `
    <!DOCTYPE HTML>
  
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="x-apple-disable-message-reformatting">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Hazedawn approve request email</title>
      <style type="text/css">
        table,
        td {
          color: #000000;
        }
    
        a {
          color: #57578a;
          text-decoration: none;
        }
    
        @media only screen and (min-width: 620px) {
          .u-row {
            width: 600px !important;
          }
    
          .u-row .u-col {
            vertical-align: top;
          }
    
          .u-row .u-col-100 {
            width: 600px !important;
          }
        }
    
        @media (max-width: 620px) {
          .u-row-container {
            max-width: 100% !important;
            padding-left: 0px !important;
            padding-right: 0px !important;
          }
    
          .u-row .u-col {
            min-width: 320px !important;
            max-width: 100% !important;
            display: block !important;
          }
    
          .u-row {
            width: calc(100% - 40px) !important;
          }
    
          .u-col {
            width: 100% !important;
          }
    
          .u-col>div {
            margin: 0 auto;
          }
        }
    
        body {
          margin: 0;
          padding: 0;
        }
    
        tr,
        td {
          vertical-align: top;
          border-collapse: collapse;
        }
    
        p {
          margin: 0;
        }
    
        .ie-container table,
        .mso-container table {
          table-layout: fixed;
        }

        * {
          line-height: inherit;
        }
    
        a[x-apple-data-detectors='true'] {
          color: inherit !important;
          text-decoration: none !important;
        }
      </style>
      <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet" type="text/css">
    </head>
    
    <body class="clean-body"
      style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #e7e7e7;color: #000000">
      <table
        style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #e7e7e7;width:100%"
        cellpadding="0" cellspacing="0">
        <tbody>
          <tr style="vertical-align: top">
            <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
              <div class="u-row-container" style="padding: 0px;background-color: transparent">
                <div class="u-row"
                  style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;">
                  <div
                    style="border-collapse: collapse;display: table;width: 100%;background-image: url('cid:email_bg');background-repeat: no-repeat;background-position: center top;background-color: transparent;">
                    <div class="u-col u-col-100"
                      style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                      <div style="width: 100% !important;">
                        <div
                          style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                          <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0"
                            cellspacing="0" width="100%" border="0">
                            <tbody>
                              <tr>
                                <td
                                  style="overflow-wrap:break-word;word-break:break-word;padding:60px 10px 20px;font-family:'Montserrat',sans-serif;"
                                  align="left">
                                  <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                      <td style="padding-right: 0px;padding-left: 0px;" align="center">
    
                                        <img align="center" border="0" src="cid:hazedawn_logo" alt="hazedawn" title="hazedawn"
                                          style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 28%;max-width: 162.4px;"
                                          width="162.4" />
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
    
                          <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0"
                            cellspacing="0" width="100%" border="0">
                            <tbody>
                              <tr>
                                <td
                                  style="overflow-wrap:break-word;word-break:break-word;padding:18px 10px 10px;font-family:'Montserrat',sans-serif;"
                                  align="left">
                                  <div
                                    style="color: #ffffff; line-height: 140%; text-align: center; word-wrap: break-word;">
                                    <p style="line-height: 140%; font-size: 14px;"><span
                                        style="font-size: 34px; line-height: 47.6px;"><strong>Reminder Mail</strong></span>
                                    </p>
                                  </div>
                                </td>
                              </tr>
                            </tbody>
                          </table>
    
                          <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0"
                            cellspacing="0" width="100%" border="0">
                            <tbody>
                              <tr>
                                <td
                                  style="overflow-wrap:break-word;word-break:break-word;padding:0px 50px;font-family:'Montserrat',sans-serif;"
                                  align="left">
                                  <div align="center">
                                    <a target="_blank"
                                      style="box-sizing: border-box;display: inline-block;font-family:'Montserrat',sans-serif;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;color: #000000; background-color: #ffffff; border-radius: 0px; -webkit-border-radius: 0px; -moz-border-radius: 0px; width:100%; max-width:100%; overflow-wrap: break-word; word-break: break-word; word-wrap:break-word; mso-border-alt: none;">
                                      <span style="display:block;padding:12px;line-height:200%;"><span
                                          style="font-size: 18px; line-height: 36px;"><strong><span style="line-height: 36px; font-size: 18px;">
                                            Total number of request from ${yesterday} to now: ${num_total}
                                              </span></strong></span></span>
                                    </a>
                                  </div>
                                  ` +
    items +
    ` 
                                </td>
                              </tr>
                            </tbody>
                          </table>
    
                          <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0"
                            cellspacing="0" width="100%" border="0">
                            <tbody>
                              <tr>
                                <td
                                  style="overflow-wrap:break-word;word-break:break-word;padding:15px 10px 10px;font-family:'Montserrat',sans-serif;"
                                  align="left">
                                  <div align="center">
                                    <strong>
                                        <a href="${webInfo.baseUrl}requesthistory" style="box-sizing: border-box;display: inline-block;text-decoration: none;-webkit-text-size-adjust: none;text-align: center; width:auto; max-width:100%; overflow-wrap: break-word; word-break: break-word; word-wrap:break-word; mso-border-alt: none;">
                                          <button type="submit" style="display:block;padding:10px 20px;font-family:'Montserrat',sans-serif;line-height:120%;font-size: 14px; line-height: 16.8px;color: #FFFFFF; background-color: #000000; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; width:auto; max-width:100%; overflow-wrap: break-word; word-break: break-word; word-wrap:break-word;">VIEW MORE</button>
                                        </a>
                                    </strong>
                                  </div>
                                </td>
                              </tr>
                            </tbody>
                          </table>
    
                          <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0"
                            cellspacing="0" width="100%" border="0">
                            <tbody>
                              <tr>
                                <td
                                  style="overflow-wrap:break-word;word-break:break-word;padding:20px 10px 29px;font-family:'Montserrat',sans-serif;"
                                  align="left">
                                  <a href="${webInfo.baseUrl}">
                                    <div
                                        style="color: #ffffff; line-height: 200%; text-align: center; word-wrap: break-word;">
                                        <p style="font-size: 14px; line-height: 200%;">hazetem</p>
                                    </div>
                                  </a>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
    
              <div class="u-row-container" style="padding: 0px;background-color: transparent">
                <div class="u-row"
                  style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #e5e6ea;">
                  <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
    
                    <div class="u-col u-col-100"
                      style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                      <div style="width: 100% !important;">
                        <div
                          style="padding: 9px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
    
                          <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0"
                            cellspacing="0" width="100%" border="0">
                            <tbody>
                              <tr>
                                <td
                                  style="overflow-wrap:break-word;word-break:break-word;padding:19px 10px 10px;font-family:'Montserrat',sans-serif;"
                                  align="left">
                                  <div style="color: #444444; line-height: 140%; text-align: left; word-wrap: break-word;">
                                    <p style="line-height: 140%; text-align: center; font-size: 14px;"><strong>HAZEDAWN
                                        CORPORATION</strong></p>
                                  </div>
                                </td>
                              </tr>
                            </tbody>
                          </table>
    
                          <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0"
                            cellspacing="0" width="100%" border="0">
                            <tbody>
                              <tr>
                                <td
                                  style="overflow-wrap:break-word;word-break:break-word;padding:10px 40px;font-family:'Montserrat',sans-serif;"
                                  align="left">
                                  <div
                                    style="color: #666666; line-height: 140%; text-align: center; word-wrap: break-word;">
                                    <p style="font-size: 14px; line-height: 140%;"><span
                                        style="font-size: 12px; line-height: 16.8px;">Address line, email address , phone
                                        number, etc goes here.</span></p>
                                    <p style="font-size: 14px; line-height: 140%;"><span
                                        style="font-size: 12px; line-height: 16.8px;">Contact Us&nbsp; /&nbsp; Privacy
                                        Policy&nbsp; /&nbsp; Unsubscribe&nbsp; /&nbsp; Terms</span></p>
                                  </div>
                                </td>
                              </tr>
                            </tbody>
                          </table>
    
                          <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0"
                            cellspacing="0" width="100%" border="0">
                            <tbody>
                              <tr>
                                <td
                                  style="overflow-wrap:break-word;word-break:break-word;padding:10px 40px;font-family:'Montserrat',sans-serif;"
                                  align="left">
                                  <div style="line-height: 140%; text-align: center; word-wrap: break-word;">
                                    <p style="font-size: 14px; line-height: 140%;"><strong><span
                                          style="font-size: 12px; line-height: 16.8px;">&copy; Hazedawn&nbsp; -&nbsp; All
                                          Rights Reserved </span></strong></p>
                                  </div>
                                </td>
                              </tr>
                            </tbody>
                          </table>
    
                          <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0"
                            cellspacing="0" width="100%" border="0">
                            <tbody>
                              <tr>
                                <td
                                  style="overflow-wrap:break-word;word-break:break-word;padding:10px;font-family:'Montserrat',sans-serif;"
                                  align="left">
                                  <table height="0px" align="center" border="0" cellpadding="0" cellspacing="0" width="100%"
                                    style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;border-top: 1px solid #f8f8f8;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                    <tbody>
                                      <tr style="vertical-align: top">
                                        <td
                                          style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;font-size: 0px;line-height: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                          <span>&#160;</span>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </body>
    
    </html>
    `
  )
}
