import { webInfo } from '../config'

export const request_approval = (status: number) => {
  let returnString = ''
  switch (status) {
    case 0: {
      returnString =
        'The request is approved! click the link below to get into request history page to view more'
      break
    }
    case 1: {
      returnString =
        'Seems like the request you are looking for has already been cancelled by user'
      break
    }
    case 2: {
      returnString =
        'Seems like the request you are looking for has already been declined'
      break
    }
    case 3: {
      returnString =
        'Seems like the request you are looking for has already been approved'
      break
    }
    case 4: {
      returnString =
        'Seems like the request you are looking for has already been expired'
      break
    }
    case 403: {
      returnString = 'Unauthorized to access this page'
      break
    }
    case 404: {
      returnString = 'Server error: company is not found'
      break
    }
    case 500: {
      returnString = 'Server error: unknown error'
      break
    }
    default: {
      returnString = 'Server error: unknown status'
      break
    }
  }

  return `
  <!DOCTYPE HTML>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Approvel Fail: Cancelled Request</title>
  
    <style type="text/css">
      * {
        font-family:'Montserrat';
        color: #FFFFFF;
        text-align: center;
      }
      html { 
          background: url('/images/backgroundImage.jpg') no-repeat center center fixed; 
          -webkit-background-size: cover;
          background-size: cover;
          height: 100%;
          width: 100%;
      }
      .container {
          max-width: 800px;
          justify-content: center;
          align-items: center;
          display: flex;
          flex-direction: column;
          margin: auto;
          height: 100%;
          padding: 70px;
          position: relative;
          background-color: #ffffff44;
      }
      .button {
          display: flex; 
          justify-content: space-between; 
          flex-wrap: wrap; 
          width: 60%;
      }
      button {
          background-color: #ffffff73;
          border-radius: 10px;
          border: #505050;
          color: black;
          padding: 10px 32px;
          text-align: center;
          text-decoration: none;
          display: inline-block;
          font-size: 16px;
          margin: 10px;
          justify-content: center;
      }
      button:hover {
          background-color: #ffffff9a;
      }
    </style>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet" type="text/css">
  </head>
  <body>
      <div class="container">
          <img src="/images/hazedawn.png" alt="hazedawn" title="hazedawn"/>
          <h2>${returnString}</h2>
          <div class="button">
              <button type="button" onClick="location.href='${webInfo.baseUrl}'">hazetem</button>
              <button type="button" onClick="location.href='${webInfo.baseUrl}'">Request History</button>
          </div>
      </div>
  </body>
  `
}
