// DEPRECATED

import passport from 'passport'
import passportJWT from 'passport-jwt'
import { jwtConfig } from '../config'
import passportAnonymous from 'passport-anonymous'
import { Request } from 'express'

// const JWTStrategy = passportJWT.Strategy

// passport.use(new passportAnonymous.Strategy())

// const cookieExtractor = (req: Request) => {
//   const jwt = req.cookies?.jwt
//   return jwt
// }

// passport.use(
//   'jwt',
//   new JWTStrategy(
//     {
//       jwtFromRequest: cookieExtractor,
//       secretOrKey: jwtConfig.secret,
//     },
//     (jwtPayload, done) => {
//       const { expiration } = jwtPayload
//       if (Date.now() > expiration) {
//         done('Unauthorized', false)
//       }
//       done(null, jwtPayload)
//     },
//   ),
// )
