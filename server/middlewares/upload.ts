import { Request } from 'express'
import multer from 'multer'
import fs from 'fs'

interface MulterRequest extends Request {
  body: any
}

const storage = multer.diskStorage({
  destination: function (req, _file, cb) {
    const dest = './server/client_uploads/' + req.body.user_id
    // fs.exists deprecated
    fs.stat(dest, (err) => {
      if (err) {
        fs.mkdir(dest, { recursive: true }, (err) => {
          if (err) {
            console.error(err)
          } else {
            req.body.uploaded = 'true'
            cb(null, dest)
          }
        })
      } else {
        req.body.uploaded = 'true'
        cb(null, dest)
      }
    })
  },
  filename: function (_req, file, cb) {
    cb(null, Date.now() + '--' + file.originalname)
  },
})

const fileFilter = (_req: MulterRequest, file: any, cb: any) => {
  if (
    file.mimetype.includes('jpeg') ||
    file.mimetype.includes('png') ||
    file.mimetype.includes('jpg') ||
    file.mimetype.includes('msword') ||
    file.mimetype.includes(
      'vnd.openxmlformats-officedocument.wordprocessingml.document',
    ) ||
    file.mimetype.includes('pdf')
  ) {
    cb(null, true)
  } else {
    cb(null, false)
  }
}

const upload = multer({ storage: storage, fileFilter: fileFilter })

export default upload.single('file')
