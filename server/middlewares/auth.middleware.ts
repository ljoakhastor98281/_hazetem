import { Request, Response, NextFunction } from 'express'
import { jwtConfig } from '../config'
import { getToken } from 'next-auth/jwt'

const authMiddleware = async function (
  req: Request,
  res: Response,
  next: NextFunction,
) {
  try {
    // @ts-ignore
    const token = await getToken({ req, secret: jwtConfig.secret })
    // @ts-ignore
    req.user = token.data
    next()
  } catch (err) {
    return res.status(401).json({ msg: 'Token is not valid' })
  }
}

export { authMiddleware }
