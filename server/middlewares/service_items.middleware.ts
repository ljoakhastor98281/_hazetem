import { IServiceItem } from '../api/Service/service.model'

const validateServiceItems = (items: IServiceItem[]) => {
  if (!items) {
    throw new Error('no items found!')
  }
  if (items.length == 0) {
    throw new Error('missing service items')
  }
  const unique = new Set()
  items.map((item: IServiceItem) => {
    if (item.title == '') throw new Error('missing item title')
    unique.add(item.title)
  })
  if (unique.size != items.length) throw new Error('conflict-item-title')
  return Promise.resolve()
}

export { validateServiceItems }
