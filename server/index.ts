import express, { Request, Response } from 'express'
import next from 'next'
import cors from 'cors'
import helmet from 'helmet'
import compression from 'compression'
import { connect } from './db'
import { isDev, corsOptions } from './config'
import authRouter from './api/auth/auth.router'
import companyRouter from './api/Company/company.router'
import userRouter from './api/User/user.router'
import mailRouter from './api/nodemailer/nodemailer.router'
import invoiceRouter from './api/Invoice/invoice.router'
import serviceRouter from './api/Service/service.router'
import requestHistoryRouter from './api/RequestHistory/requesthistory.router'
import {
  sendMail_BR_remind,
  sendMail_request_remind,
} from './api/nodemailer/nodemailer.controller'

import cookieParser from 'cookie-parser'
import passport from 'passport'
import './middlewares/passport'

const app = next({ dev: isDev })
const handle = app.getRequestHandler()
const port = process.env.PORT || 8080

;(async () => {
  try {
    await app.prepare()
    const server = express()

    // connect to mongodb
    connect()

    // setup middleware
    if (!isDev) {
      server.use(
        helmet({
          contentSecurityPolicy: false,
        }),
      )
      server.use(compression())
    }
    server.use(cors(corsOptions))
    server.use(express.json({ limit: '5mb' }))
    server.use(express.urlencoded({ limit: '5mb', extended: true }))
    server.use(cookieParser())
    server.use(express.static('public'))

    // passport
    server.use(passport.initialize())

    server.get('/api/hi', (req, res) => {
      return res.status(200).json({ msg: 'hello world' })
    })

    // routes
    server.use('/api/auth', authRouter)
    server.use('/api/company', companyRouter)
    server.use('/api/user', userRouter)
    server.use('/api/mail', mailRouter)
    server.use('/api/invoice', invoiceRouter)
    server.use('/api/service', serviceRouter)
    server.use('/api/requesthistory', requestHistoryRouter)

    // front-end
    server.all('*', (req: Request, res: Response) => {
      return handle(req, res)
    })
    server.listen(port, (err?: unknown) => {
      if (err) throw err
      console.log(`> Ready on localhost:${port}`)
    })

    // nodemail BR expiry reminder
    // setInterval(() => {
    //   sendMail_BR_remind()
    //   sendMail_request_remind()
    // }, 86400000)
  } catch (e) {
    console.error(e)
    process.exit(1)
  }
})()
