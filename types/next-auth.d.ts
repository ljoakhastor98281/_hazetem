import 'next-auth'
import { Level } from '@/types/appEnums'
declare module 'next-auth' {
  interface User {
    username?: string
  }

  interface Session {
    user: User
    level: Level
  }
}
