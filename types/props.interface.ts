import { AppProps } from 'next/app'
import { ReactNode } from 'react'

export interface IProps extends AppProps {
  children?: ReactNode
}

export interface IIconProps {
  size: string | number
  width?: string | number
  height?: string | number
}
